
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import play.Application;
import play.GlobalSettings;
import play.Play;
/**
 * The Global class allows to handle global settings for the application. 
 * Here it overrides the exixting method definition to give a new meaning.
 * 
 * @author DivyaP
 * @since 2015-08-10
 */


//http://spring-webservice-2-step-by-step.blogspot.in/2013/12/spring-framework-integration-with-play.html
public class Global extends GlobalSettings {

	private ApplicationContext ctx;

	/**
	 * This method is used to load the application-context.xml (which deals with spring utilities) on load.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	@Override
	public void onStart(Application app) {
		String configLocation = Play.application().configuration()
				.getString("spring.context.location");
		ctx = new ClassPathXmlApplicationContext(configLocation);
	}

	/**
	 * This method is used for spring DI to be implied.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	@Override
	public <A> A getControllerInstance(Class<A> clazz) {
		// This check is used to suppress any annotation if it is not injected
		// using spring DI. Example Security.Authenticated.
		if (clazz.isAnnotationPresent(Component.class))
			return ctx.getBean(clazz);
		else
			return null;
	}

}