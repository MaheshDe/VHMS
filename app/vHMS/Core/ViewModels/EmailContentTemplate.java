package vHMS.Core.ViewModels;

/**
 * The EmailContentTemplate deals with the attributes used for constructing
 * body.
 * 
 * @author ArokiaS
 * @since 2014-09-10
 */

public class EmailContentTemplate {

	String senderName;
	String receiverName;
	String requesterName;
	String recEmailId;
	String requestCode;
	String worflowName;
	String status;
	String worflowStatusName;
	String url;
	String requesterEmailId;

	/**
	 * @return the senderName
	 */
	public String getSenderName() {
		return senderName;
	}

	/**
	 * @param senderName
	 *            the senderName to set
	 */
	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	/**
	 * @return the receiverName
	 */
	public String getReceiverName() {
		return receiverName;
	}

	/**
	 * @param receiverName
	 *            the receiverName to set
	 */
	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	/**
	 * @return the requesterName
	 */
	public String getRequesterName() {
		return requesterName;
	}

	/**
	 * @param requesterName
	 *            the requesterName to set
	 */
	public void setRequesterName(String requesterName) {
		this.requesterName = requesterName;
	}

	/**
	 * @return the recEmailId
	 */
	public String getRecEmailId() {
		return recEmailId;
	}

	/**
	 * @param recEmailId
	 *            the recEmailId to set
	 */
	public void setRecEmailId(String recEmailId) {
		this.recEmailId = recEmailId;
	}

	/**
	 * @return the requestCode
	 */
	public String getRequestCode() {
		return requestCode;
	}

	/**
	 * @param requestCode
	 *            the requestCode to set
	 */
	public void setRequestCode(String requestCode) {
		this.requestCode = requestCode;
	}

	/**
	 * @return the worflowName
	 */
	public String getWorflowName() {
		return worflowName;
	}

	/**
	 * @param worflowName
	 *            the worflowName to set
	 */
	public void setWorflowName(String worflowName) {
		this.worflowName = worflowName;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the worflowStatusName
	 */
	public String getWorflowStatusName() {
		return worflowStatusName;
	}

	/**
	 * @param worflowStatusName
	 *            the worflowStatusName to set
	 */

	public void setWorflowStatusName(String worflowStatusName) {
		this.worflowStatusName = worflowStatusName;

	}

	/**
	 * @return the requesterEmailId
	 */
	public String getRequesterEmailId() {
		return requesterEmailId;
	}

	/**
	 * @param requesterEmailId
	 *            the requesterEmailId to set
	 */
	public void setRequesterEmailId(String requesterEmailId) {
		this.requesterEmailId = requesterEmailId;
	}

}
