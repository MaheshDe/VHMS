package vHMS.Core.ViewModels;

/**
 * The PagedViewModel class contains the basic details for an api call like,
 * Table name - entity object , Limit - no.of records, Skip - page no etc..,
 * this class contains the basic entities used accross all the api calls
 * 
 * @author DivyaP
 * @version 1.0
 * @since 2015-07-17
 */

public class PagedViewModel {
	public String limit;
	public String skip;
	public String entityObject;
	public String where;
}
