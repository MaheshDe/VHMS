package vHMS.Core.ViewModels;

import java.io.File;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * The DocumentViewModel deals with the attributes used for document data for
 * upload.
 * 
 * @author Arokia Felsi
 * @since 2014-09-10
 */
public class DocumentViewModel extends PagedViewModel {
    
	
	public String id;
	public String name;
	public String code;
	public String status;
	@JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	 public Date  createdAt;
	 public String modifiedBy;
	 @JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	 public Date modifiedAt;
	public String action;
	public String text;
	public String createdBy;
	public String fkcompanyCode;
	public String updateCount;
	public String contentType;
	public String fileName;
	public String requestType;
	public String documentTypeCode;
	public String requestCode;
	public String json;
	public File LoadedFile;
	public String bucket;
	public byte[] file;
}
