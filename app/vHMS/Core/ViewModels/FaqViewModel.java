package vHMS.Core.ViewModels;

/**
 * The PagedViewModel class contains the basic details for an api call like,
 * Table name - entity object , Limit - no.of records, Skip - page no etc..,
 * this class contains the basic entities used accross all the api calls
 * 
 * @author Sabariraja M
 * @version 1.0
 * @since 2015-08-26
 */

public class FaqViewModel {
	public String limit;
	public String skip;
	public String entityObject;
	public String where;
	public String code;
	public String name;
	public String question;
	public String answer;
	public String id;
}