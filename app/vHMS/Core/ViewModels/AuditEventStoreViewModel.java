package vHMS.Core.ViewModels;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * The AuditEventStoreViewModel deals with the attributes used for dashboard
 * request data view.
 * 
 * @author GadepalliM
 * 
 */
public class AuditEventStoreViewModel {
	public String id;
	public String entityDescription;
	public String entityId;
	public String auditEventDescription;
	public String json;
	public String createdBy;
	@JsonFormat(pattern = "yyyy-MM-dd,HH:00", timezone = JsonFormat.DEFAULT_TIMEZONE)
	public Date modifiedAt;
	public String fkCompanyCode;
	public String updateCount;
	public String userName;
}
