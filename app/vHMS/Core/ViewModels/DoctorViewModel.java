package vHMS.Core.ViewModels;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
/***
 *  * The DoctorViewModel deals with the attributes of the doctor like qualification,user code,experience etc..
 * notification through the mail.
 * 
 * @author ShunmugaRajaG
 *
 */
public class DoctorViewModel extends PagedViewModel {
    
	public String id;
	public String code;
	public String userCode;
	public String qualification;
	public String experience;
	public String specialization;
	public String description;
	public String designation;
	public String phone;
	public String fkCompanyCode;
	public String fkLocationCode;
	public boolean status;
	@JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	public Date createdAt;
	public String createdBy;
	@JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	public Date modifiedAt;
	public String modifiedBy;
	public String updateCount;
	public String roomCode;
	public String roleCode;
	

}
