package vHMS.Core.ViewModels;

import java.util.Date;

/**
 * The RoleViewModel deals with the attributes of the branch like name,address,location etc..
 * 
 * @author Sabariraja M
 * @since 2015-08-29
 */
public class RoleViewModel extends PagedViewModel {
	
	public String id;
	public String code;
	public String name;
	public String address;
	public Date createdAt;
	public String createdBy;
	public Date modifiedAt;
	public String modifiedBy;
	public String updateCount;
    public String fkCompanyCode;
    public String fkLocationCode;
    public String status;
    public String userType;
	
	

	
	
	
}
