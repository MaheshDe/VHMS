package vHMS.Core.ViewModels;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 
 * The AppointmentViewModel contains the viewmodel entities for the entire
 * Appointment module.
 * 
 * @version 1.0
 * @author ShunmugaRajaG
 * 
 */
public class AppointmentViewModel extends PagedViewModel {
	public String id;
	public String code;
	public boolean status;
	public String patientCode;
	public String userCode;
	public String userContactNumber;
	public String patientContactNumber;
	public String appointmentMemo;
	public String appointmentStatus;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	public Date appointmentStartDateTime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	public Date appointmentEndDateTime;
	public String createdBy;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	public Date createdAt;
	public String modifiedBy;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	public Date modifiedAt;
	public String updateCount;
	public String fkCompanyCode;
	public String fkLocationCode;
	public Object json;
	public String[] userCodes;
	public String specialityCode;

}