package vHMS.Core.ViewModels;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
/**
 * 
 * The AppointmentDetailsViewModel contains the viewmodel entities for the entire  Appointment
 *   module.
 * 
 * @version 1.0
 * @author ShunmugaRajaG
 *
 */
public class AppointmentDetailsViewModel  extends PagedViewModel
{
	public String countryName;
	public String cityName;
	public String appointmentCode;
	public String  hospitalName;
	public String specializationName;
	public String specialistName;
	public String appointmentDate;
	public String specialization; 
	public String appointmentTime;
	public String specialistDesignation; 
	public String patientFirstName; 
	public String patientMiddleName; 
	public String patientLastName ;
	public String patientEmail ;
	public String patientMobileNo;
	public String patientUHID ;
}	