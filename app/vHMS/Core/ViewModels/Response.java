package vHMS.Core.ViewModels;

import java.util.List;

import play.libs.Json;

import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * The Response class is used to send response back to any of the
 * routes/controller call, it contains the status of the call (success -
 * true/false),messages, Object(viewModel) or List of Objects(viewModels) and
 * other relevant information for an ajax call.
 * 
 * @author DivyaP
 * @version 1.0
 * @since 2015-07-17
 */

public class Response {

	// This is a boolean variable representing whether the DB call is success or
	// not
	public Boolean success;
	// This sets the message
	public String message;
	// This sets the exception message
	public String ExceptionMessage;
	// This returns the list
	public List<?> ViewModels;
	// This returns the object
	public Object ViewModel;
	// This field contains the json object returned from the service layer.
	public ObjectNode data = Json.newObject();
}