package vHMS.Core.ViewModels;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
/**
 * The OPRegistrationViewModel deals with the attributes used while Out Patient 
 * is going to register.
 * 
 * @author DivyaP
 * @since 2014-09-10
 */
    public class OPRegistrationViewModel extends PagedViewModel {

     public String id;
     public String code;
     public String patientCode;
     public String uhId;
     public String memo;
     public boolean status;
     public String createdBy;
   	 @JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
   	 public  Date  createdAt;
   	 public String modifiedBy;
   	 @JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	 public Date modifiedAt;
   	 public String updateCount;
     public String json;
     public String fkCompanyCode;
     public String fkLocationCode;
     }
