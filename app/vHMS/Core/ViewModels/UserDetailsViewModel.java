package vHMS.Core.ViewModels;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
/**
 * The UserDetailsViewModel deals with the attributes of the user like name,gender,location etc..
 * notification through the mail.
 * 
 * @author Shunmuga Raja G 
 */
public class UserDetailsViewModel extends PagedViewModel {
	
	public UserViewModel userViewModel=null;
	public DoctorViewModel userTypeViewModel=null;
}
