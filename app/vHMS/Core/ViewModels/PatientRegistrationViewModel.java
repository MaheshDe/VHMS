/**
 * 
 */
package vHMS.Core.ViewModels;

import java.io.File;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * The PatientManagementViewModel contains the viewmodel entities for the entire
 * patient registration module.
 * 
 * @author DivyaP
 * @version 1.0
 * @since 2015-07-17
 */

public class PatientRegistrationViewModel extends PagedViewModel {

	 public String id;
	 public String code;
	 public String uhId;
	 public String fkCompanyCode;
	 public String fkLocationCode;
	 public  String firstName;
	 public  String middleName;
	 public String lastName;
	 public String gender;
	 public String contactNumber;
	 public String emergencyContactNumber;
	 public String maritalStatus;
	 public String bloodGroup;
	 public String permenantAddress;
	 public String temporaryAddress;
	 public boolean status;
	 public String createdBy;
	 @JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	 public Date  createdAt;
	 public String modifiedBy;
	 @JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	 public Date modifiedAt;
	 public String updateCount;
	 public String userIdTypeCode;
	 public File userIdProof;
	 public String userIdNumber;
	 @JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	 public  Date dob;
	 public String email;
	 public String password;
	 public String confirmPassword; 
	 public String newPassword;
	 public String authToken;
	 public String role;

}
