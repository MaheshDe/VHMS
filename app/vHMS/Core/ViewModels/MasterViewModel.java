package vHMS.Core.ViewModels;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * The MasterViewModel contains the viewmodel entities for the master
 * details(Basic data captured for all the tables).
 * 
 * @author DivyaP
 * @version 1.0
 * @since 2015-07-17
 */

public class MasterViewModel extends PagedViewModel {

	public String id;
	public String name;
	public String code;
	public boolean status;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	public Date createdAt;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	public Date modifiedAt;
	public String modifiedBy;
	public String updateCount;
	public String createdBy;
	public String fkCompanyCode;
	public String fkLocationCode;
}
