package vHMS.Core.ViewModels;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
/**
 * The userViewModel deals with the attributes of the user like name,gender,location etc..
 * notification through the mail.
 * 
 * @author MaheshDe
 * @since 2015-08-19
 */
public class UserScheduleViewModel extends PagedViewModel {

	public String id;
    public String code;
    public String userCode;
    public String userTypeCode;
    public String day;
    public String startTime;
    public String endTime;
    public String appointment;
    public String consultationInterval;
    public boolean status;
    public String updateCount;
    public String fkCompanyCode;
    public String fkLocationCode;
    public Date createdAt;
    public String createdBy;
    public Date modifiedAt;
    public String modifiedBy;
	
	
}
