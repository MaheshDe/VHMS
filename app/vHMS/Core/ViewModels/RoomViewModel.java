package vHMS.Core.ViewModels;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
/**
 * The RoomViewModel deals with the attributes of the room like name,roomType,floor etc..
 * 
 * 
 * @author MaheshDe
 * @since 2015-08-27
 */
public class RoomViewModel extends PagedViewModel {

	
	public String id;
	public String code;
	public String name;
	public String floor;
	public String roomTypeCode;
	public String fkCompanyCode;
	public String fkLocationCode;
	public boolean status;
	@JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	public Date createdAt;
	public String createdBy;
	@JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	public Date modifiedAt;
	public String modifiedBy;
	public String updateCount;

	
	
}
