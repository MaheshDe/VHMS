package vHMS.Core.ViewModels;

/**
 * The DocumentDataViewModel deals with the attributes/documentdata sent for a
 * document api for audit information.
 * 
 * 
 * @author Arokia Felsi
 * @since 2014-09-18
 */
public class DocumentDataViewModel extends PagedViewModel {

	public AuditEventStoreViewModel auditTrailViewModel = new AuditEventStoreViewModel();
	public DocumentViewModel documentViewModel = new DocumentViewModel();

}
