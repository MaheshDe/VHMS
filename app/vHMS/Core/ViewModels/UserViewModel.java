package vHMS.Core.ViewModels;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
/**
 * The userViewModel deals with the attributes of the user like name,gender,location etc..
 * notification through the mail.
 * 
 * @author DivyaP
 * @since 2014-09-10
 */
public class UserViewModel extends PagedViewModel {

	public String authToken;
	public String id;
	public String code;
	public String name;
	public String firstName;
	public String middleName;
	public String lastName;
	public String gender;
	@JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	public Date dob;
	public String mobile;
	public String phone;
	public String email;
	public String manager;
	public String password;
	public String currentPassword;
	public String newPassword;
	public String confirmPassword;
	public String empNo;
	public String doj;
	public String location;
	public String fkCompanyCode;
	public String fkLocationCode;
	public boolean status;
	@JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	public Date createdAt;
	public String createdBy;
	@JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	public Date modifiedAt;
	public String modifiedBy;
	public String updateCount;
	public String gravatarUrl;
	public String role;
	public String contactNumber;
	public String profilePictureUrl;
	
	
}
