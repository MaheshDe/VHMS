package vHMS.Core.ViewModels;

/**
 * The ReportViewModel deals with the attributes used for
 * report processing
 *  
 * @author DivyaP
 * @since 2014-09-10
 */
import java.io.File;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import Base.Utilities.Report.ReportProcessor;

import com.fasterxml.jackson.databind.JsonNode;

public class ReportViewModel  
{
	public String Id;
	public boolean reportCreationStatus;
	public String inputJSONString;
	public String templateFileName;
	public String templateFileLocation;
	public String imageFileLocation;
	public String jsonFileLocation;	
	public  File reportFile;
	public String reportOutputFormat;
	public String reportOutputFileName;
	public String contentType; 
	public List <String>templateNames;
	public JsonNode inputJsonNode;
	public String entityObject;
	@SuppressWarnings("rawtypes")
	//For Report processor Config 
	public Class ModelClass;
	public ReportProcessor reportProcessor;
	//Convert from json to Model Class
	public Object ModelObject;
	//Input For jasper report creation
	public JRDataSource reportDatasource=null;
	public  Map<String,Object> reportParametersMap=null;
	
}
