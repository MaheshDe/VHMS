package vHMS.Core.Models;

import javax.persistence.Entity;

import net.vz.mongodb.jackson.Id;
import net.vz.mongodb.jackson.ObjectId;

/**
 * 
 * The Faq contains the values/datas to be mapped to the DB for
 * Faq module.
 * 
 * @author 
 * 
 */
@Entity

public class Faq {
	
	@Id
	@ObjectId
	private String id;
	private String limit;
	private String skip;
	private String entityObject;
	private String where;
	private String code;
	private String question;
	private String answer;
	
	/**
	 * 
	 */
	public Faq() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param limit
	 * @param skip
	 * @param entityObject
	 * @param where
	 * @param code
	 */
	public Faq(String limit, String skip, String entityObject, String where,
			String code,String id,String question,String answer) {
		super();
		this.limit = limit;
		this.skip = skip;
		this.entityObject = entityObject;
		this.where = where;
		this.code = code;
		this.id = id;
		this.question = question;
		this.answer = answer;
	}
	/**
	 * @return the limit
	 */
	public String getLimit() {
		return limit;
	}
	/**
	 * @param limit the limit to set
	 */
	public void setLimit(String limit) {
		this.limit = limit;
	}
	/**
	 * @return the skip
	 */
	public String getSkip() {
		return skip;
	}
	/**
	 * @param skip the skip to set
	 */
	public void setSkip(String skip) {
		this.skip = skip;
	}
	/**
	 * @return the entityObject
	 */
	public String getEntityObject() {
		return entityObject;
	}
	/**
	 * @param entityObject the entityObject to set
	 */
	public void setEntityObject(String entityObject) {
		this.entityObject = entityObject;
	}
	/**
	 * @return the where
	 */
	public String getWhere() {
		return where;
	}
	/**
	 * @param where the where to set
	 */
	public void setWhere(String where) {
		this.where = where;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}
	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}
	/**
	 * @return the answer
	 */
	public String getAnswer() {
		return answer;
	}
	/**
	 * @param answer the answer to set
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	
	
	
	
}