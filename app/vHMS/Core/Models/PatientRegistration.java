/**
 * 
 */
package vHMS.Core.Models;

import java.io.File;
import java.util.Date;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.vz.mongodb.jackson.Id;
import net.vz.mongodb.jackson.ObjectId;

/**
 * The PatientManagement contains the values/datas to be mapped to the DB for
 * patient management module.
 * 
 * @author DivyaP
 * @version 1.0
 * @since 2015-07-17
 */

@Entity
public  class PatientRegistration {

 
	@Id
	 @ObjectId
	 private String id;
	 private String code;
	 private String uhId;
	 private String fkCompanyCode;
	 private String fkLocationCode;
	 private String firstName;
	 private String middleName;
	 private String lastName;
	 private String gender;
	 private String contactNumber;
	 private String emergencyContactNumber;
	 private String maritalStatus;
	 private String bloodGroup;
	 private String permenantAddress;
	 private String temporaryAddress;
	 private boolean status;
	 private String createdBy;
	 @JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	 public Date  createdAt;
	 private String modifiedBy;
	 @JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	 public Date modifiedAt;
	 private String updateCount;
	 private String userIdTypeCode;
	 private File userIdProof;
	 private String userIdNumber;
	 @JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	 Date dob;
	 private String email;
	 private String password;
	 private String role;
	

	/**
	 * Constructor using super class
	 */
	 public PatientRegistration() {
			super();
			// TODO Auto-generated constructor stub
		}
	
	

		/**
		 * @return the email
		 */
	@JsonProperty("email")
	public String getEmail() {
		return email;
	}
		/**
		 * @param email the email to set
		 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the id
	 */
	@JsonProperty("id")
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}


	/**
	 * @return the code
	 */
	@JsonProperty("code")
	public String getCode() {
		return code;
	}
	


	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the uhId
	 */
	@JsonProperty("uhId")
	public String getUhId() {
		return uhId;
	}
	/**
	 * @param uhId the uhId to set
	 */
	public void setUhId(String uhId) {
		this.uhId = uhId;
	}

	/**
	 * @return the fkCompanyCode
	 */
	@JsonProperty("fkCompanyCode")
	public String getFkCompanyCode() {
		return fkCompanyCode;
	}
	/**
	 * @param fkCompanyCode the fkCompanyCode to set
	 */
	public void setFkCompanyCode(String fkCompanyCode) {
		this.fkCompanyCode = fkCompanyCode;
	}
	/**
	 * @return the fkLocationCode
	 */
	@JsonProperty("fkLocationCode")
	public String getFkLocationCode() {
		return fkLocationCode;
	}
	/**
	 * @param fkLocationCode the fkLocationCode to set
	 */
	public void setFkLocationCode(String fkLocationCode) {
		this.fkLocationCode = fkLocationCode;
	}
	/**
	 * @return the firstName
	 */
	@JsonProperty("firstName")
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the middleName
	 */
	@JsonProperty("middleName")
	public String getMiddleName() {
		return middleName;
	}
	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	/**
	 * @return the lastName
	 */
	@JsonProperty("lastName")
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the gender
	 */
	@JsonProperty("gender")
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return the contactNumber
	 */
	@JsonProperty("contactNumber")
	public String getContactNumber() {
		return contactNumber;
	}
	/**
	 * @param contactNumber the contactNumber to set
	 */
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	/**
	 * @return the emergencyContactNumber
	 */
	@JsonProperty("emergencyContactNumber")
	public String getEmergencyContactNumber() {
		return emergencyContactNumber;
	}
	/**
	 * @param emergencyContactNumber the emergencyContactNumber to set
	 */
	public void setEmergencyContactNumber(String emergencyContactNumber) {
		this.emergencyContactNumber = emergencyContactNumber;
	}
	/**
	 * @return the maritalStatus
	 */
	@JsonProperty("maritalStatus")
	public String getMaritalStatus() {
		return maritalStatus;
	}
	/**
	 * @param maritalStatus the maritalStatus to set
	 */
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	/**
	 * @return the id
	 */
	@JsonProperty("bloodGroup")
	public String getBloodGroup() {
		return bloodGroup;
	}
	/**
	 * @param bloodGroup the bloodGroup to set
	 */
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	/**
	 * @return the Status
	 */
	@JsonProperty("status")
	public boolean isStatus() {
		return status;
	}
	/**
	 * @param Status the Status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}
	/**
	 * @return the CreatedBy
	 */
	@JsonProperty("createdBy")
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param CreatedBy the CreatedBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	/**
	 * @return the ModifiedBy
	 */
	@JsonProperty("modifiedBy")
	public String getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param ModifiedBy the ModifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	/**
	 * @return the UpdateCount
	 */
	@JsonProperty("updateCount")
	public String getUpdateCount() {
		return updateCount;
	}
	/**
	 * @param UpdateCount the UpdateCount to set
	 */
	public void setUpdateCount(String updateCount) {
		this.updateCount = updateCount;
	}

	/**
	 * @return the permenantAddress
	 */
	@JsonProperty("permenantAddress")
	public String getPermenantAddress() {
		return permenantAddress;
	}
	/**
	 * @param permenantAddress the permenantAddress to set
	 */
	public void setPermenantAddress(String permenantAddress) {
		this.permenantAddress = permenantAddress;
	}

	/**
	 * @return the temporaryAddress
	 */
	@JsonProperty("temporaryAddress")
	public String getTemporaryAddress() {
		return temporaryAddress;
	}
	/**
	 * @param temporaryAddress the temporaryAddress to set
	 */
	public void setTemporaryAddress(String temporaryAddress) {
		this.temporaryAddress = temporaryAddress;
	}
	/**
	 * @return the id
	 */
	@JsonProperty("createdAt")
	public Date getCreatedAt() {
		return createdAt;
	}
	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	/**
	 * @return the modifiedAt
	 */
	@JsonProperty("modifiedAt")
	public Date getModifiedAt() {
		return modifiedAt;
	}
	/**
	 * @param modifiedAt the modifiedAt to set
	 */
	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}
	/**
	 * @return the userIdTypeCode
	 */
	@JsonProperty("userIdTypeCode")
	public String getUserIdTypeCode() {
		return userIdTypeCode;
	}
	/**
	 * @param userIdTypeCode the userIdTypeCode to set
	 */
	public void setUserIdTypeCode(String userIdTypeCode) {
		this.userIdTypeCode = userIdTypeCode;
	}
	/**
	 * @return the userIdProof
	 */
	@JsonProperty("userIdProof")
	public File getUserIdProof() {
		return userIdProof;
	}
	/**
	 * @param userIdProof the userIdProof to set
	 */
	public void setUserIdProof(File userIdProof) {
		this.userIdProof = userIdProof;
	}
	/**
	 * @return the id
	 */
	@JsonProperty("id")
	public String getUserIdNumber() {
		return userIdNumber;
	}
	/**
	 * @param UpdateCount the UpdateCount to set
	 */
	public void setUserIdNumber(String userIdNumber) {
		this.userIdNumber = userIdNumber;
	}
	/**
	 * @return the dob
	 */
	@JsonProperty("dob")
	public Date getDob() {
		return dob;
	}
	/**
	 * @param dob the dob to set
	 */
	public void setDob(Date dob) {
		this.dob = dob;
	}
	/**
	 * @return the password
	 */
	@JsonProperty("password")
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}


	@JsonProperty("role")
	public String getRole() {
		return role;
	}



	public void setRole(String role) {
		this.role = role;
	}
	
	
}
