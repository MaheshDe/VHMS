package vHMS.Core.Models;

import java.util.Date;

import javax.persistence.Entity;

import net.vz.mongodb.jackson.Id;
import net.vz.mongodb.jackson.ObjectId;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Document deals with the attributes used for CRUD operations for all
 * document upload data. The attribute names should be matching with the column
 * name.
 * 
 * @author Arokia Felsi
 * @since 2014-09-10
 */
@Entity
public class Document {

	@Id
	@ObjectId
	private String id;
	private String code;
	private String status;
	private String createdBy;
	private String updateCount;
	private String modifiedBy;
	private String fileName;
	private String requestType;
	private String documentTypeCode;
	private String requestCode;
	private String json;
	private String fkCompanyCode;

	@JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	public Date  createdAt;

	 @JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	 public Date modifiedAt;
	private byte[] file;
	private String contentType;

	

	/**
	 * 
	 */
	public Document() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param code
	 * @param status
	 * @param createdBy
	 * @param updateCount
	 * @param modifiedBy
	 * @param fileName
	 * @param requestType
	 * @param documentTypeCode
	 * @param requestCode
	 * @param jSON
	 * @param fkCompanyCode
	 * @param createdAt
	 * @param modifiedAt
	 * @param file
	 * @param contentType
	 */
	public Document(String id, String code, String status, String createdBy,
			String updateCount, String modifiedBy, String fileName,
			String requestType, String documentTypeCode, String requestCode,
			String json, String fkCompanyCode, Date createdAt, Date modifiedAt,
			byte[] file, String contentType) {
		super();
		this.id = id;
		this.code = code;
		this.status = status;
		this.createdBy = createdBy;
		this.updateCount = updateCount;
		this.modifiedBy = modifiedBy;
		this.fileName = fileName;
		this.requestType = requestType;
		this.documentTypeCode = documentTypeCode;
		this.requestCode = requestCode;
		this.json = json;
		this.fkCompanyCode = fkCompanyCode;
		this.createdAt = createdAt;
		this.modifiedAt = modifiedAt;
		this.file = file;
		this.contentType = contentType;
	}

	
	/**
	 * @return the id
	 */
	@JsonProperty("id")
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the json
	 */
	public String getJson() {
		return json;
	}

	/**
	 * @param json the json to set
	 */
	public void setJson(String json) {
		this.json = json;
	}

	/**
	 * @return the code
	 */
	@JsonProperty("code")
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the status
	 */
	@JsonProperty("status")
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the createdBy
	 */
	@JsonProperty("createdBy")
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the updateCount
	 */
	@JsonProperty("updateCount")
	public String getUpdateCount() {
		return updateCount;
	}

	/**
	 * @param updateCount the updateCount to set
	 */
	public void setUpdateCount(String updateCount) {
		this.updateCount = updateCount;
	}

	/**
	 * @return the modifiedBy
	 */
	@JsonProperty("modifiedBy")
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the fileName
	 */
	@JsonProperty("fileName")
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the requestType
	 */
	@JsonProperty("requestType")
	public String getRequestType() {
		return requestType;
	}

	/**
	 * @param requestType the requestType to set
	 */
	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	/**
	 * @return the documentTypeCode
	 */
	@JsonProperty("documentTypeCode")
	public String getDocumentTypeCode() {
		return documentTypeCode;
	}

	/**
	 * @param documentTypeCode the documentTypeCode to set
	 */
	public void setDocumentTypeCode(String documentTypeCode) {
		this.documentTypeCode = documentTypeCode;
	}

	/**
	 * @return the requestCode
	 */
	@JsonProperty("requestCode")
	public String getRequestCode() {
		return requestCode;
	}

	/**
	 * @param requestCode the requestCode to set
	 */
	public void setRequestCode(String requestCode) {
		this.requestCode = requestCode;
	}

	/**
	 * @return the fkCompanyCode
	 */
	@JsonProperty("fkCompanyCode")
	public String getFkCompanyCode() {
		return fkCompanyCode;
	}

	/**
	 * @param fkCompanyCode the fkCompanyCode to set
	 */
	public void setFkCompanyCode(String fkCompanyCode) {
		this.fkCompanyCode = fkCompanyCode;
	}

	/**
	 * @return the createdAt
	 */
	@JsonProperty("createdAt")
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the modifiedAt
	 */
	@JsonProperty("modifiedAt")
	public Date getModifiedAt() {
		return modifiedAt;
	}

	/**
	 * @param modifiedAt the modifiedAt to set
	 */
	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	/**
	 * @return the file
	 */
	@JsonProperty("file")
	public byte[] getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(byte[] file) {
		this.file = file;
	}

	/**
	 * @return the contentType
	 */
	@JsonProperty("contentType")
	public String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	
}
