/**
 * 
 */
package vHMS.Core.Models;

import java.util.Date;

import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The AuditEventStore deals with the attributes used for CRUD operations for
 * all auditing data. The attribute names should be matching with the column
 * name.
 * 
 * @author GadepalliM
 * 
 */
public class AuditEventStore {
	@Id
	private String Id;
	private String EntityDescription;
	private String EntityId;
	private String AuditEventDescription;
	private String JSON;
	private String CreatedBy;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd,HH:00", timezone = JsonFormat.DEFAULT_TIMEZONE)
	public Date modifiedAt;
	private String fkCompanyCode;
	private String UpdateCount;

	/**
	 * @return the id
	 */
	@JsonProperty("Id")
	public String getId() {
		return Id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		Id = id;
	}

	/**
	 * @return the entityDescription
	 */
	@JsonProperty("EntityDescription")
	public String getEntityDescription() {
		return EntityDescription;
	}

	/**
	 * @param entityDescription
	 *            the entityDescription to set
	 */
	public void setEntityDescription(String entityDescription) {
		EntityDescription = entityDescription;
	}

	/**
	 * @return the entityId
	 */
	@JsonProperty("EntityId")
	public String getEntityId() {
		return EntityId;
	}

	/**
	 * @param entityId
	 *            the entityId to set
	 */
	public void setEntityId(String entityId) {
		EntityId = entityId;
	}

	/**
	 * @return the auditEventDescription
	 */
	@JsonProperty("AuditEventDescription")
	public String getAuditEventDescription() {
		return AuditEventDescription;
	}

	/**
	 * @param auditEventDescription
	 *            the auditEventDescription to set
	 */
	public void setAuditEventDescription(String auditEventDescription) {
		AuditEventDescription = auditEventDescription;
	}

	/**
	 * @return the jSON
	 */
	@JsonProperty("JSON")
	public String getJSON() {
		return JSON;
	}

	/**
	 * @param jSON
	 *            the jSON to set
	 */
	public void setJSON(String jSON) {
		JSON = jSON;
	}

	/**
	 * @return the createdBy
	 */
	@JsonProperty("CreatedBy")
	public String getCreatedBy() {
		return CreatedBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}

	/**
	 * @return the modifiedAt
	 */
	@JsonProperty("modifiedAt")
	public Date getModifiedAt() {
		return modifiedAt;
	}
	/**
	 * @param modifiedAt the modifiedAt to set
	 */
	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	/**
	 * @return the fkCompanyCode
	 */
	@JsonProperty("fkCompanyCode")
	public String getFkCompanyCode() {
		return fkCompanyCode;
	}

	/**
	 * @param fkCompanyCode
	 *            the fkCompanyCode to set
	 */
	public void setFkCompanyCode(String fkCompanyCode) {
		this.fkCompanyCode = fkCompanyCode;
	}

	/**
	 * @return the updateCount
	 */
	@JsonProperty("UpdateCount")
	public String getUpdateCount() {
		return UpdateCount;
	}

	/**
	 * @param updateCount
	 *            the updateCount to set
	 */
	public void setUpdateCount(String updateCount) {
		UpdateCount = updateCount;
	}

	/**
	 * @param id
	 * @param entityDescription
	 * @param entityId
	 * @param auditEventDescription
	 * @param jSON
	 * @param createdBy
	 * @param dateModified
	 * @param fkCompanyCode
	 * @param updateCount
	 */
	public AuditEventStore(String id, String entityDescription,
			String entityId, String auditEventDescription, String jSON,
			String createdBy, Date modifiedAt, String fkCompanyCode,
			String updateCount) {
		super();
		Id = id;
		EntityDescription = entityDescription;
		EntityId = entityId;
		AuditEventDescription = auditEventDescription;
		JSON = jSON;
		CreatedBy = createdBy;
		this.modifiedAt = modifiedAt;
		this.fkCompanyCode = fkCompanyCode;
		UpdateCount = updateCount;
	}

	/**
	 * 
	 */
	public AuditEventStore() {
		super();
		// TODO Auto-generated constructor stub
	}

}
