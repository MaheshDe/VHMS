package vHMS.Core.Models;

import java.util.Date;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.vz.mongodb.jackson.Id;
import net.vz.mongodb.jackson.ObjectId;




/**
 * The PatientManagement contains the values/datas to be mapped to the DB for
 * patient management module.
 * 
 * @author Padmavathi
 * @version 1.0
 * @since 2015-07-21
 */

@Entity
public class OPRegistration {
	
	 @Id
	 @ObjectId
	 private String id;
	 private String code;
	 private String uhId;
	 private String memo;
	 private String fkCompanyCode;
	 private String fkLocationCode;
	 private String patientCode;
	 private boolean status;
	 private String createdBy;
	 @JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	 Date  createdAt;
	 private  String modifiedBy;
	 @JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	 Date modifiedAt;
	 private String updateCount;
	 private String json;
	 
	 
	 /**
		 * Constructor using super class
		 */
	public OPRegistration() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor using fields
	 */
	public OPRegistration(String id, String code, String uhId, String memo,
			boolean status, String createdBy, Date createdAt,
			String modifiedBy, Date modifiedAt, String updateCount,String json,String patientCode) {
		super();
		this.id = id;
		this.code = code;
		this.patientCode=patientCode;
		this.uhId = uhId;
		this.memo = memo;
		this.status = status;
		this.createdBy = createdBy;
		this.createdAt = createdAt;
		this.modifiedBy = modifiedBy;
		this.modifiedAt = modifiedAt;
		this.updateCount = updateCount;
		this.json=json;
	}
	
	/**
	 * @return the json
	 */
	@JsonProperty("json")
	public String getJson() {
		return json;
	}
	/**
	 * @param json the json to set
	 */
	public void setJson(String json) {
		this.json = json;
	}

	/**
	 * @return the id
	 */
	@JsonProperty("id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the id
	 */
	@JsonProperty("code")
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the uhId
	 */
	@JsonProperty("uhId")
	public String getUhId() {
		return uhId;
	}
	/**
	 * @param uhId the uhId to set
	 */
	public void setUhId(String uhId) {
		this.uhId = uhId;
	}
	/**
	 * @return the id
	 */
	@JsonProperty("memo")
	public String getMemo() {
		return memo;
	}
	/**
	 * @param memo the memo to set
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}
	/**
	 * @return the status
	 */
	@JsonProperty("status")
	public boolean isStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}
	/**
	 * @return the createdBy
	 */
	@JsonProperty("createdBy")
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdAt
	 */
	@JsonProperty("createdAt")
	public Date getCreatedAt() {
		return createdAt;
	}
	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	/**
	 * @return the modifiedBy
	 */
	@JsonProperty("modifiedBy")
	public String getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	/**
	 * @return the modifiedAt
	 */
	@JsonProperty("modifiedAt")
	public Date getModifiedAt() {
		return modifiedAt;
	}
	/**
	 * @param modifiedAt the modifiedAt to set
	 */
	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}
	/**
	 * @return the updateCount
	 */
	@JsonProperty("updateCount")
	public String getUpdateCount() {
		return updateCount;
	}
	/**
	 * @param updateCount the updateCount to set
	 */
	public void setUpdateCount(String updateCount) {
		this.updateCount = updateCount;
	}
	/**
	 * @return the fkCompanyCode
	 */
	@JsonProperty("fkCompanyCode")
	public String getFkCompanyCode() {
		return fkCompanyCode;
	}
	/**
	 * @param fkCompanyCode the fkCompanyCode to set
	 */
	public void setFkCompanyCode(String fkCompanyCode) {
		this.fkCompanyCode = fkCompanyCode;
	}
	/**
	 * @return the fkLocationCode
	 */
	@JsonProperty("fkLocationCode")
	public String getFkLocationCode() {
		return fkLocationCode;
	}
	/**
	 * @param fkLocationCode the fkLocationCode to set
	 */
	public void setFkLocationCode(String fkLocationCode) {
		this.fkLocationCode = fkLocationCode;
	}
	/**
	 * @return the patientcode
	 */
	@JsonProperty("patientCode")
	public String getPatientcode() {
		return patientCode;
	}
	/**
	 * @param patientcode the patientcode to set
	 */
	public void setPatientcode(String patientCode) {
		this.patientCode = patientCode;
	}
	 
	 
	 
	 
	 
}
