package vHMS.Core.Models;

import java.util.Date;

import javax.persistence.Entity;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.fasterxml.jackson.annotation.JsonFormat;

import net.vz.mongodb.jackson.Id;
import net.vz.mongodb.jackson.ObjectId;

/**
 * The User contains the values/datas to be mapped to the DB for User module.
 * 
 * @author MaheshDe
 * @version 1.0
 * @since 2015-08-19
 */

@Entity
public class UserSchedule {

	@Id
	@ObjectId
	private String id;
    private String code;
    private String userCode;
    private String userTypeCode;
    private String day;
    private String startTime;
    private String endTime;
    private String appointment;
    private String consultationInterval;
    private boolean status;
    private String updateCount;
    private String fkCompanyCode;
    private String fkLocationCode;
    private Date createdAt;
    private String createdBy;
    private Date modifiedAt;
    private String modifiedBy;
    
    
    
	/**
	 * 
	 */
	public UserSchedule() {
		super();
		// TODO Auto-generated constructor stub
	}



	/**
	 * @param id
	 * @param code
	 * @param userCode
	 * @param userTypeCode
	 * @param day
	 * @param startTime
	 * @param endTime
	 * @param appointment
	 * @param consultationInterval
	 * @param status
	 * @param updateCount
	 * @param fkCompanyCode
	 * @param fkLocationCode
	 * @param createdAt
	 * @param createdBy
	 * @param modifiedAt
	 * @param modifiedBy
	 */
	public UserSchedule(String id, String code, String userCode,
			String userTypeCode, String day, String startTime, String endTime,
			String appointment, String consultationInterval, boolean status,
			String updateCount, String fkCompanyCode, String fkLocationCode,
			Date createdAt, String createdBy, Date modifiedAt, String modifiedBy) {
		super();
		this.id = id;
		this.code = code;
		this.userCode = userCode;
		this.userTypeCode = userTypeCode;
		this.day = day;
		this.startTime = startTime;
		this.endTime = endTime;
		this.appointment = appointment;
		this.consultationInterval = consultationInterval;
		this.status = status;
		this.updateCount = updateCount;
		this.fkCompanyCode = fkCompanyCode;
		this.fkLocationCode = fkLocationCode;
		this.createdAt = createdAt;
		this.createdBy = createdBy;
		this.modifiedAt = modifiedAt;
		this.modifiedBy = modifiedBy;
	}



	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}



	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}



	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}



	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}



	/**
	 * @return the userCode
	 */
	public String getUserCode() {
		return userCode;
	}



	/**
	 * @param userCode the userCode to set
	 */
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}



	/**
	 * @return the userTypeCode
	 */
	public String getUserTypeCode() {
		return userTypeCode;
	}



	/**
	 * @param userTypeCode the userTypeCode to set
	 */
	public void setUserTypeCode(String userTypeCode) {
		this.userTypeCode = userTypeCode;
	}



	/**
	 * @return the day
	 */
	public String getDay() {
		return day;
	}



	/**
	 * @param day the day to set
	 */
	public void setDay(String day) {
		this.day = day;
	}



	/**
	 * @return the startTime
	 */
	public String getStartTime() {
		return startTime;
	}



	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}



	/**
	 * @return the endTime
	 */
	public String getEndTime() {
		return endTime;
	}



	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}



	/**
	 * @return the appointment
	 */
	public String getAppointment() {
		return appointment;
	}



	/**
	 * @param appointment the appointment to set
	 */
	public void setAppointment(String appointment) {
		this.appointment = appointment;
	}



	/**
	 * @return the consultationInterval
	 */
	public String getConsultationInterval() {
		return consultationInterval;
	}



	/**
	 * @param consultationInterval the consultationInterval to set
	 */
	public void setConsultationInterval(String consultationInterval) {
		this.consultationInterval = consultationInterval;
	}



	/**
	 * @return the status
	 */
	
	public boolean isStatus() {
		return status;
	}



	/**
	 * @param status the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}



	/**
	 * @return the updateCount
	 */
	public String getUpdateCount() {
		return updateCount;
	}



	/**
	 * @param updateCount the updateCount to set
	 */
	public void setUpdateCount(String updateCount) {
		this.updateCount = updateCount;
	}



	/**
	 * @return the fkCompanyCode
	 */
	public String getFkCompanyCode() {
		return fkCompanyCode;
	}



	/**
	 * @param fkCompanyCode the fkCompanyCode to set
	 */
	public void setFkCompanyCode(String fkCompanyCode) {
		this.fkCompanyCode = fkCompanyCode;
	}



	/**
	 * @return the fkLocationCode
	 */
	public String getFkLocationCode() {
		return fkLocationCode;
	}



	/**
	 * @param fkLocationCode the fkLocationCode to set
	 */
	public void setFkLocationCode(String fkLocationCode) {
		this.fkLocationCode = fkLocationCode;
	}



	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}



	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}



	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}



	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}



	/**
	 * @return the modifiedAt
	 */
	public Date getModifiedAt() {
		return modifiedAt;
	}



	/**
	 * @param modifiedAt the modifiedAt to set
	 */
	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}



	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}



	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
    
    
	
	
	
    
}
