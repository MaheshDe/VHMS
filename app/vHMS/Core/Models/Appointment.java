/**
 * 
 */
package vHMS.Core.Models;

import java.util.Date;

import javax.persistence.Entity;

import net.vz.mongodb.jackson.Id;
import net.vz.mongodb.jackson.ObjectId;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 
 * The Appointment contains the values/datas to be mapped to the DB for
 * Appointment module.
 * 
 * @author ShunmugaRajaG
 * 
 */
@Entity
public class Appointment {

	@Id
	@ObjectId
	private String id;
	private String code;

	private String patientCode;
	private String userCode;
	private String patientContactNumber;
	private String appointmentMemo;
	private boolean status;
	private String appointmentStatus;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	public Date appointmentStartDateTime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	public Date appointmentEndDateTime;

	private String createdBy;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	public Date createdAt;
	private String modifiedBy;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	public Date modifiedAt;
	private String updateCount;
	private String fkCompanyCode;
	private String fkLocationCode;
	private Object json;

	/**
	 * 
	 */
	public Appointment() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id
	 * @param code
	 * @param patientCode
	 * @param userCode
	 * @param patientContactNumber
	 * @param appointmentMemo
	 * @param status
	 * @param appointmentStatus
	 * @param appointmentStartDateTime
	 * @param appointmentEndDateTime
	 * @param createdBy
	 * @param createdAt
	 * @param modifiedBy
	 * @param modifiedAt
	 * @param updateCount
	 * @param fkCompanyCode
	 * @param fkLocationCode
	 * @param json
	 */
	public Appointment(String id, String code, String patientCode,
			String userCode, String patientContactNumber,
			String appointmentMemo, boolean status, String appointmentStatus,
			Date appointmentStartDateTime, Date appointmentEndDateTime,
			String createdBy, Date createdAt, String modifiedBy,
			Date modifiedAt, String updateCount, String fkCompanyCode,
			String fkLocationCode, Object json) {
		super();
		this.id = id;
		this.code = code;
		this.patientCode = patientCode;
		this.userCode = userCode;
		this.patientContactNumber = patientContactNumber;
		this.appointmentMemo = appointmentMemo;
		this.status = status;
		this.appointmentStatus = appointmentStatus;
		this.appointmentStartDateTime = appointmentStartDateTime;
		this.appointmentEndDateTime = appointmentEndDateTime;
		this.createdBy = createdBy;
		this.createdAt = createdAt;
		this.modifiedBy = modifiedBy;
		this.modifiedAt = modifiedAt;
		this.updateCount = updateCount;
		this.fkCompanyCode = fkCompanyCode;
		this.fkLocationCode = fkLocationCode;
		this.json = json;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the patientCode
	 */
	public String getPatientCode() {
		return patientCode;
	}

	/**
	 * @param patientCode
	 *            the patientCode to set
	 */
	public void setPatientCode(String patientCode) {
		this.patientCode = patientCode;
	}

	/**
	 * @return the userCode
	 */
	public String getUserCode() {
		return userCode;
	}

	/**
	 * @param userCode
	 *            the userCode to set
	 */
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	/**
	 * @return the patientContactNumber
	 */
	public String getPatientContactNumber() {
		return patientContactNumber;
	}

	/**
	 * @param patientContactNumber
	 *            the patientContactNumber to set
	 */
	public void setPatientContactNumber(String patientContactNumber) {
		this.patientContactNumber = patientContactNumber;
	}

	/**
	 * @return the appointmentMemo
	 */
	public String getAppointmentMemo() {
		return appointmentMemo;
	}

	/**
	 * @param appointmentMemo
	 *            the appointmentMemo to set
	 */
	public void setAppointmentMemo(String appointmentMemo) {
		this.appointmentMemo = appointmentMemo;
	}

	/**
	 * @return the status
	 */
	public boolean isStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}

	/**
	 * @return the appointmentStatus
	 */
	public String getAppointmentStatus() {
		return appointmentStatus;
	}

	/**
	 * @param appointmentStatus
	 *            the appointmentStatus to set
	 */
	public void setAppointmentStatus(String appointmentStatus) {
		this.appointmentStatus = appointmentStatus;
	}

	/**
	 * @return the appointmentStartDateTime
	 */
	public Date getAppointmentStartDateTime() {
		return appointmentStartDateTime;
	}

	/**
	 * @param appointmentStartDateTime
	 *            the appointmentStartDateTime to set
	 */
	public void setAppointmentStartDateTime(Date appointmentStartDateTime) {
		this.appointmentStartDateTime = appointmentStartDateTime;
	}

	/**
	 * @return the appointmentEndDateTime
	 */
	public Date getAppointmentEndDateTime() {
		return appointmentEndDateTime;
	}

	/**
	 * @param appointmentEndDateTime
	 *            the appointmentEndDateTime to set
	 */
	public void setAppointmentEndDateTime(Date appointmentEndDateTime) {
		this.appointmentEndDateTime = appointmentEndDateTime;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt
	 *            the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedAt
	 */
	public Date getModifiedAt() {
		return modifiedAt;
	}

	/**
	 * @param modifiedAt
	 *            the modifiedAt to set
	 */
	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	/**
	 * @return the updateCount
	 */
	public String getUpdateCount() {
		return updateCount;
	}

	/**
	 * @param updateCount
	 *            the updateCount to set
	 */
	public void setUpdateCount(String updateCount) {
		this.updateCount = updateCount;
	}

	/**
	 * @return the fkCompanyCode
	 */
	public String getFkCompanyCode() {
		return fkCompanyCode;
	}

	/**
	 * @param fkCompanyCode
	 *            the fkCompanyCode to set
	 */
	public void setFkCompanyCode(String fkCompanyCode) {
		this.fkCompanyCode = fkCompanyCode;
	}

	/**
	 * @return the fkLocationCode
	 */
	public String getFkLocationCode() {
		return fkLocationCode;
	}

	/**
	 * @param fkLocationCode
	 *            the fkLocationCode to set
	 */
	public void setFkLocationCode(String fkLocationCode) {
		this.fkLocationCode = fkLocationCode;
	}

	/**
	 * @return the json
	 */
	public Object getJson() {
		return json;
	}

	/**
	 * @param json
	 *            the json to set
	 */
	public void setJson(Object json) {
		this.json = json;
	}

}
