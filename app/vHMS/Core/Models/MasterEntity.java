package vHMS.Core.Models;

/**
 * The MasterEntity contains the values/datas to be mapped to the DB for all the class extending Master Model.
 * 
 * @author JasmitaB
 * @version 1.0
 * @since 2015-08-10
 */

import java.util.Date;

import javax.persistence.Entity;

import net.vz.mongodb.jackson.Id;
import net.vz.mongodb.jackson.ObjectId;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class MasterEntity {
	@Id
	@ObjectId
	private String id;

	private String name;
	private String code;
	private boolean status;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	Date createdAt;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	Date modifiedAt;
	private String modifiedBy;
	private String updateCount;
	private String createdBy;
	private String fkCompanyCode;
	private String fkLocationCode;
	
	
	public MasterEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	


	/**
	 * @param id
	 * @param name
	 * @param code
	 * @param status
	 * @param createdAt
	 * @param modifiedAt
	 * @param modifiedBy
	 * @param updateCount
	 * @param createdBy
	 * @param fkCompanyCode
	 * @param fkLocationCode
	 */
	public MasterEntity(String id, String name, String code, boolean status,
			Date createdAt, Date modifiedAt, String modifiedBy,
			String updateCount, String createdBy, String fkCompanyCode,
			String fkLocationCode) {
		super();
		this.id = id;
		this.name = name;
		this.code = code;
		this.status = status;
		this.createdAt = createdAt;
		this.modifiedAt = modifiedAt;
		this.modifiedBy = modifiedBy;
		this.updateCount = updateCount;
		this.createdBy = createdBy;
		this.fkCompanyCode = fkCompanyCode;
		this.fkLocationCode = fkLocationCode;
	}





	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}


	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}



	/**
	 * @return the status
	 */
	public boolean isStatus() {
		return status;
	}


	/**
	 * @param status the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}



	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}


	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}


	/**
	 * @return the modifiedAt
	 */
	public Date getModifiedAt() {
		return modifiedAt;
	}


	/**
	 * @param modifiedAt the modifiedAt to set
	 */
	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}


	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}


	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}


	/**
	 * @return the updateCount
	 */
	public String getUpdateCount() {
		return updateCount;
	}


	/**
	 * @param updateCount the updateCount to set
	 */
	public void setUpdateCount(String updateCount) {
		this.updateCount = updateCount;
	}


	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}


	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	/**
	 * @return the fkCompanyCode
	 */
	public String getFkCompanyCode() {
		return fkCompanyCode;
	}


	/**
	 * @param fkCompanyCode the fkCompanyCode to set
	 */
	public void setFkCompanyCode(String fkCompanyCode) {
		this.fkCompanyCode = fkCompanyCode;
	}


	/**
	 * @return the fkLocationCode
	 */
	public String getFkLocationCode() {
		return fkLocationCode;
	}


	/**
	 * @param fkLocationCode the fkLocationCode to set
	 */
	public void setFkLocationCode(String fkLocationCode) {
		this.fkLocationCode = fkLocationCode;
	}



}
