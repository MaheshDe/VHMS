package vHMS.Core.Models;

import java.util.Date;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import net.vz.mongodb.jackson.Id;
import net.vz.mongodb.jackson.ObjectId;

/**
 * The Floor contains the values/data to be mapped to the DB for Floor module.
 * 
 * @author MaheshDe
 * @version 1.0
 * @since 2015-08-27
 */

@Entity
public class Building {

	@Id
	@ObjectId
	private String id;
	private String code;
	private String name;
	private String fkCompanyCode;
	private String fkLocationCode;
	private boolean status;
	@JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	private Date createdAt;
	private String createdBy;
	@JsonFormat(pattern = "yyyy-MM-dd,HH:mm:ss", timezone = JsonFormat.DEFAULT_TIMEZONE)
	private Date modifiedAt;
	private String modifiedBy;
	private String updateCount;
	/**
	 * 
	 */
	public Building() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param id
	 * @param code
	 * @param name
	 * @param fkCompanyCode
	 * @param fkLocationCode
	 * @param status
	 * @param createdAt
	 * @param createdBy
	 * @param modifiedAt
	 * @param modifiedBy
	 * @param updateCount
	 */
	public Building(String id, String code, String name, String fkCompanyCode,
			String fkLocationCode, boolean status, Date createdAt,
			String createdBy, Date modifiedAt, String modifiedBy,
			String updateCount) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.fkCompanyCode = fkCompanyCode;
		this.fkLocationCode = fkLocationCode;
		this.status = status;
		this.createdAt = createdAt;
		this.createdBy = createdBy;
		this.modifiedAt = modifiedAt;
		this.modifiedBy = modifiedBy;
		this.updateCount = updateCount;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the fkCompanyCode
	 */
	public String getFkCompanyCode() {
		return fkCompanyCode;
	}
	/**
	 * @param fkCompanyCode the fkCompanyCode to set
	 */
	public void setFkCompanyCode(String fkCompanyCode) {
		this.fkCompanyCode = fkCompanyCode;
	}
	/**
	 * @return the fkLocationCode
	 */
	public String getFkLocationCode() {
		return fkLocationCode;
	}
	/**
	 * @param fkLocationCode the fkLocationCode to set
	 */
	public void setFkLocationCode(String fkLocationCode) {
		this.fkLocationCode = fkLocationCode;
	}
	/**
	 * @return the status
	 */
	public boolean isStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(boolean status) {
		this.status = status;
	}
	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}
	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the modifiedAt
	 */
	public Date getModifiedAt() {
		return modifiedAt;
	}
	/**
	 * @param modifiedAt the modifiedAt to set
	 */
	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}
	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}
	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	/**
	 * @return the updateCount
	 */
	public String getUpdateCount() {
		return updateCount;
	}
	/**
	 * @param updateCount the updateCount to set
	 */
	public void setUpdateCount(String updateCount) {
		this.updateCount = updateCount;
	}
	
	
	
	
}