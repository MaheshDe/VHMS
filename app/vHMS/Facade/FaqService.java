package vHMS.Facade;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.lang3.RandomStringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;














import vHMS.Core.Models.Appointment;
import vHMS.Core.Models.Faq;
import vHMS.Core.Models.MasterEntity;
import vHMS.Core.Models.PatientRegistration;
import vHMS.Core.Models.User;
import vHMS.Core.ViewModels.AppointmentViewModel;
import vHMS.Core.ViewModels.FaqViewModel;
import vHMS.Core.ViewModels.MasterViewModel;
import vHMS.Core.ViewModels.NotificationViewModel;
import vHMS.Core.ViewModels.PatientRegistrationViewModel;
import vHMS.Core.ViewModels.Response;
import vHMS.Core.ViewModels.ResponseViewModel;
import vHMS.Core.ViewModels.UserViewModel;
import Base.Data.IGenericMongoDao;
import Base.Utilities.CommonUtils;
import Base.Utilities.HelperUtil;
import Base.Utilities.NullAwareBeanUtilsBean;
import Base.Utilities.Email.SmtpSender;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * The FaqService is the implementation for the interface IFaqService.
 * 
 * @author 
 * @since 
 */

@Service
@Transactional
public class FaqService implements IFaqService {

	@Autowired
	private IGenericMongoDao genericDAO;
	private HelperUtil helperUtil = new HelperUtil();
	private ModelMapper modelmapper = new ModelMapper();
	private BeanUtilsBean notNull = new NullAwareBeanUtilsBean();
	@Autowired
	private SmtpSender defaultEmailSender;
	@Override
	public Response all(FaqViewModel viewModel) throws JsonProcessingException,	IOException, Exception {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {
			// Condition to check whether where attribute is empty
			// To check for search criteria
			if (viewModel.where != null) {
				// for appending the where and the like for search criteria
				viewModel.where = helperUtil
						.ConstructWhereClauseFromJson(viewModel.where);
			}

			List<Faq> list =  genericDAO.all(viewModel.entityObject,
						Faq.class, viewModel.where, viewModel.limit,
						viewModel.skip);
			

			// Checking whether the list is empty or not
			if (null != list && list.size() > 0) {
				// Define the target type
				Type targetListType = new TypeToken<List<FaqViewModel>>() {
				}.getType();
				// Mapping the list with the target view model
				List<FaqViewModel> lists = modelmapper.map(list,
						targetListType);
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.fetchFaqdata");
				// appending the list to the response viewModels list to display
				// in the UI
				response.ViewModels = lists;
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.fetchFaqdata");
				
			}
		} catch (Exception e) {
			// error in the database or connection
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
				
			}
		}
		return response;
		
		
	}
	@Override
	public Response create(FaqViewModel viewModel) throws Exception { 
		Response response = new Response();
		String id= null;
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			/*viewModel = helperUtil.AttachCommonFields(viewModel,
					FaqViewModel.class);*/
			
			map.put("question", Pattern.compile(viewModel.question, Pattern.CASE_INSENSITIVE));
			
			Faq faqexist = genericDAO.findOneByAttribute(
					viewModel.entityObject, map, Faq.class);
			
			if(null != faqexist){
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.createFaq.exists");
					
			}else{
				
				//calling helper class
				viewModel = helperUtil.AttachCommonFields(viewModel,
						FaqViewModel.class);
				
				// getting the source and target to set the properties
				Faq faq = modelmapper.map(viewModel,
						Faq.class);
				// Create the new record in table.
				Object[] obj = genericDAO.save(viewModel.entityObject, faq);
				
                /*map.clear();
				map.put("code", viewModel.code);
				*/
				Faq faqSave = genericDAO.findOneByAttribute(
						viewModel.entityObject, map, Faq.class);
				
				
				for (Object objId : obj) {
					id = String.valueOf(objId);
					faq.setId(id);;
					}
				
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.entity.creation");
				response.ViewModel = faqSave;
				
			}
		
		} catch (Exception e) {// Handles Exception
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();

		}
		return response;
	
	}
	@Override
	public Response update(FaqViewModel viewModel) throws Exception {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {
			// Calling the helper class to set the mandatory properties of the
						// view model
						viewModel = helperUtil.AttachCommonFields(viewModel,
								FaqViewModel.class);
			Faq faq = modelmapper.map(viewModel,
					Faq.class);
			Map<String, Object> map = new HashMap<String, Object>();
			// Fetch the first record with given conditions satisfied.
			map.put("code", viewModel.code);

			// Check whether the record exists before update.
			Faq faqdata = genericDAO.findOneByAttribute(
					viewModel.entityObject, map, Faq.class);
			
			map.clear();
			map.put("question",
					Pattern.compile(viewModel.question, Pattern.CASE_INSENSITIVE));
			
			Faq faqduplication = genericDAO.findOneByAttribute(
				viewModel.entityObject, map, Faq.class);
			
			if (faqdata != null) 
			{
				if(null != faqduplication
						&& !(faqdata.getCode()
								.equalsIgnoreCase(faqduplication
										.getCode()))) {
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.updateFaqdata.exists");
				}
				else{
					// override the old properties with new properties exists in the
					// right side object
					notNull.copyProperties(faqdata, faq);
					// Update the existing record wi8th new values.
					genericDAO.updateById(viewModel.entityObject, faqdata,
							faqdata.getId());
					Faq updatedFaq = genericDAO.findOneById(
							viewModel.entityObject, faqdata.getId(),
							Faq.class);
					response.success = true;
					response.ViewModel = modelmapper.map(updatedFaq,FaqViewModel.class);
					response.message = CommonUtils
							.getMessage("success.update.Faq");	
				}
				
			} else {
				// To check whether the entity code exists if not returns the
				// response as false
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.faqcode.not.exists");

			}
		} catch (Exception e) {
			// Exception Handling
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
		
	}
	@Override
	public Response findAllByAttributes(FaqViewModel viewModel)	throws Exception {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {
		// Converting MasterViewModel object to MasterEntity Pojo
			Faq data = modelmapper
				.map(viewModel, Faq.class);
		Gson gson = new Gson();
		// Converting MasterEntity to a JSON string
		String faq = gson.toJson(data);
		// Converting JON string to a HashMap
		Map<String, Object> map = new Gson().fromJson(faq,
				new TypeToken<HashMap<String, Object>>() {
				}.getType());
		
		if (viewModel.where != null) {
			// for appending the where and the like for search criteria
			viewModel.where = helperUtil
					.ConstructWhereClauseFromJson(viewModel.where);
		}
		// Fetch the Entity based on the search criteria
		List<Faq> allFaq = genericDAO.findAllByAttributes(
				viewModel.entityObject, map, Faq.class,
				viewModel.limit, viewModel.skip);

		
			// Returns true if there is a record matching the criteria
			if (null != allFaq && allFaq.size() > 0) {
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.faq.Criteria.search");
				response.ViewModels = allFaq;
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.faq.Criteria.search");
			}

		} catch (Exception e) {
			// Handling Exception
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			// response.status = false;
			response.ExceptionMessage = e.getMessage();
		}
		return response;
		
	}
	@Override
	public Response delete(FaqViewModel viewModel) throws Exception {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {

			Map<String, Object> map = new HashMap<String, Object>();
			// Fetch the first record with given conditions satisfied.
			map.put("code", viewModel.code);

			Faq faqfind = genericDAO.findOneByAttribute(
					viewModel.entityObject, map, Faq.class);

			// if record found then delete record and returns true

			if (faqfind != null) {
				// Deleting the Entity with the ID
				genericDAO.delete(viewModel.entityObject, faqfind.getId(),
						Faq.class);
				FaqViewModel faqdata = modelmapper.map(faqfind,
						FaqViewModel.class);
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.faq.deletion");
				response.ViewModel = faqdata;
				// response.status = true;

			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.faqcode.not.exists");
				// response.status = false;
			}

		} catch (Exception e) {

			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}

	
	
	

}
