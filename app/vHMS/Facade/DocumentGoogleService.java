package vHMS.Facade;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vHMS.Core.Models.AuditEventStore;
import vHMS.Core.Models.Document;
import vHMS.Core.Models.PatientRegistration;
import vHMS.Core.ViewModels.AuditEventStoreViewModel;
import vHMS.Core.ViewModels.DocumentDataViewModel;
import vHMS.Core.ViewModels.DocumentViewModel;
import vHMS.Core.ViewModels.Response;
import Base.Data.IGenericMongoDao;
import Base.Storage.IDocumentDao;
import Base.Utilities.CommonUtils;
import Base.Utilities.HelperUtil;

/**
 * @author Arokia Felsi For document upload
 * 
 */
@Service
@Transactional
public class DocumentGoogleService implements IDocumentService {

	// Created GenericDAO instance using DI
	@Autowired
	private IDocumentDao documentDAO;

	// Created GenericDAO instance using DI
	@Autowired
	private IGenericMongoDao genericDAO;

	// Created AuditEventStore instance.
	private AuditEventStoreViewModel auditTrailViewModel = new AuditEventStoreViewModel();

	// Created HelperUtil instance.
	private HelperUtil helperUtil = new HelperUtil();

	// Created ModelMapper instance.
	ModelMapper modelMapper = new ModelMapper();

	// Created Request Data instance.
	DocumentDataViewModel responseViewModel = new DocumentDataViewModel();

	/*
	 * Implementation of interface IDocumentService load method which is invoked
	 * for uplaoding a document.
	 * 
	 * @see vHMS.Facade.IDocumentService#load()
	 */
	public Response load(DocumentViewModel viewModel) throws Exception {
		Response response = new Response();

		PatientRegistration patientData = null;
		String bucket = null;
		try {

			// storing the file details for the google storage in a map
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("type", viewModel.contentType);
			map.put("fileName", viewModel.fileName);
			map.put("file", viewModel.LoadedFile);

			if (null != viewModel.requestType) {

				String val = viewModel.requestType;
				// converting the request type to lower case and remove
				// whitespace
				val = val.replaceAll("[^A-Za-z]+", "").toLowerCase();
				// getting the bucket name if exists or null
				bucket = documentDAO.getBucket(val);

				if (bucket == null) {
					// if bucket name not exists create a new bucket
					bucket = documentDAO.tryCreateBucket(val);
				}

				// Set projection for the bucket
				documentDAO.setBucketProjection(bucket);
				viewModel.bucket = bucket;
				// Returns document object with the values with the key and
				// storage
				// inputs
				viewModel = documentDAO.load(viewModel);

				// Calling the helper class to set the mandatory properties of
				// the
				// view model
				viewModel = helperUtil.AttachCommonFields(viewModel,
						DocumentViewModel.class);

				ModelMapper modelMapper = new ModelMapper();
				// Mapping the document and model to create a document table
				Document document = modelMapper.map(viewModel, Document.class);
				// Create a new document entry record for each document upload.
				Object[] result = genericDAO.save(
						CommonUtils.getMessage("Document"), document);
				for (Object tp : result) {
					String id = String.valueOf(tp);
					document.setId(id.toString());
				}
				if (null != document.getId()) {
					map.clear();
					map.put("code", document.getCode());
					patientData = genericDAO.findOneByAttribute(
							CommonUtils.getMessage("PatientRegistration"), map,
							PatientRegistration.class);
					responseViewModel.documentViewModel = modelMapper.map(
							document, DocumentViewModel.class);
					// Returns the response true if document contains values
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.document.upload");
					response.ViewModel = document;
				} else {
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.document.upload");
				}
			}

			/*
			 * if (response.success) { // Set the auidtrail details for
			 * audittrail aop to be // activated AuditEventStore auditEvent =
			 * modelMapper.map( auditTrailViewModel, AuditEventStore.class); //
			 * If entity id is null set it from the request details. if (null ==
			 * auditEvent.getEntityId())
			 * auditEvent.setEntityId(patientData.getId()); //
			 * auditEvent.setJSON(patientData.getJSON());
			 * auditEvent.setAuditEventDescription(CommonUtils
			 * .getMessage("upload"));
			 * auditEvent.setEntityDescription(CommonUtils
			 * .getMessage("document.upload")); helperUtil
			 * .AttachCommonFields(auditEvent, AuditEventStore.class);
			 * responseViewModel.auditTrailViewModel = modelMapper.map(
			 * auditEvent, AuditEventStoreViewModel.class);
			 * 
			 * }
			 */
		} catch (Exception e) {
			// Handling exception
			response.success = false;
			response.message = CommonUtils
					.getMessage("error.document.upload.failure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		if (response.success && null != responseViewModel)
			response.ViewModel = responseViewModel;
		return response;
	}

	/*
	 * Implementation of interface IDocumentService download method which is
	 * invoked for downloading a document.
	 * 
	 * @see vHMS.Facade.IDocumentService#download()
	 */

	public Response download(DocumentViewModel viewModel) throws Exception {
		Response response = new Response();

		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("code", viewModel.code);
			map.put("requestType", viewModel.requestType);
			// check whether the document exists for the code
			Document document = genericDAO.findOneByAttribute(
					CommonUtils.getMessage("Document"), map, Document.class);
			// If exists download the document from google storage
			if (null != document) {
				// Checking whether the request type is null
				if (null != viewModel.requestType) {

					String val = viewModel.requestType;
					// converting the request type to lower case and remove
					// whitespace
					val = val.replaceAll("[^A-Za-z]+", "").toLowerCase();
					// getting the bucket name
					viewModel.bucket = documentDAO.getBucket(val);

					if (null != viewModel.bucket) {

						DocumentViewModel view = documentDAO
								.retrieve(viewModel);
						// if the key does not check null and make the viewModel
						// null
						if (null == viewModel.contentType
								&& null == viewModel.fileName
								&& null == viewModel.LoadedFile)
							viewModel = null;

						if (view != null) {
							response.success = true;
							response.message = CommonUtils
									.getMessage("success.document.download");
							// if the record exists bind to the view model
							response.ViewModel = view;
						} else {
							response.success = false;
							response.message = CommonUtils
									.getMessage("error.download.entry.document");
						}

					} else {
						response.success = false;
						response.message = CommonUtils
								.getMessage("error.document.download");
					}
				}
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.document.download");
			}
		} catch (Exception e) {
			// return false if there is any database connection problem
			response.success = false;
			response.message = CommonUtils
					.getMessage("error.document.download.failure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

	/*
	 * Implementation of interface IDocumentService download method which is
	 * invoked for downloading a document.
	 * 
	 * @see vHMS.Facade.IDocumentService#download()
	 */

	public Response delete(DocumentViewModel viewModel) throws Exception {
		Response response = new Response();
		PatientRegistration patientData = null;

		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("code", viewModel.code);
			map.put("requestType", viewModel.requestType.toLowerCase());
			// check whether the document exists for the code
			Document document = genericDAO.findOneByAttribute(
					CommonUtils.getMessage("Document"), map, Document.class);
			// If exists, delete the document from google storage
			if (null != document) {

				if (null != viewModel.requestType) {

					String val = viewModel.requestType;
					// converting the request type to lower case and remove
					// whitespace
					val = val.replaceAll("[^A-Za-z]+", "").toLowerCase();
					// getting the bucket name
					viewModel.bucket = documentDAO.getBucket(val);

					if (null != viewModel.bucket) {

						// For getting the requestId for audit info
						viewModel.id = document.getId();
						map.clear();
						map.put("Code", document.getRequestCode());
						patientData = genericDAO.findOneByAttribute(
								CommonUtils.getMessage("Request"), map,
								PatientRegistration.class);

						DocumentViewModel view = documentDAO.delete(viewModel);
						if (view.status == "1") {
							genericDAO.delete(
									CommonUtils.getMessage("Document"),
									viewModel.id, Document.class);

							response.success = true;
							response.message = CommonUtils
									.getMessage("success.document.delete");
							responseViewModel.documentViewModel = view;
						} else {
							response.success = false;
							response.message = CommonUtils
									.getMessage("error.delete.entry.document");
						}

						// if the record exists bind to the view model

					} else {
						response.success = false;
						response.message = CommonUtils
								.getMessage("error.document.delete");
					}
				} else {
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.document.delete.norequesttype");
				}
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.document.delete.nodocumentdata");
			}
			if (response.success) {
				// Set the auidtrail details for audittrail aop to be
				// activated
				AuditEventStore auditEvent = modelMapper.map(
						auditTrailViewModel, AuditEventStore.class);
				// If entity id is null set it from the request details.
				if (null == auditEvent.getEntityId())
					auditEvent.setEntityId(patientData.getId());
				// auditEvent.setJSON(requestData.getJSON());
				auditEvent.setAuditEventDescription(CommonUtils
						.getMessage("delete"));
				auditEvent.setEntityDescription(CommonUtils
						.getMessage("document.delete"));
				helperUtil
						.AttachCommonFields(auditEvent, AuditEventStore.class);
				responseViewModel.auditTrailViewModel = modelMapper.map(
						auditEvent, AuditEventStoreViewModel.class);
			}

		} catch (Exception e) {
			// return false if there is any database connection problem
			response.success = false;
			response.message = CommonUtils
					.getMessage("error.document.delete.failure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		if (response.success && null != responseViewModel)
			response.ViewModel = responseViewModel;
		return response;
	}

	/*
	 * Implementation of interface IDocumentService list method which is invoked
	 * for getting the document list for a request.
	 * 
	 * @see vHMS.Facade.IDocumentService#list()
	 */

	public Response list(DocumentViewModel viewModel) throws Exception {
		Response response = new Response();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("RequestCode", viewModel.requestCode);
			// check whether the document exists for the code
			List<Document> documentlist = genericDAO.findAllByAttributes(
					CommonUtils.getMessage("Document"), map, Document.class,
					null, null);
			// If exists, delete the document from google storage
			if (null != documentlist && documentlist.size() > 0) {
				Type targetListType = new TypeToken<List<DocumentViewModel>>() {
				}.getType();
				// Mapping the list with the target view model
				List<DocumentViewModel> list = modelMapper.map(documentlist,
						targetListType);
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.document.fetchall");
				response.ViewModels = list;

			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.document.fetchall");
			}
		} catch (Exception e) {
			// return false if there is any database connection problem
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
		}

		return response;
	}

	@Override
	public Response findAllByAttributes(DocumentViewModel viewModel)
			throws Exception {
		// TODO Auto-generated method stub
		Response response = new Response();
		return response;
	}
}