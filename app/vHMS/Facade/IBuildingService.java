package vHMS.Facade;

import java.io.IOException;

import vHMS.Core.ViewModels.BuildingViewModel;
import vHMS.Core.ViewModels.Response;

import com.fasterxml.jackson.core.JsonProcessingException;




public interface IBuildingService {

	/*
	 * Implementation of interface IBuildingService load method which is invoked for
	 * fetching the building list.
	 * 
	 * @see vHMS.Facade.IBuildingService#all()
	 */
	public Response all(BuildingViewModel viewModel)
			throws JsonProcessingException, IOException;

	/*
	 * Implementation of interface IBuildingService create method which is invoked
	 * for creating the building.
	 * 
	 * @see vHMS.Facade.IBuildingService#create()
	 */
	public Response create(BuildingViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IBuildingService findById method which is invoked
	 * for finding/getting a building by id.
	 * 
	 * @see vHMS.Facade.IBuildingService#findById()
	 */
	public Response findById(BuildingViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IBuildingService count method which is invoked
	 * for getting the count of matching records.
	 * 
	 * @see vHMS.Facade.IBuildingService#count()
	 */
	public Response count(BuildingViewModel viewModel);

	/*
	 * Implementation of interface IBuildingService delete method which is invoked
	 * for deleting a building.
	 * 
	 * @see vHMS.Facade.IBuildingService#delete()
	 */
	public Response delete(BuildingViewModel viewModel);

	/*
	 * Implementation of interface IBuildingService update method which is invoked
	 * for updating a floor data.
	 * 
	 * @see vHMS.Facade.IBuildingService#update()
	 */
	public Response update(BuildingViewModel viewModel);

	/*
	 * Implementation of interface IBuildingService findAllByAttributes method which
	 * is invoked for finding the records/building which satisfies the given
	 * criteria.
	 * 
	 * @see vHMS.Facade.IBuildingService#findAllByAttributes()
	 */
	public Response findAllByAttributes(BuildingViewModel viewModel)
			throws Exception;


}
