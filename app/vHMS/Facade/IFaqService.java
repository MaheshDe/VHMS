package vHMS.Facade;

import java.io.IOException;

import vHMS.Core.ViewModels.FaqViewModel;
import vHMS.Core.ViewModels.Response;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * <h1>IAppointmentService</h1> The IAppointmentService deals with the
 * Appointment registration/updation/deletion of a patient Appointment. It deals
 * with the entire operations related to a Appointment.
 * 
 * @author ShunmugaRajaG
 * @version 1.0
 */
public interface IFaqService {
	/*
	 * Implementation of interface IAppointmentService all method which returns
	 * a list of allAppointment.
	 */
	public Response all(FaqViewModel viewModel)	throws JsonProcessingException, IOException, Exception;

	/*
	 * Implementation of interface IAppointmentService create method which is
	 * used to create/save a Appointment.
	 */
	public Response create(FaqViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IAppointmentService update method which is
	 * used to update the Appointment details with the given id.
	 */
	public Response update(FaqViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IAppointmentService findAllByAttributes
	 * method which is used to find the Appointment details with the given
	 * attribute.
	 */
	public Response findAllByAttributes(FaqViewModel viewModel) throws Exception;
	
	public Response delete(FaqViewModel viewModel) throws Exception;
	

	



}
