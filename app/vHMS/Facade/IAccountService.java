package vHMS.Facade;

/**
 * The IAccountService is an interface containing methods like AuthenticateUser
 * related to user management, and can be implemented by
 * other classes.
 * 
 * @author DivyaP
 * @version 1.0
 * @since 2015-07-17
 */

import vHMS.Core.ViewModels.Response;
import vHMS.Core.ViewModels.UserViewModel;

/*
 * Implementation of interface IAccountService AuthenticateUser method which is used
 * to authenticate a user.
 * 
 * @see
 * vHMS.Facade.IPatientManagementService#AuthenticateUser(vHMS.Core.ViewModels.PatientRegistrationViewModel)
 */
public interface IAccountService {
	public Response AuthenticateUser(UserViewModel viewModel) throws Exception;

}
