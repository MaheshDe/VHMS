/**
 * 
 */
package vHMS.Facade;

import vHMS.Core.ViewModels.PatientRegistrationViewModel;
import vHMS.Core.ViewModels.Response;

/**
 * The IPatientManagementService is an interface containing methods like create,
 * update delete etc.. related to patient management, and can be implemented by
 * other classes.
 * 
 * @author DivyaP
 * @version 1.0
 * @since 2015-07-17
 */

public interface IPatientRegistrationService {

	/*
	 * Implementation of interface IPatientManagementService create method which
	 * is used to create/save a PatientManagement.
	 * 
	 * @see vHMS.Facade.IPatientManagementService#create(vHMS.Core.ViewModels.
	 * PatientRegistrationViewModel)
	 */
	public Response create(PatientRegistrationViewModel viewModel)
			throws Exception;

	/*
	 * Implementation of interface IPatientManagementService all method which
	 * returns a list of allPatients.
	 * 
	 * @see vHMS.Facade.IPatientManagementService#all(vHMS.Core.ViewModels.
	 * PatientRegistrationViewModel)
	 */
	public Response all(PatientRegistrationViewModel viewModel);

	/*
	 * Implementation of interface IPatientManagementService findById method
	 * which is used to find a Patient using id(primary key).
	 * 
	 * @see
	 * vHMS.Facade.IPatientManagementService#findByCode(vHMS.Core.ViewModels
	 * .PatientRegistrationViewModel) )
	 */
	public Response findByCode(PatientRegistrationViewModel viewModel)
			throws Exception;

	/*
	 * Implementation of interface IPatientManagementService update method which
	 * is used to update the Patient details with the given id.
	 * 
	 * @see vHMS.Facade.IPatientManagementService#update(vHMS.Core.ViewModels.
	 * PatientRegistrationViewModel)
	 */
	public Response update(PatientRegistrationViewModel viewModel)
			throws Exception;

	/*
	 * Implementation of interface IPatientManagementService findAllByAttributes
	 * method which is used to find the Patient details with the given id.
	 * 
	 * @see vHMS.Facade.IPatientManagementService#findAllByAttributes(vHMS.Core.
	 * ViewModels.PatientRegistrationViewModel)
	 */

	public Response findAllByAttributes(PatientRegistrationViewModel viewModel)
			throws Exception;

	/*
	 * Implementation of interface IPatientManagementService Authenticate method
	 * which is used to authenticate the patient.
	 * 
	 * @see vHMS.Facade.IPatientManagementService#Authenticate(vHMS.Core.
	 * ViewModels.PatientRegistrationViewModel)
	 */
	public Response Authenticate(PatientRegistrationViewModel viewModel)
			throws Exception;

	/*
	 * Implementation of interface IPatientManagementService changePassword
	 * method which is initiated when user clicks on changepassword.
	 * 
	 * @see vHMS.Facade.IPatientManagementService#changePassword(vHMS.Core.
	 * ViewModels.PatientRegistrationViewModel)
	 */
	public Response changePassword(PatientRegistrationViewModel viewModel)
			throws Exception;

	/*
	 * Implementation of interface IPatientManagementService forgotPassword
	 * method which is used if a patients initiates forgotpassword requests.
	 * 
	 * @see vHMS.Facade.IPatientManagementService#forgotPassword(vHMS.Core.
	 * ViewModels.PatientRegistrationViewModel)
	 */
	public Response forgotPassword(PatientRegistrationViewModel viewModel)
			throws Exception;

}
