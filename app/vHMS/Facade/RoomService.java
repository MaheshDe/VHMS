package vHMS.Facade;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vHMS.Core.Models.Room;
import vHMS.Core.ViewModels.Response;
import vHMS.Core.ViewModels.RoomViewModel;
import Base.Data.IGenericMongoDao;
import Base.Utilities.CommonUtils;
import Base.Utilities.HelperUtil;
import Base.Utilities.NullAwareBeanUtilsBean;
import Base.Utilities.Email.SmtpSender;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

/**
 * The RoomService is the implementation for the interface IRoomService.
 * 
 * @author MaheshDe
 * @since 2015-08-19
 */

@Service
@Transactional
public class RoomService implements IRoomService {

	@Autowired
	private IGenericMongoDao genericDAO;

	@Autowired
	private SmtpSender defaultEmailSender;
	// Created HelperUtil instance.
	private HelperUtil helperUtil = new HelperUtil();
	// Created ModelMapper instance
	private ModelMapper modelmapper = new ModelMapper();
	// Created NullAwareBeanUtilsBean instance.
	private BeanUtilsBean notNull = new NullAwareBeanUtilsBean();
	
	/**
	 * Implementation of interface IRoomService all method which returns a
	 * list of room data.
	 * 
	 *  @see vHMS.Facade.IRoomService#all(vHMS.Core.Models.
	 * RoomViewModel
	 */
	
	@Override
	public Response all(RoomViewModel viewModel)
			throws JsonProcessingException, IOException {
		// TODO Auto-generated method stub
		
		Response response = new Response();
		// Fetch all the records from the table.
		List<Room> allRoom = null;

		if (viewModel.where != null) {
			// for appending the where and the like for search criteria
			viewModel.where = helperUtil
					.ConstructWhereClauseFromJson(viewModel.where);
		}
		allRoom = genericDAO.all(CommonUtils.getMessage("Room"), Room.class,
				viewModel.where, viewModel.limit, viewModel.skip);
		try {
			// to check whether there is a list of entity returns true response
			if (null != allRoom && allRoom.size() > 0) {
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.room.Criteria.search");
				response.ViewModels = allRoom;
			} else {// Returns false response
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.room.Criteria.search");
			}

		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
		
	}
	
	/**
	 * Implementation of interface IRoomService create method which
	 * saves/create a room data.
	 * 
	 *@see vHMS.Facade.IRoomService#create(vHMS.Core.Models.
	 * RoomViewModel
	 */
	
	@Override
	public Response create(RoomViewModel viewModel) throws Exception {
		// TODO Auto-generated method stub
		
		Response response = new Response();
		Room roomExist = null;
		String id = null;
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			if(viewModel != null)
			{
				map.put("name", viewModel.name);
				map.put("floor", viewModel.floor);
				
				//checking whether room is exist or not
				roomExist = genericDAO.findOneByAttribute(
						CommonUtils.getMessage("Room"), map,
						Room.class);
				
				if(roomExist != null)
				{
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.roomdata.exists");
				}
				else
				{
					// Calling the helper class to set the mandatory properties of
					// the view model
					viewModel = helperUtil.AttachCommonFields(viewModel,
							RoomViewModel.class);
					
					
					//  convert viewmodel object to model object before saving it
					// to DB
					Room room = modelmapper.map(viewModel, Room.class);
				
					// Create the new record in table.
					Object[] obj =genericDAO.save(CommonUtils.getMessage("Room"), room);
					
					for (Object objId : obj) 
					{
						id = String.valueOf(objId);
						room.setId(id);
					}
					
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.room.creation");
					response.ViewModel = modelmapper.map(room,
							RoomViewModel.class); ;
				}
			}
			
		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();

		}
		return response;
	}
	
	
	/**
	 * Implementation of interface IRoomService findById method which search a
	 * room data with the given code.
	 * 
	 * @see vHMS.Facade.IRoomService#findById(vHMS.Core.Models.
	 * RoomViewModel
	 */
	@Override
	public Response findById(RoomViewModel viewModel) throws Exception {
		// TODO Auto-generated method stub
		
		Response response = new Response();

		try {
				Map<String, Object> map = new HashMap<String, Object>();
				Room room = null;
				
				if(viewModel != null)
				{
					map.put("code", viewModel.code);
					
					// Fetch the record by code.
					room = genericDAO.findOneByAttribute(
							CommonUtils.getMessage("Room"), map, Room.class);
					
					if(room != null)
					{
						
						RoomViewModel roomViewModel = modelmapper.map(room,
								RoomViewModel.class);
						
						response.success = true;
						response.message = CommonUtils
								.getMessage("success.room.Criteria.search");
						response.ViewModel = roomViewModel;
					}
					else 
					{
						response.success = false;
						response.message = CommonUtils
							.getMessage("error.room.Criteria.search");
					}
				}

			} catch (Exception e) {
				response.success = false;
				response.message = CommonUtils.getMessage("error.processfailure");
				response.ExceptionMessage = e.getMessage();
			}
		return response;
	}
	
	/**
	 * Implementation of interface IRoomService count method which returns
	 * number of room data present in a particular table.
	 * 
	 *  @see vHMS.Facade.IRoomService#count(vHMS.Core.Models.
	 * RoomViewModel
	 */
	
	@Override
	public Response count(RoomViewModel viewModel) {
		// TODO Auto-generated method stub
		
		Response response = new Response();
		try {
			// Condition to check whether where attribute is empty to check for
			// search criteria
			if (viewModel.where != null) {
				// for appending the where and the like for search criteria
				viewModel.where = helperUtil
						.ConstructWhereClauseFromJson(viewModel.where);
			}
			// Count of the record in the list or serach by criteria
			int count = genericDAO.count(CommonUtils.getMessage("Room"), Room.class);
			// adding count to the json object
			response.data.put("count", count);
			response.success = true;
			response.message = CommonUtils
					.getMessage("success.countMasterdata");
		} catch (Exception e) {
			// Return false if it ia an error
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}
	
	
	/**
	 * Implementation of interface IRoomService delete method which deletes a
	 * room data for the given code.
	 * 
	 *  @see vHMS.Facade.IRoomService#delete(vHMS.Core.Models.RoomViewModel
	 */
	
	@Override
	public Response delete(RoomViewModel viewModel) {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {

			Map<String, Object> map = new HashMap<String, Object>();
			if(viewModel != null)
			{
				map.put("code", viewModel.code);
				
				// Fetch the record by code.
				Room room = genericDAO.findOneByAttribute(
						CommonUtils.getMessage("Room"), map, Room.class);

				// if record found then delete record and returns true
				if (room != null) {
					// Deleting the user with the code
					genericDAO.delete(CommonUtils.getMessage("Room"), room.getId(),
							Room.class);
				
					RoomViewModel roomData = modelmapper.map(room,
							RoomViewModel.class);
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.room.data.deletion");
					response.ViewModel = roomData;
					// response.status = true;

				} else {
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.room.exists");
				}
			}
		} catch (Exception e) {

			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}
	
	/**
	 * Implementation of interface IRoomService update method which updates a
	 * room data for the given code.
	 * 
	 *  @see vHMS.Facade.IRoomService#update(vHMS.Core.Models.
	 * RoomViewModel
	 */
	
	@Override
	public Response update(RoomViewModel viewModel) {
		// TODO Auto-generated method stub
		
		Response response = new Response();
		try {
			
			// Mapping the viewmodel to class of type
			Room room = modelmapper.map(viewModel,
						Room.class);
			// getting the id from the view model for updating for the specific
			// id
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("code", viewModel.code);
			
			// Check by code whether the record exists before update.
			Room roomExist = genericDAO.findOneByAttribute(CommonUtils.getMessage("Room"), map, Room.class);
			
			/*map.clear();
			map.put("floor",viewModel.floor);
			
			//checking whether floor is exist or not.
			Room roomData = genericDAO.findOneByAttribute(CommonUtils.getMessage("Room"), map, Room.class);
			*/
			if(roomExist != null )
			{
					// override the old properties with new properties exists in the
					// right side object
					notNull.copyProperties(roomExist, room);
					
					// Calling the helper class to set the mandatory properties of
					// the view model
					viewModel = helperUtil.AttachCommonFields(viewModel,
							RoomViewModel.class);
					
					// Update the existing record with new values.
					genericDAO.updateById(CommonUtils.getMessage("Room"), roomExist,
								roomExist.getId());
					
					Room updatedRoom = genericDAO.findOneById(
							CommonUtils.getMessage("Room"), roomExist.getId(),
							Room.class);
					/*RoomViewModel RoomViewModel = modelmapper.map(
							roomExist, RoomViewModel.class);
					*/
					response.success = true;
					response.ViewModel = modelmapper.map(updatedRoom,
							RoomViewModel.class);
					response.message = CommonUtils
							.getMessage("success.update.room");	
				
			}
			else
			{
				response.success = false;
				response.message = CommonUtils
					.getMessage("error.room.exists");
			} 
			
		} catch (Exception e) {
			// Exception Handling
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
		
	}
	
	
	/**
	 * Implementation of interface IRoomService findAllByAttributes method
	 * which returs a list of room data based on search criteria.
	 * 
	 * @see vHMS.Facade.IRoomService#findAllByAttributes(vHMS.Core.Models.
	 * RoomViewModel
	 */
	
	@Override
	public Response findAllByAttributes(RoomViewModel viewModel)
			throws Exception {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {
			
			// Converting UserScheduleViewModel object to UserScheduleEntity Pojo
			Room room = modelmapper.map(viewModel, Room.class);
			Gson gson = new Gson();
			
			// Converting UserScheduleEntity to a JSON string
			String master = gson.toJson(room);
			
			// Converting JSON string to a HashMap
			Map<String, Object> map = new Gson().fromJson(master,
					new TypeToken<HashMap<String, Object>>() {
					}.getType());

			if (viewModel.where != null) {
				// for appending the where and the like for search criteria
				viewModel.where = helperUtil
						.ConstructWhereClauseFromJson(viewModel.where);
			}
			// Fetch the Entity based on the search criteria
			List<Room> allroom = genericDAO.findAllByAttributes(
					CommonUtils.getMessage("Room"), map, Room.class, viewModel.limit,
					viewModel.skip);

			// Returns true if there is a record matching the criteria
			if (null != allroom && allroom.size() > 0) {
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.room.Criteria.search");
				response.ViewModels = allroom;
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.room.Criteria.search");
			}

		} catch (Exception e) {
			// Handling Exception
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			// response.status = false;
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}
	

}
