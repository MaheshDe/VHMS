package vHMS.Facade;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vHMS.Core.Models.MasterEntity;
import vHMS.Core.ViewModels.MasterViewModel;
import vHMS.Core.ViewModels.Response;
import Base.Data.IGenericMongoDao;
import Base.Utilities.CommonUtils;
import Base.Utilities.HelperUtil;
import Base.Utilities.NullAwareBeanUtilsBean;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

/**
 * The MasterService is a service implementing the interface IMasterService
 * containing methods like create, update delete etc.. related to master
 * management.
 * 
 * @author DivyaP
 * @version 1.0
 * @since 2015-08-10
 */
@Service
@Transactional
public class MasterService implements IMasterService {

	@Autowired
	private IGenericMongoDao genericDAO;

	ObjectMapper mapper = new ObjectMapper();
	// Created HelperUtil instance.
	private HelperUtil helperUtil = new HelperUtil();
	// Created ModelMapper instance
	private ModelMapper modelmapper = new ModelMapper();
	// Created NullAwareBeanUtilsBean instance.
	private BeanUtilsBean notNull = new NullAwareBeanUtilsBean();

	/*
	 * Implementation of interface IMasterService all method which returns a
	 * list of Master data.
	 */
	@Override
	public Response all(MasterViewModel viewModel) throws Exception {

		Response response = new Response();
		// Fetch all the records from the table.

		List<MasterEntity> allMasterEntity = null;

		if (viewModel.where != null) {
			// for appending the where and the like for search criteria
			viewModel.where = helperUtil
					.ConstructWhereClauseFromJson(viewModel.where);
		}
		allMasterEntity = genericDAO.all(viewModel.entityObject,
				MasterEntity.class, viewModel.where, viewModel.limit,
				viewModel.skip);
		try {
			// to check whether there is a list of entity returns true response
			if (null != allMasterEntity && allMasterEntity.size() > 0) {
				Type targetListType = new TypeToken<List<MasterViewModel>>() {
				}.getType();
				// Mapping the list with the target view model
				List<MasterViewModel> lists = modelmapper.map(allMasterEntity,
						targetListType);
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.entity.Criteria.search");
				response.ViewModels = allMasterEntity;
			} else {// Returns false response
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.entity.Criteria.search");
			}

		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}

		return response;
	}

	/*
	 * Implementation of interface IMasterService create method which
	 * saves/create a Master data
	 */
	@Override
	public Response create(MasterViewModel viewModel) throws Exception { 
		Response response = new Response();
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			// Calling the helper class to set the mandatory properties of
			// the
			// view model

			viewModel = helperUtil.AttachCommonFields(viewModel,
					MasterViewModel.class);
			map.put("name",
					Pattern.compile(viewModel.name, Pattern.CASE_INSENSITIVE));
			MasterEntity masterdataexist = genericDAO.findOneByAttribute(
					viewModel.entityObject, map, MasterEntity.class);
			if (null != masterdataexist) {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.createMasterdata.exists");

			} else {
				// getting the source and target to set the properties
				MasterEntity masterentity = modelmapper.map(viewModel,
						MasterEntity.class);
				// Create the new record in table.
				genericDAO.save(viewModel.entityObject, masterentity);
				map.clear();
				map.put("code", viewModel.code);

				MasterEntity masterSave = genericDAO.findOneByAttribute(
						viewModel.entityObject, map, MasterEntity.class);

				response.success = true;
				response.message = CommonUtils
						.getMessage("success.entity.creation");
				response.ViewModel = masterSave;

			}

		} catch (Exception e) {// Handles Exception
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();

		}
		return response;

	}

	/*
	 * Implementation of interface IMasterService findById method which search a
	 * Master data with the given code
	 */
	@Override
	public Response findById(MasterViewModel viewModel) throws Exception {
		Response response = new Response();

		try {

			Map<String, Object> map = new HashMap<String, Object>();
			// Fetch the first record with given conditions satisfied.
			map.put("code", viewModel.code);

			MasterEntity masterfind = genericDAO.findOneByAttribute(
					viewModel.entityObject, map, MasterEntity.class);
			if (null != masterfind) {
				MasterViewModel viewmodel = modelmapper.map(masterfind,
						MasterViewModel.class);

				response.success = true;
				response.message = CommonUtils
						.getMessage("success.entity.Criteria.search");
				response.ViewModel = viewmodel;

			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.entity.Criteria.search");
			}

		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}

	/*
	 * Implementation of interface IMasterService count method which returns
	 * number of Master data present in a particular table
	 */
	@Override
	public Response count(MasterViewModel viewModel) throws Exception {
		Response response = new Response();
		try {
			// Condition to check whether where attribute is empty to check for
			// search criteria
			if (viewModel.where != null) {
				// for appending the where and the like for search criteria
				viewModel.where = helperUtil
						.ConstructWhereClauseFromJson(viewModel.where);
			}
			// Count of the record in the list or serach by criteria
			int count = genericDAO.count(viewModel.entityObject,
					MasterEntity.class);
			// adding count to the json object
			response.data.put("count", count);
			response.success = true;
			response.message = CommonUtils
					.getMessage("success.countMasterdata");
		} catch (Exception e) {
			// Return false if it ia an error
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;

	}

	/*
	 * Implementation of interface IMasterService delete method which deletes a
	 * master data for the given code
	 */
	@Override
	public Response delete(MasterViewModel viewModel) throws Exception {

		Response response = new Response();
		try {

			Map<String, Object> map = new HashMap<String, Object>();
			// Fetch the first record with given conditions satisfied.
			map.put("code", viewModel.code);

			MasterEntity masterfind = genericDAO.findOneByAttribute(
					viewModel.entityObject, map, MasterEntity.class);

			// if record found then delete record and returns true

			if (masterfind != null) {
				// Deleting the Entity with the ID
				genericDAO.delete(viewModel.entityObject, masterfind.getId(),
						MasterEntity.class);
				MasterViewModel masterdata = modelmapper.map(masterfind,
						MasterViewModel.class);
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.entity.deletion");
				response.ViewModel = masterdata;
				// response.status = true;

			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.entitycode.not.exists");
				// response.status = false;
			}

		} catch (Exception e) {

			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;

	}

	/*
	 * Implementation of interface IMasterService update method which updates a
	 * master data for the given code
	 */
	@Override
	public Response update(MasterViewModel viewModel) throws Exception {

		Response response = new Response();
		try {
			// Calling the helper class to set the mandatory properties of the
			// view model
			viewModel = helperUtil.AttachCommonFields(viewModel,
					MasterViewModel.class);
			MasterEntity masterentity = modelmapper.map(viewModel,
					MasterEntity.class);
			Map<String, Object> map = new HashMap<String, Object>();
			// Fetch the first record with given conditions satisfied.
			map.put("code", viewModel.code);

			// Check whether the record exists before update.
			MasterEntity masterentitydata = genericDAO.findOneByAttribute(
					viewModel.entityObject, map, MasterEntity.class);

			map.clear();
			map.put("name",
					Pattern.compile(viewModel.name, Pattern.CASE_INSENSITIVE));
			MasterEntity masterdataduplication = genericDAO.findOneByAttribute(
					viewModel.entityObject, map, MasterEntity.class);
			if (masterentitydata != null) {
				if (null != masterdataduplication
						&& !(masterentitydata.getCode()
								.equalsIgnoreCase(masterdataduplication
										.getCode()))) {
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.updateMasterdata.exists");
				} else {
					// override the old properties with new properties exists in
					// the
					// right side object
					notNull.copyProperties(masterentitydata, masterentity);
					// Update the existing record wi8th new values.
					genericDAO.updateById(viewModel.entityObject, masterentity,
							masterentitydata.getId());
					MasterEntity updatedMaster = genericDAO.findOneById(
							viewModel.entityObject, masterentitydata.getId(),
							MasterEntity.class);
					response.success = true;
					response.ViewModel = modelmapper.map(updatedMaster,
							MasterViewModel.class);
					response.message = CommonUtils
							.getMessage("success.update.Entity");
				}

			} else {
				// To check whether the entity code exists if not returns the
				// response as false
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.entitycode.not.exists");

			}
		} catch (Exception e) {
			// Exception Handling
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;

	}

	/*
	 * Implementation of interface IMasterService findAllByAttributes method
	 * which returs a list of master data based on search criteria
	 */
	@Override
	public Response findAllByAttributes(MasterViewModel viewModel)
			throws Exception {
		Response response = new Response();
		try {
			// Converting MasterViewModel object to MasterEntity Pojo
			MasterEntity data = modelmapper.map(viewModel, MasterEntity.class);
			Gson gson = new Gson();
			// Converting MasterEntity to a JSON string
			String master = gson.toJson(data);
			// Converting JON string to a HashMap
			Map<String, Object> map = new Gson().fromJson(master,
					new TypeToken<HashMap<String, Object>>() {
					}.getType());

			if (viewModel.where != null) {
				// for appending the where and the like for search criteria
				viewModel.where = helperUtil
						.ConstructWhereClauseFromJson(viewModel.where);
			}
			// Fetch the Entity based on the search criteria
			List<MasterEntity> allMasterEntity = genericDAO
					.findAllByAttributes(viewModel.entityObject, map,
							MasterEntity.class, viewModel.limit, viewModel.skip);

			// Returns true if there is a record matching the criteria
			if (null != allMasterEntity && allMasterEntity.size() > 0) {
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.entity.Criteria.search");
				response.ViewModels = allMasterEntity;
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.entity.Criteria.search");
			}

		} catch (Exception e) {
			// Handling Exception
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			// response.status = false;
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}

}
