package vHMS.Facade;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vHMS.Core.Models.OPRegistration;
import vHMS.Core.ViewModels.OPRegistrationViewModel;
import vHMS.Core.ViewModels.Response;
import Base.Data.IGenericMongoDao;
import Base.Utilities.CommonUtils;
import Base.Utilities.HelperUtil;
import Base.Utilities.NullAwareBeanUtilsBean;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * The IOPRegistrationService is a service implementing the interface
 * IOPRegistrationService containing methods like create, update delete etc..
 * related to patient management.
 * 
 * @author DivyaP
 * @version 1.0
 * @since 2015-07-17
 */
@Service
@Transactional
public class OPRegistrationService implements IOPRegistrationService {

	// Created GenericDAO instance using DI
	@Autowired
	private IGenericMongoDao genericDAO;

	// Created HelperUtil instance.
	private HelperUtil helperUtil = new HelperUtil();
	// Created ModelMapper instance.
	private ModelMapper modelMapper = new ModelMapper();

	// Created NullAwareBeanUtilsBean instance.
	private BeanUtilsBean notNull = new NullAwareBeanUtilsBean();

	/**
	 * This method is used to create or register a patient, it will not allow to
	 * insert if the email id already exists in the table.
	 * 
	 * @see vHMS.Facade.IOPRegistrationService#create(vHMS.Core.Models.OPRegistrationViewModel
	 *      )
	 */

	/**
	 * This method is used to create or register a patient, it will not allow to
	 * insert if the email id already exists in the table.
	 * 
	 * @see vHMS.Facade.IOPRegistrationService#create(vHMS.Core.Models.OPRegistrationViewModel
	 *      )
	 */
	public Response create(OPRegistrationViewModel viewModel) {
		Response response = new Response();
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			// Calling the helper class to set the mandatory properties of the
			// view model
			viewModel = helperUtil.AttachCommonFields(viewModel,
					OPRegistrationViewModel.class);

			// convert viewmodel object to model object before saving it to DB
			OPRegistration opreg = modelMapper.map(viewModel,
					OPRegistration.class);

			// Before inserting check whether any entry is there in the DB with
			// the same emailid, to restrict duplication of data
			map.clear();
			map.put("code", viewModel.code);
			OPRegistration patientexist = genericDAO.findOneByAttribute(
					CommonUtils.getMessage("OPRegistration"), map,
					OPRegistration.class);
			// If entry already exist with the same mail id don't insert send
			// response back with success as false.
			if (null != patientexist) {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.createPatientRegister.exists");
			} else {
				// Save the data with the fields in model and get the id for the
				// saved item
				Object[] results = genericDAO.save(
						CommonUtils.getMessage("OPRegistration"), opreg);

				// Set the id back to the model.
				for (Object res : results) {
					opreg.setId(String.valueOf(res));
				}
				// After saving if the returned id is not null then send the
				// response back as success
				if (null != opreg.getId() && opreg.getId() != ""
						&& opreg.getId().trim().length() > 0) {

					// Before sending it back convert it again to viewmodel and
					// then send
					OPRegistrationViewModel patientDetails = modelMapper.map(
							opreg, OPRegistrationViewModel.class);
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.createPatientRegister");
					// Binding the created data to the view model in the
					// response
					response.ViewModel = patientDetails;
				} else {
					// If the saved id is null send the response back with
					// success as false.
					response.success = true;
					response.message = CommonUtils
							.getMessage("error.createPatientRegister");
				}

			}

		} catch (Exception e) {
			// return false if there is any database connection problem
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}

		return response;

	}

	/*
	 * Implementation of interface IOPRegistrationService all method which
	 * returns a list of all Patients.
	 * 
	 * @see
	 * 
	 * @see vHMS.Facade.IOPRegistrationService#all(vHMS.Core.Models.
	 * OPRegistrationViewModel
	 */

	public Response all(OPRegistrationViewModel viewModel) {
		Response response = new Response();
		try {
			// Condition to check whether where attribute is empty
			// To check for search criteria
			if (viewModel.where != null) {
				// for appending the where and the like for search criteria
				viewModel.where = helperUtil
						.ConstructWhereClauseFromJson(viewModel.where);
			}

			List<OPRegistration> list = null;

			Map<String, Object> map = new HashMap<String, Object>();
			if (viewModel != null) {
				list = genericDAO.all(CommonUtils.getMessage("OPRegistration"),
						OPRegistration.class, viewModel.where, viewModel.limit,
						viewModel.skip);
			}

			// Checking whether the list is empty or not
			if (null != list && list.size() > 0) {
				// Define the target type
				Type targetListType = new TypeToken<List<OPRegistrationViewModel>>() {
				}.getType();
				// Mapping the list with the target view model
				List<OPRegistrationViewModel> lists = modelMapper.map(list,
						targetListType);
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.fetchPatientdata");
				// appending the list to the response viewModels list to didplay
				// in the UI
				response.ViewModels = lists;
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.fetchPatientdata");
			}
		} catch (Exception e) {
			// error in the database or connection
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

	/*
	 * Implementation of interface IOPRegistrationService findByCode method
	 * which is used to find an patinets using code(primary key).
	 * 
	 * @see vHMS.Facade.IOPRegistrationService#findByCode(vHMS.Core.Models.
	 * OPRegistrationViewModel
	 */

	public Response findByCode(OPRegistrationViewModel viewModel)
			throws Exception {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {
			// To find where the record exists

			OPRegistration opdetails = null;
			Map<String, Object> map = new HashMap<String, Object>();
			if (viewModel != null) {
				map.put("code", viewModel.code);
				opdetails = genericDAO.findOneByAttribute(
						CommonUtils.getMessage("OPRegistration"), map,
						OPRegistration.class);
			}
			if (null != opdetails) {
				OPRegistrationViewModel catData = modelMapper.map(opdetails,
						OPRegistrationViewModel.class);
				response.success = true;
				// if the record exists bind to the view model
				response.ViewModel = catData;
				response.message = CommonUtils
						.getMessage("success.findPatientdata");
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.findPatientdata");
			}
		} catch (Exception e) {
			// return false if there is any database connection problem
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

	/*
	 * Implementation of interface IOPRegistrationService findAllByAttributes
	 * method which fetches all the record with the given criteria.
	 * 
	 * @see
	 * vHMS.Facade.IOPRegistrationService#findAllByAttributes(vHMS.Core.Models
	 * .OPRegistrationViewModel
	 */
	public Response findAllByAttributes(OPRegistrationViewModel viewModel)
			throws Exception {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {
			if (viewModel != null) {
				// Converting MasterViewModel object to MasterEntity Pojo
				OPRegistration catData = modelMapper.map(viewModel,
						OPRegistration.class);
				Gson gson = new GsonBuilder().setDateFormat(
						"yyyy-MM-dd HH:mm:ss").create();
				// Converting MasterEntity to a JSON string
				String master = gson.toJson(catData);
				// Converting JON string to a HashMap
				Map<String, Object> map = gson.fromJson(master,
						new TypeToken<HashMap<String, Object>>() {
						}.getType());
				if (map.containsKey("memo")) {
					map.put("memo", java.util.regex.Pattern.compile(
							(String) catData.getMemo(),
							Pattern.CASE_INSENSITIVE));
				}
				// Getting the list of entity for the criteria and all the
				// records
				List<OPRegistration> list = genericDAO.findAllByAttributes(
						CommonUtils.getMessage("OPRegistration"), map,
						OPRegistration.class, viewModel.limit, viewModel.skip);
				// Define the target type
				Type targetListType = new TypeToken<List<OPRegistrationViewModel>>() {
				}.getType();
				// Mapping the list with the target view model
				List<OPRegistrationViewModel> lists = modelMapper.map(list,
						targetListType);
				// Checking whether the list is empty or not
				if (null != lists && lists.size() > 0) {
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.fetchPatientdata");
					// appending the list to the response viewModels list to
					// didplay
					// in the UI
					response.ViewModels = lists;
				} else {
					// List is empty response is false
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.fetchPatientdata");
				}
			} else {
				// List is empty response is false
				response.success = false;
				response.message = CommonUtils.getMessage("error.Invalid");
			}
		} catch (Exception e) {
			// error in the database or connection
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

}
