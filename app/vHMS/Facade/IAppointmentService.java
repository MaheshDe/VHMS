package vHMS.Facade;

import java.io.IOException;

import vHMS.Core.ViewModels.AppointmentViewModel;
import vHMS.Core.ViewModels.ReportViewModel;
import vHMS.Core.ViewModels.Response;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * <h1>IAppointmentService</h1> The IAppointmentService deals with the
 * Appointment registration/updation/deletion of a patient Appointment. It deals
 * with the entire operations related to a Appointment.
 * 
 * @author ShunmugaRajaG
 * @version 1.0
 */
public interface IAppointmentService {
	/*
	 * Implementation of interface IAppointmentService all method which returns
	 * a list of allAppointment.
	 */
	public Response all(AppointmentViewModel viewModel)
			throws JsonProcessingException, IOException, Exception;

	/*
	 * Implementation of interface IAppointmentService create method which is
	 * used to create/save a Appointment.
	 */
	public Response create(AppointmentViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IAppointmentService update method which is
	 * used to update the Appointment details with the given id.
	 */
	public Response update(AppointmentViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IAppointmentService findAllByAttributes
	 * method which is used to find the Appointment details with the given
	 * attribute.
	 */
	public Response findAllByAttributes(AppointmentViewModel viewModel)
			throws Exception;

	/*
	 * Implementation of interface IAppointmentService findByCode method which
	 * is used to find the Appointment details with the given code.
	 */
	public Response findByCode(AppointmentViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IAppointmentService appointmentReport method
	 * which is used to get the report for the given inputs.
	 */
	public Response appointmentReport(ReportViewModel reportViewModel)
			throws Exception;

	/*
	 * Implementation of interface IAppointmentService
	 * checkAppointmentAvailablity method which is used to check whether
	 * appointment is booked for the same day/same slot for the same doctor.
	 */
	public Response checkAppointmentAvailablity(AppointmentViewModel viewModel)
			throws Exception;

}
