package vHMS.Facade;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import play.db.ebean.Transactional;
import vHMS.Core.Models.User;
import vHMS.Core.Models.UserSchedule;
import vHMS.Core.ViewModels.Response;
import vHMS.Core.ViewModels.UserScheduleViewModel;
import vHMS.Core.ViewModels.UserViewModel;
import Base.Data.IGenericMongoDao;
import Base.Utilities.CommonUtils;
import Base.Utilities.HelperUtil;
import Base.Utilities.NullAwareBeanUtilsBean;
import Base.Utilities.Email.SmtpSender;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;

/**
 * The UserScheduleService is the implementation for the interface
 * IUserScheduleService.
 * 
 * @author MaheshDe
 * @since 2015-08-21
 */

@Service
@Transactional
public class UserScheduleService implements IUserScheduleService {

	@Autowired
	private IGenericMongoDao genericDAO;

	@Autowired
	private SmtpSender defaultEmailSender;
	// Created HelperUtil instance.
	private HelperUtil helperUtil = new HelperUtil();
	// Created ModelMapper instance
	private ModelMapper modelmapper = new ModelMapper();
	// Created NullAwareBeanUtilsBean instance.
	private BeanUtilsBean notNull = new NullAwareBeanUtilsBean();

	/**
	 * Implementation of interface IUserScheduleService all method which returns
	 * a list of User schedule data.
	 * 
	 * @see vHMS.Facade.IUserScheduleService#all(vHMS.Core.Models.
	 *      UserScheduleViewModel
	 */

	@Override
	public Response all(UserScheduleViewModel viewModel)
			throws JsonProcessingException, IOException {
		// TODO Auto-generated method stub

		Response response = new Response();

		// Fetch all the records from the table.
		List<UserSchedule> allUserSchedule = null;

		if (viewModel.where != null) {
			// for appending the where and the like for search criteria
			viewModel.where = helperUtil
					.ConstructWhereClauseFromJson(viewModel.where);
		}
		allUserSchedule = genericDAO.all(
				CommonUtils.getMessage("Userschedule"), UserSchedule.class,
				viewModel.where, viewModel.limit, viewModel.skip);
		try {
			// to check whether there is a list of entity returns true response
			if (null != allUserSchedule && allUserSchedule.size() > 0) {
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.userSchedule.Criteria.search");
				response.ViewModels = allUserSchedule;
			} else {// Returns false response
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.userSchedule.Criteria.search");
			}

		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;

	}

	/**
	 * Implementation of interface IUserScheduleService create method which
	 * saves/create a user schedule data.
	 * 
	 * @see vHMS.Facade.IUserScheduleService#create(vHMS.Core.Models.
	 *      UserScheduleViewModel
	 */

	@Override
	public Response create(UserScheduleViewModel viewModel) throws Exception {
		// TODO Auto-generated method stub

		Response response = new Response();
		UserSchedule userScheduleExist = null;
		String id = null;
		try {

			if (viewModel != null) {
				Map<String, Object> map = new HashMap<String, Object>();

				map.put("code", viewModel.userCode);

				User userExist = genericDAO.findOneByAttribute(
						CommonUtils.getMessage("User"), map, User.class);

				if (null != userExist) {
					map.clear();
					map.put("userCode", viewModel.userCode);

					/*
					 * if (null != viewModel.startTime && viewModel.startTime !=
					 * "" && viewModel.startTime.trim().length() > 0) {
					 * map.put("startTime", BasicDBObjectBuilder .start("$gte",
					 * viewModel.startTime) .get());
					 * 
					 * }
					 * 
					 * if (null != viewModel.endTime && viewModel.endTime != ""
					 * && viewModel.endTime.trim().length() > 0) {
					 * map.put("endTime", BasicDBObjectBuilder .start("$lte",
					 * viewModel.endTime) .get());
					 * 
					 * }
					 */

					BasicDBList ORList = new BasicDBList();

					if (null != viewModel.startTime
							&& viewModel.startTime != ""
							&& viewModel.startTime.trim().length() > 0) {
						BasicDBList list = new BasicDBList();

						BasicDBObject startobj = new BasicDBObject("startTime",
								BasicDBObjectBuilder.start("$lte",
										viewModel.startTime).get());
						list.add(startobj);
						BasicDBObject endobj = new BasicDBObject("endTime",
								BasicDBObjectBuilder.start("$gte",
										viewModel.startTime).get());
						list.add(endobj);

						ORList.add(new BasicDBObject("$and", list));
					}

					if (null != viewModel.endTime && viewModel.endTime != ""
							&& viewModel.endTime.trim().length() > 0) {
						BasicDBList list = new BasicDBList();

						BasicDBObject startobj = new BasicDBObject("startTime",
								BasicDBObjectBuilder.start("$lte",
										viewModel.endTime).get());
						list.add(startobj);
						BasicDBObject endobj = new BasicDBObject("endTime",
								BasicDBObjectBuilder.start("$gte",
										viewModel.endTime).get());
						list.add(endobj);

						ORList.add(new BasicDBObject("$and", list));
					}
					if (null != viewModel.endTime && viewModel.endTime != ""
							&& viewModel.endTime.trim().length() > 0
							&& null != viewModel.startTime
							&& viewModel.startTime != ""
							&& viewModel.startTime.trim().length() > 0) {

						BasicDBList list = new BasicDBList();

						BasicDBObject startobj = new BasicDBObject("startTime",
								BasicDBObjectBuilder.start("$gte",
										viewModel.startTime).get());
						list.add(startobj);
						BasicDBObject endobj = new BasicDBObject("endTime",
								BasicDBObjectBuilder.start("$lte",
										viewModel.endTime).get());
						list.add(endobj);

						ORList.add(new BasicDBObject("$and", list));
					}
					if (ORList.size() > 0) {
						map.put("$or", ORList);
					}
					map.put("day", viewModel.day);

					userScheduleExist = genericDAO.findOneByAttribute(
							CommonUtils.getMessage("Userschedule"), map,
							UserSchedule.class);

					if (null != userScheduleExist) {
						response.success = false;
						response.message = CommonUtils
								.getMessage("error.createUserSchedule.exists");
					} else {
						// Calling the helper class to set the mandatory
						// properties of
						// the view model
						viewModel = helperUtil.AttachCommonFields(viewModel,
								UserScheduleViewModel.class);

						// convert viewmodel object to model object before
						// saving it
						// to DB

						UserSchedule userSchedule = modelmapper.map(viewModel,
								UserSchedule.class);

						// Create the new record in table.
						Object[] obj = genericDAO.save(
								CommonUtils.getMessage("Userschedule"),
								userSchedule);

						for (Object objId : obj) {
							id = String.valueOf(objId);
							userSchedule.setId(id);
						}
						response.success = true;
						response.message = CommonUtils
								.getMessage("success.userSchedule.creation");
						response.ViewModel = modelmapper.map(userSchedule,
								UserScheduleViewModel.class);

					}

				} else {
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.noUserData.exists");
				}

			}

		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();

		}
		return response;
	}

	/**
	 * Implementation of interface IUserScheduleService findById method which
	 * search a User schedule data with the given code.
	 * 
	 * @see vHMS.Facade.IUserScheduleService#findById(vHMS.Core.Models.
	 *      UserScheduleViewModel
	 */

	@Override
	public Response findById(UserScheduleViewModel viewModel) throws Exception {
		// TODO Auto-generated method stub

		Response response = new Response();

		try {
			Map<String, Object> map = new HashMap<String, Object>();
			UserSchedule userSchedule = null;

			if (viewModel != null) {
				map.put("code", viewModel.code);

				// Fetch the record by code.
				userSchedule = genericDAO.findOneByAttribute(
						CommonUtils.getMessage("Userschedule"), map,
						UserSchedule.class);

				if (userSchedule != null) {
					UserScheduleViewModel userScheduleViewModel = modelmapper
							.map(userSchedule, UserScheduleViewModel.class);

					response.success = true;
					response.message = CommonUtils
							.getMessage("success.userSchedule.Criteria.search");
					response.ViewModel = userScheduleViewModel;
				} else {
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.userSchedule.Criteria.search");
				}
			}

		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}

	/**
	 * Implementation of interface IUserScheduleService count method which
	 * returns number of user schedule data present in a particular table.
	 * 
	 * @see vHMS.Facade.IUserScheduleService#count(vHMS.Core.Models.
	 *      UserScheduleViewModel
	 */

	@Override
	public Response count(UserScheduleViewModel viewModel) {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {
			// Condition to check whether where attribute is empty to check for
			// search criteria
			if (viewModel.where != null) {
				// for appending the where and the like for search criteria
				viewModel.where = helperUtil
						.ConstructWhereClauseFromJson(viewModel.where);
			}
			// Count of the record in the list or serach by criteria
			int count = genericDAO.count(viewModel.entityObject, User.class);
			// adding count to the json object
			response.data.put("count", count);
			response.success = true;
			response.message = CommonUtils
					.getMessage("success.countMasterdata");
		} catch (Exception e) {
			// Return false if it ia an error
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

	/**
	 * Implementation of interface IUserScheduleService delete method which
	 * deletes a user schedule data for the given code.
	 * 
	 * @see vHMS.Facade.IUserScheduleService#delete(vHMS.Core.Models.
	 *      UserScheduleViewModel
	 */

	@Override
	public Response delete(UserScheduleViewModel viewModel) {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {

			Map<String, Object> map = new HashMap<String, Object>();
			if (viewModel != null) {
				map.put("code", viewModel.code);

				// Fetch the record by code.
				UserSchedule user = genericDAO.findOneByAttribute(
						viewModel.entityObject, map, UserSchedule.class);

				// if record found then delete record and returns true
				if (user != null) {
					// Deleting the user with the code
					genericDAO.delete(viewModel.entityObject, user.getId(),
							User.class);

					UserScheduleViewModel userScheduleData = modelmapper.map(
							user, UserScheduleViewModel.class);
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.user.data.deletion");
					response.ViewModel = userScheduleData;
					// response.status = true;

				} else {
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.usercode.not.exists");
				}
			}
		} catch (Exception e) {

			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}

	/**
	 * Implementation of interface IUserScheduleService update method which
	 * updates a user schedule data for the given code.
	 * 
	 * @see vHMS.Facade.IUserScheduleService#update(vHMS.Core.Models.
	 *      UserScheduleViewModel
	 */

	@Override
	public Response update(UserScheduleViewModel viewModel) {
		// TODO Auto-generated method stub
		Response response = new Response();
		UserSchedule userScheduleData = null;
		try {

			if (viewModel != null) {
				Map<String, Object> map = new HashMap<String, Object>();

				map.put("code", viewModel.code);

				UserSchedule userScheduleExist = genericDAO.findOneByAttribute(
						CommonUtils.getMessage("Userschedule"), map,
						UserSchedule.class);

				map.clear();
				map.put("userCode", viewModel.userCode);
				if (null != viewModel.startTime && viewModel.startTime != ""
						&& viewModel.startTime.trim().length() > 0) {
					map.put("startTime",
							BasicDBObjectBuilder.start("$gte",
									viewModel.startTime).get());
				}

				if (null != viewModel.endTime && viewModel.endTime != ""
						&& viewModel.endTime.trim().length() > 0) {
					map.put("endTime",
							BasicDBObjectBuilder.start("$lte",
									viewModel.endTime).get());
				}
				map.put("day", viewModel.day);

				userScheduleData = genericDAO.findOneByAttribute(
						viewModel.entityObject, map, UserSchedule.class);
				if (null != userScheduleExist) {
					if (null != userScheduleData
							&& !(userScheduleExist.getCode()
									.equals(viewModel.code))) {
						response.success = false;
						response.message = CommonUtils
								.getMessage("error.updateUserSchedule.exists");
					} else {
						// Calling the helper class to set the mandatory
						// properties of
						// the view model
						viewModel = helperUtil.AttachCommonFields(viewModel,
								UserScheduleViewModel.class);

						// Mapping the viewModel to the Model(User).
						UserSchedule userSchedule = modelmapper.map(viewModel,
								UserSchedule.class);

						// override the old properties with new properties
						// exists in the
						// right side object
						notNull.copyProperties(userScheduleExist, userSchedule);

						// Update the existing record with new values.
						genericDAO.updateById(viewModel.entityObject,
								userScheduleExist, userScheduleExist.getId());

						UserSchedule updatedUserSchedule = genericDAO
								.findOneById(
										CommonUtils.getMessage("Userschedule"),
										userScheduleExist.getId(),
										UserSchedule.class);

						response.success = true;
						response.ViewModel = modelmapper.map(
								updatedUserSchedule, UserViewModel.class);
						response.message = CommonUtils
								.getMessage("success.update.UserSchedule");
					}

				} else {
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.nodata.exists");
				}
			}
		} catch (Exception e) {
			// Exception Handling
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}

	/**
	 * Implementation of interface IUserScheduleService findAllByAttributes
	 * method which returs a list of User Schedule data based on search criteria
	 * 
	 * @see 
	 *      vHMS.Facade.IUserScheduleService#findAllByAttributes(vHMS.Core.Models
	 *      . UserScheduleViewModel
	 */

	@SuppressWarnings("deprecation")
	@Override
	public Response findAllByAttributes(UserScheduleViewModel viewModel)
			throws Exception {
		// TODO Auto-generated method stub

		Response response = new Response();
		try {

			// Converting UserScheduleViewModel object to UserScheduleEntity
			// Pojo
			UserSchedule userScheduleData = modelmapper.map(viewModel,
					UserSchedule.class);
			Gson gson = new Gson();

			// Converting UserScheduleEntity to a JSON string
			String master = gson.toJson(userScheduleData);

			// Converting JSON string to a HashMap
			Map<String, Object> map = new Gson().fromJson(master,
					new TypeToken<HashMap<String, Object>>() {
					}.getType());

			if (viewModel.where != null) {
				// for appending the where and the like for search criteria
				viewModel.where = helperUtil
						.ConstructWhereClauseFromJson(viewModel.where);
			}
			// Fetch the Entity based on the search criteria
			List<UserSchedule> allUserScheduleEntity = genericDAO
					.findAllByAttributes(viewModel.entityObject, map,
							UserSchedule.class, viewModel.limit, viewModel.skip);

			// Returns true if there is a record matching the criteria
			if (null != allUserScheduleEntity
					&& allUserScheduleEntity.size() > 0) {
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.userSchedule.Criteria.search");
				response.ViewModels = allUserScheduleEntity;
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.userSchedule.Criteria.search");
			}

		} catch (Exception e) {
			// Handling Exception
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			// response.status = false;
			response.ExceptionMessage = e.getMessage();
		}
		return response;

	}

}
