package vHMS.Facade;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.lang3.RandomStringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vHMS.Core.Models.Doctor;
import vHMS.Core.Models.User;
import vHMS.Core.ViewModels.DoctorViewModel;
import vHMS.Core.ViewModels.NotificationViewModel;
import vHMS.Core.ViewModels.Response;
import vHMS.Core.ViewModels.ResponseViewModel;
import vHMS.Core.ViewModels.UserDetailsViewModel;
import vHMS.Core.ViewModels.UserViewModel;
import Base.Data.IGenericMongoDao;
import Base.Utilities.CommonUtils;
import Base.Utilities.HelperUtil;
import Base.Utilities.NullAwareBeanUtilsBean;
import Base.Utilities.Email.SmtpSender;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

/**
 * The UserService is the implementation for the interface IUserService.
 * 
 * @author MaheshDe
 * @since 2015-08-19
 */

@Service
@Transactional
public class UserService implements IUserService {

	@Autowired
	private IGenericMongoDao genericDAO;

	@Autowired
	private SmtpSender defaultEmailSender;
	// Created HelperUtil instance.
	private HelperUtil helperUtil = new HelperUtil();
	// Created ModelMapper instance
	private ModelMapper modelmapper = new ModelMapper();
	// Created NullAwareBeanUtilsBean instance.
	private BeanUtilsBean notNull = new NullAwareBeanUtilsBean();

	
	/**
	 * Implementation of interface IUserService all method which returns a
	 * list of User data.
	 * 
	 *  @see vHMS.Facade.IUserService#all(vHMS.Core.Models.
	 * UserViewModel
	 */
	
	public Response all(UserViewModel viewModel)
			throws JsonProcessingException, IOException {
		
		Response response = new Response();
		// Fetch all the records from the table.
		List<User> allUser = null;

		if (viewModel.where != null) {
			// for appending the where and the like for search criteria
			viewModel.where = helperUtil
					.ConstructWhereClauseFromJson(viewModel.where);
		}
		allUser = genericDAO.all(CommonUtils.getMessage("User"), User.class,
				viewModel.where, viewModel.limit, viewModel.skip);
		try {
			// to check whether there is a list of entity returns true response
			if (null != allUser && allUser.size() > 0) {
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.user.Criteria.search");
				response.ViewModels = allUser;
			} else {// Returns false response
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.user.Criteria.search");
			}

		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}

	
	/**
	 * This method is used to create or register a USer, it will not allow to
	 * insert if the email id already exists in the table.
	 * 
	 * @see vHMS.Facade.IUserService#create(vHMS.Core.Models.UserViewModel
	 *      )
	 */
	
	
	public void create(User User) {
		genericDAO.save("User", User);
	}

	
	/**
	 * Implementation of interface IUserService create method which
	 * saves/create a user data.
	 * 
	 *@see vHMS.Facade.IUserService#create(vHMS.Core.Models.
	 * UserViewModel
	 */
	
	public Response create(UserViewModel viewModel) throws Exception {
		Response response = new Response();
		User userExist = null;
		String id = null;
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			if(viewModel != null)
			{
				map.put("email", viewModel.email);
			
				userExist = genericDAO.findOneByAttribute(
						CommonUtils.getMessage("User"), map,
						User.class);
				
				if(userExist != null)
				{
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.createUser.exists");
				}
				else
				{
					// Calling the helper class to set the mandatory properties of
					// the view model
					viewModel = helperUtil.AttachCommonFields(viewModel,
							UserViewModel.class);
					
					//create ramdom password
					String randomPassword = RandomStringUtils
							.randomAlphanumeric(6);
					
					// Encrypt the password to store
					String newPassword = CommonUtils.encryptor(randomPassword);
					viewModel.password = newPassword;
					
					//  convert viewmodel object to model object before saving it
					// to DB
					User user = modelmapper.map(viewModel, User.class);
				
					// Create the new record in table.
					Object[] obj =genericDAO.save(CommonUtils.getMessage("User"), user);
					
					// Create a mail message to be send.
					// NotificationMessage message = new NotificationMessage();
					NotificationViewModel message = new NotificationViewModel();
					ResponseViewModel responseViewModel = new ResponseViewModel();
					 
					message.setTo(user.getEmail());
					message.setFrom(CommonUtils.getMessage("from"));
					message.setSubject(CommonUtils.getMessage("subject"));
					
					// Get the email body content from the scala temapltes
					String body = views.html.emailtemplates.forgotpassword.render(
							user.getName(), user.getEmail(), randomPassword).body();

					message.setBody(body);
					responseViewModel.notificationViewModel = message;
					
										for (Object objId : obj) {
											id = String.valueOf(objId);
										user.setId(id);
									}
										
										responseViewModel.viewModel = modelmapper.map(user,
												UserViewModel.class);
										
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.user.creation");
					response.ViewModel = responseViewModel ;
					
					
				}
			}
			
		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();

		}
		return response;
	}

	/**
	 * Implementation of interface IUserService findAllByAttributes method
	 * which returs a list of user data based on search criteria.
	 * 
	 * @see vHMS.Facade.IUserService#findAllByAttributes(vHMS.Core.Models.
	 * UserViewModel
	 */
	
	public Response findAllByAttributes(UserViewModel viewModel) {
		Response response = new Response();
		try {
			
			// Converting UserViewModel object to UserEntity Pojo
			User userData = modelmapper.map(viewModel, User.class);
			Gson gson = new Gson();
			
			// Converting UserEntity to a JSON string
			String master = gson.toJson(userData);
			
			// Converting JON string to a HashMap
			Map<String, Object> map = new Gson().fromJson(master,
					new TypeToken<HashMap<String, Object>>() {
					}.getType());

			if (viewModel.where != null) {
				// for appending the where and the like for search criteria
				viewModel.where = helperUtil
						.ConstructWhereClauseFromJson(viewModel.where);
			}
			// Fetch the Entity based on the search criteria
			List<User> allUserEntity = genericDAO.findAllByAttributes(
					viewModel.entityObject, map, User.class, viewModel.limit,
					viewModel.skip);

			// Returns true if there is a record matching the criteria
			if (null != allUserEntity && allUserEntity.size() > 0) {
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.user.Criteria.search");
				response.ViewModels = allUserEntity;
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.user.Criteria.search");
			}

		} catch (Exception e) {
			// Handling Exception
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			// response.status = false;
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}

	
	/**
	 * Implementation of interface IUserService findById method which search a
	 * User data with the given code.
	 * 
	 * @see vHMS.Facade.IUserService#findById(vHMS.Core.Models.
	 * UserViewModel
	 */
	
	public Response findById(UserViewModel viewModel) throws Exception {

		Response response = new Response();

		try {
				Map<String, Object> map = new HashMap<String, Object>();
				User user = null;
				
				if(viewModel != null)
				{
					map.put("code", viewModel.code);
					
					// Fetch the record by code.
					user = genericDAO.findOneByAttribute(
							CommonUtils.getMessage("User"), map, User.class);
					
					if(user != null)
					{
						UserViewModel userViewModel = modelmapper.map(user,
						UserViewModel.class);
						
						response.success = true;
						response.message = CommonUtils
								.getMessage("success.user.Criteria.search");
						response.ViewModel = userViewModel;
					}
					else 
					{
						response.success = false;
						response.message = CommonUtils
							.getMessage("error.user.Criteria.search");
					}
				}

			} catch (Exception e) {
				response.success = false;
				response.message = CommonUtils.getMessage("error.processfailure");
				response.ExceptionMessage = e.getMessage();
			}
		return response;
	}

	
	/**
	 * Implementation of interface IUserService count method which returns
	 * number of user data present in a particular table.
	 * 
	 *  @see vHMS.Facade.IUserService#count(vHMS.Core.Models.
	 * UserViewModel
	 */
	
	public Response count(UserViewModel viewModel) {
		Response response = new Response();
		try {
			// Condition to check whether where attribute is empty to check for
			// search criteria
			if (viewModel.where != null) {
				// for appending the where and the like for search criteria
				viewModel.where = helperUtil
						.ConstructWhereClauseFromJson(viewModel.where);
			}
			// Count of the record in the list or serach by criteria
			int count = genericDAO.count(CommonUtils.getMessage("User"), User.class);
			// adding count to the json object
			response.data.put("count", count);
			response.success = true;
			response.message = CommonUtils
					.getMessage("success.countMasterdata");
		} catch (Exception e) {
			// Return false if it ia an error
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

	/**
	 * Implementation of interface IUserService delete method which deletes a
	 * user data for the given code.
	 * 
	 *  @see vHMS.Facade.IUserService#delete(vHMS.Core.Models.UserViewModel
	 */
	
	public Response delete(UserViewModel viewModel) {

		Response response = new Response();
		try {

			Map<String, Object> map = new HashMap<String, Object>();
			if(viewModel != null)
			{
				map.put("code", viewModel.code);
				
				// Fetch the record by code.
				User user = genericDAO.findOneByAttribute(
						CommonUtils.getMessage("User"), map, User.class);

				// if record found then delete record and returns true
				if (user != null) {
					// Deleting the user with the code
					genericDAO.delete(CommonUtils.getMessage("User"), user.getId(),
							User.class);
				
					UserViewModel userData = modelmapper.map(user,
							UserViewModel.class);
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.user.data.deletion");
					response.ViewModel = userData;
					// response.status = true;

				} else {
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.usercode.not.exists");
				}
			}
		} catch (Exception e) {

			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}

	
	/**
	 * Implementation of interface IUserService update method which updates a
	 * user data for the given code.
	 * 
	 *  @see vHMS.Facade.IUserService#update(vHMS.Core.Models.
	 * UserViewModel
	 */
	
	public Response update(UserViewModel viewModel) {

		Response response = new Response();
		try {
			// Calling the helper class to set the mandatory properties of the
			// view model
			viewModel = helperUtil.AttachCommonFields(viewModel,
					UserViewModel.class);
			
			// Mapping the viewModel to the Model(User).
			User user = modelmapper.map(viewModel,
								User.class);
			// getting the id from the view model for updating for the specific
			// id
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("code", viewModel.code);
			
			// Check by code whether the record exists before update.
			User userExist = genericDAO.findOneByAttribute(CommonUtils.getMessage("User"), map, User.class);
			
			map.clear();
			map.put("email", viewModel.email);
			
			// Check whether the email id exists before update.
			User userData = genericDAO.findOneByAttribute(CommonUtils.getMessage("User"), map, User.class);
			
			if(userExist != null )
			{
				if(userData != null && !(userExist.getCode().equals(viewModel.code)))
				{
					response.success = false;
					response.message = CommonUtils
						.getMessage("error.updateUser.exists");
				}
				else
				{
					// override the old properties with new properties exists in the
					// right side object
					notNull.copyProperties(userExist, user);
					
					// Update the existing record with new values.
					genericDAO.updateById(CommonUtils.getMessage("User"), userExist,
							userExist.getId());
					
					User updatedUser = genericDAO.findOneById(
							CommonUtils.getMessage("User"), userExist.getId(),
							User.class);
					
					response.success = true;
					response.ViewModel = modelmapper.map(updatedUser,
							UserViewModel.class);
					response.message = CommonUtils
							.getMessage("success.update.User");	
				}
			}
			else
			{
				// To check whether the user code exists if not returns the
				// response as false
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.usercode.not.exists");
			}
			
		} catch (Exception e) {
			// Exception Handling
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}

	/**
	 * Implementation of interface IUserService forgotPassword method which is
	 * used to get back user's password if User forgot his password.
	 * 
	 * @see vHMS.Facade.IUserService#forgotPassword(vHMS.Core.ViewModels.
	 * UserViewModel)
	 */

	public Response forgotPassword(UserViewModel viewModel) throws Exception {
		Response response = new Response();
		// Get the emailId
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("email", viewModel.email);
		try {
			// Check if it is valid email by confirming its existence
			User User = genericDAO.findOneByAttribute(
					CommonUtils.getMessage("User"), map, User.class);

			// com.fasterxml.jackson.databind.node.ObjectNode result =
			// Json.newObject();
			// If exists and valid, modify the password and send email
			if (null != User) {
				// Generate random password
				String randomPassword = RandomStringUtils.randomAlphanumeric(6);
				// Encrypt the password to store
				String newPassword = CommonUtils.encryptor(randomPassword);

				// Create a mail message t be send.
				// NotificationMessage message = new NotificationMessage();
				NotificationViewModel message = new NotificationViewModel();
				ResponseViewModel responseViewModel = new ResponseViewModel();

				message.setTo(User.getEmail());
				message.setFrom(CommonUtils.getMessage("from"));
				message.setSubject(CommonUtils.getMessage("subject"));
				// Get the email body content from the scala temapltes
				String body = views.html.emailtemplates.forgotpassword.render(
						User.getName(), User.getEmail(), randomPassword).body();

				message.setBody(body);

				// defaultEmailSender.send(message);

				// Update user password
				User.setPassword(newPassword);
				genericDAO.updateById(CommonUtils.getMessage("User"), User,
						User.getId());

				responseViewModel.notificationViewModel = message;
				response.success = true;
				response.message = CommonUtils.getMessage("success.emailSent");
				response.ViewModel = responseViewModel;
				// response.ViewModel = message;
				// response.userResponseViewModel=message;
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.notregistered");
			}
		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}

	/**
	 * Implementation of interface IUserService changePassword method which is
	 * used to change the password when an User requests for the same.
	 * 
	 * @see vHMS.Facade.IUserService#changePassword(vHMS.Core.ViewModels.
	 * UserViewModel)
	 */

	public Response changePassword(UserViewModel viewModel) throws Exception {
		Response response = new Response();
		// Get the emailId
		Map<String, Object> map = new HashMap<String, Object>();

		// String password = CommonUtils.encryptor(viewModel.currentPassword);
		// map.put("password", password);
		map.put("password", CommonUtils.encryptor(viewModel.currentPassword));
		map.put("email", viewModel.email);
		try {
			// Check if it is valid email by confirming its existence
			User user = genericDAO.findOneByAttribute(
					CommonUtils.getMessage("User"), map, User.class);

			// Encrypt the password to store
			// String newPassword =
			// CommonUtils.encryptor(viewModel.newPassword);
			String newPassword = viewModel.newPassword;

			// If exists and valid, modify the password and send email
			if (null != user) {

				// Update user password
				user.setPassword(CommonUtils.encryptor(newPassword));
				genericDAO.updateById(CommonUtils.getMessage("User"), user,
						user.getId());

				response.success = true;
				response.message = CommonUtils
						.getMessage("success.passwordsuccess");
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.passworderror");
			}
		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils
					.getMessage("error.processfailurechangepwd");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}
	
	
	
	/**
	 * Implementation of interface IUserService create method which
	 * saves/create a user data
	 */
	@Override
	public Response createUserbyType(UserViewModel viewModel,DoctorViewModel doctorViewModel) throws Exception {
		Response response = new Response();
		Doctor doctorExist = null;
		UserDetailsViewModel userDetailsViewModel = new UserDetailsViewModel();
		//Creating the user
		response=create(viewModel);
		
		if(response.success)
		{
			
		String userCode=((UserViewModel)((ResponseViewModel)response.ViewModel).viewModel).code;
		
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			if(doctorViewModel != null)
			{
				
				doctorViewModel.userCode=userCode;
				
				
				map.put("userCode",doctorViewModel.userCode);
			//checking whether user already exist 
				doctorExist = genericDAO.findOneByAttribute(CommonUtils.getMessage("Doctor"), map,Doctor.class);
				
				if(doctorExist != null)
				{
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.createDoctor.exists");
				}
				else
				{
					// Calling the helper class to set the mandatory properties of
					// the view model
					doctorViewModel = helperUtil.AttachCommonFields(doctorViewModel,
							DoctorViewModel.class);
					
					//  convert viewmodel object to model object before saving it
					// to DB
					Doctor doctor = modelmapper.map(doctorViewModel, Doctor.class);
				
					// Create the new record in table.
					genericDAO.save(CommonUtils.getMessage("Doctor"), doctor);
					
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.doctor.creation");
					userDetailsViewModel.userTypeViewModel =  modelmapper.map(doctor,DoctorViewModel.class);
					userDetailsViewModel.userViewModel =   ((UserViewModel)((ResponseViewModel)response.ViewModel).viewModel);
					response.ViewModel = userDetailsViewModel ;
				}
			}
			
		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();

		}
		
		}
		return response;
	}
	
	
	/**
	 * Implementation of interface IUserService updateUserbyType  method which
	 * Update Doctor data
	 */
	@Override
	public Response updateUserbyType(UserViewModel viewModel,DoctorViewModel doctorViewModel) throws Exception {
		Response response = new Response();
		Doctor doctorExist = null;
		UserDetailsViewModel userDetailsViewModel = new UserDetailsViewModel();
		
		//Update the user table 
		response=update(viewModel);
		if(response.success)
		{
			
			//update the doctor table.
			String userCode=viewModel.code;
			Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			if(doctorViewModel != null)
			{
				doctorViewModel.userCode=userCode;
				map.put("userCode",doctorViewModel.userCode);
				doctorExist = genericDAO.findOneByAttribute(CommonUtils.getMessage("Doctor"), map,Doctor.class);
				if(doctorExist == null)
				{
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.udpateDoctor.notexists");
				}
				else
				{
					// Calling the helper class to set the mandatory properties of
					// the view model
					doctorViewModel = helperUtil.AttachCommonFields(doctorViewModel,
							DoctorViewModel.class);
					
					//  convert viewmodel object to model object before saving it
					// to DB
					Doctor doctor = modelmapper.map(doctorViewModel, Doctor.class);
				
					// override the old properties with new properties exists in the
					// right side object
					notNull.copyProperties(doctorExist, doctor);
					
					// Update the existing record with new values.
					genericDAO.updateById(CommonUtils.getMessage("Doctor"), doctor,
							doctorExist.getId());
					
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.doctor.Updated");
					userDetailsViewModel.userTypeViewModel =  modelmapper.map(doctor,DoctorViewModel.class);
					userDetailsViewModel.userViewModel =   ((UserViewModel)response.ViewModel);
					response.ViewModel = userDetailsViewModel ;
				}
			}
			
		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		
		}
		return response;
	}
	
	
	/**
	 * Implementation of interface IUserService findById method which search a
	 * User data with the given code
	 */
	@Override
	public Response findUserDetailById(UserViewModel viewModel) throws Exception {

		Response response = new Response();
		UserDetailsViewModel userDetailsViewModel = new UserDetailsViewModel();
		try {
				Map<String, Object> map = new HashMap<String, Object>();
				User user = null;
				Doctor doctor=null;
 				if(viewModel != null)
				{
					map.put("code", viewModel.code);
					
					// Fetch the record by code.
					user = genericDAO.findOneByAttribute(
					viewModel.entityObject, map, User.class);
					
					if(user != null)
					{
						UserViewModel userViewModel = modelmapper.map(user,UserViewModel.class);
						
						map.clear();
						map.put("userCode", viewModel.code);
						doctor = genericDAO.findOneByAttribute(CommonUtils.getMessage("Doctor"), map, Doctor.class);				
						
						if(doctor!=null)
						{
						response.success = true;
						response.message = CommonUtils.getMessage("success.user.Criteria.search");
						DoctorViewModel doctorViewModel = modelmapper.map(doctor,DoctorViewModel.class);
						userDetailsViewModel.userViewModel=userViewModel;
						userDetailsViewModel.userTypeViewModel=doctorViewModel;
						response.ViewModel = userDetailsViewModel;
						}
						else 
						{
							response.success = false;
							response.message = CommonUtils
								.getMessage("error.user.Criteria.search");
						}
					}
					else 
					{
						response.success = false;
						response.message = CommonUtils
							.getMessage("error.user.Criteria.search");
					}
				}

			} catch (Exception e) {
				response.success = false;
				response.message = CommonUtils.getMessage("error.processfailure");
				response.ExceptionMessage = e.getMessage();
			}
		return response;
	}


}
