/**
 * 
 */
package vHMS.Facade;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.lang3.RandomStringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vHMS.Core.Models.Appointment;
import vHMS.Core.Models.PatientRegistration;
import vHMS.Core.ViewModels.NotificationViewModel;
import vHMS.Core.ViewModels.PatientRegistrationViewModel;
import vHMS.Core.ViewModels.Response;
import vHMS.Core.ViewModels.ResponseViewModel;
import Base.Data.IGenericMongoDao;
import Base.Utilities.CommonUtils;
import Base.Utilities.HelperUtil;
import Base.Utilities.NullAwareBeanUtilsBean;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * The PatientRegistrationService is an implementation for
 * IPatientRegistrationService containing methods like create, update delete
 * etc.. related to patient management, and can be implemented by other classes.
 * 
 * @author DivyaP
 * @version 1.0
 * @since 2015-07-17
 */
@Service
@Transactional
public class PatientRegistrationService implements IPatientRegistrationService {

	// Created GenericDAO instance using DI
	@Autowired
	private IGenericMongoDao genericDAO;

	// Created HelperUtil instance.
	private HelperUtil helperUtil = new HelperUtil();
	// Created ModelMapper instance.
	private ModelMapper modelMapper = new ModelMapper();

	// Created NullAwareBeanUtilsBean instance.
	private BeanUtilsBean notNull = new NullAwareBeanUtilsBean();

	/**
	 * This method is used to create or register a patient, it will not allow to
	 * insert if the email id already exists in the table.
	 * 
	 * @see vHMS.Facade.IPatientRegistrationService#create(vHMS.Core.Models.PatientRegistrationViewModel
	 *      )
	 */
	@SuppressWarnings("unused")
	public Response create(PatientRegistrationViewModel viewModel) {
		Response response = new Response();
		ResponseViewModel responseViewModel = new ResponseViewModel();
		PatientRegistration patientexist = null;
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			if (viewModel.uhId != null && viewModel.uhId.length()>0 ) {

				map.put("email", viewModel.email);
				map.put("uhId", viewModel.uhId);

				patientexist = genericDAO.findOneByAttribute(
						CommonUtils.getMessage("PatientRegistration"), map,
						PatientRegistration.class);

				if (null != patientexist) {

					// PatientRegistrationViewModel patientDetails =
					// modelMapper.map(patientexist,PatientRegistrationViewModel.class);
					responseViewModel.viewModel = modelMapper.map(patientexist,
							PatientRegistrationViewModel.class);
					response.ViewModel = responseViewModel;
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.patientRegister.exist");
				} else {
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.patientRegister.exists");
				}

			} else {

				// Before inserting check whether any entry is there in the DB
				// with
				// the same email id, to restrict duplication of data
				map.clear();

				map.put("email", viewModel.email);

				patientexist = genericDAO.findOneByAttribute(
						CommonUtils.getMessage("PatientRegistration"), map,
						PatientRegistration.class);
				// If entry already exist with the same mail id don't insert
				// send
				// response back with success as false.
				if (null != patientexist) {
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.createPatientRegister.exists");
				} else {
					response.success = null;
					// Calling the helper class to set the mandatory properties
					// of the
					// view model
					viewModel = helperUtil.AttachCommonFields(viewModel,
							PatientRegistrationViewModel.class);
					// Generate random password
					String randomPassword = RandomStringUtils
							.randomAlphanumeric(6);
					// Encrypt the password to store
					String newPassword = CommonUtils.encryptor(randomPassword);
					viewModel.password = newPassword;
					// convert viewmodel object to model object before saving it
					// to DB
					PatientRegistration patientReg = modelMapper.map(viewModel,
							PatientRegistration.class);
					// Save the data with the fields in model and get the id for
					// the
					// saved item
					patientReg.setStatus(true);
					Object[] results = genericDAO.save(
							CommonUtils.getMessage("PatientRegistration"),
							patientReg);

					// Set the id back to the model.
					for (Object res : results) {
						patientReg.setId(String.valueOf(res));
					}
					// After saving if the returned id is not null then send the
					// response back as success
					if (null != patientReg.getId() && patientReg.getId() != ""
							&& patientReg.getId().trim().length() > 0) {

						// Before sending it back convert it again to viewmodel
						// and
						// then send
						PatientRegistrationViewModel patientDetails = modelMapper
								.map(patientReg,
										PatientRegistrationViewModel.class);

						NotificationViewModel smsNotificationViewModel = new NotificationViewModel();
						smsNotificationViewModel
								.setNotificationType("sms");
						smsNotificationViewModel
								.setTo(patientDetails.contactNumber);
						smsNotificationViewModel.setBody(CommonUtils
								.getMessage("sms.message"));

						responseViewModel.smsNotificationViewModel = smsNotificationViewModel;

						NotificationViewModel notificationViewModel = new NotificationViewModel();

						notificationViewModel.setTo(patientDetails.email);
						notificationViewModel.setFrom(CommonUtils
								.getMessage("from"));
						notificationViewModel.setSubject(CommonUtils
								.getMessage("emailsubject"));
						// Get the email body content from the scala temapltes
						String body = views.html.emailtemplates.sendmail
								.render(patientDetails.firstName,
										patientDetails.email,
										patientDetails.uhId, randomPassword)
								.body();

						notificationViewModel.setBody(body);

						responseViewModel.notificationViewModel = notificationViewModel;

						responseViewModel.viewModel = modelMapper.map(
								patientDetails,
								PatientRegistrationViewModel.class);

						response.success = true;
						response.message = CommonUtils
								.getMessage("success.createPatientRegister");
						// Binding the created data to the view model in the
						// response
						response.ViewModel = responseViewModel;

					} else {
						// If the saved id is null send the response back with
						// success as false.
						response.success = false;
						response.message = CommonUtils
								.getMessage("error.createPatientRegister");
					}

				}

			}
		} catch (Exception e) {
			// return false if there is any database connection problem
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}

		return response;

	}

	/*
	 * Implementation of interface IPatientRegistrationService all method which
	 * returns a list of all Patients.
	 * 
	 * @see
	 * 
	 * @see vHMS.Facade.IPatientRegistrationService#all(vHMS.Core.Models.
	 * PatientRegistrationViewModel
	 */

	@SuppressWarnings("serial")
	public Response all(PatientRegistrationViewModel viewModel) {
		Response response = new Response();
		try {
			// Condition to check whether where attribute is empty
			// To check for search criteria
			if (viewModel.where != null) {
				// for appending the where and the like for search criteria
				viewModel.where = helperUtil
						.ConstructWhereClauseFromJson(viewModel.where);
			}

			List<PatientRegistration> list = null;

			if (viewModel != null) {
				list = genericDAO.all(viewModel.entityObject,
						PatientRegistration.class, viewModel.where,
						viewModel.limit, viewModel.skip);
			}

			// Checking whether the list is empty or not
			if (null != list && list.size() > 0) {
				// Define the target type
				Type targetListType = new TypeToken<List<PatientRegistrationViewModel>>() {
				}.getType();
				// Mapping the list with the target view model
				List<PatientRegistrationViewModel> lists = modelMapper.map(
						list, targetListType);
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.fetchPatientdata");
				// appending the list to the response viewModels list to didplay
				// in the UI
				response.ViewModels = lists;
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.fetchPatientdata");
			}
		} catch (Exception e) {
			// error in the database or connection
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

	/*
	 * Implementation of interface IPatientRegistrationService findByCode method
	 * which is used to find an patinets using code(primary key).
	 * 
	 * @see vHMS.Facade.IPatientRegistrationService#findByCode(vHMS.Core.Models.
	 * PatientRegistrationViewModel
	 */

	public Response findByCode(PatientRegistrationViewModel viewModel)
			throws Exception {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {
			// To find where the record exists
			PatientRegistration patientexist = null;
			Map<String, Object> map = new HashMap<String, Object>();
			if (viewModel != null) {
				map.put("code", viewModel.code);
				// get the patient object by passing code
				patientexist = genericDAO.findOneByAttribute(
						CommonUtils.getMessage("PatientRegistration"), map,
						PatientRegistration.class);
			}
			if (null != patientexist) {
				// ModelMapper modelMapper = new ModelMapper();
				PatientRegistrationViewModel catData = modelMapper.map(
						patientexist, PatientRegistrationViewModel.class);
				response.success = true;
				// if the record exists bind to the view model
				response.ViewModel = catData;
				response.message = CommonUtils
						.getMessage("success.findPatientdata");
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.findPatientdata");
			}
		} catch (Exception e) {
			// return false if there is any database connection problem
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

	/*
	 * Implementation of interface IPatientRegistrationService update method
	 * which is used to update the IPatientRegistrationService details with the
	 * given code.
	 * 
	 * @see vHMS.Facade.IPatientRegistrationService#update(vHMS.Core.Models.
	 * PatientRegistrationViewModel )
	 */

	public Response update(PatientRegistrationViewModel viewModel)
			throws Exception {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {

			// Mapping the viewmodel to class of type
			PatientRegistration patient = modelMapper.map(viewModel,
					PatientRegistration.class);
			// getting the id from the view model for updating for the specific
			// id
			Map<String, Object> map = new HashMap<String, Object>();

			map.put("code", patient.getCode());

			// get the patient object by passing code
			PatientRegistration patientexist = genericDAO.findOneByAttribute(
					CommonUtils.getMessage("PatientRegistration"), map,
					PatientRegistration.class);

			if (patientexist != null) {

				// override the old properties with new properties exists in the
				// right side object
				
				patient.setPassword(patientexist.getPassword());
				notNull.copyProperties(patientexist, patient);
				// Calling the helper class to set the mandatory properties of
				// the
				// view model
				viewModel = helperUtil.AttachCommonFields(viewModel,
						PatientRegistrationViewModel.class);
				// Updating the record by ID
				genericDAO.updateById(viewModel.entityObject, patientexist,
						patientexist.getId());
				// ModelMapper modelmapper = new ModelMapper();
				PatientRegistrationViewModel patientdata = modelMapper.map(
						patientexist, PatientRegistrationViewModel.class);
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.updatePatientdata");
				// sending the response object as view model
				response.ViewModel = patientdata;
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.updatePatientdata");
			}
		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

	/*
	 * Implementation of interface IPatientRegistrationService
	 * findAllByAttributes method which fetches all the record with the given
	 * criteria.
	 * 
	 * @see
	 * vHMS.Facade.IPatientRegistrationService#findAllByAttributes(vHMS.Core
	 * .Models.PatientRegistrationViewModel
	 */
	@SuppressWarnings("serial")
	public Response findAllByAttributes(PatientRegistrationViewModel viewModel)
			throws Exception {
		// TODO Auto-generated method stub
		Response response = new Response();
		int count = 0;
		try {
			if (viewModel != null) {
				// ModelMapper mapper = new ModelMapper();
				// Converting MasterViewModel object to MasterEntity Pojo
				PatientRegistration catData = modelMapper.map(viewModel,
						PatientRegistration.class);
				Gson gson = new GsonBuilder().setDateFormat(
						"yyyy-MM-dd HH:mm:ss").create();
				// Converting MasterEntity to a JSON string
				String master = gson.toJson(catData);
				// Converting JON string to a HashMap
				Map<String, Object> map = gson.fromJson(master,
						new TypeToken<HashMap<String, Object>>() {
						}.getType());
				
				BasicDBList ORList = new BasicDBList();
				
				if (map.containsKey("firstName")) {
					DBObject firstName = new BasicDBObject("firstName", java.util.regex.Pattern.compile(
							(String) catData.getFirstName(),
							Pattern.CASE_INSENSITIVE));
					map.remove("firstName");
					ORList.add(firstName);
				}
				
				if (map.containsKey("middleName")) {
					DBObject middleName = new BasicDBObject("middleName", java.util.regex.Pattern.compile(
							(String) catData.getFirstName(),
							Pattern.CASE_INSENSITIVE));
					map.remove("middleName");
					ORList.add(middleName);
				}
				
				if (map.containsKey("lastName")) {
					DBObject lastName = new BasicDBObject("lastName", java.util.regex.Pattern.compile(
							(String) catData.getFirstName(),
							Pattern.CASE_INSENSITIVE));
					map.remove("lastName");
					ORList.add(lastName);
				}
				
					if(ORList.size()>0)
						{
							map.put("$or", ORList);
						}
				// Getting the list of entity for the criteria and all the
				// records
				List<PatientRegistration> list = genericDAO
						.findAllByAttributes(viewModel.entityObject, map,
								PatientRegistration.class, viewModel.limit,
								viewModel.skip);
				// ModelMapper modelmapper = new ModelMapper();
				// Define the target type
				Type targetListType = new TypeToken<List<PatientRegistrationViewModel>>() {
				}.getType();
				// Mapping the list with the target view model
				List<PatientRegistrationViewModel> lists = modelMapper.map(
						list, targetListType);
				
				// get count of records based on the filter

				if (viewModel.limit != null && null != list && Integer.parseInt(viewModel.limit) >= list.size()) {
						count = genericDAO.count(viewModel.entityObject, map,
								PatientRegistration.class);
				}
				else {
					count = list.size();
				}
				
				// Checking whether the list is empty or not
				if (null != lists && lists.size() > 0) {
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.fetchPatientdata");
					// appending the list to the response viewModels list to
					// didplay
					// in the UI
					response.ViewModels = lists;
					response.ViewModel = count;
				} else {
					// List is empty response is false
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.fetchPatientdata");
				}
			} else {
				// List is empty response is false
				response.success = false;
				response.message = CommonUtils.getMessage("error.Invalid");
			}
		} catch (Exception e) {
			// error in the database or connection
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

	/*
	 * Implementation of interface IPatientRegistrationService Authenticate
	 * method which is used to authenticate a patient with the given attributes.
	 * 
	 * @see
	 * vHMS.Facade.IPatientRegistrationService#Authenticate(vHMS.Core.ViewModels
	 * . PatientRegistrationViewModel)
	 */

	public Response Authenticate(PatientRegistrationViewModel viewModel)
			throws Exception {
		Response response = new Response();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			// storing email id of logging in user in map
			map.put("email", viewModel.email);
			map.put("password", CommonUtils.encryptor(viewModel.password));
			map.put("status", true);

			// Check if it is valid email by confirming its existence
			PatientRegistration patient = genericDAO.findOneByAttribute(
					CommonUtils.getMessage("PatientRegistration"), map,
					PatientRegistration.class);

			if (null != patient
					&& patient.getPassword().equals(
							CommonUtils.encryptor(viewModel.password))) {
				// ModelMapper modelmapper = new ModelMapper();
				PatientRegistrationViewModel patientViewModel = modelMapper
						.map(patient, PatientRegistrationViewModel.class);
				// setting Token
				patientViewModel.authToken = userAuthToken(patient);

				// setting passwd as empty,so it won't be there in respose
				patientViewModel.password = "";

				response.success = true;
				response.message = CommonUtils
						.getMessage("login.patient.success");
				response.ViewModel = patientViewModel;
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.patient.login");
			}
		} catch (Exception e) {
			// Handling Exception
			response.success = false;
			response.message = CommonUtils.getMessage("error.patient.login");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

	/*
	 * Implementation of interface IPatientRegistrationService changePassword
	 * method which is used to change the password when an User requests for the
	 * same.
	 * 
	 * @see
	 * vHMS.Facade.IPatientRegistrationService#changePassword(vHMS.Core.ViewModels
	 * . PatientRegistrationViewModel)
	 */

	public Response changePassword(PatientRegistrationViewModel viewModel)
			throws Exception {
		Response response = new Response();
		// Get the emailId
		Map<String, Object> map = new HashMap<String, Object>();

		// puuting encrypted password into Map
		map.put("password", CommonUtils.encryptor(viewModel.password));
		map.put("email", viewModel.email);
		try {
			// Check if it is valid email by confirming its existence
			PatientRegistration patientRegistration = genericDAO
					.findOneByAttribute(
							CommonUtils.getMessage("PatientRegistration"), map,
							PatientRegistration.class);

			// Encrypt the password to store
			String newPassword = CommonUtils.encryptor(viewModel.newPassword);

			// If exists and valid, modify the password and send email
			if (null != patientRegistration) {

				// Update user password
				patientRegistration.setPassword(newPassword);
				genericDAO.updateById(
						CommonUtils.getMessage("PatientRegistration"),
						patientRegistration, patientRegistration.getId());

				response.success = true;
				response.message = CommonUtils
						.getMessage("success.passwordsuccess");
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.passworderror");
			}
		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils
					.getMessage("error.processfailurechangepwd");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}

	/*
	 * Implementation of interface IPatientRegistrationService forgotPassword
	 * method which is used to get back user's password if User forgot his
	 * password.
	 * 
	 * @see
	 * vHMS.Facade.IPatientRegistrationService#forgotPassword(vHMS.Core.ViewModels
	 * . PatientRegistrationViewModel)
	 */

	public Response forgotPassword(PatientRegistrationViewModel viewModel)
			throws Exception {
		Response response = new Response();
		// Get the emailId
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("email", viewModel.email);
		try {
			// Check if it is valid email by confirming its existence
			PatientRegistration patientRegistration = genericDAO
					.findOneByAttribute(
							CommonUtils.getMessage("PatientRegistration"), map,
							PatientRegistration.class);

			// com.fasterxml.jackson.databind.node.ObjectNode result =
			// Json.newObject();
			// If exists and valid, modify the password and send email
			if (null != patientRegistration) {
				// Generate random password
				String randomPassword = RandomStringUtils.randomAlphanumeric(6);
				// Encrypt the password to store
				String newPassword = CommonUtils.encryptor(randomPassword);

				// Create a mail message t be send.
				// NotificationMessage message = new NotificationMessage();
				NotificationViewModel message = new NotificationViewModel();
				ResponseViewModel responseViewModel = new ResponseViewModel();

				message.setTo(patientRegistration.getEmail());
				message.setFrom(CommonUtils.getMessage("from"));
				message.setSubject(CommonUtils.getMessage("subject"));
				// Get the email body content from the scala temapltes
				String body = views.html.emailtemplates.forgotpassword.render(
						patientRegistration.getFirstName(),
						patientRegistration.getEmail(), randomPassword).body();

				message.setBody(body);

				// defaultEmailSender.send(message);

				// Update user password
				patientRegistration.setPassword(newPassword);
				genericDAO.updateById(
						CommonUtils.getMessage("PatientRegistration"),
						patientRegistration, patientRegistration.getId());

				responseViewModel.notificationViewModel = message;
				response.success = true;
				response.message = CommonUtils.getMessage("success.emailSent");
				response.ViewModel = responseViewModel;

			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.notregistered");
			}
		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}

	/*
	 * userAuthToken is a private method which is used to generate JWT token.
	 */
	private String userAuthToken(PatientRegistration patientRegistration)
			throws Exception {
		if (patientRegistration != null) {
			// create a map and put the unique information for generating the
			// token.
			Map<String, Object> claims = new HashMap<String, Object>();
			claims.put("uid", patientRegistration.getId());
			claims.put("userName", patientRegistration.getFirstName());
			claims.put("userEmail", patientRegistration.getEmail());
			return HelperUtil.GenerateAuthToken(claims);
		}
		return null;
	}

}
