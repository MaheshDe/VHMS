package vHMS.Facade;

import vHMS.Core.ViewModels.OPRegistrationViewModel;
import vHMS.Core.ViewModels.Response;

/**
 * The IOPRegistrationService is an interface containing methods like create,
 * update delete etc.. related to patient management, and can be implemented by
 * other classes.
 * 
 * @author DivyaP
 * @version 1.0
 * @since 2015-07-17
 */
public interface IOPRegistrationService {

	/*
	 * Implementation of interface IOPRegistrationService create method which is
	 * used to create/save a PatientManagement.
	 * 
	 * @see vHMS.Facade.IOPRegistrationService#create(vHMS.Core.ViewModels.
	 * OPRegistrationViewModel)
	 */
	public Response create(OPRegistrationViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IOPRegistrationService all method which
	 * returns a list of allPatients.
	 * 
	 * @see vHMS.Facade.IOPRegistrationService#all(vHMS.Core.ViewModels.
	 * OPRegistrationViewModel)
	 */
	public Response all(OPRegistrationViewModel viewModel);

	/*
	 * Implementation of interface IOPRegistrationService findById method which
	 * is used to find a Patient using id(primary key).
	 * 
	 * @see vHMS.Facade.IOPRegistrationService#findById(vHMS.Core.ViewModels.
	 * OPRegistrationViewModel) )
	 */
	public Response findByCode(OPRegistrationViewModel viewModel)
			throws Exception;

	/*
	 * Implementation of interface IOPRegistrationService findAllByAttributes
	 * method which is used to find the Patient details with the given id.
	 * 
	 * @see
	 * vHMS.Facade.IOPRegistrationService#findAllByAttributes(vHMS.Core.ViewModels
	 * .OPRegistrationViewModel)
	 */

	public Response findAllByAttributes(OPRegistrationViewModel viewModel)
			throws Exception;

}
