package vHMS.Facade;

import java.util.HashMap;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vHMS.Core.Models.User;
import vHMS.Core.ViewModels.Response;
import vHMS.Core.ViewModels.UserViewModel;
import Base.Data.IGenericMongoDao;
import Base.Utilities.CommonUtils;
import Base.Utilities.HelperUtil;

/**
 * The AccountService is the implementation for the interface IAccountService.
 * 
 * @author DivyaP
 * @since 2015-07-21
 */

@Service
@Transactional
public class AccountService implements IAccountService {

	@Autowired
	private IGenericMongoDao genericDAO;

	/*
	 * Implementation of interface IAccountService AuthenticateUser method which
	 * is used to authenticate a user with the given attributes.
	 * 
	 * @see vHMS.Facade.IAccountService#AuthenticateUser(vHMS.Core.ViewModels.
	 * UserViewModel)
	 */

	@Override
	public Response AuthenticateUser(UserViewModel viewModel) throws Exception {
		Response response = new Response();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			// storing email id of logging in user in map
			map.put("email", viewModel.email);
			map.put("password", CommonUtils.encryptor(viewModel.password));
			map.put("status", true);

			// Check if it is valid email by confirming its existence
			User user = genericDAO.findOneByAttribute(
					CommonUtils.getMessage("User"), map, User.class);

			if (null != user
					&& user.getPassword().equals(
							CommonUtils.encryptor(viewModel.password))) {
				ModelMapper modelmapper = new ModelMapper();
				UserViewModel userViewModel = modelmapper.map(user,
						UserViewModel.class);
				// setting Token
				userViewModel.authToken = userAuthToken(user);

				// setting passwd as empty,so it won't be there in respose
				userViewModel.password = "";

				response.success = true;
				response.message = CommonUtils.getMessage("login.user.success");
				response.ViewModel = userViewModel;
			} else {
				response.success = false;
				response.message = CommonUtils.getMessage("error.user.login");
			}
		} catch (Exception e) {
			// Handling Exception
			response.success = false;
			response.message = CommonUtils.getMessage("error.user.login");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

	/*
	 * userAuthToken is a private method which is used to generate JWT token.
	 */

	private String userAuthToken(User userInfo) throws Exception {
		if (userInfo != null) {
			// create a map and put the unique information for generating the
			// token.
			Map<String, Object> claims = new HashMap<String, Object>();
			claims.put("uid", userInfo.getId());
			claims.put("userName", userInfo.getFirstName());
			claims.put("userEmail", userInfo.getEmail());
			return HelperUtil.GenerateAuthToken(claims);
		}
		return null;
	}

}
