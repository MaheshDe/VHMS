package vHMS.Facade;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

import vHMS.Core.Models.AuditEventStore;
import vHMS.Core.Models.Document;
import vHMS.Core.Models.MasterEntity;
import vHMS.Core.Models.PatientRegistration;
import vHMS.Core.ViewModels.AuditEventStoreViewModel;
import vHMS.Core.ViewModels.DocumentDataViewModel;
import vHMS.Core.ViewModels.DocumentViewModel;
import vHMS.Core.ViewModels.Response;
import Base.Data.IGenericMongoDao;
import Base.Storage.IDocumentDao;
import Base.Utilities.CommonUtils;
import Base.Utilities.HelperUtil;

/**
 * @author Arokia Felsi For document upload
 * 
 */
@Service
@Transactional
public class DocumentMongoGridFsService implements IDocumentService {

	// Created GenericDAO instance using DI
	@Autowired
	private IDocumentDao documentDAO;

	// Created GenericDAO instance using DI
	@Autowired
	private IGenericMongoDao genericDAO;

	// Created AuditEventStore instance.
	private AuditEventStoreViewModel auditTrailViewModel = new AuditEventStoreViewModel();

	// Created HelperUtil instance.
	private HelperUtil helperUtil = new HelperUtil();

	// Created ModelMapper instance.
	ModelMapper modelMapper = new ModelMapper();

	// Created Request Data instance.
	DocumentDataViewModel responseViewModel = new DocumentDataViewModel();

	/*
	 * Implementation of interface IDocumentService load method which is invoked
	 * for uplaoding a document.
	 * 
	 * @see BPM.Facade.IDocumentService#load()
	 */
	
	public Response load(DocumentViewModel viewModel) throws Exception {
		Response response = new Response();

		PatientRegistration patientData = null;
		String bucket = null;
		try {
			viewModel = genericDAO.loadFileToGridFs(viewModel);
			// Calling the helper class to set the mandatory properties of
			// the
			// view model
			Map<String, Object> map = new HashMap<String, Object>();
			viewModel = helperUtil.AttachCommonFields(viewModel,
					DocumentViewModel.class);
			/*ModelMapper modelMapper = new ModelMapper();
			Document document = modelMapper.map(viewModel, Document.class);
			document.setId(viewModel.gridfsObject.toString());*/
			ModelMapper modelMapper = new ModelMapper();
			Document document = modelMapper.map(viewModel, Document.class);
			Object[] result = genericDAO.save(
					CommonUtils.getMessage("Document"), document);
			for (Object tp : result) {
				String id = String.valueOf(tp);
				document.setId(id.toString());
			}
			/*responseViewModel.documentViewModel = modelMapper.map(
					document, DocumentViewModel.class);*/
			if(viewModel.code != null){
				map.clear();
				map.put("code", viewModel.requestCode);
				patientData = genericDAO.findOneByAttribute(
						CommonUtils.getMessage("PatientRegistration"), map,
						PatientRegistration.class);
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.document.upload");	
				response.ViewModel = modelMapper.map(
						document, DocumentViewModel.class);
				
			}else{
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.document.upload");	
			}
		//	if (response.success) {
				// Set the auidtrail details for audittrail aop to be
				// activated
				// Set the auidtrail details for audittrail aop to be
				// activated
				/*AuditEventStore auditEvent = modelMapper.map(
						auditTrailViewModel, AuditEventStore.class);

				auditEvent.setEntityId(patientData.getId());
				auditEvent.setAuditEventDescription(CommonUtils
						.getMessage("AuditDocument")
						+ "("
						+ document.getFileName()
						+ ")"
						+ " "
						+ CommonUtils.getMessage("uploaded"));
				auditEvent.setEntityDescription(CommonUtils
						.getMessage("document.upload"));
				helperUtil.AttachCommonFields(auditEvent,
						AuditEventStore.class);
				responseViewModel.auditTrailViewModel = modelMapper.map(
						auditEvent, AuditEventStoreViewModel.class);*/

		//	}
			// storing the file details for the google storage in a map
			
			/*map.put("type", viewModel.contentType);
			map.put("fileName", viewModel.fileName);
			map.put("file", viewModel.LoadedFile);*/

			//if (null != viewModel.requestType) {

				//String val = viewModel.requestType;
				// converting the request type to lower case and remove
				// whitespace
				//val = val.replaceAll("[^A-Za-z]+", "").toLowerCase();
				// getting the bucket name if exists or null
				//bucket = documentDAO.getBucket(val);

				/*if (bucket == null) {
					// if bucket name not exists create a new bucket
					bucket = documentDAO.tryCreateBucket(val);
				}
*/
				// Set projection for the bucket
				//documentDAO.setBucketProjection(bucket);
				//viewModel.bucket = bucket;
				// Returns document object with the values with the key and
				// storage
				// inputs
				//viewModel = documentDAO.load(viewModel);

				// Calling the helper class to set the mandatory properties of
				// the
				// view model
				//viewModel = helperUtil.AttachCommonFields(viewModel,
						//DocumentViewModel.class);

				//ModelMapper modelMapper = new ModelMapper();
				// Mapping the document and model to create a document table
				//Document document = modelMapper.map(viewModel, Document.class);
				// Create a new document entry record for each document upload.
				/*Object[] result = genericDAO.save(
						CommonUtils.getMessage("Document"), document);
				for (Object tp : result) {
					String id = String.valueOf(tp);
					document.setId(id.toString());
				}
				if (null != document) {
					map.clear();
					map.put("Code", viewModel.code);
					patientData = genericDAO.findOneByAttribute(
							CommonUtils.getMessage("PatientManagement"), map,
							PatientManagement.class);
					responseViewModel.documentViewModel = modelMapper.map(
							document, DocumentViewModel.class);
					// Returns the response true if document contains values
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.document.upload");
					response.ViewModel = document;
				} else {
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.document.upload");
				}*/
		//	}

		} catch (Exception e) {
			// Handling exception
			response.success = false;
			response.message = CommonUtils
					.getMessage("error.document.upload.failure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
/*		if (response.success && null != responseViewModel)
			response.ViewModel = responseViewModel;*/
	
		return response;
	}

	/*
	 * Implementation of interface IDocumentService download method which is
	 * invoked for downloading a document.
	 * 
	 * @see BPM.Facade.IDocumentService#download()
	 */
	
	public Response download(DocumentViewModel viewModel) throws Exception {
		Response response = new Response();

		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("code", viewModel.code);
			map.put("requestType", viewModel.requestType);
			Document document = genericDAO.findOneByAttribute(
					CommonUtils.getMessage("Document"), map, Document.class);
			// check whether the document exists for the code
			if(document != null){
				viewModel = genericDAO.downloadFileFromGridFs(viewModel);
				
				if(viewModel.status.equalsIgnoreCase("true") && viewModel != null){
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.document.download");
					// if the record exists bind to the view model
					response.ViewModel = viewModel;
				}else{
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.document.download");
				}	
			}else{
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.document.download");	
			}
			
			//Document document = genericDAO.findOneByAttribute(
					//CommonUtils.getMessage("Document"), map, Document.class);
			// If exists download the document from google storage
			/*if (null != document) {
				// Checking whether the request type is null
				if (null != viewModel.requestType) {

					String val = viewModel.requestType;
					// converting the request type to lower case and remove
					// whitespace
					val = val.replaceAll("[^A-Za-z]+", "").toLowerCase();
					// getting the bucket name
					viewModel.bucket = documentDAO.getBucket(val);

					if (null != viewModel.bucket) {

						DocumentViewModel view = documentDAO
								.retrieve(viewModel);
						// if the key does not check null and make the viewModel
						// null
						if (null == viewModel.contentType
								&& null == viewModel.fileName
								&& null == viewModel.LoadedFile)
							viewModel = null;

						if (view != null) {
							response.success = true;
							response.message = CommonUtils
									.getMessage("success.document.download");
							// if the record exists bind to the view model
							response.ViewModel = view;
						} else {
							response.success = false;
							response.message = CommonUtils
									.getMessage("error.download.entry.document");
						}

					} else {
						response.success = false;
						response.message = CommonUtils
								.getMessage("error.document.download");
					}
				}
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.document.download");
			}*/
		} catch (Exception e) {
			// return false if there is any database connection problem
			response.success = false;
			response.message = CommonUtils
					.getMessage("error.document.download.failure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

	/*
	 * Implementation of interface IDocumentService download method which is
	 * invoked for downloading a document.
	 * 
	 * @see BPM.Facade.IDocumentService#download()
	 */
	
	public Response delete(DocumentViewModel viewModel) throws Exception {
		Response response = new Response();
		PatientRegistration patientData = null;
		Document document= null;
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("code", viewModel.code);
			map.put("requestType", viewModel.requestType);
			document = genericDAO.findOneByAttribute(CommonUtils.getMessage("Document"), map, Document.class);
			if(document != null){
             viewModel = genericDAO.deleteFileFromGridFs(viewModel);
				
				if(viewModel.status.equalsIgnoreCase("true")){
					genericDAO.delete(
							CommonUtils.getMessage("Document"),
							document.getId(), Document.class);

					response.success = true;
					response.message = CommonUtils
							.getMessage("success.document.delete");
					
					responseViewModel.documentViewModel = modelMapper.map(document, DocumentViewModel.class);
				}else{
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.document.delete");	
				}	
			}else{
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.document.delete");	

			}
				
			
			
			
			
			// check whether the document exists for the code
			/*Document document = genericDAO.findOneByAttribute(
					CommonUtils.getMessage("Document"), map, Document.class);
			// If exists, delete the document from google storage
			if (null != document) {

				if (null != viewModel.requestType) {

					String val = viewModel.requestType;
					// converting the request type to lower case and remove
					// whitespace
					val = val.replaceAll("[^A-Za-z]+", "").toLowerCase();
					// getting the bucket name
					viewModel.bucket = documentDAO.getBucket(val);

					if (null != viewModel.bucket) {

						// For getting the requestId for audit info
						viewModel.id = document.getId();
						map.clear();
						map.put("Code", document.getRequestCode());
						patientData = genericDAO.findOneByAttribute(
								CommonUtils.getMessage("Request"), map,
								PatientRegistration.class);

						DocumentViewModel view = documentDAO.delete(viewModel);
						if (view.status == "1") {
							genericDAO.delete(
									CommonUtils.getMessage("Document"),
									viewModel.id, Document.class);

							response.success = true;
							response.message = CommonUtils
									.getMessage("success.document.delete");
							responseViewModel.documentViewModel = view;
						} else {
							response.success = false;
							response.message = CommonUtils
									.getMessage("error.delete.entry.document");
						}

						// if the record exists bind to the view model

					} else {
						response.success = false;
						response.message = CommonUtils
								.getMessage("error.document.delete");
					}
				} else {
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.document.delete.norequesttype");
				}
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.document.delete.nodocumentdata");
			}*/
			/*if (response.success) {
				// Set the auidtrail details for audittrail aop to be
				// activated
			map.clear();
				map.put("requestCode", document.getRequestCode());
				patientData = genericDAO.findOneByAttribute(
						CommonUtils.getMessage("PatientRegistration"), map,
						PatientRegistration.class);
				AuditEventStore auditEvent = modelMapper.map(
						auditTrailViewModel, AuditEventStore.class);
				// If entity id is null set it from the request details.
				if (null == auditEvent.getEntityId())
					auditEvent.setEntityId(patientData.getId());
				// auditEvent.setJSON(requestData.getJSON());
				auditEvent.setAuditEventDescription(CommonUtils
						.getMessage("delete"));
				auditEvent.setEntityDescription(CommonUtils
						.getMessage("document.delete"));
				helperUtil
						.AttachCommonFields(auditEvent, AuditEventStore.class);
				responseViewModel.auditTrailViewModel = modelMapper.map(
						auditEvent, AuditEventStoreViewModel.class);
			}*/

		} catch (Exception e) {
			// return false if there is any database connection problem
			response.success = false;
			response.message = CommonUtils
					.getMessage("error.document.delete.failure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		if (response.success && null != responseViewModel)
			response.ViewModel = responseViewModel;
		return response;
	}

	/*
	 * Implementation of interface IDocumentService list method which is invoked
	 * for getting the document list for a request.
	 * 
	 * @see BPM.Facade.IDocumentService#download()
	 */
	
	public Response list(DocumentViewModel viewModel) throws Exception {
		Response response = new Response();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("RequestCode", viewModel.requestCode);
			// check whether the document exists for the code
			List<Document> documentlist = genericDAO.findAllByAttributes(
					CommonUtils.getMessage("Document"), map, Document.class,
					null, null);
			// If exists, delete the document from google storage
			if (null != documentlist && documentlist.size() > 0) {
				Type targetListType = new TypeToken<List<DocumentViewModel>>() {
				}.getType();
				// Mapping the list with the target view model
				List<DocumentViewModel> list = modelMapper.map(documentlist,
						targetListType);
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.document.fetchall");
				response.ViewModels = list;

			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.document.fetchall");
			}
		} catch (Exception e) {
			// return false if there is any database connection problem
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
		}

		return response;
	}

	@Override
	public Response findAllByAttributes(DocumentViewModel viewModel)
			throws Exception {
		Response response = new Response();
		try {
			// Converting DocumentViewModel object to DocumentEntity Pojo
						Document data = modelMapper
							.map(viewModel, Document.class);
					Gson gson = new Gson();
					// Converting DocumentEntity to a JSON string
					String master = gson.toJson(data);
					// Converting JON string to a HashMap
					Map<String, Object> map = new Gson().fromJson(master,
							new TypeToken<HashMap<String, Object>>() {
							}.getType());
					
					if (viewModel.where != null) {
						// for appending the where and the like for search criteria
						viewModel.where = helperUtil
								.ConstructWhereClauseFromJson(viewModel.where);
					}
					// Fetch the Entity based on the search criteria
					
					List<Document> allDocumentEntity = genericDAO.findAllByAttributes(viewModel.entityObject, map, Document.class, viewModel.limit, viewModel.skip);

					
						// Returns true if there is a record matching the criteria
						if (null != allDocumentEntity && allDocumentEntity.size() > 0) {
							response.success = true;
							response.message = CommonUtils
									.getMessage("success.entity.Criteria.search");
							response.ViewModels = allDocumentEntity;
						} else {
							response.success = false;
							response.message = CommonUtils
									.getMessage("error.entity.Criteria.search");
						}
			
			}catch (Exception e) {
			// return false if there is any database connection problem
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
		}
		return response;
	}
}