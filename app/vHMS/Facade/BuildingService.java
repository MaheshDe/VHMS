package vHMS.Facade;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vHMS.Core.Models.Building;
import vHMS.Core.ViewModels.BuildingViewModel;
import vHMS.Core.ViewModels.Response;
import Base.Data.IGenericMongoDao;
import Base.Utilities.CommonUtils;
import Base.Utilities.HelperUtil;
import Base.Utilities.NullAwareBeanUtilsBean;
import Base.Utilities.Email.SmtpSender;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;


/**
 * The BuildingService is the implementation for the interface IBuildingService.
 * 
 * @author MaheshDe
 * @since 2015-08-19
 */

@Service
@Transactional
public class BuildingService implements IBuildingService {

	@Autowired
	private IGenericMongoDao genericDAO;

	@Autowired
	private SmtpSender defaultEmailSender;
	// Created HelperUtil instance.
	private HelperUtil helperUtil = new HelperUtil();
	// Created ModelMapper instance
	private ModelMapper modelmapper = new ModelMapper();
	// Created NullAwareBeanUtilsBean instance.
	private BeanUtilsBean notNull = new NullAwareBeanUtilsBean();
	
	
	/**
	 * Implementation of interface IBuldingService all method which returns a
	 * list of building data.
	 * 
	 *  @see vHMS.Facade.IBuldingService#all(vHMS.Core.Models.
	 * BuildingViewModel
	 */
	
	@Override
	public Response all(BuildingViewModel viewModel)
			throws JsonProcessingException, IOException {
		// TODO Auto-generated method stub
		
		Response response = new Response();
		
		// Fetch all the records from the table.
		List<Building> allBuilding = null;

		if (viewModel.where != null) {
			// for appending the where and the like for search criteria
			viewModel.where = helperUtil
					.ConstructWhereClauseFromJson(viewModel.where);
		}
		allBuilding = genericDAO.all(CommonUtils.getMessage("Building"), Building.class,
				viewModel.where, viewModel.limit, viewModel.skip);
		try {
				// to check whether there is a list of entity returns true response
				if (null != allBuilding && allBuilding.size() > 0) {
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.bilding.Criteria.search");
					response.ViewModels = allBuilding;
				} else {// Returns false response
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.building.Criteria.search");
			}

		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}
	
	
	
	/**
	 * Implementation of interface IBuldingService create method which
	 * saves/create a building data.
	 * 
	 *@see vHMS.Facade.IBuldingService#create(vHMS.Core.Models.
	 * BuildingViewModel
	 */
	
	@Override
	public Response create(BuildingViewModel viewModel) throws Exception {
		// TODO Auto-generated method stub
		
		Response response = new Response();
		Building buildingExist = null;
		String id = null;
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			if(viewModel != null)
			{
				map.put("name", viewModel.name);
			
				buildingExist = genericDAO.findOneByAttribute(
						CommonUtils.getMessage("Building"), map,
						Building.class);
				
				if(buildingExist != null)
				{
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.building.exists");
				}
				else
				{
					// Calling the helper class to set the mandatory properties of
					// the view model
					viewModel = helperUtil.AttachCommonFields(viewModel,
							BuildingViewModel.class);
					
					
					//  convert viewmodel object to model object before saving it
					// to DB
					Building building = modelmapper.map(viewModel, Building.class);
				
					// Create the new record in table.
					Object[] obj =genericDAO.save(CommonUtils.getMessage("Building"), building);
					
					for (Object objId : obj) 
					{
						id = String.valueOf(objId);
						building.setId(id);
					}
					
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.building.creation");
					response.ViewModel = modelmapper.map(building,
							BuildingViewModel.class); ;
				}
			}
			
		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();

		}
		return response;
	}
	
	
	/**
	 * Implementation of interface IBuldingService findById method which search a
	 * building data with the given code.
	 * 
	 * @see vHMS.Facade.IBuldingService#findById(vHMS.Core.Models.
	 * BuildingViewModel
	 */
	
	@Override
	public Response findById(BuildingViewModel viewModel) throws Exception {
		// TODO Auto-generated method stub
		
		Response response = new Response();

		try {
				Map<String, Object> map = new HashMap<String, Object>();
				Building building = null;
				
				if(viewModel != null)
				{
					map.put("code", viewModel.code);
					
					// Fetch the record by code.
					building = genericDAO.findOneByAttribute(
							CommonUtils.getMessage("Building"), map, Building.class);
					
					if(building != null)
					{
						
						BuildingViewModel buildingViewModel = modelmapper.map(building,
								BuildingViewModel.class);
						
						response.success = true;
						response.message = CommonUtils
								.getMessage("success.building.Criteria.search");
						response.ViewModel = buildingViewModel;
					}
					else 
					{
						response.success = false;
						response.message = CommonUtils
							.getMessage("error.building.Criteria.search");
					}
				}

			} catch (Exception e) {
				response.success = false;
				response.message = CommonUtils.getMessage("error.processfailure");
				response.ExceptionMessage = e.getMessage();
			}
		return response;
	}
	
	
	/**
	 * Implementation of interface IBuldingService count method which returns
	 * number of building data present in a particular table.
	 * 
	 *  @see vHMS.Facade.IBuldingService#count(vHMS.Core.Models.
	 *  BuildingViewModel
	 */
	
	@Override
	public Response count(BuildingViewModel viewModel) {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {
			// Condition to check whether where attribute is empty to check for
			// search criteria
			if (viewModel.where != null) {
				// for appending the where and the like for search criteria
				viewModel.where = helperUtil
						.ConstructWhereClauseFromJson(viewModel.where);
			}
			// Count of the record in the list or serach by criteria
			int count = genericDAO.count(CommonUtils.getMessage("Building"), Building.class);
			// adding count to the json object
			response.data.put("count", count);
			response.success = true;
			response.message = CommonUtils
					.getMessage("success.countMasterdata");
		} catch (Exception e) {
			// Return false if it ia an error
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}
	
	
	/**
	 * Implementation of interface IBuldingService delete method which deletes a
	 * building data for the given code.
	 * 
	 *  @see vHMS.Facade.IBuldingService#delete(vHMS.Core.Models.BuildingViewModel
	 */
	
	@Override
	public Response delete(BuildingViewModel viewModel) {
		// TODO Auto-generated method stub
		
		Response response = new Response();
		try {

			Map<String, Object> map = new HashMap<String, Object>();
			
			if(viewModel != null)
			{
				map.put("code", viewModel.code);
				
				// Fetch the record by code.
				Building building = genericDAO.findOneByAttribute(
						CommonUtils.getMessage("Building"), map, Building.class);

				// if record found then delete record and returns true
				if (building != null) 
				{
					// Deleting the user with the code
					genericDAO.delete(CommonUtils.getMessage("Building"), building.getId(),
							Building.class);
				
					BuildingViewModel buildingData = modelmapper.map(building,
							BuildingViewModel.class);
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.building.data.deletion");
					response.ViewModel = buildingData;
					// response.status = true;

				} else {
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.building.not.exists");
				}
			}
		} catch (Exception e) {

			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}
	
	
	/**
	 * Implementation of interface IBuldingService update method which updates a
	 * building data for the given code.
	 * 
	 *  @see vHMS.Facade.IBuldingService#update(vHMS.Core.Models.
	 *  BuildingViewModel
	 */
	
	@Override
	public Response update(BuildingViewModel viewModel) {
		// TODO Auto-generated method stub
		
		Response response = new Response();
		try {
			
			// Mapping the viewmodel to class of type
			Building building = modelmapper.map(viewModel,
					Building.class);
			
			// getting the id from the view model for updating for the specific
			// id
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("code", viewModel.code);
			
			// Check by code whether the record exists before update.
			Building buildingExist = genericDAO.findOneByAttribute(CommonUtils.getMessage("Building"), map, Building.class);
			
			if(buildingExist != null )
			{
					// override the old properties with new properties exists in the
					// right side object
					notNull.copyProperties(buildingExist, building);
					
					// Calling the helper class to set the mandatory properties of
					// the view model
					viewModel = helperUtil.AttachCommonFields(viewModel,
							BuildingViewModel.class);
					
					// Update the existing record with new values.
					genericDAO.updateById(CommonUtils.getMessage("Building"), buildingExist,
							buildingExist.getId());
					
					Building updatedBuilding = genericDAO.findOneById(
							CommonUtils.getMessage("Building"), buildingExist.getId(),
							Building.class);
					
					response.success = true;
					response.ViewModel = modelmapper.map(updatedBuilding,
							BuildingViewModel.class);
					response.message = CommonUtils
							.getMessage("success.update.building");	
			}
			else
			{
				response.success = false;
				response.message = CommonUtils
					.getMessage("error.building.exists");
			} 
			
		} catch (Exception e) {
			// Exception Handling
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}
	
	
	
	/**
	 * Implementation of interface IBuldingService findAllByAttributes method
	 * which returns a list of building data based on search criteria.
	 * 
	 * @see vHMS.Facade.IBuldingService#findAllByAttributes(vHMS.Core.Models.
	 * BuildingViewModel
	 */
	
	@Override
	public Response findAllByAttributes(BuildingViewModel viewModel)
			throws Exception {
		// TODO Auto-generated method stub
		
		Response response = new Response();
		try {
			
			// Converting UserScheduleViewModel object to UserScheduleEntity Pojo
			Building building = modelmapper.map(viewModel, Building.class);
			Gson gson = new Gson();
			
			// Converting UserScheduleEntity to a JSON string
			String master = gson.toJson(building);
			
			// Converting JSON string to a HashMap
			Map<String, Object> map = new Gson().fromJson(master,
					new TypeToken<HashMap<String, Object>>() {
					}.getType());

			if (viewModel.where != null) {
				// for appending the where and the like for search criteria
				viewModel.where = helperUtil
						.ConstructWhereClauseFromJson(viewModel.where);
			}
			// Fetch the Entity based on the search criteria
			List<Building> allBuilding = genericDAO.findAllByAttributes(
					CommonUtils.getMessage("Building"), map, Building.class, viewModel.limit,
					viewModel.skip);

			// Returns true if there is a record matching the criteria
			if (null != allBuilding && allBuilding.size() > 0) {
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.bilding.Criteria.search");
				response.ViewModels = allBuilding;
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.building.Criteria.search");
			}

		} catch (Exception e) {
			// Handling Exception
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			// response.status = false;
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}

	
}