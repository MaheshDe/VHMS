package vHMS.Facade;

import java.io.IOException;

import vHMS.Core.ViewModels.DoctorViewModel;
import vHMS.Core.ViewModels.Response;
import vHMS.Core.ViewModels.UserViewModel;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface IUserService {

	/*
	 * Implementation of interface IUserService load method which is invoked for
	 * fetching the user list.
	 * 
	 * @see vHMS.Facade.IUserService#all()
	 */
	public Response all(UserViewModel viewModel)
			throws JsonProcessingException, IOException;

	/*
	 * Implementation of interface IUserService create method which is invoked
	 * for creating the user.
	 * 
	 * @see vHMS.Facade.IUserService#create()
	 */
	public Response create(UserViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IUserService findById method which is invoked
	 * for finding/getting a user by id.
	 * 
	 * @see vHMS.Facade.IUserService#findById()
	 */
	public Response findById(UserViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IUserService count method which is invoked
	 * for getting the count of matching records.
	 * 
	 * @see vHMS.Facade.IUserService#count()
	 */
	public Response count(UserViewModel viewModel);

	/*
	 * Implementation of interface IUserService delete method which is invoked
	 * for deleting a user.
	 * 
	 * @see vHMS.Facade.IUserService#delete()
	 */
	public Response delete(UserViewModel viewModel);

	/*
	 * Implementation of interface IUserService update method which is invoked
	 * for updating a user data.
	 * 
	 * @see vHMS.Facade.IUserService#update()
	 */
	public Response update(UserViewModel viewModel);

	/*
	 * Implementation of interface IUserService forgotPassword method which is
	 * invoked for reseting the password.
	 * 
	 * @see vHMS.Facade.IUserService#forgotPassword()
	 */
	public Response forgotPassword(UserViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IUserService changePassword method which is
	 * invoked for changing the password.
	 * 
	 * @see vHMS.Facade.IUserService#changePassword()
	 */
	public Response changePassword(UserViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IUserService findAllByAttributes method which
	 * is invoked for finding the records/masters which satisfies the given
	 * criteria.
	 * 
	 * @see vHMS.Facade.IUserService#findAllByAttributes()
	 */
	public Response findAllByAttributes(UserViewModel viewModel)
			throws Exception;
	/*
	 * Implementation of interface IUserService updateUserbyType method which
	 * is invoked for update the records/users  
	 * criteria.
	 * 
	 * @see vHMS.Facade.IUserService#updateUserbyType()
	 */
	public Response updateUserbyType(UserViewModel viewModel,DoctorViewModel doctorViewModel) throws Exception;
	/*
	 * Implementation of interface IUserService createUserbyType method which
	 * is invoked for create the records/users 
	 * 
	 * 
	 * @see vHMS.Facade.IUserService#createUserbyType()
	 */
	public Response createUserbyType(UserViewModel viewModel,DoctorViewModel doctorViewModel) throws Exception;
	/*
	 * Implementation of interface IUserService findUserDetailById method which
	 * is invoked for finding the records/users which satisfies the given
	 * criteria.
	 * 
	 * @see vHMS.Facade.IUserService#findUserDetailById()
	 */
	public Response findUserDetailById(UserViewModel viewModel) throws Exception;

}
