package vHMS.Facade;

import java.io.IOException;

import vHMS.Core.ViewModels.MasterViewModel;
import vHMS.Core.ViewModels.Response;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * The IMasterService is an interface containing methods like create, update
 * delete etc.. related to master management, and can be implemented by other
 * classes.
 * 
 * @author DivyaP
 * @version 1.0
 * @since 2015-08-10
 */
public interface IMasterService {
	/*
	 * Implementation of interface IMasterService load method which is invoked
	 * for fetching the master list.
	 * 
	 * @see vHMS.Facade.IMasterService#all()
	 */
	public Response all(MasterViewModel viewModel)
			throws JsonProcessingException, IOException, Exception;

	/*
	 * Implementation of interface IMasterService create method which is invoked
	 * for creating the master.
	 * 
	 * @see vHMS.Facade.IMasterService#create()
	 */
	public Response create(MasterViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IMasterService findById method which is
	 * invoked for finding/getting a master by id.
	 * 
	 * @see vHMS.Facade.IMasterService#findById()
	 */
	public Response findById(MasterViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IMasterService count method which is invoked
	 * for getting the count of matching records.
	 * 
	 * @see vHMS.Facade.IMasterService#count()
	 */
	public Response count(MasterViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IMasterService delete method which is invoked
	 * for deleting a master.
	 * 
	 * @see vHMS.Facade.IMasterService#delete()
	 */
	public Response delete(MasterViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IMasterService update method which is invoked
	 * for updating a master data.
	 * 
	 * @see vHMS.Facade.IMasterService#update()
	 */
	public Response update(MasterViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IMasterService findAllByAttributes method
	 * which is invoked for finding the records/masters which satisfies the
	 * given criteria.
	 * 
	 * @see vHMS.Facade.IMasterService#findAllByAttributes()
	 */
	public Response findAllByAttributes(MasterViewModel viewModel)
			throws Exception;

}
