package vHMS.Facade;

import java.io.IOException;

import vHMS.Core.ViewModels.FloorViewModel;
import vHMS.Core.ViewModels.Response;
import com.fasterxml.jackson.core.JsonProcessingException;




public interface IFloorService {

	/*
	 * Implementation of interface IFloorService load method which is invoked for
	 * fetching the floor list.
	 * 
	 * @see vHMS.Facade.IFloorService#all()
	 */
	public Response all(FloorViewModel viewModel)
			throws JsonProcessingException, IOException;

	/*
	 * Implementation of interface IFloorService create method which is invoked
	 * for creating the floor.
	 * 
	 * @see vHMS.Facade.IFloorService#create()
	 */
	public Response create(FloorViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IFloorService findById method which is invoked
	 * for finding/getting a floor by id.
	 * 
	 * @see vHMS.Facade.IFloorService#findById()
	 */
	public Response findById(FloorViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IFloorService count method which is invoked
	 * for getting the count of matching records.
	 * 
	 * @see vHMS.Facade.IFloorService#count()
	 */
	public Response count(FloorViewModel viewModel);

	/*
	 * Implementation of interface IFloorService delete method which is invoked
	 * for deleting a floor.
	 * 
	 * @see vHMS.Facade.IFloorService#delete()
	 */
	public Response delete(FloorViewModel viewModel);

	/*
	 * Implementation of interface IFloorService update method which is invoked
	 * for updating a floor data.
	 * 
	 * @see vHMS.Facade.IFloorService#update()
	 */
	public Response update(FloorViewModel viewModel);

	/*
	 * Implementation of interface IFloorService findAllByAttributes method which
	 * is invoked for finding the records/floor which satisfies the given
	 * criteria.
	 * 
	 * @see vHMS.Facade.IFloorService#findAllByAttributes()
	 */
	public Response findAllByAttributes(FloorViewModel viewModel)
			throws Exception;


}
