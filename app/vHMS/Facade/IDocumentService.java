package vHMS.Facade;

import vHMS.Core.ViewModels.DocumentViewModel;
import vHMS.Core.ViewModels.Response;

/**
 * @author Arokia Felsi For document upload
 * 
 */

public interface IDocumentService {

	/*
	 * Implementation of interface IDocumentService load method which is invoked
	 * for uploading.
	 * 
	 * @see vHMS.Facade.IDocumentService#load()
	 */
	public Response load(DocumentViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IDocumentService download method which is
	 * invoked for downloading.
	 * 
	 * @see vHMS.Facade.IDocumentService#load()
	 */
	public Response download(DocumentViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IDocumentService download method which is
	 * invoked for delete.
	 * 
	 * @see vHMS.Facade.IDocumentService#delete()
	 */
	public Response delete(DocumentViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IDocumentService list method which is invoked
	 * for getting all the documents for a request.
	 * 
	 * @see vHMS.Facade.IDocumentService#list()
	 */
	public Response findAllByAttributes(DocumentViewModel viewModel) throws Exception;

	public Response list(DocumentViewModel viewModel) throws Exception;

		

}
