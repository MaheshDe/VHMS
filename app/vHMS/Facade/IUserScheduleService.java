package vHMS.Facade;

import java.io.IOException;

import vHMS.Core.ViewModels.Response;


import vHMS.Core.ViewModels.UserScheduleViewModel;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface IUserScheduleService {

	/*
	 * Implementation of interface IUserService load method which is invoked for
	 * fetching the master list.
	 * 
	 * @see vHMS.Facade.IUserService#all()
	 */
	public Response all(UserScheduleViewModel viewModel)
			throws JsonProcessingException, IOException;

	/*
	 * Implementation of interface IUserService create method which is invoked
	 * for creating the master.
	 * 
	 * @see vHMS.Facade.IUserService#create()
	 */
	public Response create(UserScheduleViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IUserService findById method which is invoked
	 * for finding/getting a master by id.
	 * 
	 * @see vHMS.Facade.IUserService#findById()
	 */
	public Response findById(UserScheduleViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IUserService count method which is invoked
	 * for getting the count of matching records.
	 * 
	 * @see vHMS.Facade.IUserService#count()
	 */
	public Response count(UserScheduleViewModel viewModel);

	/*
	 * Implementation of interface IUserService delete method which is invoked
	 * for deleting a master.
	 * 
	 * @see vHMS.Facade.IUserService#delete()
	 */
	public Response delete(UserScheduleViewModel viewModel);

	/*
	 * Implementation of interface IUserService update method which is invoked
	 * for updating a master data.
	 * 
	 * @see vHMS.Facade.IUserService#update()
	 */
	public Response update(UserScheduleViewModel viewModel);

	
	/*
	 * Implementation of interface IUserService findAllByAttributes method which
	 * is invoked for finding the records/masters which satisfies the given
	 * criteria.
	 * 
	 * @see vHMS.Facade.IUserService#findAllByAttributes()
	 */
	public Response findAllByAttributes(UserScheduleViewModel viewModel)
			throws Exception;

}
