package vHMS.Facade;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vHMS.Core.Models.Branch;
import vHMS.Core.Models.Floor;
import vHMS.Core.ViewModels.FloorViewModel;
import vHMS.Core.ViewModels.Response;
import Base.Data.IGenericMongoDao;
import Base.Utilities.CommonUtils;
import Base.Utilities.HelperUtil;
import Base.Utilities.NullAwareBeanUtilsBean;
import Base.Utilities.Email.SmtpSender;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;


/**
 * The FloorService is the implementation for the interface IFloorService.
 * 
 * @author MaheshDe
 * @since 2015-08-27
 */

@Service
@Transactional
public class FloorService implements IFloorService {

	@Autowired
	private IGenericMongoDao genericDAO;

	@Autowired
	private SmtpSender defaultEmailSender;
	// Created HelperUtil instance.
	private HelperUtil helperUtil = new HelperUtil();
	// Created ModelMapper instance
	private ModelMapper modelmapper = new ModelMapper();
	// Created NullAwareBeanUtilsBean instance.
	private BeanUtilsBean notNull = new NullAwareBeanUtilsBean();
	
	
	/**
	 * Implementation of interface IFloorService all method which returns a
	 * list of floor data.
	 * 
	 *  @see vHMS.Facade.IFloorService#all(vHMS.Core.Models.
	 * FloorViewModel
	 */
	
	
	@Override
	public Response all(FloorViewModel viewModel)
			throws JsonProcessingException, IOException {
		// TODO Auto-generated method stub
		Response response = new Response();
		// Fetch all the records from the table.
		List<Floor> allFloor = null;

		if (viewModel.where != null) {
			// for appending the where and the like for search criteria
			viewModel.where = helperUtil
					.ConstructWhereClauseFromJson(viewModel.where);
		}
		allFloor = genericDAO.all(CommonUtils.getMessage("Floor"), Floor.class,
				viewModel.where, viewModel.limit, viewModel.skip);
		try {
			// to check whether there is a list of entity returns true response
			if (null != allFloor && allFloor.size() > 0) {
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.floor.Criteria.search");
				response.ViewModels = allFloor;
			} else {// Returns false response
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.floor.Criteria.search");
			}

		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}
	
	
	/**
	 * Implementation of interface IFloorService create method which
	 * saves/create a floor data.
	 * 
	 *@see vHMS.Facade.IFloorService#create(vHMS.Core.Models.
	 * FloorViewModel
	 */
	
	@Override
	public Response create(FloorViewModel viewModel) throws Exception {
		// TODO Auto-generated method stub
		
		Response response = new Response();
		Floor floorExist = null;
		String id = null;
		
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			if(viewModel != null)
			{
				map.put("name", Pattern.compile(viewModel.name, Pattern.CASE_INSENSITIVE));
				map.put("buildingCode", viewModel.buildingCode);
				
				floorExist = genericDAO.findOneByAttribute(
						CommonUtils.getMessage("Floor"), map,
						Floor.class);
				
				
				if(floorExist != null)
				{
					
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.createFloor.exists");
				}
				else
				{
					
						// Calling the helper class to set the mandatory properties of
						// the view model
						viewModel = helperUtil.AttachCommonFields(viewModel,
								FloorViewModel.class);
					
						//  convert viewmodel object to model object before saving it
						// to DB
						Floor floor = modelmapper.map(viewModel, Floor.class);
				
						// Create the new record in table.
						Object[] obj =genericDAO.save(CommonUtils.getMessage("Floor"), floor);
						
						for (Object objId : obj) 
						{
							id = String.valueOf(objId);
							floor.setId(id);
						}
					
						response.success = true;
						response.message = CommonUtils
								.getMessage("success.floor.creation");
						response.ViewModel = modelmapper.map(floor,
								FloorViewModel.class); ;
				}
			}
			
		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();

		}
		return response;
	}
	
	
	/**
	 * Implementation of interface IFloorService findById method which search a
	 * floor data with the given code.
	 * 
	 * @see vHMS.Facade.IFloorService#findById(vHMS.Core.Models.
	 * FloorViewModel
	 */
	
	@Override
	public Response findById(FloorViewModel viewModel) throws Exception {
		// TODO Auto-generated method stub
		Response response = new Response();

		try {
			
				Map<String, Object> map = new HashMap<String, Object>();
				Floor floor = null;
				
				if(viewModel != null)
				{
					map.put("code", viewModel.code);
					
					// Fetch the record by code.
					floor = genericDAO.findOneByAttribute(
							CommonUtils.getMessage("Floor"), map, Floor.class);
					
					if(floor != null)
					{
						FloorViewModel floorViewModel = modelmapper.map(floor,
								FloorViewModel.class);
						
						response.success = true;
						response.message = CommonUtils
								.getMessage("success.floor.Criteria.search");
						response.ViewModel = floorViewModel;
					}
					else 
					{
						response.success = false;
						response.message = CommonUtils
							.getMessage("error.floor.Criteria.search");
					}
				}

			} catch (Exception e) {
				response.success = false;
				response.message = CommonUtils.getMessage("error.processfailure");
				response.ExceptionMessage = e.getMessage();
			}
		return response;
	}
	
	
	/**
	 * Implementation of interface IFloorService count method which returns
	 * number of floor data present in a particular table.
	 * 
	 *  @see vHMS.Facade.IFloorService#count(vHMS.Core.Models.
	 *  FloorViewModel
	 */
	
	@Override
	public Response count(FloorViewModel viewModel) {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {
			// Condition to check whether where attribute is empty to check for
			// search criteria
			if (viewModel.where != null) {
				// for appending the where and the like for search criteria
				viewModel.where = helperUtil
						.ConstructWhereClauseFromJson(viewModel.where);
			}
			// Count of the record in the list or serach by criteria
			int count = genericDAO.count(CommonUtils.getMessage("Floor"), Floor.class);
			// adding count to the json object
			response.data.put("count", count);
			response.success = true;
			response.message = CommonUtils
					.getMessage("success.countMasterdata");
		} catch (Exception e) {
			// Return false if it ia an error
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}
	
	
	
	/**
	 * Implementation of interface IFloorService delete method which deletes a
	 * floor data for the given code.
	 * 
	 *  @see vHMS.Facade.IFloorService#delete(vHMS.Core.Models.FloorViewModel
	 */
	
	@Override
	public Response delete(FloorViewModel viewModel) {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {

			Map<String, Object> map = new HashMap<String, Object>();
			if(viewModel != null)
			{
				map.put("code", viewModel.code);
				
				// Fetch the record by code.
				Floor floor = genericDAO.findOneByAttribute(
						CommonUtils.getMessage("Floor"), map, Floor.class);

				// if record found then delete record and returns true
				if (floor != null) {
					// Deleting the user with the code
					genericDAO.delete(CommonUtils.getMessage("Floor"), floor.getId(),
							Floor.class);
				
					FloorViewModel floorData = modelmapper.map(floor,
							FloorViewModel.class);
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.floor.data.deletion");
					response.ViewModel = floorData;
					// response.status = true;

				} else {
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.floor.exists");
				}
			}
		} catch (Exception e) {

			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}
	
	
	
	/**
	 * Implementation of interface IFloorService update method which updates a
	 * floor data for the given code.
	 * 
	 *  @see vHMS.Facade.IFloorService#update(vHMS.Core.Models.
	 *  FloorViewModel
	 */
	
	@Override
	public Response update(FloorViewModel viewModel) {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {
			
			// Mapping the viewmodel to class of type
			Floor floor = modelmapper.map(viewModel,
						Floor.class);
			// getting the id from the view model for updating for the specific
			// id
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("code", viewModel.code);
			
			// Check by code whether the record exists before update.
			Floor floorExist = genericDAO.findOneByAttribute(CommonUtils.getMessage("Floor"), map, Floor.class);
			
			map.clear();
			map.put("name",Pattern.compile(viewModel.name, Pattern.CASE_INSENSITIVE));
			map.put("buildingCode",viewModel.buildingCode);
			Floor floorExistduplication = genericDAO.findOneByAttribute(
					viewModel.entityObject, map, Floor.class);
			
			if (floorExist != null) 
			{
				if(null != floorExistduplication
						&& !(floorExist.getCode()
								.equalsIgnoreCase(floorExistduplication
										.getCode()))) {
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.updateBranchdata.exists");
				}
				else
				{
					
					// override the old properties with new properties exists in the
					// right side object
					notNull.copyProperties(floorExist, floor);
					
					// Calling the helper class to set the mandatory properties of
					// the view model
					viewModel = helperUtil.AttachCommonFields(viewModel,
							FloorViewModel.class);
					
					// Update the existing record with new values.
					genericDAO.updateById(CommonUtils.getMessage("Floor"), floorExist,
							floorExist.getId());
					
					Floor updatedFloor = genericDAO.findOneById(
							CommonUtils.getMessage("Floor"), floorExist.getId(),
							Floor.class);
					
					response.success = true;
					response.ViewModel = modelmapper.map(updatedFloor,
							FloorViewModel.class);
					response.message = CommonUtils
							.getMessage("success.update.floor");	
					
				}
			}
			else
			{
				response.success = false;
				response.message = CommonUtils
					.getMessage("error.floor.exists");
			} 
			
		} catch (Exception e) {
			// Exception Handling
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}
	
	
	
	/**
	 * Implementation of interface IFloorService findAllByAttributes method
	 * which returns a list of floor data based on search criteria.
	 * 
	 * @see vHMS.Facade.IFloorService#findAllByAttributes(vHMS.Core.Models.
	 * FloorViewModel
	 */
	
	@Override
	public Response findAllByAttributes(FloorViewModel viewModel)
			throws Exception {
		// TODO Auto-generated method stub
		
		Response response = new Response();
		try {
			
			// Converting UserViewModel object to UserEntity Pojo
			Floor floorData = modelmapper.map(viewModel, Floor.class);
			Gson gson = new Gson();
			
			// Converting UserEntity to a JSON string
			String master = gson.toJson(floorData);
			
			// Converting JON string to a HashMap
			Map<String, Object> map = new Gson().fromJson(master,
					new TypeToken<HashMap<String, Object>>() {
					}.getType());

			if (viewModel.where != null) {
				// for appending the where and the like for search criteria
				viewModel.where = helperUtil
						.ConstructWhereClauseFromJson(viewModel.where);
			}
			// Fetch the Entity based on the search criteria
			List<Floor> allFloor = genericDAO.findAllByAttributes(
					CommonUtils.getMessage("Floor"), map, Floor.class, viewModel.limit,
					viewModel.skip);

			// Returns true if there is a record matching the criteria
			if (null != allFloor && allFloor.size() > 0) {
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.floor.Criteria.search");
				response.ViewModels = allFloor;
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.floor.Criteria.search");
			}

		} catch (Exception e) {
			// Handling Exception
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			// response.status = false;
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}
	
}