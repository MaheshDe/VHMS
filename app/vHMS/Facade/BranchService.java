package vHMS.Facade;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import vHMS.Core.Models.Branch;
import vHMS.Core.ViewModels.BranchViewModel;
import vHMS.Core.ViewModels.Response;
import Base.Data.IGenericMongoDao;
import Base.Utilities.CommonUtils;
import Base.Utilities.HelperUtil;
import Base.Utilities.NullAwareBeanUtilsBean;
/**
 * The BranchService is the implementation for the interface IBranchService.
 * 
 * @author Sabariraja M
 * @since 2015-08-28
 */

@Service
@Transactional
public class BranchService implements IBranchService {
	
	@Autowired
	private IGenericMongoDao genericDAO;
	private HelperUtil helperUtil = new HelperUtil();
	private ModelMapper modelmapper = new ModelMapper();
	private BeanUtilsBean notNull = new NullAwareBeanUtilsBean();
	
	@Override
	public Response all(BranchViewModel viewModel)throws JsonProcessingException, IOException {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {
			// Condition to check whether where attribute is empty
			// To check for search criteria
			if (viewModel.where != null) {
				// for appending the where and the like for search criteria
				viewModel.where = helperUtil
						.ConstructWhereClauseFromJson(viewModel.where);
			}

			List<Branch> list =  genericDAO.all(viewModel.entityObject,
					Branch.class, viewModel.where, viewModel.limit,
						viewModel.skip);
			

			// Checking whether the list is empty or not
			if (null != list && list.size() > 0) {
				// Define the target type
				Type targetListType = new TypeToken<List<BranchViewModel>>() {
				}.getType();
				// Mapping the list with the target view model
				List<BranchViewModel> lists = modelmapper.map(list,
						targetListType);
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.fetchBranchdata");
				// appending the list to the response viewModels list to display
				// in the UI
				response.ViewModels = lists;
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.fetchBranchdata");
				
			}
		} catch (Exception e) {
			// error in the database or connection
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
				
			}
		}
		return response;
	}

	@Override
	public Response create(BranchViewModel viewModel) throws Exception { 
		// TODO Auto-generated method stub
		Response response = new Response();
		//Branch branchExist = null;
		String id = null;
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			if(viewModel != null)
			{
				map.put("name", Pattern.compile(viewModel.name, Pattern.CASE_INSENSITIVE));
			
				Branch branchExist = genericDAO.findOneByAttribute(
						CommonUtils.getMessage("Branch"), map,
						Branch.class);
				
				if(branchExist != null)
				{
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.branch.exists");
				}
				else
				{
					// Calling the helper class to set the mandatory properties of
					// the view model
					viewModel = helperUtil.AttachCommonFields(viewModel,
							BranchViewModel.class);
					
					
					//  convert viewmodel object to model object before saving it
					// to DB
					Branch branch = modelmapper.map(viewModel, Branch.class);
				
					// Create the new record in table.
					Object[] obj =genericDAO.save(CommonUtils.getMessage("Branch"), branch);
					
					for (Object objId : obj) 
					{
						id = String.valueOf(objId);
						branch.setId(id);
					}
					map.clear();
					map.put("code", viewModel.code);

					Branch branchSave = genericDAO.findOneByAttribute(
							viewModel.entityObject, map, Branch.class);
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.Branch.creation");
					response.ViewModel = modelmapper.map(branchSave,
							BranchViewModel.class); ;
				}
			}
			
		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();

		}
		return response;

	}

	@Override
	public Response findById(BranchViewModel viewModel) throws Exception {
		// TODO Auto-generated method stub
		Response response = new Response();

		try {

			Map<String, Object> map = new HashMap<String, Object>();
			// Fetch the first record with given conditions satisfied.
			map.put("code", viewModel.code);

			Branch branchfind = genericDAO.findOneByAttribute(
					viewModel.entityObject, map, Branch.class);
			
			if (null != branchfind) {
				
			BranchViewModel viewmodel = modelmapper.map(branchfind,
					BranchViewModel.class);

				response.success = true;
				response.message = CommonUtils
						.getMessage("success.branch.Criteria.search");
				response.ViewModel = viewmodel;

			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.branch.Criteria.search"); 
			}

		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}

	@Override
	public Response count(BranchViewModel viewModel) {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {
			// Condition to check whether where attribute is empty to check for
			// search criteria
			if (viewModel.where != null) {
				// for appending the where and the like for search criteria
				viewModel.where = helperUtil
						.ConstructWhereClauseFromJson(viewModel.where);
			}
			// Count of the record in the list or serach by criteria
			int count = genericDAO.count(CommonUtils.getMessage("Branch"), Branch.class);
			// adding count to the json object
			response.data.put("count", count);
			response.success = true;
			response.message = CommonUtils
					.getMessage("success.countBranchdata");
		} catch (Exception e) {
			// Return false if it is an error
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

	@Override
	public Response delete(BranchViewModel viewModel) {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {

			Map<String, Object> map = new HashMap<String, Object>();
			// Fetch the first record with given conditions satisfied.
			map.put("code", viewModel.code);

			Branch branchfind = genericDAO.findOneByAttribute(
					viewModel.entityObject, map, Branch.class);

			// if record found then delete record and returns true

			if (branchfind != null) {
				// Deleting the Entity with the ID
				genericDAO.delete(viewModel.entityObject, branchfind.getId(),
						Branch.class);
				BranchViewModel branchdata = modelmapper.map(branchfind,
						BranchViewModel.class);
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.branch.deletion");
				response.ViewModel = branchdata;
				// response.status = true;

			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.branchcode.not.exists");
				// response.status = false;
			}

		} catch (Exception e) {

			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}

	@Override
	public Response update(BranchViewModel viewModel) {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {
			// Calling the helper class to set the mandatory properties of the
						// view model
						viewModel = helperUtil.AttachCommonFields(viewModel,
								BranchViewModel.class);
			Branch branch = modelmapper.map(viewModel,
					Branch.class);
			Map<String, Object> map = new HashMap<String, Object>();
			// Fetch the first record with given conditions satisfied.
			map.put("code", viewModel.code);

			// Check whether the record exists before update.
			Branch branchdata = genericDAO.findOneByAttribute(
					viewModel.entityObject, map, Branch.class);
			map.clear();
			map.put("name",
					Pattern.compile(viewModel.name, Pattern.CASE_INSENSITIVE));
			Branch branchdataduplication = genericDAO.findOneByAttribute(
					viewModel.entityObject, map, Branch.class);
			
			if (branchdata != null) 
			{
				if(null != branchdataduplication
						&& !(branchdata.getCode()
								.equalsIgnoreCase(branchdataduplication
										.getCode()))) {
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.updateBranchdata.exists");
				}
				else{
					// override the old properties with new properties exists in the
					// right side object
					notNull.copyProperties(branchdata, branch);
					// Update the existing record wi8th new values.
					genericDAO.updateById(viewModel.entityObject, branchdata,
							branchdata.getId());
					Branch updatedBranch = genericDAO.findOneById(
							viewModel.entityObject, branchdata.getId(),
							Branch.class);
					response.success = true;
					response.ViewModel = modelmapper.map(updatedBranch,BranchViewModel.class);
					response.message = CommonUtils
							.getMessage("success.update.Branch");	
				}
				
			} else {
				// To check whether the entity code exists if not returns the
				// response as false
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.branchcode.not.exists");

			}
		} catch (Exception e) {
			// Exception Handling
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}

	

	@Override
	public Response findAllByAttributes(BranchViewModel viewModel)throws Exception {
		// TODO Auto-generated method stub
		Response response = new Response();
		try {
		// Converting MasterViewModel object to MasterEntity Pojo
			Branch data = modelmapper
				.map(viewModel, Branch.class);
		Gson gson = new Gson();
		// Converting MasterEntity to a JSON string
		String branch = gson.toJson(data);
		// Converting JON string to a HashMap
		Map<String, Object> map = new Gson().fromJson(branch,
				new TypeToken<HashMap<String, Object>>() {
				}.getType());
		
		if (viewModel.where != null) {
			// for appending the where and the like for search criteria
			viewModel.where = helperUtil
					.ConstructWhereClauseFromJson(viewModel.where);
		}
		// Fetch the Entity based on the search criteria
		List<Branch> allBranch = genericDAO.findAllByAttributes(
				viewModel.entityObject, map, Branch.class,
				viewModel.limit, viewModel.skip);

		
			// Returns true if there is a record matching the criteria
			if (null != allBranch && allBranch.size() > 0) {
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.branch.Criteria.search");
				response.ViewModels = allBranch;
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.branch.Criteria.search");
			}

		} catch (Exception e) {
			// Handling Exception
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			// response.status = false;
			response.ExceptionMessage = e.getMessage();
		}
		return response;
	}

	


	
}