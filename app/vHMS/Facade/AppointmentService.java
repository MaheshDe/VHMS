package vHMS.Facade;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vHMS.Core.Models.Appointment;
import vHMS.Core.ViewModels.AppointmentDetailsViewModel;
import vHMS.Core.ViewModels.AppointmentViewModel;
import vHMS.Core.ViewModels.NotificationViewModel;
import vHMS.Core.ViewModels.ReportViewModel;
import vHMS.Core.ViewModels.Response;
import vHMS.Core.ViewModels.ResponseViewModel;
import Base.Data.IGenericMongoDao;
import Base.Utilities.CommonUtils;
import Base.Utilities.HelperUtil;
import Base.Utilities.NullAwareBeanUtilsBean;
import Base.Utilities.Report.ReportConstants;
import Base.Utilities.Report.ReportProcessor;
import Base.Utilities.Report.ReportUtils;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;

/**
 * The AppointmentService is a Class containing methods like create, update
 * delete etc.. related to Appointment , and implements IAppointmentService
 * 
 * 
 * @author ShunmugaRajaG
 * @version 1.0
 */
@Service
@Transactional
public class AppointmentService implements IAppointmentService {

	// created instance using DI.
	@Autowired
	private IGenericMongoDao genericDAO;

	// Created NullAwareBeanUtilsBean instance.
	private BeanUtilsBean notNull = new NullAwareBeanUtilsBean();

	// Created HelperUtil instance.
	private HelperUtil helperUtil = new HelperUtil();
	private ModelMapper modelmapper = new ModelMapper();

	/*
	 * Implementation of interface IAppointmentService all method which returns
	 * a list of all appointment.
	 */
	public Response all(AppointmentViewModel viewModel) throws Exception {
		Response response = new Response();
		try {
			// Condition to check whether where attribute is empty
			// To check for search criteria
			if (viewModel.where != null) {
				// for appending the where and the like for search criteria
				viewModel.where = helperUtil
						.ConstructWhereClauseFromJson(viewModel.where);
			}

			List<Appointment> list = null;

			if (viewModel != null) {
				list = genericDAO.all(viewModel.entityObject,
						Appointment.class, viewModel.where, viewModel.limit,
						viewModel.skip);
			}

			// Checking whether the list is empty or not
			if (null != list && list.size() > 0) {
				// Define the target type
				Type targetListType = new TypeToken<List<AppointmentViewModel>>() {
				}.getType();
				// Mapping the list with the target view model
				List<AppointmentViewModel> lists = modelmapper.map(list,
						targetListType);
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.fetchAppointmentdata");
				// appending the list to the response viewModels list to didplay
				// in the UI
				response.ViewModels = lists;
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.fetchAppointmentdata");
			}
		} catch (Exception e) {
			// error in the database or connection
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

	/*
	 * Implementation of interface IAppointmentService create method which is
	 * used to create/save an Appointment.
	 */
	public Response create(AppointmentViewModel viewModel) throws Exception {
		Response response = new Response();
		String id = null;
		ResponseViewModel responseViewModel = new ResponseViewModel();
		try {
			if (viewModel != null && !checkAppointment(viewModel)) {
				// Calling the helper class to set the mandatory properties of
				// the
				// view model
				viewModel = helperUtil.AttachCommonFields(viewModel,
						AppointmentViewModel.class);

				// getting the source and target to set the properties
				Appointment appointment = modelmapper.map(viewModel,
						Appointment.class);
				// saving the data with the pojo and collection name
				// to get the ID for the saved item
				Object[] obj = genericDAO.save(viewModel.entityObject,
						appointment);

				// to sms to patient contact number
				NotificationViewModel notificationViewModel = new NotificationViewModel();
				notificationViewModel.setNotificationType("sms");
				notificationViewModel.setTo(viewModel.patientContactNumber
						+ "," + viewModel.userContactNumber);
				notificationViewModel.setBody(CommonUtils
						.getMessage("sms.message.appointment.creation"));
				responseViewModel.smsNotificationViewModel = notificationViewModel;

				for (Object objId : obj) {
					id = String.valueOf(objId);
					appointment.setId(id);
				}
				responseViewModel.viewModel = modelmapper.map(appointment,
						AppointmentViewModel.class);
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.createAppointment");
				// Binding the created data to the view model in the response
				response.ViewModel = responseViewModel;

			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.createAppointment");
			}

		} catch (Exception e) {
			// return false if there is any database connection problem
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

	/*
	 * Implementation of interface IAppointmentService update method which is
	 * used to update the Appointment details with the given id.
	 */
	@SuppressWarnings("unused")
	public Response update(AppointmentViewModel viewModel) throws Exception {
		Response response = new Response();
		ResponseViewModel responseViewModel = new ResponseViewModel();
		try {
			// Calling the helper class to set the mandatory properties of the
			// view model
			viewModel = helperUtil.AttachCommonFields(viewModel,
					AppointmentViewModel.class);

			// Mapping the viewmodel to class of type
			Appointment appointment = modelmapper.map(viewModel,
					Appointment.class);
			// getting the id from the view model for updating for the specific
			// id
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("code", appointment.getCode());

			Appointment appointmentdata = genericDAO.findOneByAttribute(
					viewModel.entityObject, map, Appointment.class);

			if (appointmentdata != null) {

				// override the old properties with new properties exists in the
				// right side object
				notNull.copyProperties(appointmentdata, appointment);

				// Updating the record by ID
				genericDAO.updateById(viewModel.entityObject, appointmentdata,
						appointmentdata.getId());
				AppointmentViewModel data = modelmapper.map(appointmentdata,
						AppointmentViewModel.class);
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.updateAppointmentdata");
				// sending the response object as view model
				responseViewModel.viewModel = data;

				NotificationViewModel notificationViewModel = new NotificationViewModel();
				notificationViewModel.setNotificationType("sms");
				notificationViewModel.setTo(data.patientContactNumber + ","
						+ viewModel.userContactNumber);
				notificationViewModel.setBody(CommonUtils
						.getMessage("sms.message.appointment.updation"));
				responseViewModel.smsNotificationViewModel = notificationViewModel;
				response.ViewModel = responseViewModel;

			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.findAppointmentdata");
			}
		} catch (Exception e) {
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

	/*
	 * Implementation of interface IAppointmentService findAllByAttributes
	 * method which fetches all the record with the given criteria.
	 */

	public Response findAllByAttributes(AppointmentViewModel viewModel)
			throws Exception {
		Response response = new Response();
		int count = 0;
		try {
			if (viewModel != null) {
				// Converting MasterViewModel object to MasterEntity Pojo
				Appointment data = modelmapper
						.map(viewModel, Appointment.class);
				Gson gson = new Gson();
				// Converting MasterEntity to a JSON string
				String master = gson.toJson(data);

				Map<String, Object> map = gson.fromJson(master,
						new TypeToken<HashMap<String, Object>>() {
						}.getType());
				// changing query -- appointmentstartdatetime greater and equal
				// than input date
				if (null != viewModel.userCodes
						&& viewModel.userCodes.length > 0) {
					map.put("userCode",
							BasicDBObjectBuilder.start("$in",
									viewModel.userCodes).get());
				}
				if (map.containsKey("appointmentStartDateTime")) {
					map.put("appointmentStartDateTime", BasicDBObjectBuilder
							.start("$gte", viewModel.appointmentStartDateTime)
							.get());
				}
				// changing query -- appointmentenddatetime less and equal than
				// input date
				if (map.containsKey("appointmentEndDateTime")) {
					map.put("appointmentEndDateTime", BasicDBObjectBuilder
							.start("$lte", viewModel.appointmentEndDateTime)
							.get());
				}
				if (null != viewModel.specialityCode
						&& viewModel.specialityCode != ""
						&& viewModel.specialityCode.trim().length() > 0) {
					/*
					 * map.put("json.specialityCode",
					 * BasicDBObjectBuilder.start("$match",
					 * viewModel.specialityCode).get());
					 */
					// elemMatchObj.put("$elemMatch", new
					// BasicDBObject("device", "10.10.20.2"));
					/*
					 * map.put("json",
					 * java.util.regex.Pattern.compile("specialityCode:" +
					 * viewModel.specialityCode));
					 */

					/*
					 * BasicDBObject elemMatchObj = new BasicDBObject();
					 * elemMatchObj.put("", new BasicDBObject("device",
					 * "10.10.20.2"));
					 */

					map.put("json.specialityCode", viewModel.specialityCode);
				}
				BasicDBObject orderBy= new BasicDBObject("appointmentStartDateTime",-1);
				List<Appointment> list = genericDAO.findAllByAttributes(
						viewModel.entityObject, map, Appointment.class,
						viewModel.limit, viewModel.skip,orderBy);

				// get count of records based on the filter

				if (viewModel.limit != null && null != list) {

					if (Integer.parseInt(viewModel.limit) >= list.size()) {
						count = genericDAO.count(viewModel.entityObject, map,
								Appointment.class);
					} else {
						count = list.size();
					}
				}

				// getting The list appointment by findbyQuery method and
				// passing query as param
				/*
				 * List<Appointment> list = genericDAO.findByQuery(
				 * viewModel.entityObject,contructQuery(map,viewModel),
				 * Appointment.class, viewModel.limit, viewModel.skip);
				 */

				// ModelMapper modelmapper = new ModelMapper();

				// Define the target type
				Type targetListType = new TypeToken<List<AppointmentViewModel>>() {
				}.getType();
				// Mapping the list with the target view model
				List<AppointmentViewModel> lists = modelmapper.map(list,
						targetListType);
				// Checking whether the list is empty or not
				if (null != lists && lists.size() > 0) {
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.fetchAppointmentdata");
					response.ViewModel = count;
					// appending the list to the response viewModels list to
					// display
					// in the UI
					response.ViewModels = lists;
				} else {
					// List is empty response is false
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.fetchAppointmentdata");
				}
			}

		} catch (Exception e) {
			// error in the database or connection
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

	/*
	 * Implementation of interface IAppointmentService findByCode method which
	 * is used to find an appointment using code(primary key).
	 * 
	 * @see vHMS.Facade.IAppointmentService#findByCode(vHMS.Core.Models.
	 * AppointmentViewModel
	 */

	public Response findByCode(AppointmentViewModel viewModel) throws Exception {

		Response response = new Response();
		try {
			// To find where the record exists
			Appointment appointmentexist = null;
			Map<String, Object> map = new HashMap<String, Object>();
			if (viewModel != null) {
				map.put("code", viewModel.code);
				// get the patient object by passing code
				appointmentexist = genericDAO.findOneByAttribute(
						CommonUtils.getMessage("Appointment"), map,
						Appointment.class);
			}
			if (null != appointmentexist) {
				AppointmentViewModel appData = modelmapper.map(
						appointmentexist, AppointmentViewModel.class);
				response.success = true;
				// if the record exists bind to the view model
				response.ViewModel = appData;
				response.message = CommonUtils
						.getMessage("success.findPatientdata");
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.findPatientdata");
			}
		} catch (Exception e) {
			// return false if there is any database connection problem
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

	/*
	 * Implementation of interface IUserService findByIdUser method which is
	 * used to find an User using id(primary key).
	 * 
	 * @see
	 * HPFCR.Facade.IUserService#findById(HPFCR.Core.ViewModels.UserViewModel )
	 */

	public Response appointmentReport(ReportViewModel reportViewModel)
			throws Exception {
		Response response = new Response();
		try {
			reportViewModel.templateFileName = CommonUtils
					.getMessage("report.appointment.templateName");
			reportViewModel.reportProcessor = new ReportProcessor();
			reportViewModel.ModelClass = AppointmentDetailsViewModel.class;
			reportViewModel.reportDatasource = ReportUtils.getDataSource(null,
					null);
			reportViewModel.imageFileLocation = CommonUtils
					.getApplicationAbsolutePath() + "/public/jasper/";
			reportViewModel.templateFileLocation = CommonUtils
					.getApplicationAbsolutePath() + "/public/jasper/";
			reportViewModel.reportOutputFileName = reportViewModel.templateFileName
					+ new Date()
					+ ReportConstants.DOT
					+ reportViewModel.reportOutputFormat.toLowerCase();
			Map<String, Object> parameters = new HashMap<String, Object>();
			// parameters.put("userList", userList);
			// parameters.put("loggedonUser", "TEST USER");
			reportViewModel.reportParametersMap = parameters;
			response = reportViewModel.reportProcessor
					.executeReport(reportViewModel);

			// response.success = true;
			response.message = CommonUtils
					.getMessage("success.report.generated");

		} catch (Exception e) {
			// return false if there is any database connection problem
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

	/*
	 * Implementation of interface IAppointmentService findAllByAttributes
	 * method which fetches all the record with the given criteria.
	 */
	private boolean checkAppointment(AppointmentViewModel viewModel)
			throws Exception {
		boolean appointmentExist = false;

		try {
			if (viewModel != null) {
				// Converting MasterViewModel object to MasterEntity Pojo
				Appointment data = modelmapper
						.map(viewModel, Appointment.class);
				Gson gson = new Gson();
				// Converting MasterEntity to a JSON string
				String master = gson.toJson(data);
				// Converting JON string to a HashMap
				Map<String, Object> map = new Gson().fromJson(master,
						new TypeToken<HashMap<String, Object>>() {
						}.getType());
				Map<String, Object> newMap = new HashMap<String, Object>();
				// changing query -- appointmentstartdatetime greater and equal
				// than input date
			/*	if (map.containsKey("appointmentStartDateTime")) {
					newMap.put("appointmentStartDateTime", BasicDBObjectBuilder
							.start("$gte", viewModel.appointmentStartDateTime)
							.get());
				}
				// changing query -- appointmentenddatetime less and equal than
				// input date
				if (map.containsKey("appointmentEndDateTime")) {
					newMap.put("appointmentEndDateTime", BasicDBObjectBuilder
							.start("$lte", viewModel.appointmentEndDateTime)
							.get());
				}*/
				
				
				BasicDBList ORList = new BasicDBList();

				//checking the less than and greater than appointmentStartDateTime 
				if (null != viewModel.appointmentStartDateTime) {
					BasicDBList list = new BasicDBList();

					BasicDBObject startobj = new BasicDBObject("appointmentStartDateTime",
							BasicDBObjectBuilder.start("$lte",
									viewModel.appointmentStartDateTime).get());
					list.add(startobj);
					BasicDBObject endobj = new BasicDBObject("appointmentEndDateTime",
							BasicDBObjectBuilder.start("$gte",
									viewModel.appointmentStartDateTime).get());
					list.add(endobj);
					
					ORList.add(new BasicDBObject("$and", list));
				}
				
				//checking the less than and greater than appointmentEndDateTime 
				if (null != viewModel.appointmentEndDateTime) {
					BasicDBList list = new BasicDBList();

					BasicDBObject startobj = new BasicDBObject("appointmentStartDateTime",
							BasicDBObjectBuilder.start("$lte",
									viewModel.appointmentEndDateTime).get());
					list.add(startobj);
					BasicDBObject endobj = new BasicDBObject("appointmentEndDateTime",
							BasicDBObjectBuilder.start("$gte",
									viewModel.appointmentEndDateTime).get());
					list.add(endobj);

					ORList.add(new BasicDBObject("$and", list));
				}
				
				//checking greater than appointmentStartDateTime and less than appointmentEndDateTime
				if (null != viewModel.appointmentEndDateTime && null != viewModel.appointmentStartDateTime) 
				{
					BasicDBList list = new BasicDBList();

					BasicDBObject startobj = new BasicDBObject("appointmentStartDateTime",
							BasicDBObjectBuilder.start("$gte",
									viewModel.appointmentStartDateTime).get());
					list.add(startobj);
					BasicDBObject endobj = new BasicDBObject("appointmentEndDateTime",
							BasicDBObjectBuilder.start("$lte",
									viewModel.appointmentEndDateTime).get());
					list.add(endobj);

					ORList.add(new BasicDBObject("$and", list));
				}
				//putting into map.
				if (ORList.size() > 0) {
					newMap.put("$or", ORList);
				}
				
				
				
				if (map.containsKey("userCode")) {
					newMap.put("userCode", map.get("userCode"));
				}

				if (map.containsKey("status")) {
					newMap.put("status", map.get("status"));
				}

				if (map.containsKey("appointmentStatus")) {
					newMap.put("appointmentStatus",
							map.get("appointmentStatus"));
				}

				List<Appointment> list = genericDAO.findAllByAttributes(
						viewModel.entityObject, newMap, Appointment.class,
						viewModel.limit, viewModel.skip);
				if (list != null && !list.isEmpty()) {
					appointmentExist = true;
				}

			}

		} catch (Exception e) {
			appointmentExist = true;
		}
		return appointmentExist;
	}

	/*
	 * Implementation of interface IAppointmentService findAllByAttributes
	 * method which fetches all the record with the given criteria.
	 */

	public Response checkAppointmentAvailablity(AppointmentViewModel viewModel)
			throws Exception {
		Response response = new Response();
		int count = 0;
		try {
			if (viewModel != null) {

				if (!checkAppointment(viewModel)) {
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.availableBooking");
					response.ViewModel = count;
					// appending the list to the response viewModels list to
					// display
					// in the UI
					response.ViewModels = null;
				} else {
					// List is empty response is false
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.AlreadyBooked");
				}
			}

		} catch (Exception e) {
			// error in the database or connection
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;
	}

}
