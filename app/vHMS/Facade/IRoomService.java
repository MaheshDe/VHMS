package vHMS.Facade;

import java.io.IOException;

import vHMS.Core.ViewModels.DoctorViewModel;
import vHMS.Core.ViewModels.Response;
import vHMS.Core.ViewModels.RoomViewModel;


import com.fasterxml.jackson.core.JsonProcessingException;

public interface IRoomService {

	/*
	 * Implementation of interface IRoomService load method which is invoked for
	 * fetching the room list.
	 * 
	 * @see vHMS.Facade.IRoomService#all()
	 */
	public Response all(RoomViewModel viewModel)
			throws JsonProcessingException, IOException;

	/*
	 * Implementation of interface IRoomService create method which is invoked
	 * for creating the room.
	 * 
	 * @see vHMS.Facade.IRoomService#create()
	 */
	public Response create(RoomViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IRoomService findById method which is invoked
	 * for finding/getting a room by id.
	 * 
	 * @see vHMS.Facade.IRoomService#findById()
	 */
	public Response findById(RoomViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IRoomService count method which is invoked
	 * for getting the count of matching records.
	 * 
	 * @see vHMS.Facade.IRoomService#count()
	 */
	public Response count(RoomViewModel viewModel);

	/*
	 * Implementation of interface IRoomService delete method which is invoked
	 * for deleting a user.
	 * 
	 * @see vHMS.Facade.IRoomService#delete()
	 */
	public Response delete(RoomViewModel viewModel);

	/*
	 * Implementation of interface IRoomService update method which is invoked
	 * for updating a room data.
	 * 
	 * @see vHMS.Facade.IRoomService#update()
	 */
	public Response update(RoomViewModel viewModel);

	/*
	 * Implementation of interface IRoomService findAllByAttributes method which
	 * is invoked for finding the records/room which satisfies the given
	 * criteria.
	 * 
	 * @see vHMS.Facade.IRoomService#findAllByAttributes()
	 */
	public Response findAllByAttributes(RoomViewModel viewModel)
			throws Exception;


}
