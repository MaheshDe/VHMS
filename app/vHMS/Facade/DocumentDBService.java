package vHMS.Facade;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.RandomStringUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;

import vHMS.Core.Models.AuditEventStore;
import vHMS.Core.Models.Document;
import vHMS.Core.Models.PatientRegistration;
import vHMS.Core.ViewModels.AuditEventStoreViewModel;
import vHMS.Core.ViewModels.DocumentDataViewModel;
import vHMS.Core.ViewModels.DocumentViewModel;
import vHMS.Core.ViewModels.Response;
import Base.Data.IGenericMongoDao;
import Base.Utilities.CommonUtils;
import Base.Utilities.HelperUtil;

import com.sun.jersey.core.util.Base64;

/**
 * The DocumentDBService is an implementation of the interface IDocumentService
 * containing methods like load, download, delete etc.. This service saves the
 * document in the DB inform of byte array.
 * 
 * @author DivyaP
 * @version 1.0
 * @since 2015-08-10
 */
public class DocumentDBService implements IDocumentService {

	// Using spring DI the bean will be initialised.
	@Autowired
	private IGenericMongoDao genericDAO;

	// Created HelperUtil instance.
	private HelperUtil helperUtil = new HelperUtil();

	private ModelMapper modelmapper = new ModelMapper();

	// Created AuditEventStore instance.
	private AuditEventStoreViewModel auditTrailViewModel = new AuditEventStoreViewModel();

	// storing the file details for the google storage in a map
	Map<String, Object> map = new HashMap<String, Object>();

	// Created Request Data instance.
	DocumentDataViewModel responseViewModel = new DocumentDataViewModel();

	/*
	 * Implementation of interface IDocumentService load method which is invoked
	 * for uplaoding a document.
	 * 
	 * @see vHMS.Facade.IDocumentService#load()
	 */
	public Response load(DocumentViewModel viewModel) throws Exception {

		Response response = new Response();
		try {
			map.clear();
			map.put("Code", viewModel.requestCode);

			PatientRegistration patientData = genericDAO.findOneByAttribute(
					CommonUtils.getMessage("PatientRegistration"), map,
					PatientRegistration.class);
			if (null != patientData) {
				// Calling the helper class to set the mandatory properties of
				// the
				// view model
				viewModel = helperUtil.AttachCommonFields(viewModel,
						DocumentViewModel.class);

				ModelMapper modelMapper = new ModelMapper();
				// Mapping the document and model to create a document table
				Document document = modelMapper.map(viewModel, Document.class);

				/*
				 * BufferedImage[] bFile = new BufferedImage[((int)
				 * viewModel.LoadedFile .length())];
				 */

				byte[] bFile = new byte[(int) viewModel.LoadedFile.length()];
				FileInputStream fileInputStream = new FileInputStream(
						viewModel.LoadedFile); // convert file into array of
												// bytes
				fileInputStream.read(bFile);
				fileInputStream.close();

				document.setFile(bFile);
				document.setCode(RandomStringUtils.randomNumeric(10));
				// Create a new document entry record for each document upload.
				Object[] savedid = genericDAO.save(viewModel.entityObject,
						document);
				for (Object id : savedid) {
					document.setId(String.valueOf(id));
				}
				if (document.getId() != null) {
					// Returns the response true if document contains values
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.document.upload");
					responseViewModel.documentViewModel = modelmapper.map(
							document, DocumentViewModel.class);
				} else {
					response.success = false;
					response.message = CommonUtils
							.getMessage("error.document.upload");
				}
				if (response.success) {

					// Set the auidtrail details for audittrail aop to be
					// activated
					AuditEventStore auditEvent = modelMapper.map(
							auditTrailViewModel, AuditEventStore.class);

					auditEvent.setEntityId(patientData.getId());
					// auditEvent.setJSON(patientData.getJSON());
					auditEvent.setAuditEventDescription(CommonUtils
							.getMessage("upload"));
					auditEvent.setEntityDescription(CommonUtils
							.getMessage("document.upload"));
					helperUtil.AttachCommonFields(auditEvent,
							AuditEventStore.class);
					responseViewModel.auditTrailViewModel = modelMapper.map(
							auditEvent, AuditEventStoreViewModel.class);

				}
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.document.upload");
			}

		} catch (Exception e) {
			// Handling exception
			response.success = false;
			response.message = CommonUtils
					.getMessage("error.document.upload.failure");
			response.ExceptionMessage = e.getMessage();

		}
		if (response.success && null != responseViewModel)
			response.ViewModel = responseViewModel;
		return response;
	}

	/*
	 * Implementation of interface IDocumentService download method which is
	 * invoked for downloading a document.
	 * 
	 * @see vHMS.Facade.IDocumentService#download()
	 */

	public Response download(DocumentViewModel viewModel) throws Exception {

		Response response = new Response();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("code", viewModel.code);

			// check whether the document exists for the code
			Document document = genericDAO.findOneByAttribute(
					CommonUtils.getMessage("Document"), map, Document.class);

			// If exists download the document from DataBase
			if (null != document) {
				if (null != document.getFile() && document.getFile().length > 0
						&& null != document.getContentType()) {
					// DocumentViewModel viewmodel = modelmapper.map(document,
					// DocumentViewModel.class);
					// Checking whether the request type is null
					// if (null != viewModel.requestType) {

					// String filepath = "D:/" + document.getFileName();
					File file = new File(document.getFileName());
					// viewmodel.LoadedFile = file;
					// converting the request type to lower case and remove

					FileOutputStream fos = new FileOutputStream(file);

					byte[] buffer = new byte[(int) document.getFile().length];

					// base 64 decode for getting image byte data
					byte[] imageData = Base64.decode(document.getFile());

					BufferedImage img = ImageIO.read(new ByteArrayInputStream(
							imageData));

					String extension = document.getFileName().substring(
							document.getFileName().lastIndexOf(".") + 1);
					// FilenameUtils.getExtension(document.getFileName());

					ImageIO.write(img, extension, file);

					viewModel.contentType = document.getContentType();
					viewModel.fileName = document.getFileName();
					viewModel.LoadedFile = file;

					response.success = true;
					response.message = CommonUtils
							.getMessage("success.document.download");
					// if the record exists bind to the view model
					response.ViewModel = viewModel;
				}
			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.download.entry.document");
			}

		} catch (Exception e) {
			// return false if there is any database connection problem
			response.success = false;
			response.message = CommonUtils
					.getMessage("error.document.download.failure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}
		return response;

	}

	/*
	 * Implementation of interface IDocumentService download method which is
	 * invoked for downloading a document.
	 * 
	 * @see vHMS.Facade.IDocumentService#download()
	 */

	public Response delete(DocumentViewModel viewModel) throws Exception {

		Response response = new Response();

		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("Code", viewModel.code);
			// check whether the document exists for the code
			Document document = genericDAO.findOneByAttribute(
					CommonUtils.getMessage("Document"), map, Document.class);
			// If exists, delete the document from database
			if (null != document) {
				if (null != document.getFile() && document.getFile().length > 0
						&& null != document.getContentType()) {
					genericDAO.delete(CommonUtils.getMessage("Document"),
							document.getId(), Document.class);
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.document.delete");
					// response.ViewModel = view;
				}

			}

			else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.document.delete");
			}
			if (response.success) {
				map.clear();
				map.put("Code", document.getRequestCode());
				PatientRegistration patientData = genericDAO
						.findOneByAttribute(
								CommonUtils.getMessage("PatientRegistration"),
								map, PatientRegistration.class);

				// Set the auidtrail details for audittrail aop to be
				// activated
				AuditEventStore auditEvent = modelmapper.map(
						auditTrailViewModel, AuditEventStore.class);
				auditEvent.setEntityId(patientData.getId());
				// auditEvent.setJSON(patientData.getJSON());
				auditEvent.setAuditEventDescription(CommonUtils
						.getMessage("delete"));
				auditEvent.setEntityDescription(CommonUtils
						.getMessage("document.delete"));
				helperUtil
						.AttachCommonFields(auditEvent, AuditEventStore.class);
				responseViewModel.auditTrailViewModel = modelmapper.map(
						auditEvent, AuditEventStoreViewModel.class);
			}

		} catch (Exception e) {
			// return false if there is any database connection problem
			response.success = false;
			response.message = CommonUtils
					.getMessage("error.document.delete.failure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}

		return response;
	}

	/*
	 * Implementation of interface IDocumentService list method which is invoked
	 * for getting the document list for a request.
	 * 
	 * @see vHMS.Facade.IDocumentService#list()
	 */
	public Response list(DocumentViewModel viewModel) throws Exception {

		Response response = new Response();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("RequestCode", viewModel.requestCode);
			// check whether the document exists for the code
			List<Document> documentlist = genericDAO.findAllByAttributes(
					CommonUtils.getMessage("Document"), map, Document.class,
					null, null);
			// If exists, delete the document from google storage
			if (null != documentlist && documentlist.size() > 0) {
				Type targetListType = new TypeToken<List<DocumentViewModel>>() {
				}.getType();
				// Mapping the list with the target view model
				List<DocumentViewModel> list = modelmapper.map(documentlist,
						targetListType);
				response.success = true;
				response.message = CommonUtils
						.getMessage("success.document.fetchall");
				response.ViewModels = list;

			} else {
				response.success = false;
				response.message = CommonUtils
						.getMessage("error.document.fetchall");
			}
		} catch (Exception e) {
			// return false if there is any database connection problem
			response.success = false;
			response.message = CommonUtils.getMessage("error.processfailure");
			if (null != e.getMessage()) {
				response.ExceptionMessage = e.getMessage();
			} else {
				response.ExceptionMessage = e.toString();
			}
		}

		return response;
	}

	@Override
	public Response findAllByAttributes(DocumentViewModel viewModel) throws Exception {
		// TODO Auto-generated method stub
		Response response = new Response();
		return response;
	}

}
