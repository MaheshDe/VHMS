package vHMS.Facade;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import vHMS.Core.Models.Role;
import vHMS.Core.ViewModels.Response;
import vHMS.Core.ViewModels.RoleViewModel;
import Base.Data.IGenericMongoDao;
import Base.Utilities.CommonUtils;
import Base.Utilities.HelperUtil;
import Base.Utilities.NullAwareBeanUtilsBean;
/**
 * The RoleService is the implementation for the interface IRoleService.
 * 
 * @author Sabariraja M
 * @since 2015-08-29
 */

@Service
@Transactional
public class RoleService implements IRoleService {
	
	@Autowired
	private IGenericMongoDao genericDAO;
	private HelperUtil helperUtil = new HelperUtil();
	private ModelMapper modelmapper = new ModelMapper();
	private BeanUtilsBean notNull = new NullAwareBeanUtilsBean();
	

	@Override
	public Response all(RoleViewModel viewModel)
			throws JsonProcessingException, IOException {
		// TODO Auto-generated method stub
				Response response = new Response();
				try {
					// Condition to check whether where attribute is empty
					// To check for search criteria
					if (viewModel.where != null) {
						// for appending the where and the like for search criteria
						viewModel.where = helperUtil
								.ConstructWhereClauseFromJson(viewModel.where);
					}

					List<Role> list =  genericDAO.all(viewModel.entityObject,
							Role.class, viewModel.where, viewModel.limit,
								viewModel.skip);
					

					// Checking whether the list is empty or not
					if (null != list && list.size() > 0) {
						// Define the target type
						Type targetListType = new TypeToken<List<RoleViewModel>>() {
						}.getType();
						// Mapping the list with the target view model
						List<RoleViewModel> lists = modelmapper.map(list,
								targetListType);
						response.success = true;
						response.message = CommonUtils
								.getMessage("success.fetchRoledata");
						// appending the list to the response viewModels list to display
						// in the UI
						response.ViewModels = lists;
					} else {
						response.success = false;
						response.message = CommonUtils
								.getMessage("error.fetchRoledata");
						
					}
				} catch (Exception e) {
					// error in the database or connection
					response.success = false;
					response.message = CommonUtils.getMessage("error.processfailure");
					if (null != e.getMessage()) {
						response.ExceptionMessage = e.getMessage();
					} else {
						response.ExceptionMessage = e.toString();
						
					}
				}
				return response;
	}

	@Override
	public Response create(RoleViewModel viewModel) throws Exception {
		// TODO Auto-generated method stub
				Response response = new Response();
				//Role roleExist = null;
				String id = null;
				Map<String, Object> map = new HashMap<String, Object>();
				try {
					
					if(viewModel != null)
					{
						map.put("name", Pattern.compile(viewModel.name, Pattern.CASE_INSENSITIVE));
						map.put("userType",viewModel.userType);
						Role roleExist = genericDAO.findOneByAttribute(
								CommonUtils.getMessage("Role"), map,
								Role.class);
						
						if(roleExist != null)
						{
							response.success = false;
							response.message = CommonUtils
									.getMessage("error.role.exists");
						}
						else
						{
							// Calling the helper class to set the mandatory properties of
							// the view model
							viewModel = helperUtil.AttachCommonFields(viewModel,
									RoleViewModel.class);
							
							
							//  convert viewmodel object to model object before saving it
							// to DB
							Role role = modelmapper.map(viewModel, Role.class);
						
							// Create the new record in table.
							Object[] obj =genericDAO.save(CommonUtils.getMessage("Role"), role);
							
							for (Object objId : obj) 
							{
								id = String.valueOf(objId);
								role.setId(id);
							}
							map.clear();
							map.put("code", viewModel.code);

							Role roleSave = genericDAO.findOneByAttribute(
									viewModel.entityObject, map, Role.class);
							response.success = true;
							response.message = CommonUtils
									.getMessage("success.Role.creation");
							response.ViewModel = modelmapper.map(roleSave,
									RoleViewModel.class); ;
						}
					}
					
				} catch (Exception e) {
					response.success = false;
					response.message = CommonUtils.getMessage("error.processfailure");
					response.ExceptionMessage = e.getMessage();

				}
				return response;
	}

	@Override
	public Response findById(RoleViewModel viewModel) throws Exception {
		// TODO Auto-generated method stub
				Response response = new Response();

				try {

					Map<String, Object> map = new HashMap<String, Object>();
					// Fetch the first record with given conditions satisfied.
					map.put("code", viewModel.code);

					Role rolefind = genericDAO.findOneByAttribute(
							viewModel.entityObject, map, Role.class);
					
					if (null != rolefind) {
						
						RoleViewModel viewmodel = modelmapper.map(rolefind,
								RoleViewModel.class);

						response.success = true;
						response.message = CommonUtils
								.getMessage("success.role.Criteria.search");
						response.ViewModel = viewmodel;

					} else {
						response.success = false;
						response.message = CommonUtils
								.getMessage("error.role.Criteria.search"); 
					}

				} catch (Exception e) {
					response.success = false;
					response.message = CommonUtils.getMessage("error.processfailure");
					response.ExceptionMessage = e.getMessage();
				}
				return response;
	}

	@Override
	public Response count(RoleViewModel viewModel) {
		// TODO Auto-generated method stub
				Response response = new Response();
				try {
					// Condition to check whether where attribute is empty to check for
					// search criteria
					if (viewModel.where != null) {
						// for appending the where and the like for search criteria
						viewModel.where = helperUtil
								.ConstructWhereClauseFromJson(viewModel.where);
					}
					// Count of the record in the list or search by criteria
					int count = genericDAO.count(CommonUtils.getMessage("Role"), Role.class);
					// adding count to the json object
					response.data.put("count", count);
					response.success = true;
					response.message = CommonUtils
							.getMessage("success.countRoledata");
				} catch (Exception e) {
					// Return false if it ia an error
					response.success = false;
					response.message = CommonUtils.getMessage("error.processfailure");
					if (null != e.getMessage()) {
						response.ExceptionMessage = e.getMessage();
					} else {
						response.ExceptionMessage = e.toString();
					}
				}
				return response;
	}

	@Override
	public Response delete(RoleViewModel viewModel) {
		// TODO Auto-generated method stub
				Response response = new Response();
				try {

					Map<String, Object> map = new HashMap<String, Object>();
					// Fetch the first record with given conditions satisfied.
					map.put("code", viewModel.code);

					Role rolefind = genericDAO.findOneByAttribute(
							viewModel.entityObject, map, Role.class);

					// if record found then delete record and returns true

					if (rolefind != null) {
						// Deleting the Entity with the ID
						genericDAO.delete(viewModel.entityObject, rolefind.getId(),
								Role.class);
						RoleViewModel roledata = modelmapper.map(rolefind,
								RoleViewModel.class);
						response.success = true;
						response.message = CommonUtils
								.getMessage("success.role.deletion");
						response.ViewModel = roledata;
						// response.status = true;

					} else {
						response.success = false;
						response.message = CommonUtils
								.getMessage("error.rolecode.not.exists");
						// response.status = false;
					}

				} catch (Exception e) {

					response.success = false;
					response.message = CommonUtils.getMessage("error.processfailure");
					response.ExceptionMessage = e.getMessage();
				}
				return response;
	}

	@Override
	public Response update(RoleViewModel viewModel) {
		// TODO Auto-generated method stub
				Response response = new Response();
				try {
					// Calling the helper class to set the mandatory properties of the
								// view model
								viewModel = helperUtil.AttachCommonFields(viewModel,
										RoleViewModel.class);
					Role role = modelmapper.map(viewModel,
							Role.class);
					Map<String, Object> map = new HashMap<String, Object>();
					// Fetch the first record with given conditions satisfied.
					map.put("code", viewModel.code);

					// Check whether the record exists before update.
					Role roledata = genericDAO.findOneByAttribute(
							viewModel.entityObject, map, Role.class);
				
					map.clear();
					map.put("name",
							Pattern.compile(viewModel.name, Pattern.CASE_INSENSITIVE));
					map.put("userType",viewModel.userType);
					Role roledataduplication = genericDAO.findOneByAttribute(
							viewModel.entityObject, map, Role.class);
					
					if (roledata != null) 
					{
						if(null != roledataduplication
								&& !(roledata.getCode()
										.equalsIgnoreCase(roledataduplication
												.getCode()))) {
							response.success = false;
							response.message = CommonUtils
									.getMessage("error.updateRoledata.exists");
						}
						else{
							// override the old properties with new properties exists in the
							// right side object
							notNull.copyProperties(roledata, role);
							// Update the existing record wi8th new values.
							genericDAO.updateById(viewModel.entityObject, roledata,
									roledata.getId());
							Role updatedRole = genericDAO.findOneById(
									viewModel.entityObject, roledata.getId(),
									Role.class);
							response.success = true;
							response.ViewModel = modelmapper.map(updatedRole,RoleViewModel.class);
							response.message = CommonUtils
									.getMessage("success.update.Role");	
						}
						
					} else {
						// To check whether the entity code exists if not returns the
						// response as false
						response.success = false;
						response.message = CommonUtils
								.getMessage("error.rolecode.not.exists");

					}
				} catch (Exception e) {
					// Exception Handling
					response.success = false;
					response.message = CommonUtils.getMessage("error.processfailure");
					response.ExceptionMessage = e.getMessage();
				}
				return response;
	}

	@Override
	public Response findAllByAttributes(RoleViewModel viewModel)
			throws Exception {
		// TODO Auto-generated method stub
				Response response = new Response();
				try {
				// Converting MasterViewModel object to MasterEntity Pojo
					Role data = modelmapper
						.map(viewModel, Role.class);
				Gson gson = new Gson();
				// Converting MasterEntity to a JSON string
				String role = gson.toJson(data);
				// Converting JON string to a HashMap
				Map<String, Object> map = new Gson().fromJson(role,
						new TypeToken<HashMap<String, Object>>() {
						}.getType());
				
				if (viewModel.where != null) {
					// for appending the where and the like for search criteria
					viewModel.where = helperUtil
							.ConstructWhereClauseFromJson(viewModel.where);
				}
				// Fetch the Entity based on the search criteria
				List<Role> allRole = genericDAO.findAllByAttributes(
						viewModel.entityObject, map, Role.class,
						viewModel.limit, viewModel.skip);

				
					// Returns true if there is a record matching the criteria
					if (null != allRole && allRole.size() > 0) {
						response.success = true;
						response.message = CommonUtils
								.getMessage("success.role.Criteria.search");
						response.ViewModels = allRole;
					} else {
						response.success = false;
						response.message = CommonUtils
								.getMessage("error.role.Criteria.search");
					}

				} catch (Exception e) {
					// Handling Exception
					response.success = false;
					response.message = CommonUtils.getMessage("error.processfailure");
					// response.status = false;
					response.ExceptionMessage = e.getMessage();
				}
				return response;
	}

	
	
}