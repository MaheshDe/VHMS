package vHMS.Facade;

import java.io.IOException;

import vHMS.Core.ViewModels.Response;
import vHMS.Core.ViewModels.RoleViewModel;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface IRoleService {

	/*
	 * Implementation of interface IRoleService load method which is invoked for
	 * fetching the master list.
	 * 
	 * @see vHMS.Facade.IRoleService#all()
	 */
	public Response all(RoleViewModel viewModel)
			throws JsonProcessingException, IOException;

	/*
	 * Implementation of interface IRoleService create method which is invoked
	 * for creating the Role.
	 * 
	 * @see vHMS.Facade.IRoleService#create()
	 */
	public Response create(RoleViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IRoleService findById method which is invoked
	 * for finding/getting a Role by id.
	 * 
	 * @see vHMS.Facade.IRoleService#findById()
	 */
	public Response findById(RoleViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IRoleService count method which is invoked
	 * for getting the count of matching records.
	 * 
	 * @see vHMS.Facade.IRoleService#count()
	 */
	public Response count(RoleViewModel viewModel);

	/*
	 * Implementation of interface IRoleService delete method which is invoked
	 * for deleting a Role.
	 * 
	 * @see vHMS.Facade.IRoleService#delete()
	 */
	public Response delete(RoleViewModel viewModel);

	/*
	 * Implementation of interface IRoleService update method which is invoked
	 * for updating a Role data.
	 * 
	 * @see vHMS.Facade.IRoleService#update()
	 */
	public Response update(RoleViewModel viewModel);


	/*
	 * Implementation of interface IRoleService findAllByAttributes method which
	 * is invoked for finding the records/Role which satisfies the given
	 * criteria.
	 * 
	 * @see vHMS.Facade.IRoleService#findAllByAttributes()
	 */
	public Response findAllByAttributes(RoleViewModel viewModel)
			throws Exception;
	

}
