package vHMS.Facade;

import java.io.IOException;

import vHMS.Core.ViewModels.BranchViewModel;
import vHMS.Core.ViewModels.DoctorViewModel;
import vHMS.Core.ViewModels.Response;


import com.fasterxml.jackson.core.JsonProcessingException;

public interface IBranchService {

	/*
	 * Implementation of interface IBranchService load method which is invoked for
	 * fetching the master list.
	 * 
	 * @see vHMS.Facade.IBranchService#all()
	 */
	public Response all(BranchViewModel viewModel)
			throws JsonProcessingException, IOException;

	/*
	 * Implementation of interface IBranchService create method which is invoked
	 * for creating the branch.
	 * 
	 * @see vHMS.Facade.IBranchService#create()
	 */
	public Response create(BranchViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IBranchService findById method which is invoked
	 * for finding/getting a branch by id.
	 * 
	 * @see vHMS.Facade.IBranchService#findById()
	 */
	public Response findById(BranchViewModel viewModel) throws Exception;

	/*
	 * Implementation of interface IBranchService count method which is invoked
	 * for getting the count of matching records.
	 * 
	 * @see vHMS.Facade.IBranchService#count()
	 */
	public Response count(BranchViewModel viewModel);

	/*
	 * Implementation of interface IBranchService delete method which is invoked
	 * for deleting a branch.
	 * 
	 * @see vHMS.Facade.IBranchService#delete()
	 */
	public Response delete(BranchViewModel viewModel);

	/*
	 * Implementation of interface IBranchService update method which is invoked
	 * for updating a branch data.
	 * 
	 * @see vHMS.Facade.IBranchService#update()
	 */
	public Response update(BranchViewModel viewModel);


	/*
	 * Implementation of interface IBranchService findAllByAttributes method which
	 * is invoked for finding the records/branch which satisfies the given
	 * criteria.
	 * 
	 * @see vHMS.Facade.IBranchService#findAllByAttributes()
	 */
	public Response findAllByAttributes(BranchViewModel viewModel)
			throws Exception;
	

}
