/*
 * Action filter to allow Cross domain access for the rest apis exposed
 * */

package vHMS.Web.controllers;

import play.libs.F.Promise;
import play.mvc.Action;
import play.mvc.Http.Context;
import play.mvc.Http.Response;

/**
 * The CorsActionFilter to allow Cross domain access for the rest apis exposed
 * to the outside world
 * 
 * @author Karthiga Baskaran
 * @since 2014-09-17
 */
public class CorsActionFilter extends Action.Simple {
	// override call method to apply cors to response headers

	@Override
	public Promise call(Context context) throws Throwable {
		// Get response from the context
		Response response = context.response();
		// Apply cross orgin for all
		response.setHeader("Access-Control-Allow-Origin", "*");

		// Handle preflight requests with options
		if (context.request().method().equals("OPTIONS")) {
			// specify the methods types for which cors should be enabled
			response.setHeader("Access-Control-Allow-Methods",
					"POST, GET, OPTIONS, PUT, DELETE");
			response.setHeader("Access-Control-Max-Age", "3600");
			response.setHeader("Access-Control-Allow-Headers",
					"Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Auth-Token");
			response.setHeader("Access-Control-Allow-Credentials", "true");
			// return successful response
			return Promise.pure(ok());
		}

		response.setHeader("Access-Control-Allow-Headers",
				"X-Requested-With, Content-Type, X-Auth-Token");
		return delegate.call(context);
	}
}