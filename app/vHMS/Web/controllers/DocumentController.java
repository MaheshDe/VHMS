/*
 * File uploading controller and saving it in a local storage
 * */

package vHMS.Web.controllers;

import java.io.File;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import play.data.Form;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Result;
import vHMS.Core.ViewModels.DocumentViewModel;
import vHMS.Core.ViewModels.MasterViewModel;
import vHMS.Core.ViewModels.Response;
import vHMS.Facade.IDocumentService;
import Base.Common.Attribute.CorsComposition;
import Base.Utilities.CommonUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * The DocumentController deals with the document upload and download
 * 
 * @author Arokia Felsi
 * @since 2014-09-10
 */
@CorsComposition.Cors
@Component
public class DocumentController extends Controller {
	// created instance using DI.
	@Autowired
	@Qualifier("documentService")
	private IDocumentService documentService;

	// For invoking index page
	//@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	public Result index() {
		return play.mvc.Controller.ok();
	}

	/*
	 * For uploading a document.
	 * 
	 * @see BPM.Facade.IDocumentService#load() and
	 * 
	 * @play.mvc.Security.Authenticated(AuthenticateAttribute.class) is for
	 * authentication if the user is using the url without logining in to the
	 * application
	 */
	//@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.MultipartFormData.class)
	public Result Upload() throws Exception {
		Response response = new Response();

		if (request().body().isMaxSizeExceeded()) {
			response.success = false;
			response.message = CommonUtils
					.getMessage("error.request.exceeds.maxsize");
			return play.mvc.Controller.ok(play.libs.Json.toJson(response));
		}
		// Form data from the view
		MultipartFormData body = request().body().asMultipartFormData();

		DocumentViewModel viewModel = new DocumentViewModel();
		// For getting the request details as a map
		Map<String, String[]> key1 = body.asFormUrlEncoded();
		if (key1.size() > 0) {
			// For getting the request object by passing the key to the map
			String[] list = key1.get("data");
			if (list.length > 0) {
				String requestDetails = list[0];

				viewModel = new Gson().fromJson(requestDetails,
						DocumentViewModel.class);
			}
		}
		// key for getting the file upload info
		FilePart key = body.getFile(CommonUtils.getMessage("key.file.info"));

		viewModel.entityObject = CommonUtils.getMessage("document.entity");
		// if document does not exists
		if (key != null) {
			// set the file details in the viewmodel
			viewModel.fileName = key.getFilename();
			viewModel.contentType = key.getContentType();
			File file = key.getFile();
			// viewModel.file = new byte[(int) file.length()];
			viewModel.LoadedFile = file;
			viewModel.requestType = viewModel.requestType.toLowerCase();
			// upload the document
			response = documentService.load(viewModel);

			// return true if the document is uploaded
			if (response.success) {
				return play.mvc.Controller.ok(play.libs.Json.toJson(response));

			} else {
				return play.mvc.Controller.ok(play.libs.Json.toJson(response));
			}

			// return false if the document is not attached
		} else {
			response.success = false;
			response.message = CommonUtils.getMessage("error.document.attach");

			if (response.success) {
				return play.mvc.Controller.ok(play.libs.Json.toJson(response));

			} else {
				return play.mvc.Controller.ok(play.libs.Json.toJson(response));
			}
		}

	}

	/*
	 * For Downloading a document.
	 * 
	 * @see BPM.Facade.IDocumentService#load() and
	 * 
	 * @play.mvc.Security.Authenticated(AuthenticateAttribute.class) is for
	 * authentication if the user is using the url without logining in to the
	 * application
	 */
	// @play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	public Result download(String code) throws Exception {
		DocumentViewModel viewModel = new DocumentViewModel();
		// Entity document
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.code = code;
		// getting requesttype from the request url
		viewModel.requestType = Form.form().bindFromRequest()
				.get("requestType").toLowerCase();
		// Response from the service for download
		Response response = documentService.download(viewModel);
		if (response.success) {
			// Getting input Stream after downloading
			DocumentViewModel view = (DocumentViewModel) response.ViewModel;
			// Set the response content type
			response().setContentType(view.contentType);
			// Set the response relevant headers. TODO: the file name needs to
			// dynamic
			response().setHeader("Content-disposition",
					"attachment; filename=" + view.fileName);
			// obtains response's output stream

			return play.mvc.Controller.ok(view.LoadedFile);
		}

		else
			return play.mvc.Controller.ok(play.libs.Json.toJson(response));
	}

	/*
	 * For delete a document.
	 * 
	 * @see BPM.Facade.IDocumentService#load() and
	 * 
	 * @play.mvc.Security.Authenticated(AuthenticateAttribute.class) is for
	 * authentication if the user is using the url without logining in to the
	 * application
	 */
	// @play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	public Result delete(String code) throws Exception {
		DocumentViewModel viewModel = new DocumentViewModel();
		viewModel.code = code;
		// getting requesttype from the request url
		viewModel.requestType = Form.form().bindFromRequest()
				.get("requestType").toLowerCase();

		// Response from the service for delete
		Response response = documentService.delete(viewModel);

		return play.mvc.Controller.ok(play.libs.Json.toJson(response));
	}

	// getting list of documents

	public Result list(String code) throws Exception {
		DocumentViewModel viewModel = new DocumentViewModel();
		viewModel.requestCode = code;
		// Response from the service for download
		Response response = documentService.list(viewModel);
		return play.mvc.Controller.ok(play.libs.Json.toJson(response));
	}

	/**
	 * This method is used for cross domain url access
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	public Result checkPreFlight() {
		response().setHeader("Access-Control-Allow-Origin", "*"); // Need to add
																	// the
																	// correct
																	// domain in
																	// here!!
		response().setHeader("Access-Control-Allow-Methods",
				"POST, GET, OPTIONS, PUT, DELETE"); // Only allow POST
		response().setHeader("Access-Control-Max-Age", "300"); // Cache response
																// for 5 minutes
		response().setHeader("Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept"); // Ensure
																	// this
																	// header is
																	// also
																	// allowed!
		return ok();
	}

	@BodyParser.Of(BodyParser.Json.class)
	public Result findAllByAttributes() throws Exception {
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
				.create();
		DocumentViewModel viewModel = gson.fromJson(body,
				DocumentViewModel.class);
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.where = Form.form().bindFromRequest().get("where");
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip = Form.form().bindFromRequest().get("skip");
		return play.mvc.Controller.ok(play.libs.Json.toJson(documentService
				.findAllByAttributes(viewModel)));
	}
	
	
}
