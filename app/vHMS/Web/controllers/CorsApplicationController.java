package vHMS.Web.controllers;

import org.springframework.stereotype.Component;

import play.mvc.Controller;
import play.mvc.Result;
import Base.Common.Attribute.CorsComposition;

/**
 * The CorsApplicationController to allow Cross domain access for the rest apis
 * exposed to the outside world
 * 
 * @author Arokia Felsi.S
 * @since 2014-10-10
 */

// For Cross Doamin url access
@CorsComposition.Cors
@Component
public class CorsApplicationController extends Controller {

	/**
	 * The preflight method for accessing all the url from the outside world
	 * 
	 */
	public Result preflight(String path) {
		return play.mvc.Controller.ok("");
	}
}