package vHMS.Web.controllers;

import org.springframework.stereotype.Component;

/**
 * The Genger Controller deals with basic operations like create, update, delete ,search 
 * for an user's gender in VHMS
 * 
 * 
 * @author JasmitaB
 * @since 2015-08-11
 */

@Component
public class GenderController extends MasterController{
	
}