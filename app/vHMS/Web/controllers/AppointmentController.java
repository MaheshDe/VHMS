package vHMS.Web.controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import play.data.Form;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import vHMS.Core.ViewModels.AppointmentDetailsViewModel;
import vHMS.Core.ViewModels.AppointmentViewModel;
import vHMS.Core.ViewModels.ReportViewModel;
import vHMS.Core.ViewModels.Response;
import vHMS.Facade.IAppointmentService;
import Base.Common.Attribute.AuthenticateAttribute;
import Base.Utilities.DateDeserializer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * * <h1>AppointmentController</h1> The AppointmentController deals with the
 * Appointment Cration/updation/deletion/Retrival of a patient Appointment. It
 * deals with the entire operations related to a Appointment.
 * 
 * 
 * @author ShunmugaRajaG
 * 
 */
@Component
public class AppointmentController extends Controller {
	@Autowired
	private IAppointmentService appointmentService;

	/**
	 * This method is used to create/insert Appointment.
	 * 
	 * @return Result(Play.mvc.Result).
	 */

	@BodyParser.Of(BodyParser.Json.class)
	public Result create() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		// Specify the date format along with the Gson object if the incoming
		// requests contains date.
		/*
		 * Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
		 * .create();
		 */
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
		Gson gson = gsonBuilder.create();
		// Converting JSON Object into ViewModel Object
		AppointmentViewModel viewModel = gson.fromJson(body,
				AppointmentViewModel.class);
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];

		return play.mvc.Controller.ok(play.libs.Json.toJson(appointmentService
				.create(viewModel)));
	}

	/**
	 * This method is used to update/edit a Appointment depending on the id.
	 * 
	 * @param id
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result updateByCode(String code) throws Exception {
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		/*
		 * Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
		 * .create();
		 */
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
		Gson gson = gsonBuilder.create();
		// Converting JSON Object into ViewModel Object
		AppointmentViewModel viewModel = gson.fromJson(body,
				AppointmentViewModel.class);
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		// Set the code of the record for which it should be updated.
		viewModel.code = code;
		return play.mvc.Controller.ok(play.libs.Json.toJson(appointmentService
				.update(viewModel)));
	}

	/**
	 * This method is used to find/fetch all the entries for the Appointment.
	 * 
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	 
	public Result findAll() throws Exception {

		AppointmentViewModel viewModel = new AppointmentViewModel();
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.where = Form.form().bindFromRequest().get("where");
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip = Form.form().bindFromRequest().get("skip");

		return play.mvc.Controller.ok(play.libs.Json.toJson(appointmentService
				.all(viewModel)));

	}

	/**
	 * This method is used to find/fetch the entries By Criteria for the
	 * Appointment.
	 * 
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result findAllByAttributes() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		// Specify the date format along with the Gson object if the incoming
		// requests contains date.
		/*
		 * Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
		 * .create();
		 */
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
		Gson gson = gsonBuilder.create();
		// Converting JSON Object into ViewModel Object
		AppointmentViewModel viewModel = gson.fromJson(body,
				AppointmentViewModel.class);
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip = Form.form().bindFromRequest().get("skip");
		return play.mvc.Controller.ok(play.libs.Json.toJson(appointmentService
				.findAllByAttributes(viewModel)));
	}

	/**
	 * This method is used to find/fetch a Appointment depending on the code.
	 * 
	 * @param code
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	
	public Result findByCode(String code) throws Exception {

		AppointmentViewModel viewModel = new AppointmentViewModel();
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.code = code;

		return play.mvc.Controller.ok(play.libs.Json.toJson(appointmentService
				.findByCode(viewModel)));
	}

	/***
	 * This method is will generate report for appointment
	 * 
	 * @param reportFormat
	 * @return
	 * @throws Exception
	 */
	// @play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.FormUrlEncoded.class)
	public Result appointmentReport(String reportFormat) throws Exception {
		// get all the params from the request.
		String map = request().body().asFormUrlEncoded().get("request")[0];
		ObjectMapper mapper = new ObjectMapper();
		JsonNode actualObj = mapper.readTree(map);
		String body = actualObj.findPath("request").toString();
		ReportViewModel reportViewModel = new ReportViewModel();
		// Converting JSON Object into ViewModel Object
		AppointmentDetailsViewModel viewModel = new Gson().fromJson(body,
				AppointmentDetailsViewModel.class);
		reportViewModel.ModelObject = viewModel;
		reportViewModel.reportOutputFormat = reportFormat;
		Response response = appointmentService
				.appointmentReport(reportViewModel);
		if (response.success) {
			reportViewModel = (ReportViewModel) response.ViewModel;
			response().setContentType(reportViewModel.contentType);
			response().setHeader(
					"Content-disposition",
					"attachment; filename="
							+ reportViewModel.reportOutputFileName);
			return play.mvc.Controller.ok(reportViewModel.reportFile);
		} else
			return play.mvc.Controller.ok(play.libs.Json.toJson(response));
	}

	/**
	 * This method is used to find/fetch the entries By Criteria for the
	 * Appointment.
	 * 
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */
	// @play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result checkAppointmentAvailablity() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		// Specify the date format along with the Gson object if the incoming
		// requests contains date.
		/*
		 * Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
		 * .create();
		 */
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
		Gson gson = gsonBuilder.create();

		// Converting JSON Object into ViewModel Object
		AppointmentViewModel viewModel = gson.fromJson(body,
				AppointmentViewModel.class);
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip = Form.form().bindFromRequest().get("skip");
		return play.mvc.Controller.ok(play.libs.Json.toJson(appointmentService
				.checkAppointmentAvailablity(viewModel)));
	}

}
