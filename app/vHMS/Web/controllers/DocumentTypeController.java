package vHMS.Web.controllers;

import org.springframework.stereotype.Component;

/**
 * The DocumentType Controller deals with basic operations like create, update, delete ,search 
 * for the type of documents/attachments user can upload in VHMS .
 * 
 * 
 * @author JasmitaB
 * @since 2015-08-10
 */

@Component
public class DocumentTypeController extends MasterController{
	
}