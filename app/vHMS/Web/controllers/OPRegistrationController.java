package vHMS.Web.controllers;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import play.data.Form;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import vHMS.Core.ViewModels.OPRegistrationViewModel;
import vHMS.Facade.OPRegistrationService;
import Base.Common.Attribute.AuthenticateAttribute;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;



/**
 * <h1>OP Registration</h1> The OPatientRegistration deals with the op
 * registration/updation/deletion of a op registration. It deals with the
 * entire operations related to a patient.
 * 
 * @author padmavathi
 * @version 1.0
 * @since 2015-07-17
 */
@Component
public class OPRegistrationController extends Controller {
	
	@Autowired
	private OPRegistrationService opRegistrationService;

	/**
	 * This method is used to create/register a patient in the hospital
	 * 
	 * @return Result(Play.mvc.result) This returns the response object
	 *         including the object created.
	 * @throws Exception
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result create() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();

		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
		// Converting JSON Object into ViewModel Object
		OPRegistrationViewModel viewModel = gson.fromJson(body,
				OPRegistrationViewModel.class);

		viewModel.entityObject = request().path().split("/")[1];

		return play.mvc.Controller.ok(play.libs.Json
				.toJson(opRegistrationService.create(viewModel)));
	}
	
	
	/**
	 * This method is used to find/fetch all the entries for the patient.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	public Result findAll() throws JsonProcessingException, IOException {

		OPRegistrationViewModel viewModel = new OPRegistrationViewModel();
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.where = Form.form().bindFromRequest().get("where");
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip = Form.form().bindFromRequest().get("skip");

		return play.mvc.Controller.ok(play.libs.Json.toJson(opRegistrationService
				.all(viewModel)));

	}

	/**
	 * This method is used to find/fetch a Patients depending on the code.
	 * 
	 * @param code
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	*/
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	public Result findByCode(String code) throws Exception {

		OPRegistrationViewModel viewModel = new OPRegistrationViewModel();
		// getting the entity from the url
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.code = code;

		return play.mvc.Controller.ok(play.libs.Json.toJson(opRegistrationService
				.findByCode(viewModel)));
	}

	
	/**
	 * This method is used to find/fetch a patient depending on select.
	 * 
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result findAllByAttributes() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();

		Gson gson =new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
		// Converting JSON Object into ViewModel Object
		OPRegistrationViewModel viewModel = gson.fromJson(body,
				OPRegistrationViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip = Form.form().bindFromRequest().get("skip");
		return play.mvc.Controller.ok(play.libs.Json.toJson(opRegistrationService
				.findAllByAttributes(viewModel)));
	}
	
	

	
	
	
}
