package vHMS.Web.controllers;


import java.io.IOException;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import Base.Common.Attribute.AuthenticateAttribute;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import play.data.Form;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import vHMS.Core.ViewModels.FloorViewModel;
import vHMS.Facade.IFloorService;




/**
 * The RoomController deals with the all the operations like create/update/find/search
 * for floor for the vHMS app.
 * 
 * 
 * @author MaheshDe
 * @since 2015-08-27
 */ 

@Component
public class FloorController extends Controller {
	@Autowired
	private IFloorService FloorService;
	

	/**
	 * This method is used to find/fetch all the entries for the Floors.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	
	//@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	public Result findAll() throws JsonProcessingException, IOException {

		FloorViewModel viewModel = new FloorViewModel();
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.where = Form.form().bindFromRequest().get("where");
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip = Form.form().bindFromRequest().get("skip");	

		return play.mvc.Controller.ok(play.libs.Json.toJson(FloorService.all(viewModel)));

	}

	/**
	 * This method is used to find/fetch a Floor depending on the code.
	 * 
	 * @param code
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 * @throws Exception 
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	
	public Result findById(String code) throws Exception {

		FloorViewModel viewModel = new FloorViewModel();
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.code = code;		
		return play.mvc.Controller.ok(play.libs.Json.toJson(FloorService.findById(viewModel)));
	}

	/**
	 * This method is used to find/fetch all Floors depending on select.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result findAllByAttributes() throws Exception {
		
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		Gson gson =new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
				
		FloorViewModel viewModel = gson.fromJson(body,
				FloorViewModel.class);
		
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.where = Form.form().bindFromRequest().get("where");
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip = Form.form().bindFromRequest().get("skip");		
		return play.mvc.Controller.ok(play.libs.Json.toJson(FloorService.findAllByAttributes(viewModel)));
	}

	/**
	 * This method is used to create/insert an Floor record.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result create() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
				.create();
		// Converting JSON Object into ViewModel Object
		FloorViewModel viewModel = new Gson()
				.fromJson(body, FloorViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];
		//UserService.create(viewModel);
		return play.mvc.Controller.ok(play.libs.Json.toJson(FloorService
				.create(viewModel)));
	}

	
	/**
	 * This method is used to update/edit a Floor depending on the code.
	 * 
	 * @param id
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result updateById(String code) throws JsonProcessingException,
			IOException {
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		// Converting JSON Object into ViewModel Object
		FloorViewModel viewModel = new Gson()
				.fromJson(body, FloorViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.code = code;
		//UserService.update(viewModel);
		return play.mvc.Controller.ok(play.libs.Json.toJson(FloorService
				.update(viewModel)));
	}

	/**
	 * This method returns the count/ total number of the Floor.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result count() throws JSONException {
		FloorViewModel viewModel = new FloorViewModel();
		viewModel.entityObject = request().path().split("/")[1];		
		return play.mvc.Controller.ok(play.libs.Json.toJson( FloorService.count(viewModel)));

	}

	/**
	 * This method is used to delete an Floor based on code.
	 * 
	 * @param id
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 */
	
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result delete(String code) throws JSONException,
			JsonProcessingException, IOException {
		
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		// Converting JSON Object into ViewModel Object
		FloorViewModel viewModel = new Gson()
		  .fromJson(body, FloorViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.code = code;
		//UserService.delete(viewModel);
		return play.mvc.Controller.ok(play.libs.Json.toJson(FloorService
				.delete(viewModel)));
	}




	

	
	
	
	
	
	
}