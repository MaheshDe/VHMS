package vHMS.Web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import play.data.Form;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import vHMS.Core.ViewModels.MasterViewModel;
import vHMS.Facade.IMasterService;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * MasterController deals with all the Master
 * Creation/updation/deletion/Retrival. It deals with the entire operations
 * related to all the master data.
 * 
 * @author DivyaP
 * @version 1.0
 * @since 2015-08-10
 */
@Component
public class MasterController extends Controller {

	@Autowired
	private IMasterService masterService;

	/**
	 * This method is used to find/fetch all the entries for the requested
	 * master.
	 * 
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */
	public Result findAll() throws Exception {
		MasterViewModel viewModel = new MasterViewModel();
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.where = Form.form().bindFromRequest().get("where");
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip = Form.form().bindFromRequest().get("skip");
		return play.mvc.Controller.ok(play.libs.Json.toJson(masterService
				.all(viewModel)));
	}

	/**
	 * This method is used to find/fetch a master depending on the code.
	 * 
	 * @param code
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */
	public Result findByCode(String code) throws Exception {
		MasterViewModel viewModel = new MasterViewModel();
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		// Set the id of the record to be found.
		viewModel.code = code;

		return play.mvc.Controller.ok(play.libs.Json.toJson(masterService
				.findById(viewModel)));
	}

	/**
	 * This method is used to find/fetch a master depending on select.
	 * 
	 * @return Result(Play.mvc.Result).
	 */

	@BodyParser.Of(BodyParser.Json.class)
	public Result findAllByAttributes() throws Exception {
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
				.create();
		MasterViewModel viewModel = gson.fromJson(body,
				MasterViewModel.class);
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.where = Form.form().bindFromRequest().get("where");
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip = Form.form().bindFromRequest().get("skip");
		return play.mvc.Controller.ok(play.libs.Json.toJson(masterService
				.findAllByAttributes(viewModel)));
	}

	/**
	 * This method is used to create/insert a master.
	 * 
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */

	@BodyParser.Of(BodyParser.Json.class)
	public Result create() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
				.create();
		// Converting JSON Object into ViewModel Object
		MasterViewModel viewModel =gson.fromJson(body,
				MasterViewModel.class);
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		return play.mvc.Controller.ok(play.libs.Json.toJson(masterService
				.create(viewModel)));
	}

	/**
	 * This method is used to update/edit a master depending on the code.
	 * 
	 * @param code 
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */

	@BodyParser.Of(BodyParser.Json.class)
	public Result updateByCode(String code) throws Exception {
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();

		// Converting JSON Object into ViewModel Object
		MasterViewModel viewModel = new Gson().fromJson(body,
				MasterViewModel.class);
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.code = code;

		return play.mvc.Controller.ok(play.libs.Json.toJson(masterService
				.update(viewModel)));
	}

	/**
	 * This method returns the count/ total number of the requested master.
	 * 
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */

	public Result count() throws Exception {
		MasterViewModel viewModel = new MasterViewModel();
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		return play.mvc.Controller.ok(play.libs.Json.toJson(masterService
				.count(viewModel)));
	}

	/**
	 * This method is used to delte a master based on id.
	 * 
	 * @param id
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */

	@BodyParser.Of(BodyParser.Json.class)
	public Result delete(String code) throws Exception {
		MasterViewModel viewModel = new MasterViewModel();
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		// Set the id of the record to be deleted.
		viewModel.code = code;

		return play.mvc.Controller.ok(play.libs.Json.toJson(masterService
				.delete(viewModel)));
	}

}
