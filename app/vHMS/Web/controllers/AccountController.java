/**
 * Controller for User Account. Restapi for user account  UI
 */
package vHMS.Web.controllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import vHMS.Core.Models.User;
import vHMS.Core.ViewModels.Response;
import vHMS.Core.ViewModels.UserViewModel;
import vHMS.Facade.IAccountService;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;

/**
 * The AccountController deals with the account/authentication for an
 * employee/user for the vHMS app, it deals with login,logout and session
 * controls.
 * 
 * @author DivyaP
 * @version 1.0
 * @since 2015-08-10
 */

@Component
public class AccountController extends Controller {

	@Autowired
	private IAccountService accountService;

	/**
	 * This method is used to return the index page for the home.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	public Result index() {
		return play.mvc.Controller.ok(views.html.main.render());
	}

	/**
	 * This method is used to authenticate and employee when he/she tries to
	 * login with credentials.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	@BodyParser.Of(BodyParser.Json.class)
	//@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	public Result Authenticate() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();

		// Converting JSON Object into ViewModel Object
		UserViewModel viewModel = new Gson()
				.fromJson(body, UserViewModel.class);
		// getting URL for authentication
		viewModel.entityObject = request().path().split("/")[1];

		Response response = accountService.AuthenticateUser(viewModel);
		// after getting the response checking whether the response.ViewModel is
		// null
		if (response.ViewModel != null) {
			ModelMapper modelmapper = new ModelMapper();
			User user = modelmapper.map(response.ViewModel, User.class);
			// Set the looged on user variables in session.
			play.mvc.Controller.session().clear();
			play.mvc.Controller.session("LoggedOnUser", user.getId());
		}
		return play.mvc.Controller.ok(play.libs.Json.toJson(response));
	}

	/**
	 * This method is used to signout process when an employee tries to logout
	 * from the app.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	public Result SignOut() {
		play.mvc.Controller.session().clear();
		String data = "Success";
		return play.mvc.Controller.ok(play.libs.Json.toJson(data));
	}
}
