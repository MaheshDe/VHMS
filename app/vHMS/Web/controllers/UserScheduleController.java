package vHMS.Web.controllers;

import java.io.IOException;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import play.data.Form;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import vHMS.Core.ViewModels.UserScheduleViewModel;
import vHMS.Core.ViewModels.UserViewModel;
import vHMS.Facade.IUserScheduleService;
import vHMS.Facade.IUserService;
import Base.Common.Attribute.AuthenticateAttribute;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * The UserScheduleController deals with the all the operations like create/update/find/serach
 * for an employee/user for the vHMS app.
 * 
 * 
 * @author MaheshDe
 * @since 2015-08-21
 */ 

@Component
public class UserScheduleController extends Controller {
	@Autowired
	private IUserScheduleService UserScheduleService;

	/**
	 * This method is used to return the index page for the User/user.
	 * 
	 * @return Result(Play.mvc.Result).
	 */

	/*public Result index() {
		return play.mvc.Controller.ok(views.html.employee.index.render());
	}*/

	/**
	 * This method is used to find/fetch all the entries for the User/user schedule.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	//@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	
	public Result findAll() throws JsonProcessingException, IOException {

		UserScheduleViewModel viewModel = new UserScheduleViewModel();
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.where = Form.form().bindFromRequest().get("where");
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip = Form.form().bindFromRequest().get("skip");	

		return play.mvc.Controller.ok(play.libs.Json.toJson(UserScheduleService.all(viewModel)));

	}

	/**
	 * This method is used to find/fetch a User/user schedule depending on the code.
	 * 
	 * @param code
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 * @throws Exception 
	 */
	//@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	
	public Result findById(String code) throws Exception {

		UserScheduleViewModel viewModel = new UserScheduleViewModel();
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.code = code;		
		return play.mvc.Controller.ok(play.libs.Json.toJson(UserScheduleService.findById(viewModel)));
	}



	/**
	 * This method is used to create/insert an User/user schedule.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	
	@BodyParser.Of(BodyParser.Json.class)
	public Result create() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
				.create();
		// Converting JSON Object into ViewModel Object
		UserScheduleViewModel viewModel = new Gson()
				.fromJson(body, UserScheduleViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];
		//UserService.create(viewModel);
		return play.mvc.Controller.ok(play.libs.Json.toJson(UserScheduleService
				.create(viewModel)));
	}

	
	/**
	 * This method is used to update/edit a User/user schedule depending on the code.
	 * 
	 * @param code
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 */
	//@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result updateById(String code) throws JsonProcessingException,
			IOException {
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		// Converting JSON Object into ViewModel Object
		UserScheduleViewModel viewModel = new Gson()
				.fromJson(body, UserScheduleViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.code = code;
		//UserService.update(viewModel);
		return play.mvc.Controller.ok(play.libs.Json.toJson(UserScheduleService
				.update(viewModel)));
	}

	/**
	 * This method returns the count/ total number of the User/user schedule .
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	
	//@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result count() throws JSONException {
		UserScheduleViewModel viewModel = new UserScheduleViewModel();
		viewModel.entityObject = request().path().split("/")[1];		
		return play.mvc.Controller.ok(play.libs.Json.toJson( UserScheduleService.count(viewModel)));

	}

	/**
	 * This method is used to delete an User/user schedule based on Code.
	 * 
	 * @param code
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 */
	//@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result delete(String code) throws JSONException,
			JsonProcessingException, IOException {
		
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		// Converting JSON Object into ViewModel Object
		UserScheduleViewModel viewModel = new Gson()
		  .fromJson(body, UserScheduleViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.code = code;
		//UserService.delete(viewModel);
		return play.mvc.Controller.ok(play.libs.Json.toJson(UserScheduleService
				.delete(viewModel)));
	}
	
	/**
	 * This method is used to find/fetch all Users/users schedule depending on select.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	//@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result findAllByAttributes() throws Exception {
		
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		Gson gson =new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
		
		UserScheduleViewModel viewModel = gson.fromJson(body,
				UserScheduleViewModel.class);
	
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.where = Form.form().bindFromRequest().get("where");
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip = Form.form().bindFromRequest().get("skip");		
		return play.mvc.Controller.ok(play.libs.Json.toJson(UserScheduleService.findAllByAttributes(viewModel)));
	}

}
