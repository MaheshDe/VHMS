package vHMS.Web.controllers;

import java.io.IOException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import play.data.Form;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import vHMS.Core.Models.PatientRegistration;
import vHMS.Core.ViewModels.PatientRegistrationViewModel;
import vHMS.Core.ViewModels.Response;
import vHMS.Facade.IPatientRegistrationService;
import Base.Common.Attribute.AuthenticateAttribute;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * <h1>Patient Management</h1> The PatientManagement deals with the patient
 * registration/updation/deletion of a patient registration. It deals with the
 * entire operations related to a patient.
 * 
 * @author DivyaP
 * @version 1.0
 * @since 2015-07-17
 */
@Component
public class PatientRegistrationController extends Controller {

	@Autowired
	private IPatientRegistrationService patientRegistrationService;

	/**
	 * This method is used to create/register a patient in the hospital
	 * 
	 * @return Result(Play.mvc.result) This returns the response object
	 *         including the object created.
	 * @throws Exception
	 */
	@BodyParser.Of(BodyParser.Json.class)
	public Result create() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();

		Gson gson =new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
		// Converting JSON Object into ViewModel Object
		PatientRegistrationViewModel viewModel = gson.fromJson(body,
				PatientRegistrationViewModel.class);

		viewModel.entityObject = request().path().split("/")[1];

		return play.mvc.Controller.ok(play.libs.Json
				.toJson(patientRegistrationService.create(viewModel)));
	}
	
	
	/**
	 * This method is used to find/fetch all the entries for the patient.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	public Result findAll() throws JsonProcessingException, IOException {

		PatientRegistrationViewModel viewModel = new PatientRegistrationViewModel();
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.where = Form.form().bindFromRequest().get("where");
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip = Form.form().bindFromRequest().get("skip");

		return play.mvc.Controller.ok(play.libs.Json.toJson(patientRegistrationService
				.all(viewModel)));

	}

	/**
	 * This method is used to find/fetch a Patients depending on the code.
	 * 
	 * @param code
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	*/
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	public Result findByCode(String code) throws Exception {

		PatientRegistrationViewModel viewModel = new PatientRegistrationViewModel();
		// getting the entity from the url
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.code = code;

		return play.mvc.Controller.ok(play.libs.Json.toJson(patientRegistrationService
				.findByCode(viewModel)));
	}

	
	/**
	 * This method is used to find/fetch a patient depending on select.
	 * 
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result findAllByAttributes() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();

		Gson gson =new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
		// Converting JSON Object into ViewModel Object
		PatientRegistrationViewModel viewModel = gson.fromJson(body,
				PatientRegistrationViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip = Form.form().bindFromRequest().get("skip");
		return play.mvc.Controller.ok(play.libs.Json.toJson(patientRegistrationService
				.findAllByAttributes(viewModel)));
	}
	/**
	 * This method is used to update/edit a patient depending on the code.
	 * 
	 * @param code
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 * @throws Exception 
	 */
	
	@BodyParser.Of(BodyParser.Json.class)
	public Result updateByCode(String code) throws Exception {
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd,HH:mm:ss").create();
		// Converting JSON Object into ViewModel Object
		PatientRegistrationViewModel viewModel = gson.fromJson(body,
				PatientRegistrationViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];

		viewModel.code = code;

		//patientManagementService.update(viewModel);
		return play.mvc.Controller.ok(play.libs.Json.toJson(patientRegistrationService
				.update(viewModel)));
	}

	
	@BodyParser.Of(BodyParser.Json.class)
	//@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	public Result Authenticate() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();

		// Converting JSON Object into ViewModel Object
		PatientRegistrationViewModel viewModel = new Gson().fromJson(body,PatientRegistrationViewModel.class);
		//getting URL for authentication
		viewModel.entityObject = request().path().split("/")[1];
		
		Response response = patientRegistrationService.Authenticate(viewModel);
		//after getting the response checking whether the response.ViewModel is null
		if (response.ViewModel != null) {
			ModelMapper modelmapper = new ModelMapper();
			PatientRegistration user = modelmapper.map(response.ViewModel,
					PatientRegistration.class);

			play.mvc.Controller.session().clear();
			play.mvc.Controller.session("LoggedOnUser", user.getFirstName());
		}
		return play.mvc.Controller.ok(play.libs.Json.toJson(response));		
	}
	

	/**
	 * This method is used reset the password of an User when requested for the
	 * same.
	 * 
	 * @return Result(Play.mvc.Result).
	 */

	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result changePassword() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		// Converting JSON Object into ViewModel Object
		PatientRegistrationViewModel viewModel = new Gson()
				.fromJson(body, PatientRegistrationViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];

		return play.mvc.Controller.ok(play.libs.Json.toJson(patientRegistrationService
				.changePassword(viewModel)));
	}
	
	
	/**
	 * This method is used to reset the password of an User and send a mail with
	 * the new password when requested for the same.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	@BodyParser.Of(BodyParser.Json.class)
	public Result forgotPassword() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		// Converting JSON Object into ViewModel Object
		PatientRegistrationViewModel viewModel = new Gson()
				.fromJson(body, PatientRegistrationViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];

		return play.mvc.Controller.ok(play.libs.Json.toJson(patientRegistrationService
				.forgotPassword(viewModel)));
	}
	
}
