package vHMS.Web.controllers;

import org.springframework.stereotype.Component;

/**
 * The MaritalStatus Controller deals with basic operations like create, update, delete ,search 
 * for a Marital status for an user in VHMS
 * 
 * 
 * @author JasmitaB
 * @since 2015-08-11
 */

@Component
public class MaritalStatusController extends MasterController{
	
}