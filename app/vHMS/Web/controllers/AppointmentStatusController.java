package vHMS.Web.controllers;

import org.springframework.stereotype.Component;

/**
 * The Appointment Status Controller deals with basic operations like create, update, delete ,search 
 * for an appointment status in vHMS.
 * 
 * 
 * @author JasmitaB
 * @since 2015-08-11
 */

@Component
public class AppointmentStatusController extends MasterController{
	
}