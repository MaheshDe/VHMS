package vHMS.Web.controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import play.data.Form;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import vHMS.Core.ViewModels.FaqViewModel;
import vHMS.Facade.IFaqService;
import Base.Common.Attribute.AuthenticateAttribute;
import Base.Utilities.DateDeserializer;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * * FaqController The FaqController deals with the
 * Faq Cration/updation/deletion/Retrival of a patient Faq. It
 * deals with the entire operations related to a Faq.
 * 
 * 
 * @author 
 * 
 */
@Component
public class FaqController extends Controller {
	@Autowired
	private IFaqService faqService;

	/**
	 * This method is used to create/insert faq.
	 * 
	 * @return Result(Play.mvc.Result).
	 */

	@BodyParser.Of(BodyParser.Json.class)
	public Result create() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		// Specify the date format along with the Gson object if the incoming
		// requests contains date.
		/*
		 * Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
		 * .create();
		 */
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
		Gson gson = gsonBuilder.create();
		// Converting JSON Object into ViewModel Object
		FaqViewModel viewModel = gson.fromJson(body,
				FaqViewModel.class);
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];

		return play.mvc.Controller.ok(play.libs.Json.toJson(faqService
				.create(viewModel)));
	}

	/**
	 * This method is used to update/edit a faq depending on the id.
	 * 
	 * @param id
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */

	@BodyParser.Of(BodyParser.Json.class)
	public Result updateByCode(String code) throws Exception {
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		/*
		 * Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
		 * .create();
		 */
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
		Gson gson = gsonBuilder.create();
		// Converting JSON Object into ViewModel Object
		FaqViewModel viewModel = gson.fromJson(body,
				FaqViewModel.class);
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		// Set the code of the record for which it should be updated.
		viewModel.code = code;
		return play.mvc.Controller.ok(play.libs.Json.toJson(faqService
				.update(viewModel)));
	}

	/**
	 * This method is used to find/fetch all the entries for the faq.
	 * 
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	//@BodyParser.Of(BodyParser.Json.class)
	public Result findAll() throws Exception {

		FaqViewModel viewModel = new FaqViewModel();
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.where = Form.form().bindFromRequest().get("where");
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip  = Form.form().bindFromRequest().get("skip");

		return play.mvc.Controller.ok(play.libs.Json.toJson(faqService
				.all(viewModel)));

	}

	/**
	 * This method is used to find/fetch the entries By Criteria for the
	 * faq.
	 * 
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result findAllByAttributes() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		// Specify the date format along with the Gson object if the incoming
		// requests contains date.
		/*
		 * Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
		 * .create();
		 */
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
		Gson gson = gsonBuilder.create();
		// Converting JSON Object into ViewModel Object
		FaqViewModel viewModel = gson.fromJson(body,
				FaqViewModel.class);
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip = Form.form().bindFromRequest().get("skip");
		return play.mvc.Controller.ok(play.libs.Json.toJson(faqService
				.findAllByAttributes(viewModel)));
	}
	/**
	 * This method is used to delete the faq
	 * 
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */

	@BodyParser.Of(BodyParser.Json.class)
	public Result delete(String code) throws Exception {
		FaqViewModel viewModel = new FaqViewModel();
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		// Set the id of the record to be deleted.
		viewModel.code = code;

		return play.mvc.Controller.ok(play.libs.Json.toJson(faqService
				.delete(viewModel)));
	}

	

}
