package vHMS.Web.controllers;


import java.io.IOException;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import Base.Common.Attribute.AuthenticateAttribute;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import play.data.Form;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import vHMS.Core.ViewModels.BuildingViewModel;
import vHMS.Facade.IBuildingService;




/**
 * The BuildingController deals with the all the operations like create/update/find/serach
 * for building for the vHMS app.
 * 
 * 
 * @author MaheshDe
 * @since 2015-08-27
 */ 

@Component
public class BuildingController extends Controller {
	@Autowired
	private IBuildingService BuildingService;
	

	/**
	 * This method is used to find/fetch all the entries for the building.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	//@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	public Result findAll() throws JsonProcessingException, IOException {

		BuildingViewModel viewModel = new BuildingViewModel();
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.where = Form.form().bindFromRequest().get("where");
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip = Form.form().bindFromRequest().get("skip");	

		return play.mvc.Controller.ok(play.libs.Json.toJson(BuildingService.all(viewModel)));

	}

	/**
	 * This method is used to find/fetch a building depending on the code.
	 * 
	 * @param code
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 * @throws Exception 
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	
	public Result findById(String code) throws Exception {

		BuildingViewModel viewModel = new BuildingViewModel();
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.code = code;		
		return play.mvc.Controller.ok(play.libs.Json.toJson(BuildingService.findById(viewModel)));
	}

	/**
	 * This method is used to find/fetch all buildings depending on search criteria.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result findAllByAttributes() throws Exception {
		
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		Gson gson =new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
				
		BuildingViewModel viewModel = gson.fromJson(body,
				BuildingViewModel.class);
		
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.where = Form.form().bindFromRequest().get("where");
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip = Form.form().bindFromRequest().get("skip");		
		return play.mvc.Controller.ok(play.libs.Json.toJson(BuildingService.findAllByAttributes(viewModel)));
	}

	/**
	 * This method is used to create/insert an building record.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result create() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
				.create();
		// Converting JSON Object into ViewModel Object
		BuildingViewModel viewModel = new Gson()
				.fromJson(body, BuildingViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];
		//UserService.create(viewModel);
		return play.mvc.Controller.ok(play.libs.Json.toJson(BuildingService
				.create(viewModel)));
	}

	
	/**
	 * This method is used to update/edit a building depending on the code.
	 * 
	 * @param id
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result updateById(String code) throws JsonProcessingException,
			IOException {
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		// Converting JSON Object into ViewModel Object
		BuildingViewModel viewModel = new Gson()
				.fromJson(body, BuildingViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.code = code;
		//UserService.update(viewModel);
		return play.mvc.Controller.ok(play.libs.Json.toJson(BuildingService
				.update(viewModel)));
	}

	/**
	 * This method returns the count/ total number of the building.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result count() throws JSONException {
		BuildingViewModel viewModel = new BuildingViewModel();
		viewModel.entityObject = request().path().split("/")[1];		
		return play.mvc.Controller.ok(play.libs.Json.toJson( BuildingService.count(viewModel)));

	}

	/**
	 * This method is used to delete an building based on code.
	 * 
	 * @param id
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 */
	
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result delete(String code) throws JSONException,
			JsonProcessingException, IOException {
		
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		// Converting JSON Object into ViewModel Object
		BuildingViewModel viewModel = new Gson()
		  .fromJson(body, BuildingViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.code = code;
		//UserService.delete(viewModel);
		return play.mvc.Controller.ok(play.libs.Json.toJson(BuildingService
				.delete(viewModel)));
	}




	

	
	
	
	
	
	
}