package vHMS.Web.controllers;

import java.io.IOException;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import play.data.Form;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import vHMS.Core.ViewModels.DoctorViewModel;
import vHMS.Core.ViewModels.UserViewModel;
import vHMS.Facade.IUserService;
import Base.Common.Attribute.AuthenticateAttribute;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * The UserController deals with the all the operations like create/update/find/serach/changePassword/forgotPassword
 * for an employee/user for the vHMS app, it deals with deals with changing the password,forgot Password.
 * 
 * 
 * @author MaheshDe
 * @since 2015-07-21
 */ 

@Component
public class UserController extends Controller {
	@Autowired
	private IUserService UserService;

	/**
	 * This method is used to return the index page for the User/user.
	 * 
	 * @return Result(Play.mvc.Result).
	 */

	/*public Result index() {
		return play.mvc.Controller.ok(views.html.employee.index.render());
	}*/

	/**
	 * This method is used to find/fetch all the entries for the User/user.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	
	public Result findAll() throws JsonProcessingException, IOException {

		UserViewModel viewModel = new UserViewModel();
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.where = Form.form().bindFromRequest().get("where");
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip = Form.form().bindFromRequest().get("skip");	

		return play.mvc.Controller.ok(play.libs.Json.toJson(UserService.all(viewModel)));

	}

	/**
	 * This method is used to find/fetch a User/user depending on the code.
	 * 
	 * @param code
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 * @throws Exception 
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	
	public Result findById(String code) throws Exception {

		UserViewModel viewModel = new UserViewModel();
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.code = code;		
		return play.mvc.Controller.ok(play.libs.Json.toJson(UserService.findById(viewModel)));
	}

	/**
	 * This method is used to find/fetch all Users/users depending on select.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result findAllByAttributes() throws Exception {
		
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		Gson gson =new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
				
		UserViewModel viewModel = gson.fromJson(body,
				UserViewModel.class);
		
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.where = Form.form().bindFromRequest().get("where");
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip = Form.form().bindFromRequest().get("skip");		
		return play.mvc.Controller.ok(play.libs.Json.toJson(UserService.findAllByAttributes(viewModel)));
	}

	/**
	 * This method is used to create/insert an User/user.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result create() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
				.create();
		// Converting JSON Object into ViewModel Object
		UserViewModel viewModel = new Gson()
				.fromJson(body, UserViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];
		//UserService.create(viewModel);
		return play.mvc.Controller.ok(play.libs.Json.toJson(UserService
				.create(viewModel)));
	}

	
	/**
	 * This method is used to update/edit a User/user depending on the id.
	 * 
	 * @param id
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result updateById(String code) throws JsonProcessingException,
			IOException {
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		// Converting JSON Object into ViewModel Object
		UserViewModel viewModel = new Gson()
				.fromJson(body, UserViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.code = code;
		//UserService.update(viewModel);
		return play.mvc.Controller.ok(play.libs.Json.toJson(UserService
				.update(viewModel)));
	}

	/**
	 * This method returns the count/ total number of the User/user.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result count() throws JSONException {
		UserViewModel viewModel = new UserViewModel();
		viewModel.entityObject = request().path().split("/")[1];		
		return play.mvc.Controller.ok(play.libs.Json.toJson( UserService.count(viewModel)));

	}

	/**
	 * This method is used to delete an User/user based on id.
	 * 
	 * @param id
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 */
	
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result delete(String code) throws JSONException,
			JsonProcessingException, IOException {
		
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		// Converting JSON Object into ViewModel Object
		UserViewModel viewModel = new Gson()
		  .fromJson(body, UserViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.code = code;
		//UserService.delete(viewModel);
		return play.mvc.Controller.ok(play.libs.Json.toJson(UserService
				.delete(viewModel)));
	}

	/**
	 * This method is used to reset the password of an User and send a mail with
	 * the new password when requested for the same.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	@BodyParser.Of(BodyParser.Json.class)
	public Result forgotPassword() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		// Converting JSON Object into ViewModel Object
		UserViewModel viewModel = new Gson()
				.fromJson(body, UserViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];

		return play.mvc.Controller.ok(play.libs.Json.toJson(UserService
				.forgotPassword(viewModel)));
	}

	/**
	 * This method is used reset the password of an User when requested for the
	 * same.
	 * 
	 * @return Result(Play.mvc.Result).
	 */

	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result changePassword() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		// Converting JSON Object into ViewModel Object
		UserViewModel viewModel = new Gson()
				.fromJson(body, UserViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];

		return play.mvc.Controller.ok(play.libs.Json.toJson(UserService
				.changePassword(viewModel)));
	}
	
	/**
	 * This method is used to create/insert an User/user.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result createUserByType() throws Exception {
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").findPath("user").toString();
		String doctor = json.findPath("request").findPath("userType").toString();
		// Converting JSON Object into ViewModel Object
		UserViewModel viewModel = new Gson().fromJson(body, UserViewModel.class);
		
		DoctorViewModel doctorViewModel = new Gson().fromJson(doctor, DoctorViewModel.class);
		
		viewModel.entityObject = request().path().split("/")[1];
		 return play.mvc.Controller.ok(play.libs.Json.toJson(UserService.createUserbyType(viewModel,doctorViewModel)));
	}
	
	/**
	 * This method is used to create/insert an User/user.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result updateUserByType(String code) throws Exception {
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").findPath("user").toString();
		String doctor = json.findPath("request").findPath("userType").toString();
		// Converting JSON Object into ViewModel Object
		UserViewModel viewModel = new Gson().fromJson(body, UserViewModel.class);
		viewModel.code=code;
		DoctorViewModel doctorViewModel = new Gson().fromJson(doctor, DoctorViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];
		 return play.mvc.Controller.ok(play.libs.Json.toJson(UserService.updateUserbyType(viewModel,doctorViewModel)));
	}
	
	
	

	/**
	 * This method is used to find/fetch a User/user depending on the code.
	 * 
	 * @param code
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 * @throws Exception 
	 */
	
	@play.mvc.Security.Authenticated(AuthenticateAttribute.class)

	public Result findUserDetailById(String code) throws Exception {

		UserViewModel viewModel = new UserViewModel();
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.code = code;		
		return play.mvc.Controller.ok(play.libs.Json.toJson(UserService.findUserDetailById(viewModel)));
	}
	
}
