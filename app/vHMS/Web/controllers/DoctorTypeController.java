package vHMS.Web.controllers;

import org.springframework.stereotype.Component;

/**
 * The DoctorType Controller deals with basic operations like create, update, delete ,search 
 * for the different type of doctors.
 * 
 * 
 * @author JasmitaB
 * @since 2015-08-10
 */

@Component
public class DoctorTypeController extends MasterController{
	
}