package vHMS.Web.controllers;

import java.util.Date;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import Base.Utilities.DateDeserializer;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import play.data.Form;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import vHMS.Core.ViewModels.BranchViewModel;
import vHMS.Facade.IBranchService;


/**
 * The BranchController deals with the all the operations like create/update/find/search/changePassword/forgotPassword
 * for an branch for the vHMS app, it deals with deals with changing the password,forgot Password.
 * 
 * 
 * @author Sabariraja M
 * @since 2015-08-28
 */ 

@Component
public class BranchController extends Controller {
	@Autowired
	private IBranchService BranchService;
	/**
	 * This method is used to create/insert branch.
	 * 
	 * @return Result(Play.mvc.Result).
	 */

	@BodyParser.Of(BodyParser.Json.class)
	public Result create() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		/*Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
				.create();*/
		// Converting JSON Object into ViewModel Object
		BranchViewModel viewModel = new Gson()
				.fromJson(body, BranchViewModel.class);
		viewModel.entityObject = request().path().split("/")[1];
		//branch.create(viewModel);
		return play.mvc.Controller.ok(play.libs.Json.toJson(BranchService
				.create(viewModel)));
	}
	
	/**
	 * This method is used to update/edit a branch depending on the id.
	 * 
	 * @param id
	 *            :String.
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */

	@BodyParser.Of(BodyParser.Json.class)
	public Result updateByCode(String code) throws Exception {
		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		/*
		 * Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
		 * .create();
		 */
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
		Gson gson = gsonBuilder.create();
		// Converting JSON Object into ViewModel Object
		BranchViewModel viewModel = gson.fromJson(body,
				BranchViewModel.class);
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		// Set the code of the record for which it should be updated.
		viewModel.code = code;
		return play.mvc.Controller.ok(play.libs.Json.toJson(BranchService
				.update(viewModel)));
	}
	
	/**
	 * This method is used to find/fetch all the entries for the branch.
	 * 
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */
	//@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	//@BodyParser.Of(BodyParser.Json.class)
	public Result findAll() throws Exception {

		BranchViewModel viewModel = new BranchViewModel();
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.where = Form.form().bindFromRequest().get("where");
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip  = Form.form().bindFromRequest().get("skip");

		return play.mvc.Controller.ok(play.libs.Json.toJson(BranchService
				.all(viewModel)));

	}
	
	/**
	 * This method is used to find/fetch the entries By Criteria for the
	 * branch.
	 * 
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */
	//@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result findAllByAttributes() throws Exception {

		JsonNode json = request().body().asJson();
		String body = json.findPath("request").toString();
		// Specify the date format along with the Gson object if the incoming
		// requests contains date.
		/*
		 * Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss")
		 * .create();
		 */
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
		Gson gson = gsonBuilder.create();
		// Converting JSON Object into ViewModel Object
		BranchViewModel viewModel = gson.fromJson(body,
				BranchViewModel.class);
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.limit = Form.form().bindFromRequest().get("limit");
		viewModel.skip = Form.form().bindFromRequest().get("skip");
		return play.mvc.Controller.ok(play.libs.Json.toJson(BranchService
				.findAllByAttributes(viewModel)));
	}
	
	/**
	 * This method returns the count/ total number of the branch.
	 * 
	 * @return Result(Play.mvc.Result).
	 */
	//@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	@BodyParser.Of(BodyParser.Json.class)
	public Result count() throws JSONException {
		BranchViewModel viewModel = new BranchViewModel();
		viewModel.entityObject = request().path().split("/")[1];		
		return play.mvc.Controller.ok(play.libs.Json.toJson( BranchService.count(viewModel)));

	}

	/**
	 * This method is used to delete the branch
	 * 
	 * @return Result(Play.mvc.Result).
	 * @throws Exception
	 */

	@BodyParser.Of(BodyParser.Json.class)
	public Result delete(String code) throws Exception {
		BranchViewModel viewModel = new BranchViewModel();
		// To set the entity(table name) name.
		viewModel.entityObject = request().path().split("/")[1];
		// Set the id of the record to be deleted.
		viewModel.code = code;

		return play.mvc.Controller.ok(play.libs.Json.toJson(BranchService
				.delete(viewModel)));
	}
	
	//@play.mvc.Security.Authenticated(AuthenticateAttribute.class)
	public Result findById(String code) throws Exception {

		BranchViewModel viewModel = new BranchViewModel();
		viewModel.entityObject = request().path().split("/")[1];
		viewModel.code = code;		
		return play.mvc.Controller.ok(play.libs.Json.toJson(BranchService.findById(viewModel)));
	}
	
}