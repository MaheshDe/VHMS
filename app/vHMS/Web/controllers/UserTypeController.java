package vHMS.Web.controllers;

import org.springframework.stereotype.Component;

/**
 * The UserType Controller deals with basic operations like create, update, delete ,search 
 * for a type of user in VHMS.
 * 
 * 
 * @author MaheshDe
 * @since 2015-08-10
 */
@Component
public class UserTypeController extends MasterController{
	
}