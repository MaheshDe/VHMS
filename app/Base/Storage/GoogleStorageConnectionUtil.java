package Base.Storage;

import java.io.IOException;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.HashSet;
import java.util.Set;

import play.Play;
import vHMS.Core.ViewModels.Response;
import Base.Utilities.CommonUtils;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.StorageScopes;

/**
 * Cloud Storage API connection Authenticated API call using OAuth 2 to cloud
 * storage.
 * 
 * @author Arokia Felsi
 * @date 16-09-2014
 */

public class GoogleStorageConnectionUtil {

	// Directory to store user credentials of cloud storage

	// private static final java.io.File DATA_STORE_DIR = new
	// java.io.File("D:/felsi/googlestore");
	// private static final java.io.File DATA_STORE = new
	// java.io.File(System.getProperty("user.home"), ".store/storage_sample");
	private static final java.io.File DATA_STORE = new java.io.File(Play
			.application().getFile("/public/data").toString());
	// Global instance of the DataStoreFactory
	private static FileDataStoreFactory dataStoreFactory;

	// Global instance of the JSON factory
	private static final JsonFactory JSON_FACTORY = JacksonFactory
			.getDefaultInstance();

	// Global instance of the HTTP transport
	private static HttpTransport httpTransport;

	// Global instance of the Storage
	private static Storage client;

	/**
	 * Connection for Google storage
	 * 
	 * @return client
	 * @throws IOException
	 */
	public static Storage getConnection() throws IOException {

		try {

			// Initialize the transport.
			httpTransport = GoogleNetHttpTransport.newTrustedTransport();

			// Initialize the data store factory.
			dataStoreFactory = new FileDataStoreFactory(DATA_STORE);

			// Getting the user for google cloud storage credentials
			Credential credential = authorize();

			// Making a connection to the google storage with the application
			// name
			client = new Storage.Builder(httpTransport, JSON_FACTORY,
					credential).setApplicationName(
					CommonUtils.getMessage("google.aplication.name")).build();

		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return client;
	}

	/**
	 * Authorizes the installed application to access user's protected data
	 * 
	 * @return credentials object
	 * @throws Exception
	 */
	private static Credential authorize() throws Exception {

		Response response = new Response();
		// Load google client credentials through a file.
		GoogleClientSecrets clientSecrets = null;

		try {

			/*
			 * clientSecrets = GoogleClientSecrets.load( JSON_FACTORY, new
			 * InputStreamReader(GoogleStorageConnectionUtil.class
			 * .getResourceAsStream("/client_secrets.json")));
			 */

			clientSecrets = GoogleClientSecrets.load(
					JSON_FACTORY,
					new InputStreamReader(Play.application().resourceAsStream(
							"/public/data/client_secrets.json")));

			// getting the client details from the loaded file
			if (clientSecrets.getDetails().getClientId() == null
					|| clientSecrets.getDetails().getClientSecret() == null) {
				throw new Exception(CommonUtils.getMessage("error.file.format"));
			}

		} catch (Exception e) {

			response.message = CommonUtils.getMessage("error.loading.file");
			response.ExceptionMessage = e.getMessage();
		}
		// listing all of the available scopes
		Set<String> scopes = new HashSet<String>();
		scopes.add(StorageScopes.DEVSTORAGE_FULL_CONTROL);
		scopes.add(StorageScopes.DEVSTORAGE_READ_ONLY);
		scopes.add(StorageScopes.DEVSTORAGE_READ_WRITE);

		// Set up authorization code flow.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
				httpTransport, JSON_FACTORY, clientSecrets, scopes)
				.setDataStoreFactory(dataStoreFactory).build();
		// Authorize.
		return new AuthorizationCodeInstalledApp(flow,
				new LocalServerReceiver()).authorize(CommonUtils
				.getMessage("google.email.Id"));
	}

}
