package Base.Storage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vHMS.Core.ViewModels.DocumentViewModel;
import Base.Data.IGenericMongoDao;
import Base.Utilities.CommonUtils;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.InputStreamContent;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.model.Bucket;
import com.google.api.services.storage.model.StorageObject;

/**
 * GoogleStorageDao deals with operations like Upload, download , delete a document etc.
 * using google APIs.
 * 
 * @author JasmitaB
 * @version 1.0
 * @since 2015-08-12
 */
@Service
@Transactional
public class GoogleStorageDao implements IDocumentDao {

	@Autowired
	private IGenericMongoDao genericDAO;

	private Storage client;

	public GoogleStorageDao() throws IOException {

		client = GoogleStorageConnectionUtil.getConnection();
	}

	/**
	 * Load document into google apps
	 * 
	 * @param pojo
	 *            (required) the Java bean to store,
	 * @throws IOException
	 */

	@SuppressWarnings("unchecked")
	@Override
	public <T> T load(DocumentViewModel viewModel) throws Exception {

		InputStream inputStream = new FileInputStream(viewModel.LoadedFile);

		// Getting the mediacontent by binding the Inputstream and
		// content
		// type of the file
		InputStreamContent mediaContent = new InputStreamContent(
				viewModel.contentType, inputStream);

		// Storing the file in the cloud storage with file name and
		// bucket
		// name in the cloud storage
		StorageObject uploadObject = client.objects()
				.insert(viewModel.bucket, null, mediaContent)
				.setName(viewModel.fileName).execute();

		viewModel.code = String.valueOf(uploadObject.getGeneration());
		viewModel.json = "";

		return (T) viewModel;
	}

	/**
	 * DownLoading document from google apps
	 * 
	 * @param pojo
	 *            (required) the Java bean to store,
	 * @throws IOException
	 */

	@SuppressWarnings("unchecked")
	@Override
	public <T> T retrieve(DocumentViewModel viewModel) throws Exception {
		String bucket;

		// Get the list of files in the bucket
		Storage.Objects.List listObjects = client.objects().list(
				viewModel.bucket);
		// Intializing the objects
		com.google.api.services.storage.model.Objects objects;

		do {
			// execute to get the list of files from buckets as an
			// Model
			// object
			objects = listObjects.execute();
			// Iterating the list of files
			for (StorageObject object : objects.getItems()) {
				// comparing the list of files key with the
				// specified
				// key
				if (String.valueOf(object.getGeneration()).equals(
						viewModel.code)) {
					Storage.Objects.Get get = client.objects().get(
							viewModel.bucket, object.getName());
					// Creating new File
					File tempFile = new File(object.getName());
					OutputStream outputStream = new FileOutputStream(tempFile);

					// Passing the outputstream downloading the file
					get.executeMediaAndDownloadTo(outputStream);

					// Setting required parameter in view model
					viewModel.contentType = object.getContentType();
					viewModel.fileName = object.getName();
					viewModel.LoadedFile = tempFile;

				}
			}
			listObjects.setPageToken(objects.getNextPageToken());
		} while (null != objects.getNextPageToken());

		return (T) viewModel;

	}
	/**
	 * Delete a document from google apps
	 * 
	 * @param pojo
	 *            (required) the Java bean to store,
	 * @throws IOException
	 */
	@Override
	public <T> T delete(DocumentViewModel viewModel) throws IOException,
			Exception {

		// Get the list of files in the bucket
		Storage.Objects.List listObjects = client.objects().list(
				viewModel.bucket);
		// Intializing the objects
		com.google.api.services.storage.model.Objects objects;

		do {
			// execute to get the list of files from buckets as an
			// Model
			// object
			objects = listObjects.execute();
			// Iterating the list of files
			for (StorageObject object : objects.getItems()) {
				// comparing the list of files key with the
				// specified key
				if (String.valueOf(object.getGeneration()).equals(
						viewModel.code)) {

					client.objects()
							.delete(object.getBucket(), object.getName())
							.execute();
					viewModel.status = "1";
				}
			}
			listObjects.setPageToken(objects.getNextPageToken());
		} while (null != objects.getNextPageToken());

		return (T) viewModel;
	}

	/**
	 * This method details with creating a new Bucket if the bucket does not
	 * exists and return the bucket name
	 * 
	 * @param BucketName
	 * @param client
	 * @return
	 * @throws Exception
	 */
	@Override
	public String tryCreateBucket(String BucketName) throws Exception,
			GoogleJsonResponseException {

		// Getting the application id and bucket to insert in the application
		Storage.Buckets.Insert insertBucket = client.buckets().insert(
				CommonUtils.getMessage("google.application.Id"),
				new Bucket().setName(BucketName));

		@SuppressWarnings("unused")
		// creating the bucket with the above details
		Bucket createdBucket = insertBucket.execute();

		// return the bucket name
		return BucketName;
	}

	/**
	 * This method deals with checking whether the bucket exists and returns the
	 * bucket name
	 * 
	 * @param BucketName
	 * @param client
	 * @return
	 * @throws IOException
	 */
	public String getBucket(String BucketName) throws Exception {

		String bucketName = null;

		try {
			// getting the details by passing bucket name
			Storage.Buckets.Get getBucket = client.buckets().get(BucketName);
			getBucket.setProjection("full");
			// Execute the bucket name to get the bucket details
			Bucket bucket = getBucket.execute();
			// getting the bucket name
			bucketName = bucket.getName();
		} catch (Exception e) {

			e.printStackTrace();
		}
		// return the bucket name if exists or null
		return bucketName;
	}

	/**
	 * This method deals with setting the projection for the buclket specified,
	 * projection determines what all properties to be returned back and if it
	 * has been set to full, it will returnall the properties. bucket name
	 * 
	 * @param BucketName
	 * @return
	 * @throws IOException
	 */

	@Override
	public void setBucketProjection(String Bucket) throws Exception {
		// Declaring the getbucket
		Storage.Buckets.Get getBucket;
		// getting the bucket to set projection
		getBucket = client.buckets().get(Bucket);

		getBucket.setProjection("full");
	}

}
