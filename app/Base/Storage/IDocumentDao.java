package Base.Storage;

import java.io.IOException;

import vHMS.Core.ViewModels.DocumentViewModel;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;

/**
 * IDocumentDao  is an interface containing methods like Upload, Download, 
 * Delete document etc..
 *  
 * 
 * @author JasmitaB
 * @version 1.0
 * @since 2015-08-12
 */

public interface IDocumentDao {
	/**
	 * Load document into google apps
	 * 
	 * @param pojo
	 *            (required) the Java bean to store,
	 * @throws IOException
	 */
	public <T> T load(DocumentViewModel viewModel) throws Exception;

	/**
	 * Downloading a document from google apps
	 * 
	 * @param pojo
	 *            (required) the Java bean to store,
	 * @throws IOException
	 */
	public <T> T retrieve(DocumentViewModel viewModel) throws Exception;

	/**
	 * Remove a document from google apps
	 * 
	 * @param pojo
	 *            (required) the Java bean to store,
	 * @throws IOException
	 * @throws Exception
	 */
	public <T> T delete(DocumentViewModel viewModel) throws Exception;

	/**
	 * Get a bucket instance , if the bucket is there already , it will get the
	 * bucket name else it will return null.
	 * 
	 * @param BucketName
	 *            (required) the bucket name to be checked,
	 * @throws IOException
	 * @throws Exception
	 */
	public String getBucket(String BucketName) throws Exception;

	/**
	 * This method is used to create a bucket with the name specified.
	 * 
	 * @param BucketName
	 *            (required) the bucket name to be created,
	 * @throws IOException
	 * @throws Exception
	 */
	public String tryCreateBucket(String BucketName) throws Exception,
			GoogleJsonResponseException;

	/**
	 * This method is used to set the projection for a bucket.
	 * 
	 * @param BucketName
	 *            (required) the bucket name to be created,
	 * @throws IOException
	 * @throws Exception
	 */
	public void setBucketProjection(String Bucket) throws Exception;

}
