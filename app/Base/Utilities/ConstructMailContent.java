package Base.Utilities;

import vHMS.Core.ViewModels.EmailContentTemplate;
import vHMS.Core.ViewModels.NotificationViewModel;

/**
 * @author Arokia Felsi For Constructing the mail Content for different levels
 *         with the status
 * 
 */

public class ConstructMailContent {

	// To get the message for the mail body content and send as Notification
	// message
	public static NotificationViewModel getMessage(
			EmailContentTemplate emailTemplate) throws Exception {

		String body = null;
		// Create a mail message to be send.
		NotificationViewModel message = new NotificationViewModel();
		if (CommonUtils.getMessage("actual.email.send").equalsIgnoreCase(
				"false")) {

			message.setTo(CommonUtils.getMessage("actual.email.send.false"));
		} else {
			message.setTo(emailTemplate.getRecEmailId());
		}

		message.setFrom(CommonUtils.getMessage("from"));
		String Status = emailTemplate.getStatus();
		// Checking the status and appending the body content with exact html
		// form
		if (Status.equalsIgnoreCase(CommonUtils.getMessage("request.approved"))) {
			message.setSubject(emailTemplate.getWorflowName() + " "
					+ CommonUtils.getMessage("approvesubject") + " "
					+ emailTemplate.getRequestCode());
			// Get the email body content from the scala
			// temapltes

			body = views.html.emailtemplates.approve.render(
					emailTemplate.getReceiverName(),
					emailTemplate.getRecEmailId(),
					emailTemplate.getRequestCode(),
					emailTemplate.getWorflowName(),
					emailTemplate.getSenderName(),
					emailTemplate.getRequesterName(), emailTemplate.getUrl())
					.body();
		}

		if (Status.equalsIgnoreCase(CommonUtils.getMessage("request.rejected"))) {
			message.setSubject(emailTemplate.getWorflowName() + " "
					+ CommonUtils.getMessage("rejectedSubject") + " "
					+ emailTemplate.getRequestCode());
			// Get the email body content from the scala
			// temapltes

			body = views.html.emailtemplates.rejected.render(
					emailTemplate.getReceiverName(),
					emailTemplate.getRecEmailId(),
					emailTemplate.getRequestCode(),
					emailTemplate.getWorflowName(),
					emailTemplate.getSenderName(), emailTemplate.getUrl())
					.body();
		}
		if (Status.equalsIgnoreCase(CommonUtils
				.getMessage("request.rejectandclose"))) {
			message.setSubject(emailTemplate.getWorflowName() + " "
					+ CommonUtils.getMessage("rejectandcloseSubject") + " "
					+ emailTemplate.getRequestCode());
			// Get the email body content from the scala
			// temapltes

			body = views.html.emailtemplates.rejectedandclosed.render(
					emailTemplate.getReceiverName(),
					emailTemplate.getRecEmailId(),
					emailTemplate.getRequestCode(),
					emailTemplate.getWorflowName(),
					emailTemplate.getSenderName(), emailTemplate.getUrl())
					.body();
		}
		if (Status.equalsIgnoreCase(CommonUtils.getMessage("request.closed"))) {
			message.setSubject(emailTemplate.getWorflowName() + " "
					+ CommonUtils.getMessage("closedSubject") + " "
					+ emailTemplate.getRequestCode());
			// Get the email body content from the scala
			// temapltes

			body = views.html.emailtemplates.closed.render(
					emailTemplate.getReceiverName(),
					emailTemplate.getRecEmailId(),
					emailTemplate.getRequestCode(),
					emailTemplate.getWorflowName(),
					emailTemplate.getSenderName(), emailTemplate.getUrl())
					.body();
		}
		if (Status
				.equalsIgnoreCase(CommonUtils.getMessage("request.responded"))) {
			message.setSubject(emailTemplate.getWorflowName() + " "
					+ CommonUtils.getMessage("respondedSubject") + " "
					+ emailTemplate.getRequestCode());
			// Get the email body content from the scala
			// temapltes

			body = views.html.emailtemplates.responded.render(
					emailTemplate.getReceiverName(),
					emailTemplate.getRecEmailId(),
					emailTemplate.getRequestCode(),
					emailTemplate.getWorflowName(),
					emailTemplate.getSenderName(), emailTemplate.getUrl())
					.body();
		}
		if (Status.equalsIgnoreCase(CommonUtils.getMessage("request.void"))) {
			message.setSubject(emailTemplate.getWorflowName() + " "
					+ CommonUtils.getMessage("voidsubject") + " "
					+ emailTemplate.getRequestCode());
			// Get the email body content from the scala
			// temapltes

			body = views.html.emailtemplates.voided.render(
					emailTemplate.getReceiverName(),
					emailTemplate.getRecEmailId(),
					emailTemplate.getRequestCode(),
					emailTemplate.getWorflowName(),
					emailTemplate.getSenderName(), emailTemplate.getUrl())
					.body();
		}

		if (Status.equalsIgnoreCase(CommonUtils.getMessage("request.pending"))) {
			message.setSubject(emailTemplate.getWorflowName() + " "
					+ CommonUtils.getMessage("requestSubject") + " "
					+ emailTemplate.getRequestCode());
			// Get the email body content from the scala
			// temapltes
			body = views.html.emailtemplates.requestsubmitted.render(
					emailTemplate.getReceiverName(),
					emailTemplate.getRecEmailId(),
					emailTemplate.getWorflowName(),
					emailTemplate.getRequestCode(),
					emailTemplate.getSenderName(), emailTemplate.getUrl())
					.body();

		}

		if (Status.equalsIgnoreCase(CommonUtils.getMessage("send.reminder"))) {
			message.setSubject(emailTemplate.getWorflowName() + " "
					+ CommonUtils.getMessage("remainderSubject") + " "
					+ emailTemplate.getRequestCode());
			// Get the email body content from the scala
			// temapltes
			body = views.html.emailtemplates.sendreminder.render(
					emailTemplate.getReceiverName(),
					emailTemplate.getRecEmailId(),
					emailTemplate.getWorflowName(),
					emailTemplate.getRequestCode(),
					emailTemplate.getWorflowStatusName(),
					emailTemplate.getUrl()).body();
		}

		if (Status
				.equalsIgnoreCase(CommonUtils.getMessage("send.reassignment"))) {
			message.setCc(emailTemplate.getRequesterEmailId());
			message.setSubject(emailTemplate.getWorflowName() + " "
					+ CommonUtils.getMessage("reassignmentSubject") + " "
					+ emailTemplate.getRequestCode());
			// Get the email body content from the scala
			// temapltes
			body = views.html.emailtemplates.reassignment.render(
					emailTemplate.getReceiverName(),
					emailTemplate.getRecEmailId(),
					emailTemplate.getWorflowName(),
					emailTemplate.getRequestCode(),
					emailTemplate.getWorflowStatusName(),
					emailTemplate.getUrl()).body();
		}
		message.setBody(body);

		return message;

	}

	/**
	 * construct the landing URL For Email Notification based on the Parameter
	 * 
	 * @param Context
	 * @param QueryString
	 * @return
	 */
	public static String buildLandingURL(String Context, String QueryString)
			throws Exception {
		StringBuffer urlStrBuf = new StringBuffer(
				CommonUtils.getMessage("app.protocol"));

		urlStrBuf.append(CommonUtils.getHostURL());

		if (Context != null && Context.length() > 0) {
			urlStrBuf.append("/");
			urlStrBuf.append(Context);
		}

		if (QueryString != null && QueryString.length() > 0) {
			urlStrBuf.append("?");
			urlStrBuf.append(QueryString);
		}

		return urlStrBuf.toString();

	}

}
