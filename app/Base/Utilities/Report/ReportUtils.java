package Base.Utilities.Report;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JsonDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;

/**
* ReportProcessor generates reports for different format(PDF, Excel..etc)   
* 
* @author Shunmuga Raja G
* @version 1.0
* @since 2015-08-12
*/
public class ReportUtils
{
	//Generate reprts using the template, and saves into file location
	public static synchronized File constructReport(String templateName,String fileLocation,Map<String,Object> parameters, JRDataSource jrDataSource,String reportFormat) throws Exception
	{
		System.out.println("ReportUtils constructReport()");
		JasperReport jasperReport=getCompiledFile( fileLocation, templateName);
		//templateName= templateName+ReportConstants.DOT+reportFormat.toLowerCase();
		File tempFile = File.createTempFile(templateName,ReportConstants.DOT+reportFormat.toLowerCase());
		tempFile.deleteOnExit();
	      if  (reportFormat.equalsIgnoreCase(ReportConstants.PDF) )  {
	  			return generateReportPDF( parameters, jasperReport, jrDataSource, tempFile); // For PDF report
	  		    }
	      else if(reportFormat.equalsIgnoreCase(ReportConstants.XLS))
	    		  {
	    	  return generateReportXLS( parameters, jasperReport, jrDataSource,tempFile); // For xls report
	    		  }
	      
		return null;
		
	}
	
	//Get the compiled Jrxml file
	  public  static synchronized  JasperReport getCompiledFile(String filePath,String fileName)   {
		  
		 System.out.println("ReportUtils getCompiledFile() STARTS ==>>"+fileName);
		 boolean recompileReport=false;
		  File reportFile = new File(filePath+"/"+fileName+".jasper");
			// If compiled file is not found, then compile XML template
		  JasperReport jasperReport=null;
		  try {
				//if (recompileReport || !reportFile.exists()) {
							JasperCompileManager.compileReportToFile(filePath+"/" + fileName + ".jrxml",filePath+"/" + fileName + ".jasper");
						//} 
						jasperReport = (JasperReport) JRLoader.loadObjectFromFile(reportFile.getPath());
						
					} catch (JRException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			           catch (Exception e) {
			   			// TODO Auto-generated catch block
			   			e.printStackTrace();
			   		}
			
			 return jasperReport;
	  }  
	//Generate Report as PDF  
	public static synchronized File generateReportPDF (Map<String,Object> parameters, JasperReport jasperReport,  JRDataSource jrDataSource,File tempFile) throws IOException   {
		byte[] reportBytesArray = null;
		//new File(outputFileName);
		try {
				reportBytesArray = JasperRunManager.runReportToPdf (jasperReport,parameters,jrDataSource);
				FileOutputStream fos = new FileOutputStream(tempFile);
				fos.write( reportBytesArray);
				fos.flush();
				fos.close();
				System.out.println("ReportUtils generateReportPDF() outputFileName "+tempFile.getName());
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tempFile;
	} 
	//Generate Report as Excel
	public static synchronized File generateReportXLS (Map<String,Object> parameters, JasperReport jasperReport,  JRDataSource jrDataSource,File tempFile) throws IOException   {
		byte[] reportBytesArray = null;
		try {
				JasperPrint jasperPrint = JasperFillManager.fillReport(
						jasperReport, parameters, jrDataSource);

				JRXlsExporter exporter = new JRXlsExporter();
				ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();

 			exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT,
						jasperPrint);
				exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET,
						Boolean.TRUE);
				exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE,
						Boolean.FALSE);
				exporter.setParameter(
						JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND,
						Boolean.TRUE);
				exporter.setParameter(
						JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS,
						Boolean.FALSE);
				exporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM,
						xlsReport);
			//	exporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME,						outputFileName); 
				
				exporter.exportReport();
				System.out.println("Size of byte array:" + xlsReport.size());
				reportBytesArray = xlsReport.toByteArray();

				xlsReport.close();
				FileOutputStream fos = new FileOutputStream(tempFile);
				fos.write( reportBytesArray);
				fos.flush();
				fos.close();
				System.out.println("ReportUtils generateReportXLS() outputFileName "+tempFile.getName());
				
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tempFile;
	} 
	
/*	public static File generateReportHTML (Map<String,Object> parameters, JasperReport jasperReport,  JRDataSource jrDataSource,String outputFileName)throws JRException, NamingException, SQLException, IOException {
		  System.out.println("ReportUtils generateReportPDF() STARTS");
		byte[] reportBytesArray = null;
		//reportBytesArray = JasperRunManager.runReportToHtmlFile(jasperReport,parameters,jrDataSource);
		File someFile = new File(outputFileName);
		FileOutputStream fos = new FileOutputStream(someFile);
		fos.write( reportBytesArray);
		fos.flush();
		fos.close();
		  System.out.println("ReportUtils generateReportPDF() ENDS");
		return someFile;
	} 
	
	
	*/
	
	public static synchronized  JRDataSource getDataSource(String jsonData,String selectExpression) {
	System.out.println ("jsonData = " + jsonData);
		JRDataSource dataSource = null;

		if ("null".equals(jsonData) || jsonData == null || "".equals(jsonData)) {
			System.out.println("jsonData parameter value is null. Creating JREmptyDataSource");
			dataSource = new JREmptyDataSource();
			return dataSource;
		}

		InputStream jsonInputStream = null;
		try {
			// Convert the jsonData string to inputStream
			jsonInputStream = IOUtils.toInputStream(jsonData, "UTF-8");
			// selectExpression is based on the jsonData that your string contains
			dataSource = new JsonDataSource(jsonInputStream);
		} catch (IOException ex) {
			System.out.println("Couldn't covert string into inputStream");
			ex.printStackTrace();
		} catch (JRException e) {
			System.out.println ("Couldn't create JsonDataSource");
			e.printStackTrace();
		}

		if (dataSource == null) {
			dataSource = new JREmptyDataSource();
			System.out.println ("dataSource is null. Request parameter jsondData is null");
		}
		return dataSource;
	}
	
}


