package Base.Utilities.Report;

import java.io.IOException;
import java.util.HashMap;

import vHMS.Core.ViewModels.ReportViewModel;
import vHMS.Core.ViewModels.Response;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
 
/**
 * ReportProcessor generates reports for different format(PDF, Excel..etc)   
 * 
 * @author Shunmuga Raja G
 * @version 1.0
 * @since 2015-08-12
 */
public  class ReportProcessor
{
	
	private ObjectMapper objectMapper=null;
	
	public  Response executeReport(ReportViewModel reportViewModel)
	{
		Response  response =new Response();
		try {
			if(reportViewModel!=null){
				
				reportViewModel=preProcessor(reportViewModel);
				reportViewModel=processor(reportViewModel);
				reportViewModel=postProcessor(reportViewModel);
			
			}
			if(reportViewModel!=null){
				//Returns the response true if document contains values
				
				//reportViewModel.contentType="application/pdf";
				response.success = true;
				response.message = "Report Created Successfully";//CommonUtils.getMessage("success.document.upload") ;
				response.ViewModel=reportViewModel;
			} else {
				response.success = false;
				response.message ="Report Creation Failed"; //CommonUtils.getMessage("error.document.upload") ;				
			}
			
			
			} catch(Exception e){
				//Handling exception
				response.success = false;
				response.message = "Report Creation Failed";
				response.ExceptionMessage = e.getMessage();
			}
			return response;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public  ReportViewModel preProcessor(ReportViewModel reportViewModel) throws JsonParseException, JsonMappingException, IOException
	{
			reportViewModel.reportCreationStatus=true;
			reportViewModel.reportParametersMap =reportViewModel.reportParametersMap==null? new HashMap<String,Object>() :reportViewModel.reportParametersMap;
			return reportViewModel;
	}
	
	public ReportViewModel processor(ReportViewModel reportViewModel) throws Exception
	{	
		 			  if( reportViewModel.inputJsonNode!=null)
		 			  {
					  objectMapper = new ObjectMapper();
					  reportViewModel.ModelObject  = objectMapper.readValue(reportViewModel.inputJsonNode.toString(),reportViewModel.ModelClass);
					  reportViewModel.reportDatasource =ReportUtils.getDataSource(reportViewModel.inputJsonNode.toString(),"data");
		 			  }
					  reportViewModel.reportParametersMap.put("modelObject",reportViewModel.ModelObject);
					  reportViewModel.reportParametersMap.put("realPath",reportViewModel.imageFileLocation);	
					  reportViewModel.reportOutputFormat=reportViewModel.reportOutputFormat==null?ReportConstants.PDF: reportViewModel.reportOutputFormat;
					  reportViewModel.contentType = reportViewModel.reportOutputFormat.equalsIgnoreCase(ReportConstants.PDF)?  "application/pdf" : "application/vnd.ms-excel";
		      		  return reportViewModel;
	}
	
	public  ReportViewModel postProcessor(ReportViewModel reportViewModel)  throws Exception
	{
		if(reportViewModel.reportCreationStatus)
		{
			reportViewModel.reportFile=ReportUtils.constructReport(reportViewModel.templateFileName,reportViewModel.templateFileLocation,reportViewModel.reportParametersMap,reportViewModel.reportDatasource,reportViewModel.reportOutputFormat);
			return reportViewModel;
		}
		
		return null;
	}

}