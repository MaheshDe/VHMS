package Base.Utilities.Report;


/**
 * 
 * @author ShunmugaRajaG
 *
 */
public class ReportConstants
{

	public static final  String Page_Footer_URL="www.vmokshagroup.com";
	public static final  String PDF="PDF";
	public static final  String XLS="XLS";
	public static final  String HTML="HTML";
	public static final  String DOT=".";
}