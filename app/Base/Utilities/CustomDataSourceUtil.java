package Base.Utilities;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class CustomDataSourceUtil extends HikariDataSource{

	/* (non-Javadoc)
	 * @see com.zaxxer.hikari.AbstractHikariConfig#setJdbcUrl(java.lang.String)
	 */
	@Override
	public void setJdbcUrl(String jdbcUrl) {
		String decodedURL="";
		try{
			decodedURL= CommonUtils.decryptor(jdbcUrl);
		}catch(Exception e){
		 e.printStackTrace();	
		}
		super.setJdbcUrl(decodedURL);
	}

	/* (non-Javadoc)
	 * @see com.zaxxer.hikari.AbstractHikariConfig#setPassword(java.lang.String)
	 */
	@Override
	public void setPassword(String password) {
		String decodedpassword="";
		try{
			decodedpassword= CommonUtils.decryptor(password);
		}catch(Exception e){
		 e.printStackTrace();	
		}
		super.setPassword(decodedpassword);
	}

	/* (non-Javadoc)
	 * @see com.zaxxer.hikari.AbstractHikariConfig#setUsername(java.lang.String)
	 */
	@Override
	public void setUsername(String username) {
		String decodedusename="";
		try{
			decodedusename= CommonUtils.decryptor(username);
		}catch(Exception e){
		 e.printStackTrace();	
		}
		super.setUsername(decodedusename);
	}
	
}