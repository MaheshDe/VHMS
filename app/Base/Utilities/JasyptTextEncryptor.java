package Base.Utilities;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

/**
 * The JasyptTextEncryptor is for encrypting the text using jasypt encrptor
 * 
 * @author Arokia Felsi
 * 
 */

public class JasyptTextEncryptor {

	public static void main(String[] args) {
		// Creating an instance for StandardPBEStringEncryptor
		StandardPBEStringEncryptor standardPBEStringEncryptor = new StandardPBEStringEncryptor();
		// Setting the algorithm as PBEWithMD5AndDES for encrypting and
		// decrypting with same algorithm
		standardPBEStringEncryptor.setAlgorithm("PBEWithMD5AndDES");
		// set some password as key for using while decrypting
		standardPBEStringEncryptor.setPassword("anyPasswordForPBEEncryptor");
		// encrypt the property text
		String encodedPass = standardPBEStringEncryptor
				.encrypt("ashoka.vmokshagroup.com");
		String dec = standardPBEStringEncryptor.decrypt(encodedPass);
		// Copy the encrpted text as ENC(the encrpted string) in the property
		// file
		System.out.println("Encrypted text for application context : "
				+ encodedPass);
		System.out.println("Decrypted text for application context : " + dec);

	}

}