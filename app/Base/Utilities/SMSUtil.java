package Base.Utilities;


import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

/**
 *This class initiates the connection to the solutionsinfini for sending sms 
 * 
 * @author Padmavathi
 * @since 2015-06-05
 */
public class SMSUtil {
	
	String domainUrl;
	
	/**
	 * @return the domainUrl
	 */
	public static String getDomainUrl() throws Exception {
		return "http://alerts.solutionsinfini.com/api/v3/index.php?method=sms.json&api_key="+CommonUtils.getMessage("sms.api_key");
	}
	/**
	 * @param domainUrl
	 *            the domainUrl to set
	 */
	public void setDomainUrl(String domainUrl) {
		this.domainUrl = domainUrl;
	}
	
	/**
	 * This method get the host properties when the class loads and sets the
	 * properties when the class is invoked 
	 * 
	 * @throws Exception
	 * */
	
	public static WebResource GetInstance() throws Exception {
		Client client = Client.create();
		WebResource webResource = client.resource(getDomainUrl());
		webResource.type(MediaType.APPLICATION_JSON);
		return webResource;
	}

}