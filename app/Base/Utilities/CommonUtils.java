package Base.Utilities;

import org.apache.commons.codec.binary.Base64;

import play.Play;

/**
 * The CommonUtils contains the common functionalities or methods used accross
 * the application.
 * 
 * @author DivyaP
 * @since 2014-09-11
 */
public class CommonUtils {

	/**
	 * Encrypting a given string using base64.
	 * 
	 * @param text
	 * @return
	 * @throws Exception
	 */
	public static String encryptor(String text) throws Exception {

		byte[] encoded = Base64.encodeBase64(text.getBytes());

		return new String(encoded);
	}

	/**
	 * Decrypting a given string (Which is encrypted using base64) using base64.
	 * 
	 * @param text
	 * @return
	 * @throws Exception
	 */
	public static String decryptor(String text) throws Exception {

		byte[] decoded = Base64.decodeBase64(text.getBytes());

		return new String(decoded);

	}

	/**
	 * This method is used to fetch the fields that are declared in the settings
	 * file.
	 * 
	 * Eg. All the settings/properties file used in the application is declared
	 * and invoked using application-context.xml, this method inturns calls the
	 * utility method and read the property
	 * 
	 * @param text
	 * @return
	 */
	public static String getMessage(String text) {
		// To get the message from the websettings or appsettings by Passing the
		// key
		String Message = "";
		try {
			Message = SpringPropertiesUtil.getProperty(text);
		} catch (Exception e) {
			Message = "Default Message";
			if (null != e.getMessage()) {
				System.out.println("Exception for " + text + " is "
						+ e.getMessage());
			} else {
				System.out.println("Exception for " + text + " is "
						+ e.toString());
			}

		}
		return Message;
	}

	/**
	 * This method is used to convert a string to uppercase.
	 * 
	 * @param str
	 * @return
	 */
	public static String randomUpper(String str) {

		StringBuilder sb = new StringBuilder(str.length());
		// For converting the LowerCase to upper case
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (Character.isLetter(c))
				c = Character.toUpperCase(c);
			sb.append(c);
		}
		return sb.toString();
	}

	/**
	 * To get the host url from request
	 * 
	 * @return
	 */
	public static String getHostURL() {
		return play.mvc.Http.Context.current.get().request().host();
	}

	/**
	 * To get absolute path from request
	 * 
	 * @return
	 */
	public static String getAbsolutePath() {
		return getHostURL()
				+ play.mvc.Http.Context.current.get().request().path();
	}

	/**
	 * To get Application Absolute Path
	 * 
	 * @return
	 */

	public static String getApplicationAbsolutePath() {
		return Play.application().path().getPath();
	}

}
