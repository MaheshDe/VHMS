/**
 * Implementation of the PropertyPlaceholderConfigurer for
 * setting the properties of multiple property file
 */

package Base.Utilities;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**
 * The SpringPropertiesUtil implements PropertyPlaceholderConfigurer to initialize 
 * the appsetting and websetting when the application loads
 * @author Arokia Felsi
 *
 */

public class SpringPropertiesUtil extends PropertyPlaceholderConfigurer {

    private static Map<String, String> propertiesMap;
    // Default as in PropertyPlaceholderConfigurer
    private int springSystemPropertiesMode = SYSTEM_PROPERTIES_MODE_FALLBACK;

    /**set the system properties mode for setting 
     * when the class loads from the application-context.xml*/
    @Override
    public void setSystemPropertiesMode(int systemPropertiesMode) {
        super.setSystemPropertiesMode(systemPropertiesMode);
        springSystemPropertiesMode = systemPropertiesMode;
    }
    /**set the properties from the appsettings and websettings
     * when the class loads from the application-context.xml*/
    @Override
    protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props) throws BeansException {
        super.processProperties(beanFactory, props);

        propertiesMap = new HashMap<String, String>();
        for (Object key : props.keySet()) {
            String keyStr = key.toString();
            String valueStr = resolvePlaceholder(keyStr, props, springSystemPropertiesMode);
            propertiesMap.put(keyStr, valueStr);
        }
    }
    /**whenever the appsettings and websettings properties are required this method is invoked
     * */
    public static String getProperty(String name) {
        return propertiesMap.get(name).toString();
    }

}