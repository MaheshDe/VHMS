/**
 * 
 */
package Base.Utilities;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

/**
 * The DateDeserializer is used for changing the timezone for GSON serializer,
 * by default it takes local time zone.
 * 
 * @author DivyaP
 * @since 2015-08-11
 */
public class DateDeserializer implements JsonDeserializer<Date> {
	/**
	 * This method overrides the existing deserialize method for changing the
	 * dateformat and timezone.
	 */
	@Override
	public Date deserialize(JsonElement element, Type arg1,
			JsonDeserializationContext arg2) throws JsonParseException {
		// Get the date object
		String date = element.getAsString();
		// Declare the date format and timezone
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

		try {
			// Parsing the date as per the given format
			return formatter.parse(date);
		} catch (ParseException e) {
			System.err.println("Failed to parse Date due to:" + e.getMessage());
			return null;
		}
	}
}