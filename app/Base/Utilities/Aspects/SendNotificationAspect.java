package Base.Utilities.Aspects;

import java.lang.reflect.Field;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import vHMS.Core.ViewModels.NotificationViewModel;
import vHMS.Core.ViewModels.Response;
import Base.Utilities.Email.IEmailSender;

/**
 * The SendNotificationAspect is a spring aspect which is used to send emails
 * from the application.
 * 
 * @author DivyaP
 * @since 2015-08-12
 */
@Aspect
public class SendNotificationAspect {
	@Autowired
	@Qualifier("defaultEmailSender")
	private IEmailSender sender;

	/**
	 * After returning advice injected through spring configuration file, this
	 * method will be invoked, this settings has been done in the
	 * application-context.xml, it will parse the return object and then process
	 * the object to send a notification.
	 * 
	 * @param joinPoint
	 * @param returnValue
	 * @throws Throwable
	 */
	//
	public void SendNotification(JoinPoint joinPoint, Object returnValue)
			throws Throwable {
		try {
			// Convert the returnValue to response type
			ModelMapper modelMapper = new ModelMapper();
			// mapping the response object with the return value
			Response response = modelMapper.map(returnValue, Response.class);
			// Depending on the response convert the response object and process
			// it
			if (null != response.ViewModel) {
				// fetch the response object
				Object notification = response.ViewModel;
				// get all the fields from the response object
				Field fields[] = notification.getClass().getDeclaredFields();
				NotificationViewModel viewmodel = new NotificationViewModel();
				if (fields.length < 0) {
					fields = notification.getClass().getFields();
				}
				for (Field field : fields) {
					/*
					 * check whether the field is matching with the given name,
					 * to make it generialised we always check for the field
					 * name, since there might be more than one object returned
					 * from the method, we always check for the name.
					 * 
					 * Eg: Return Object Might be Object { public Object
					 * viewModel=null; public NotificationViewModel
					 * notificationViewModel = null; public
					 * NotificationViewModel secondaryNotificationViewModel =
					 * null;}, this object contain a field by name
					 * notificationViewModel and this is what it checks for
					 */
					if (field.getName().toLowerCase()
							.contains("notificationviewmodel")) {
						viewmodel = (NotificationViewModel) field
								.get(notification);
					}
					if (response.success && null != viewmodel
							&& null != viewmodel.getTo()) {
						// if the return response is success and there is a
						// noticiation object to be send, then call send
						// notification.
						sender.send(viewmodel);

					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}