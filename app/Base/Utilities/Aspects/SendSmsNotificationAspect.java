package Base.Utilities.Aspects;

import java.lang.reflect.Field;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import vHMS.Core.ViewModels.NotificationViewModel;
import vHMS.Core.ViewModels.Response;
import Base.Utilities.Email.IEmailSender;
import Base.Utilities.Sms.SendSms;

/**
 * The SendNotificationAspect is a spring aspect which is used to send emails
 * from the application.
 * 
 * @author Padmavathi
 * @since 2015-08-12
 */
@Aspect
public class SendSmsNotificationAspect {
	/**
	 * After returning advice injected through spring configuration file, this
	 * method will be invoked, this settings has been done in the
	 * application-context.xml, it will parse the return object and then process
	 * the object to send a notification.
	 * 
	 * @param joinPoint
	 * @param returnValue
	 * @throws Throwable
	 */
	//

	// After returning advice injected through spring configuration file
			public void SendSmsNotification(JoinPoint joinPoint, Object returnValue)
					throws Throwable {
				try {
					// Convert the returnValue to response type
					ModelMapper modelMapper = new ModelMapper();
					// mapping the response object with the return value
					Response response = modelMapper.map(returnValue, Response.class);
					if (null != response.ViewModel) {
						Object notification = response.ViewModel;
						Field fields[] = notification.getClass().getDeclaredFields();
						NotificationViewModel viewmodel = new NotificationViewModel();
						if (fields.length < 0) {
							fields = notification.getClass().getFields();
						}
						for (Field field : fields) {
							/*
							 * check whether the field is matching with the given name,
							 * to make it generialised we always check for the field
							 * name, since there might be more than one object returned
							 * from the method, we always check for the name.
							 * 
							 * Eg: Return Object Might be Object { public Object
							 * viewModel=null; public NotificationViewModel
							 * notificationViewModel = null; public
							 * NotificationViewModel smsNotificationViewModel =
							 * null;}, this object contain a field by name
							 * notificationViewModel and this is what it checks for
							 */
							if (field.getName().equalsIgnoreCase(
									"smsNotificationViewModel")) {
								viewmodel = (NotificationViewModel) field
										.get(notification);
							}
						}
                        //if response is success and recipient number is not null the send sms
						if (response.success && null != viewmodel
								&& null != viewmodel.getTo()) {
							// If success publish the message
							SendSms.sendSMS(viewmodel);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		
}