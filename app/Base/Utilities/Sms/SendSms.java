package Base.Utilities.Sms;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import vHMS.Core.ViewModels.NotificationViewModel;
import Base.Utilities.CommonUtils;
import Base.Utilities.SMSUtil;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class SendSms {
	
	// by pass the NotificationViewModel to send the sms
	public static void  sendSMS( NotificationViewModel viewmodel ) throws JSONException
	{
		//create the json object to set the necessary parameters to send the sms
		JSONObject json = new JSONObject();
		//settting the sender
		json.put("sender", CommonUtils.getMessage("sms.sender"));
		//setting the message
		json.put("message",viewmodel.getBody() );
		//setting the sms format
		json.put("format", CommonUtils.getMessage("sms.format"));
		json.put("flash", CommonUtils.getMessage("sms.flash"));
		Map<String,String> values = new HashMap<String, String>();
		//receiver number 
		values.put("to", viewmodel.getTo());
		values.put("custom", CommonUtils.getMessage("sms.custom"));
		JSONArray jsonArray = new JSONArray();
		jsonArray.put(values);
		json.put("sms", jsonArray);
		String smsMessage=json.toString();
		
		 
			try {
				//Get the URL from WebResource
				WebResource smsObj = SMSUtil.GetInstance();
				//Sending post request with json data
		        ClientResponse clientResponse = smsObj.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, smsMessage);
		        //getting the response from ClientResponse class
		        @SuppressWarnings("unused")
				String output = clientResponse.getEntity(String.class);
		        //Displaying result

			} catch (Exception e) {
				
				e.printStackTrace();
				
			}
			
		
	}

}
