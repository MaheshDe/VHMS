package Base.Utilities;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;

import play.mvc.Http;
import play.mvc.Http.Session;
import Base.Utilities.Security.JWT.Algorithm;
import Base.Utilities.Security.JWT.JWTSigner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The HelperUtil is used for setting the common fields while creating and
 * updating in the DB.
 * 
 * @author DivyaP
 * @since 2014-09-16
 */
public class HelperUtil {

	// creating an instance for mapping the object to a map and vice versa
	ObjectMapper objectMapper = new ObjectMapper();

	/*
	 * Used for attaching the common fields used accross the application
	 */
	@SuppressWarnings("unchecked")
	public <T> T AttachCommonFields(Object viewData, Class<T> type)
			throws Exception {

		// For getting the logged on user
		Session session = Http.Context.current().session();
		String LoggedOnUserId = "0";
		String fkcompanycode = CommonUtils.getMessage("fkcompanycode");
		// Get the session for logged on userid and company code
		if (null != session.get("LoggedOnUserId")) {
			LoggedOnUserId = (session.get("LoggedOnUserId").length() > 0) ? session
					.get("LoggedOnUserId") : "0";
		}
		if (null != session.get("LoggedOnUserCompanyCode")) {
			fkcompanycode = (session.get("LoggedOnUserCompanyCode").length() > 0) ? session
					.get("LoggedOnUserCompanyCode") : "0";
		}

		// For getting the fields and declared getter setter methods
		Field fields[] = viewData.getClass().getDeclaredFields();
		if (fields.length < 0) {
			fields = viewData.getClass().getFields();
		}
		// Iterateing through the fields and
		for (Field field : fields) {
			// check whether the field is matching
			if (field.getName().equalsIgnoreCase("code")) {

				field.setAccessible(true);// Very important, this allows the
											// setting to work for getting and
											// setting the value.
				// getting the value with field of the type class t which we are
				// passing by object view data
				Object fieldValue = field.get(viewData);
				// setting the value for null check
				if (null == fieldValue)
					field.set(viewData, RandomStringUtils.randomAlphanumeric(5));
			}

			if (field.getName().equalsIgnoreCase("uhId")) {

				field.setAccessible(true);// Very important, this allows the
											// setting to work for getting and
											// setting the value.
				// getting the value with field of the type class t which we are
				// passing by object view data
				Object fieldValue = field.get(viewData);
				// setting the value for null check
				if (null == fieldValue)
					field.set(viewData, RandomStringUtils.randomAlphanumeric(5));
			}

			// check whether the field is matching
			if (field.getName().equalsIgnoreCase("createdby")) {
				field.setAccessible(true);
				// getting the value with field of the type class t which we are
				// passing by object view data
				Object fieldValue = field.get(viewData);
				// setting the value for null check
				if (null == fieldValue)
					field.set(viewData, LoggedOnUserId);
			}

			// Check for datecreated/createdat
			if (field.getName().equalsIgnoreCase("datecreated")
					|| field.getName().equalsIgnoreCase("createdat")) {
				field.setAccessible(true);
				Object fieldValue = field.get(viewData);

				if (null == fieldValue)
					field.set(viewData, new Date());
			}
			// Check for datemodified/modifiedat
			if (field.getName().equalsIgnoreCase("datemodified")
					|| field.getName().equalsIgnoreCase("modifiedat")) {
				field.setAccessible(true);
				field.set(viewData, new Date());
			}
			// Check for modifiedby
			if (field.getName().equalsIgnoreCase("modifiedby")) {
				field.setAccessible(true);
				field.set(viewData, LoggedOnUserId);
			}
			// Check for updatecount
			if (field.getName().equalsIgnoreCase("updatecount")) {
				field.setAccessible(true);
				Object fieldValue = field.get(viewData);
				// if the field is null|| empty then initialise it with 1 else
				// add 1 to the existing count
				if (null == fieldValue)
					field.set(viewData, "1");
				else {
					int updateCount = Integer.valueOf(fieldValue.toString());
					field.set(viewData, String.valueOf(updateCount + 1));
				}
			}
			// Check for fkcompanycode
			if (field.getName().equalsIgnoreCase("fkcompanycode")) {
				field.setAccessible(true);
				Object fieldValue = field.get(viewData);
				if (null == fieldValue)
					field.set(viewData, fkcompanycode);
			}
		}

		// Returning the object of type T
		return (T) viewData;
	}

	/**
	 * This method is used to convert the given json to construct where clause
	 * for fetching the records from DB.
	 * 
	 * @param json
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 */
	public String ConstructWhereClauseFromJson(Object json)
			throws JsonProcessingException, IOException {

		// Converting JSon object to string for the first object in the
		// viewModel
		JsonNode actualObj = objectMapper.readTree(json.toString());
		/*
		 * Passing the key string of the first object in the view model and
		 * fetching the second json object in the the view Model
		 */
		String treeMap = actualObj.findPath("name").toString();
		// Converting JSon object to string for the second object in the
		// viewModel
		actualObj = objectMapper.readTree(treeMap);
		/*
		 * Passing the key string of the second object in the view model and
		 * fetching the value
		 */
		treeMap = actualObj.findPath("contains").asText();
		// Appending the value with the characters on the left on the right
		String regex = ".*" + treeMap + ".*";

		return regex;
	}

	/***
	 * Generate JWT token For Authorization based on configuration in
	 * appsettings
	 * 
	 * @param claims
	 * @return
	 * @throws Exception
	 */
	public static String GenerateAuthToken(Map<String, Object> claims)
			throws Exception {
		// Setting Security Key
		JWTSigner signer = new JWTSigner(CommonUtils.decryptor(CommonUtils
				.getMessage("JWT.AuthSecurityKey")));
		// Generating AuthToken
		String authToken = signer
				.sign(claims,
						// Setting Options for JWT Token
						new JWTSigner.Options()
								.setAlgorithm(Algorithm.HS384)
								.setExpirySeconds(
										Integer.parseInt(CommonUtils
												.getMessage("JWT.TokenExpiryInSecond")))
								.setIssuedAt(true).setJwtId(true));
		return authToken;
	}
}
