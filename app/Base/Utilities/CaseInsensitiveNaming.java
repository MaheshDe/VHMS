package Base.Utilities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;

/**
 * The CaseInsensitiveNaming is the implementation for the interface PropertyNamingStrategy.
 * 
 * @author DivyaP
 * @since 2014-09-11
 */
public class CaseInsensitiveNaming extends PropertyNamingStrategy {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * This method is used to fetch the jsonannotation from the getter method of the properties 
	 * while using an object mapper.
	 * @param MapperConfig
	 * @param AnnotatedMethod
	 * @param String
	 */
	@Override
    public String nameForGetterMethod(MapperConfig<?> config,
         AnnotatedMethod method, String defaultName)
    {
      // case-insensitive, underscores etc. mapping to property names
      // so you need to implement the convert method in way you need.
		final JsonProperty annotation = method.getAnnotation(JsonProperty.class);
		String name="";
        if (annotation != null) {
        	//If getter method is annotated with JsonProperty get that.
        	name = annotation.value();
        }        
        else name = defaultName;
      return name; 
    }	
	
  }