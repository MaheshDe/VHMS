package Base.Utilities.Email;

import vHMS.Core.ViewModels.NotificationViewModel;

/**
 * The IEmailSender is an interface containing methods for sending mails
 * 
 * @author DivyaP
 * @version 1.0
 * @since 2015-08-12
 */
public interface IEmailSender {
	/**
	 * Sends e-mail using Commons.Email with scala html for the body and the
	 * properties passed in as variables.
	 * 
	 * @param msg
	 *            The e-mail message to be sent, except for the body.
	 * @param message
	 *            Viewmodel to hold mail message.
	 */
	public void send(NotificationViewModel message);

}
