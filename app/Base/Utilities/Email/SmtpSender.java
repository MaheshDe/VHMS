package Base.Utilities.Email;

import java.util.ArrayList;
import java.util.Arrays;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.HtmlEmail;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vHMS.Core.ViewModels.NotificationViewModel;
import Base.Utilities.CommonUtils;

/**
 * The SmtpSender is an implementation for interface IEmailSender containing
 * methods for sending mails
 * 
 * @author DivyaP
 * @version 1.0
 * @since 2015-08-12
 */
@Component
@Service
@Transactional
public class SmtpSender implements IEmailSender {

	/**
	 * Sends e-mail using Commons.Email with scala html for the body and the
	 * properties passed in as variables.
	 * 
	 * @param msg
	 *            The e-mail message to be sent, except for the body.
	 * @param message
	 *            Viewmodel to hold mail message.
	 */
	public void send(NotificationViewModel message) {
		HtmlEmail email = new HtmlEmail();
		try {
			// Enable authentication
			email.setAuthenticator(new DefaultAuthenticator(CommonUtils
					.getMessage("mail.username"), CommonUtils
					.getMessage("mail.password")));
			// Enable SSL
			email.setSSL(true);
			// Add host
			email.setHostName(CommonUtils.getMessage("mail.host"));
			// Add port
			email.setSmtpPort(Integer.parseInt(CommonUtils
					.getMessage("mail.port")));
			// Add To
			if (null != message.getTo()) {
				email.setTo(convertRecipientToList(message.getTo()));
			} else {
				email.setTo(convertRecipientToList(CommonUtils
						.getMessage("mail.to")));
			}
			// Add From
			email.setFrom(CommonUtils.getMessage("mail.from"));
			// Add Subject
			if (null != message.getSubject()) {
				email.setSubject(message.getSubject());
			} else {
				email.setSubject((CommonUtils.getMessage("mail.subject")));
			}
			// Add CC
			if (null != message.getCc()) {
				email.setCc(convertRecipientToList(message.getCc()));
			} else if (null != CommonUtils.getMessage("mail.cc")) {
				email.setCc(convertRecipientToList(CommonUtils
						.getMessage("mail.cc")));
			}
			// Add BCC
			if (null != message.getBcc()) {
				email.setBcc(convertRecipientToList(message.getCc()));
			}
			// Add Body
			email.setHtmlMsg(message.getBody());
			// Add Text Message
			email.setTextMsg("Your email client does not support HTML messages");
			// Send Email
			email.send();
			System.out.println("Email Sent Successfully using SMTP");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// Method to convert mail recipients to Collection
	private ArrayList<InternetAddress> convertRecipientToList(
			String commaSepratedRecipients) {
		ArrayList<InternetAddress> recipients = new ArrayList<InternetAddress>(
				Arrays.asList(getAddressFromStrArray(commaSepratedRecipients
						.split(","))));
		return recipients;
	}

	// Get the address from string array
	private InternetAddress[] getAddressFromStrArray(final String[] address) {
		int sizeTo = address.length;
		InternetAddress[] addressArray = new InternetAddress[sizeTo];
		for (int i = 0; i < sizeTo; i++) {
			try {
				addressArray[i] = new InternetAddress(address[i]);
			} catch (AddressException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return addressArray;
	}
}