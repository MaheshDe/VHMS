package Base.Utilities;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtilsBean;

/**
 * The NullAwareBeanUtilsBean is for copying the existing data 
 * and updating only the new data from viewModel to the existing data.
 * 
 * @author Arokia Felsi
 * @since 2014-09-11
 */
public class NullAwareBeanUtilsBean extends BeanUtilsBean{

	/* Used to copy the object properties and 
	 bind it with the existing data in the destination object */
	
    @Override
    public void copyProperty(Object dest, String name, Object value)
            throws IllegalAccessException, InvocationTargetException {
    	/* if the object properties is null it set the old properties from the destination object
    	 * otherwise it binds the new data to destination object*/
        if(value==null)return;
        super.copyProperty(dest, name, value);
    }

}