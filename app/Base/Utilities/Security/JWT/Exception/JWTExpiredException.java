package Base.Utilities.Security.JWT.Exception;
/**
 * JWTExpiredException checks for the expired time for an access token,
 * while validating a token and throws exception if validation failed.  
 * 
 * @author Shunmuga Raja G
 * @version 1.0
 * @since 2015-08-12
 */

public class JWTExpiredException extends JWTVerifyException {
    private long expiration;

    public JWTExpiredException(long expiration) {
        this.expiration = expiration;
    }

    public JWTExpiredException(String message, long expiration) {
        super(message);
        this.expiration = expiration;
    }

    public long getExpiration() {
        return expiration;
    };
}
