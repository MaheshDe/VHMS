package Base.Utilities.Security.JWT.Exception;
/**
 * JWTIssuerException validates an access token based on an issuer.  
 * 
 * @author Shunmuga Raja G
 * @version 1.0
 * @since 2015-08-12
 */
public class JWTIssuerException extends JWTVerifyException {
    private final String issuer;

    public JWTIssuerException(String issuer) {
        this.issuer = issuer;
    }

    public JWTIssuerException(String message, String issuer) {
        super(message);
        this.issuer = issuer;
    }

    public String getIssuer() {
        return issuer;
    }
}
