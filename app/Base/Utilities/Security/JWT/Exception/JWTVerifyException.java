package Base.Utilities.Security.JWT.Exception;
/**
 * JWTVerifyException throws exception when expiration, issuer or audience are invalid.  
 * 
 * @author Shunmuga Raja G
 * @version 1.0
 * @since 2015-08-12
 */
public class JWTVerifyException extends Exception {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4911506451239107610L;

	public JWTVerifyException() {
    }
	
	

    public JWTVerifyException(String message, Throwable cause) {
		super(message, cause);
	}


	public JWTVerifyException(String message) {
        super(message);
    }
}
