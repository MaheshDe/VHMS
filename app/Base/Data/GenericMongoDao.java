package Base.Data;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import net.vz.mongodb.jackson.JacksonDBCollection;
import net.vz.mongodb.jackson.WriteResult;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
import com.mongodb.WriteConcern;
/**
 * GenericMongoDao deals with basic operation like Insert, Update, 
 * Search one record, Search all records based on criteria, Delete etc.
 * Its a generic DAO which has to be used by every class performing any crud operation.  
 * 
 * @author karthiga
 * @version 1.0
 * @since 2015-08-12
 */


import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;

import org.bson.types.ObjectId;

import Base.Utilities.CommonUtils;

import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;


@Service
@Transactional
public class GenericMongoDao implements IGenericMongoDao {

	private Mongo mongo;

	private final JacksonCollectionHelper pojoDbHelper = new JacksonCollectionHelper();

	public GenericMongoDao() {

	}

	public GenericMongoDao(Mongo mongo) {
		this.mongo = mongo;
		this.mongo.setWriteConcern(WriteConcern.SAFE);
		// this.pojoDbHelper = //new JacksonCollectionHelper(getMongoDb());
	}

	/**
	 * Fetch all the documents from a collection and return them as Java maps.
	 * 
	 * @param collectionName
	 *            (required)
	 */
	public Iterable<Map<String, Object>> getDocumentsAsMap(String collectionName) {
		try {
			final DB db = getMongoDb();
			final DBCollection collection = db.getCollection(collectionName);

			final DBCursor allDocuments = collection.find();

			return new Iterable<Map<String, Object>>() {

				@Override
				public Iterator<Map<String, Object>> iterator() {
					return new DBCollectionMapIterator(allDocuments);
				}
			};

		} catch (MongoException me) {
			throw new RuntimeException(me);
		}
	}

	/**
	 * Fetch all the documents from a collection and return them as Java Array.
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @type pojo type class
	 */
	public <T> List<T> all(String collectionName, Class<T> type, String name,
			String limit, String skip) throws JsonProcessingException,
			IOException {
		try {
			List<T> list;

			BasicDBObject query = new BasicDBObject();
			Map<String, String> map = new HashMap<String, String>();
			map.put("name", name);

			if (null == limit || limit.length() == 0 || limit.equals("NaN"))
				limit = "0";
			if (null == skip || skip.length() == 0 || skip.equals("NaN"))
				skip = "0";

			else if (null != name) {

				for (Map.Entry<String, String> entry : map.entrySet()) {
					// Converting string to regexp
					query.put(entry.getKey(), java.util.regex.Pattern.compile(
							entry.getValue(), Pattern.CASE_INSENSITIVE));
				}

			}
			return pojoDbHelper.getPojoCollectionFor(collectionName, type)
					.find(query).limit(Integer.parseInt(limit))
					.skip(Integer.parseInt(skip)).toArray();

		} catch (MongoException me) {
			throw new RuntimeException(me);
		}
	}

	/**
	 * Look ma, automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations! Update By Id
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @param id
	 *            to search for
	 * @type pojo type class
	 */
	public <T> void updateById(String collectionName, T pojo, Object id) {
		JacksonDBCollection<T, Object> pojos = pojoDbHelper
				.getPojoCollectionFor(collectionName, pojo);
		final int updateCount = pojos.updateById(id, pojo).getN();
		if (updateCount != 1) {
			throw new IllegalStateException("The object to be updated doesn't "
					+ "exist in the collection '" + collectionName
					+ "' with the id '" + id + "'. Update pojo: " + pojo);
		}
	}

	/**
	 * Look ma, automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations!
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @param pojo
	 *            (required) the Java bean to store, optionally annotated with
	 *            some Jackson annotations
	 */
	public <T> void insert(String collectionName, T pojo) {
		try {
			// SAFE = Require Mongo to wait for response from the DB to verify
			// there isn't a duplicate
			pojoDbHelper.getPojoCollectionFor(collectionName, pojo).insert(
					pojo, WriteConcern.SAFE);
		} catch (MongoException.DuplicateKey e) {
			throw new IllegalStateException(
					"A document with the same id exists already: "
							+ e.getMessage());
		}
	}

	/**
	 * Look ma, automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations!
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @param pojo
	 *            (required) the Java bean to store, optionally annotated with
	 *            some Jackson annotations
	 */
	public <T> Object[] save(String collectionName, T pojo) {
		Object[] results = null;
		try {
			// SAFE = Require Mongo to wait for response from the DB to verify
			// there isn't a duplicate
			WriteResult<T, Object> result = null;
			result = pojoDbHelper.getPojoCollectionFor(collectionName, pojo)
					.save(pojo, WriteConcern.SAFE);
			results = result.getSavedIds();

		} catch (MongoException.DuplicateKey e) {
			throw new IllegalStateException(
					"A document with the same id exists already: "
							+ e.getMessage());
		}
		return results;
	}

	/** For testing */
	public void dropDatabase() {
		getMongoDb().dropDatabase();
	}

	/**
	 * automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations! FindById
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @param id
	 *            to search for
	 * @type pojo type class
	 */
	public <T> T findOneById(String collectionName, Object id, Class<T> type) {
		return pojoDbHelper.getPojoCollectionFor(collectionName, type)
				.findOneById(id);
	}

	/**
	 * automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations! Remove All elements from the collection automatic POJO
	 * to Mongo's BSON object mapping without any manual transformations!
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @type pojo type class
	 */
	public <T> void removeAll(String collectionName, Class<T> type) {
		pojoDbHelper.getPojoCollectionFor(collectionName, type).drop();
	}

	/**
	 * automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations! Return the total collection count automatic POJO to
	 * Mongo's BSON object mapping without any manual transformations!
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @type pojo type class
	 */
	public <T> int count(String collectionName, Class<T> type) {
		return pojoDbHelper.getPojoCollectionFor(collectionName, type).find()
				.count();
	}

	/**
	 * automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations! delete by id
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @param id
	 *            to search for
	 * @type pojo type class
	 */
	public <T> void delete(String collectionName, Object id, Class<T> type) {
		T entity = pojoDbHelper.getPojoCollectionFor(collectionName, type)
				.findOneById(id);
		if (entity != null)
			pojoDbHelper.getPojoCollectionFor(collectionName, type).remove(
					entity);
	}

	/**
	 * automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations! Return DB
	 */
	private DB getMongoDb() {
		return mongo.getDB("");
	}

	/**
	 * automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations! FindById
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @param attribute
	 *            /column to search for like email,mobile
	 * @param attributevalue
	 *            value to search for given attribute
	 * @type pojo type class
	 */
	@Deprecated
	public <T> List<T> findAllByAttributes(String collectionName,
			Map<String, Object> map, Class<T> type, String limit, String skip) {
		
		
		return findAllByAttributes(collectionName, map, type, limit, skip, new BasicDBObject());
	}
 
	
	/**
	 * automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations! FindById-- Sortable  by the orderBy parameter
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @param attribute
	 *            /column to search for like email,mobile
	 * @param attributevalue
	 *            value to search for given attribute
	 * @type pojo type class
	 */
	
	public <T> List<T> findAllByAttributes(String collectionName,
			Map<String, Object> map, Class<T> type, String limit, String skip,BasicDBObject orderBy) {
		if (null == limit || limit.length() == 0 || limit.equals("NaN"))
			limit = "0";
		if (null == skip || skip.length() == 0 || skip.equals("NaN"))
			skip = "0";
		if (null == orderBy )
			orderBy = new BasicDBObject();

		BasicDBObject query = new BasicDBObject();
		for (Map.Entry<String, Object> entry : map.entrySet()) {
	 
			query.put(entry.getKey(),  entry.getValue());
			 
			/*query.put(entry.getKey(), java.util.regex.Pattern.compile(
					(String) entry.getValue(), Pattern.CASE_INSENSITIVE));*/
		}
		return pojoDbHelper.getPojoCollectionFor(collectionName, type)
				.find(query).sort(orderBy).limit(Integer.parseInt(limit))
				.skip(Integer.parseInt(skip)).toArray();
	}
 

	/**
	 * automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations! FindById
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @param attribute
	 *            /column to search for like email,mobile
	 * @param attributevalue
	 *            value to search for given attribute
	 * @type pojo type class
	 */
	public <T> T findOneByAttribute(String collectionName,
			Map<String, Object> map, Class<T> type) {

		BasicDBObject query = new BasicDBObject();
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			query.put(entry.getKey(), entry.getValue());
		}
		// return pojoDbHelper.getPojoCollectionFor(collectionName,
		// type).findOne(DBQuery.is(attribute,
		// attributevalue).and(DBQuery.is(attributeTwo , attributeTwovalue)));
		return pojoDbHelper.getPojoCollectionFor(collectionName, type).findOne(
				query);
	}
	
	
	
	/**
	 * automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations! count by attributes
	 * 
	 * @param collectionName
	 *            
	 * @param attribute
	 *            
	 * @param attributevalue
	 *            
	 * @type pojo type class 
	 */
	public <T> int count(String collectionName,
			Map<String, Object> map, Class<T> type) {


		BasicDBObject query = new BasicDBObject();
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			query.put(entry.getKey(),  entry.getValue());
		}
		return pojoDbHelper.getPojoCollectionFor(collectionName, type)
				.find(query).count();
	}
 
	 /**
	 * Save file into Mongo gridfs and store the meta data
	 * 
	 * @param pojo
	 *            (required) the Java bean to store which holds the file object
	 *            metadata, optionally annotated with some Jackson annotations
	 */
	public <T> T loadFileToGridFs(T pojo) throws IOException {
		try {
			// For getting the fields and declared getter setter methods
			// Get the filename
			Field fileNamefield = pojo.getClass().getDeclaredField("fileName");
			if (null == fileNamefield) {
				fileNamefield = pojo.getClass().getField("fileName");
			}
			fileNamefield.setAccessible(true);// Very important, this allows the
			// setting to work for getting and
			// setting the value.
			// getting the value with field of the type class t which we are
			// passing by object view data

			Object filename = fileNamefield.get(pojo);
			// Get the filename
			Field filefield = pojo.getClass().getDeclaredField("LoadedFile");
			if (null == filefield) {
				filefield = pojo.getClass().getField("LoadedFile");
			}
			filefield.setAccessible(true);// Very important, this allows the
											// setting to work for getting and
											// setting the value.
			// getting the value with field of the type class t which we are
			// passing by object view data

			File file = (File) filefield.get(pojo);
			// Get the filename
						Field requestTypefield = pojo.getClass().getDeclaredField("requestType");
						if (null == requestTypefield) {
							requestTypefield = pojo.getClass().getField("requestType");
						}
						requestTypefield.setAccessible(true);// Very important, this allows the
														// setting to work for getting and
														// setting the value.
						// getting the value with field of the type class t which we are
						// passing by object view data

						Object requestType =  requestTypefield.get(pojo);
			 // Get the contentType
						Field contentTypefield = pojo.getClass().getDeclaredField("contentType");
						if (null == contentTypefield) {
							contentTypefield = pojo.getClass().getField("contentType");
						}
						contentTypefield.setAccessible(true);// Very important, this allows the
														// setting to work for getting and
														// setting the value.
						// getting the value with field of the type class t which we are
						// passing by object view data

						Object contentTypedata =  contentTypefield.get(pojo);
			GridFS gfsPhoto = new GridFS(pojoDbHelper.getDB(CommonUtils.getMessage("Document"),pojo.getClass()), (requestType.toString()).replaceAll("[^A-Za-z]+", "").toLowerCase());
			GridFSInputFile gfsFile = gfsPhoto.createFile(file);
			gfsFile.setFilename(filename.toString());
			gfsFile.setContentType(contentTypedata.toString());
			gfsFile.save();
			Field gridfsObjectIdfield = pojo.getClass().getDeclaredField("code");
			gridfsObjectIdfield.setAccessible(true);
			Object gridfsObjectIdValue = gridfsObjectIdfield.get(pojo);
			if (null == gridfsObjectIdValue){
				gridfsObjectIdfield.set(pojo, gfsFile.getId().toString());
		}
			DBCursor cursor = gfsPhoto.getFileList();
			
			/*while (cursor.hasNext()) {
				System.out.println(cursor.next());
			
			}*/
		} catch (IllegalArgumentException | IllegalAccessException
				| NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pojo;

	}
	 /**
	 * Save file from Mongo gridfs and store the meta data
	 * 
	 * @param pojo
	 *            (required) the Java bean to store which holds the file object
	 *            metadata, optionally annotated with some Jackson annotations
	 */
	public <T> T downloadFileFromGridFs(T pojo) throws IOException {
		
		try {
			// Get the filename
			Field requestTypefield = pojo.getClass().getDeclaredField("requestType");
			if (null == requestTypefield) {
				requestTypefield = pojo.getClass().getField("requestType");
			}
			requestTypefield.setAccessible(true);// Very important, this allows the
											// setting to work for getting and
											// setting the value.
			// getting the value with field of the type class t which we are
			// passing by object view data
			Field ObjectIdfield = pojo.getClass().getDeclaredField("code");
			if (null == ObjectIdfield) {
				ObjectIdfield = pojo.getClass().getField("code");
			}
			ObjectIdfield.setAccessible(true);// Very important, this allows the
			// setting to work for getting and
			// setting the value.
			// getting the value with field of the type class t which we are
			// passing by object view data

			Object gridFSObjectId = ObjectIdfield.get(pojo);
			ObjectId id = new ObjectId(gridFSObjectId.toString());
			Object requestType =  requestTypefield.get(pojo);
			GridFS gfsPhoto = new GridFS(pojoDbHelper.getDB(CommonUtils.getMessage("Document"),pojo.getClass()), (requestType.toString()).replaceAll("[^A-Za-z]+", "").toLowerCase());
		GridFSDBFile imageForOutput = gfsPhoto.findOne(id);
		Field statusfield = pojo.getClass().getDeclaredField("status");
		if (null == statusfield) {
			statusfield = pojo.getClass().getField("status");
		}
		statusfield.setAccessible(true);
		Object statusfieldValue = statusfield.get(pojo);
		if(imageForOutput != null){
			
			statusfield.set(pojo, "true");
			File tempFile = new File(imageForOutput.getFilename());
			OutputStream outputStream = new FileOutputStream(tempFile);

			// Passing the outputstream downloading the file
			imageForOutput.writeTo(outputStream);
			//imageForOutput.writeTo("D:/DemoImageNew.png");
			Field fields[] = pojo.getClass().getDeclaredFields();
			// Setting required parameter in view model
			if (fields.length < 0) {
				fields = pojo.getClass().getFields();
			}
			// Iterateing through the fields and
			for (Field field : fields) {
				if (field.getName().equalsIgnoreCase("contentType")) {

					field.setAccessible(true);// Very important, this allows the
												// setting to work for getting and
												// setting the value.
					// getting the value with field of the type class t which we are
					// passing by object view data
					Object fieldValue = field.get(pojo);
					// setting the value for null check
					if (null == fieldValue)
						field.set(pojo, imageForOutput.getContentType());
				}
				if (field.getName().equalsIgnoreCase("fileName")) {

					field.setAccessible(true);// Very important, this allows the
												// setting to work for getting and
												// setting the value.
					// getting the value with field of the type class t which we are
					// passing by object view data
					Object fieldValue = field.get(pojo);
					// setting the value for null check
					if (null == fieldValue)
						field.set(pojo, imageForOutput.getFilename());
				}
				if (field.getName().equalsIgnoreCase("LoadedFile")) {

					field.setAccessible(true);// Very important, this allows the
												// setting to work for getting and
												// setting the value.
					// getting the value with field of the type class t which we are
					// passing by object view data
					Object fieldValue = field.get(pojo);
					// setting the value for null check
					if (null == fieldValue)
						field.set(pojo,tempFile);
				}
			}
		
		}else{
			
				statusfield.set(pojo, "false");
		}
		
		}catch (IllegalArgumentException | IllegalAccessException
				| NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pojo;
	}
	
	 /**
	 * delete file from Mongo gridfs 
	 * 
	 * @param pojo
	 *            (required) the Java bean to store which holds the file object
	 *            metadata, optionally annotated with some Jackson annotations
	 */
	public <T> T deleteFileFromGridFs(T pojo) throws IOException {
		
		try {
			Field ObjectIdfield = pojo.getClass().getDeclaredField("code");
			if (null == ObjectIdfield) {
				ObjectIdfield = pojo.getClass().getField("code");
			}
			ObjectIdfield.setAccessible(true);// Very important, this allows the
			// setting to work for getting and
			// setting the value.
			// getting the value with field of the type class t which we are
			// passing by object view data

			Object gridFSObjectId = ObjectIdfield.get(pojo);
			ObjectId id = new ObjectId(gridFSObjectId.toString());
			//ObjectId id = (ObjectId) gridFSObjectId;
			Field requestTypefield = pojo.getClass().getDeclaredField("requestType");
			if (null == requestTypefield) {
				requestTypefield = pojo.getClass().getField("requestType");
			}
			requestTypefield.setAccessible(true);
			Object requestType =  requestTypefield.get(pojo);
			GridFS gfsPhoto = new GridFS(pojoDbHelper.getDB(CommonUtils.getMessage("Document"),pojo.getClass()), (requestType.toString()).replaceAll("[^A-Za-z]+", "").toLowerCase());
		GridFSDBFile  GfsFileExist = gfsPhoto.findOne(id);
		Field statusfield = pojo.getClass().getDeclaredField("status");
		if (null == statusfield) {
			statusfield = pojo.getClass().getField("status");
		}
		statusfield.setAccessible(true);
		Object statusfieldValue = statusfield.get(pojo);
		if(GfsFileExist != null){
			 gfsPhoto.remove(id);
					statusfield.set(pojo, "true");
					Field gridfsObjectIdfield = pojo.getClass().getDeclaredField("code");
					gridfsObjectIdfield.setAccessible(true);
					Object gridfsObjectIdValue = gridfsObjectIdfield.get(pojo);
					if (null == gridfsObjectIdValue){
						gridfsObjectIdfield.set(pojo, GfsFileExist.getId());
				}
		}else{
			
				statusfield.set(pojo, "false");
		}
		}catch (IllegalArgumentException | IllegalAccessException
				| NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pojo;
	}
}
