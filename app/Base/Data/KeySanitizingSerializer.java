package Base.Data;

/**
 * Replace the characters that are not legal in JSON field names,
 * namely '.' and leading '$'.
 * @author karthiga
 * @version 1.0
 * @since 2015-08-12
 */

//https://github.com/jakubholynet/blog/blob/master/miniprojects/generic-pojo-mappers/src/main/java/net/jakubholy/blog/genericmappers/mongo/KeySanitizingSerializer.java

import java.io.IOException;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

class KeySanitizingSerializer extends JsonSerializer<String> {

    @Override
    public void serialize(String value, JsonGenerator jgen, SerializerProvider provider) throws IOException,
            JsonProcessingException {
        if (value.startsWith("$")) {
            value = "#" + value.substring(1);
        }
        jgen.writeFieldName(value.replace('.', '-'));

    }

}