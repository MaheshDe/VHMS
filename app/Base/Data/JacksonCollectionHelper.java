package Base.Data;


import java.util.HashMap;
import java.util.Map;

import net.vz.mongodb.jackson.JacksonDBCollection;
import net.vz.mongodb.jackson.internal.MongoAnnotationIntrospector;
import net.vz.mongodb.jackson.internal.MongoJacksonHandlerInstantiator;
import net.vz.mongodb.jackson.internal.MongoJacksonMapperModule;

import org.codehaus.jackson.Version;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.module.SimpleModule;

import com.mongodb.DB;

import play.modules.mongodb.jackson.MongoDB;

/**
 * Helper for working with Mongo collections via POJOs, using the Jackson object
 * mapper.
 * @author karthiga
 * @version 1.0
 * @since 2015-08-12
 */
class JacksonCollectionHelper {

	private static final ObjectMapper MAP_KEY_SANITIZING_MAPPER = createCustomizedObjectMapper();
	// private final DB mongoDb;

	private final Map<String, JacksonDBCollection<?, ?>> cache = new HashMap<String, JacksonDBCollection<?, ?>>();

	public JacksonCollectionHelper() {
		// this.mongoDb = MongoDB;
	}

	/**
	 * Create our customized object mapper that f.ex. takes care of removing
	 * invalid characters from map keys.
	 */
	private static ObjectMapper createCustomizedObjectMapper() {
		SimpleModule mapKeyModule = new SimpleModule(
				"MyMapKeySanitizingSerializerModule",
				new Version(1, 0, 0, null));
		mapKeyModule.addKeySerializer(String.class,
				new KeySanitizingSerializer());

		final ObjectMapper customizedMapper = new ObjectMapper();
		customizedMapper.registerModule(mapKeyModule);

		// The following code has been copied from JacksonDBCollection and
		// with mongo-jackson-mapper 1.4.x it can be replaced with call to
		// MongoJacksonMapperModule.configure(mapper)
		customizedMapper.registerModule(MongoJacksonMapperModule.INSTANCE);
		customizedMapper
				.setHandlerInstantiator(new MongoJacksonHandlerInstantiator(
						new MongoAnnotationIntrospector(customizedMapper
								.getDeserializationConfig())));
		// end copy

		return customizedMapper;
	}

	<T> JacksonDBCollection<T, Object> wrap(
			JacksonDBCollection<T, Object> dbCollection, Class<T> type) {
		return null; // JacksonDBCollection.wrap((DBCollection)dbCollection,
						// type, Object.class, MAP_KEY_SANITIZING_MAPPER);
	}

	public <T> JacksonDBCollection<T, Object> getPojoCollectionFor(
			String collectionName, Class<T> pojoType) {
		JacksonDBCollection<T, Object> rawCollection = MongoDB.getCollection(
				collectionName, pojoType, Object.class);

		@SuppressWarnings("unchecked")
		JacksonDBCollection<T, Object> pojoCollection = (JacksonDBCollection<T, Object>) cache
				.get(collectionName);
		if (pojoCollection == null) {
			// pojoCollection = wrap(rawCollection, pojoType);
			pojoCollection = rawCollection;
			cache.put(collectionName, pojoCollection);
		}

		return pojoCollection;
	}

	public <T> JacksonDBCollection<T, Object> getPojoCollectionFor(
			String collectionName, T pojo) {
		@SuppressWarnings("unchecked")
		Class<T> pojoType = (Class<T>) pojo.getClass();
		return getPojoCollectionFor(collectionName, pojoType);
	}
	
	//Get the DB instance of mongodb
	public <T> DB getDB(String collectionName,Class<T> pojoType) {
		JacksonDBCollection<T, Object> rawCollection = MongoDB.getCollection(
				collectionName, pojoType, Object.class);

		/*@SuppressWarnings("unchecked")
		JacksonDBCollection<T, Object> pojoCollection = (JacksonDBCollection<T, Object>) cache
				.get(collectionName);
		pojoCollection = rawCollection;
		cache.put(collectionName, pojoCollection);*/
		return  getPojoCollectionFor(collectionName, pojoType).getDB();
		/*return pojoCollection.getDB();*/
	}

}