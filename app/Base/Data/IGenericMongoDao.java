package Base.Data;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import vHMS.Core.Models.Appointment;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mongodb.BasicDBObject;
/**
 * IGenericMongoDao  is an interface containing methods like Insert, Update, 
 * Search one record, Search all records based on criteria, Delete etc.
 *  
 * 
 * @author karthiga
 * @version 1.0
 * @since 2015-08-12
 */
public interface IGenericMongoDao {
	/**
	 * Fetch all the documents from a collection and return them as Java maps.
	 * 
	 * @param collectionName
	 *            (required)
	 */
	public Iterable<Map<String, Object>> getDocumentsAsMap(String collectionName);

	/**
	 * Fetch all the documents from a collection and return them as Java Array.
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @type pojo type class
	 */
	public <T> List<T> all(String collectionName, Class<T> type, String name,
			String limit, String skip) throws JsonProcessingException,
			IOException;

	/**
	 * Look ma, automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations! Update By Id
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @param id
	 *            to search for
	 * @type pojo type class
	 */
	public <T> void updateById(String collectionName, T pojo, Object id);

	/**
	 * Look ma, automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations!
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @param pojo
	 *            (required) the Java bean to store, optionally annotated with
	 *            some Jackson annotations
	 */
	public <T> void insert(String collectionName, T pojo);

	/**
	 * Look ma, automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations!
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @param pojo
	 *            (required) the Java bean to store, optionally annotated with
	 *            some Jackson annotations
	 */
	public <T> Object[] save(String collectionName, T pojo);

	/** For testing */
	public void dropDatabase();

	/**
	 * automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations! FindById
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @param id
	 *            to search for
	 * @type pojo type class
	 */
	public <T> T findOneById(String collectionName, Object id, Class<T> type);

	/**
	 * automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations! Remove All elements from the collection automatic POJO
	 * to Mongo's BSON object mapping without any manual transformations!
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @type pojo type class
	 */
	public <T> void removeAll(String collectionName, Class<T> type);

	/**
	 * automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations! Return the total collection count automatic POJO to
	 * Mongo's BSON object mapping without any manual transformations!
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @type pojo type class
	 */
	public <T> int count(String collectionName, Class<T> type);

	/**
	 * automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations! delete by id
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @param id
	 *            to search for
	 * @type pojo type class
	 */
	public <T> void delete(String collectionName, Object id, Class<T> type);

	/**
	 * automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations! delete by id
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @param attribute
	 *            /column to search for like email,mobile
	 * @param attributevalue
	 *            value to search for given attribute
	 * @type pojo type class
	 */
	@Deprecated
	public <T> List<T> findAllByAttributes(String collectionName,
			Map<String, Object> map, Class<T> type, String limit, String skip);
	/**
	 * automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations! find by Attribute, can pass  order By As parameter 
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @param attribute
	 *            /column to search for like email,mobile
	 * @param attributevalue
	 *            value to search for given attribute
	 * @type pojo type class
	 */
	
	public <T> List<T> findAllByAttributes(String collectionName,
			Map<String, Object> map, Class<T> type, String limit, String skip, BasicDBObject orderBy);

	/**
	 * automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations! delete by id
	 * 
	 * @param collectionName
	 *            (required) the collection to insert the document representing
	 *            the pojo into
	 * @param attribute
	 *            /column to search for like email,mobile
	 * @param attributevalue
	 *            value to search for given attribute
	 * @type pojo type class
	 */
	public <T> T findOneByAttribute(String collectionName,
			Map<String, Object> map, Class<T> type);
	
	
	/***
	 *  * automatic POJO to Mongo's BSON object mapping without any manual
	 * transformations! Count  record by attribute
	 * 
	 * @param collectionName
	 * @param map
	 * @param type
	 * @return
	 */
	public <T> int count(String collectionName,	Map<String, Object> map, Class<T> type);
	
	/**
	 * Save file into Mongo gridfs and store the meta data
	 * 
	 * @param pojo
	 *            (required) the Java bean to store which holds the file object metadata, optionally annotated with
	 *            some Jackson annotations
	 */
	public <T> T loadFileToGridFs(T pojo) throws IOException;
	/**
	 * Download file from Mongo gridfs and store in to the system.
	 * 
	 * @param pojo
	 *            (required) the Java bean to store which holds the file object metadata, optionally annotated with
	 *            some Jackson annotations
	 */
	public <T> T downloadFileFromGridFs(T pojo) throws IOException;
	/**
	 * delete file from Mongo gridfs 
	 * 
	 * @param pojo
	 *            (required) the Java bean to store which holds the file object
	 *            metadata, optionally annotated with some Jackson annotations
	 */
	public <T> T deleteFileFromGridFs(T pojo) throws IOException;

 
	
	


}
