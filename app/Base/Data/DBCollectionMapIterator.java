package Base.Data;

import java.util.Iterator;
import java.util.Map;

import com.mongodb.DBCursor;
import com.mongodb.DBObject;

/**
 * Iterate over search results returning the elements as Maps instead of the
 * native BSON objects.
 * @author karthiga
 * @version 1.0
 * @since 2015-08-12
 */
final class DBCollectionMapIterator implements Iterator<Map<String, Object>> {
	private final Iterator<DBObject> allDocuments;

	public DBCollectionMapIterator(DBCursor allDocuments) {
		this.allDocuments = allDocuments.iterator();
	}

	public DBCollectionMapIterator(Iterator<DBObject> iterator) {
		this.allDocuments = iterator;
	}

	@Override
	public boolean hasNext() {
		return allDocuments.hasNext();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> next() {
		return allDocuments.next().toMap();
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException("remove");
	}
}