package Base.Common.Attribute;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import play.libs.F.Promise;
import play.mvc.Action;
import play.mvc.Http.Context;
import play.mvc.Http.Response;
import play.mvc.With;

/**
 * The CorsComposition to allow Cross domain access for the rest apis exposed to
 * the outside world
 * 
 * @author Arokia Felsi.S
 * @since 2014-10-10
 */

public class CorsComposition {

	@With(CorsAction.class)
	@Target({ ElementType.TYPE, ElementType.METHOD })
	@Retention(RetentionPolicy.RUNTIME)
	public @interface Cors {
		String value() default "*";
	}

	public static class CorsAction extends Action<Cors> {

		/**
		 * This method will be called when an api is called from outside
		 * sources, overriding this method to apply cors filter
		 */
		@Override
		public Promise call(Context context) throws Throwable {
			// Get response from the context
			Response response = context.response();
			// Apply cross orgin for all
			response.setHeader("Access-Control-Allow-Origin",
					configuration.value());

			// Handle preflight requests with options
			if (context.request().method().equals("OPTIONS")) {
				// specify the methods types for which cors should be enabled
				response.setHeader("Access-Control-Allow-Methods",
						"POST, GET, OPTIONS, PUT, DELETE");
				response.setHeader("Access-Control-Max-Age", "3600");
				response.setHeader("Access-Control-Allow-Headers",
						"Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Auth-Token");
				response.setHeader("Access-Control-Allow-Credentials", "true");
				// return successful response
				return delegate.call(context);
			}

			response.setHeader("Access-Control-Allow-Headers",
					"X-Requested-With, Content-Type, X-Auth-Token");
			return delegate.call(context);
		}
	}
}