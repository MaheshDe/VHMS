/**
* Retrieves the username from the HTTP context
* 
* @return null if the user is not authenticated.
* @author karthiga
* @version 1.0
* @since 2015-08-12
*/


package Base.Common.Attribute;

import java.util.Map;

import play.mvc.Http.Context;
import play.mvc.Http.RequestHeader;
import play.mvc.Result;
import play.mvc.Security;
import vHMS.Core.ViewModels.Response;
import Base.Utilities.CommonUtils;
import Base.Utilities.Security.JWT.JWTVerifier;
import Base.Utilities.Security.JWT.Exception.JWTExpiredException;

/**
 * For AuthenticateAttribute is a generic class authenticating the user for each
 * page
 */

public class AuthenticateAttribute extends Security.Authenticator {

	/**
	 * Retrieves the username from the HTTP context
	 * 
	 * @return null if the user is not authenticated.
	 */
	public String getUsername(Context ctx) {
		// whether the session contains the user
		// String username = ctx.session().get("LoggedOnUser");
		// if the username is null returns null otherwise returns username value
		// /String[] authTokenHeaderValues = ctx.request().headers().get("");
		String username = tokenAuthorization(ctx);
		// if (username==null ) return null;
		return username;
	}

	/**
	 * Generates an alternative result if the user is not authenticated
	 */
	public Result onUnauthorized(Context ctx) {
		Response response = new Response();
		response.success = false;
		response.message = "Unauthorized Request : No Token or  Invalid Token";
		response.ViewModel = new String("Unauthorized");

		return play.mvc.Controller.ok(play.libs.Json.toJson(response));
	}

	private static String tokenAuthorization(Context ctx) {
		try {
			if (ctx == null)
				return null;
			if (!CommonUtils.getMessage("JWT.IsAuthToken").equalsIgnoreCase(
					"true"))
				return "";
			String masterAccessKey = ctx.request().getHeader(
					"master-access-token");

			if (masterAccessKey == null) {
				String authToken = ctx.request().getHeader("x-access-token");
				if (authToken == null) {
					authToken = ctx.request().queryString()
							.get("x-access-token")[0].toString();
				}
				JWTVerifier jwtVerifier = new JWTVerifier(
						CommonUtils.decryptor(CommonUtils
								.getMessage("JWT.AuthSecurityKey")));
				Map<String, Object> claims = jwtVerifier.verify(authToken);
				return (String) claims.get("uid");
			} else {
				String decodeKey = CommonUtils.decryptor(CommonUtils
						.getMessage("JWT.MasterAccessKey"));
				if (CommonUtils.decryptor(masterAccessKey).equals(decodeKey))
					return "ExternalUser";
				else
					return null;
			}

		}

		catch (Exception ex) {
			return null;
		}

	}

	public static boolean tokenAuthorization(RequestHeader request)
			throws Exception {
		try {
			if (request == null)
				return true;
			if (!CommonUtils.getMessage("JWT.IsAuthToken").equalsIgnoreCase(
					"true"))
				return true;
			String authToken = request.getHeader("x-access-token");
			JWTVerifier jwtVerifier = new JWTVerifier(
					CommonUtils.decryptor(CommonUtils
							.getMessage("JWT.AuthSecurityKey")));
			jwtVerifier.verify(authToken);
			return true;
		}

		catch (JWTExpiredException expired) {
			expired.printStackTrace();
			throw new Exception(
					CommonUtils.getMessage("JWT.Exception.TokenExpiry"));
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new Exception(
					CommonUtils.getMessage("JWT.Exception.AuthFailed"));
		}
	}
}
