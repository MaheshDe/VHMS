﻿/**
Service to get master data
Getting data from the json
* Author:  JasmitaB
* Created On: 29/07/2015
* Modified For: 
* Modified On:
* Modified By:
*/
'use strict';
reachApp.factory('masterDataService', ['$http', '$q',function ($http, $q) {
    return {
/**
 * This method deals with getting master data for dropdowns
 */
        findAllMasters: function () {
            var deferred = $q.defer(); //promise
            $http({ method: 'GET', url: 'data/dropdown-data.json' }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        },
    
    
    findAllDataMasterByParam: function (param) {        
        var deferred = $q.defer(); //promise
        $http({ method: 'GET', url: param }).
            success(function (data, status, headers, config) {
                deferred.resolve(data.ViewModels);
            }).
            error(function (data, status, headers, config) {
                deferred.reject(status);
            });
        return deferred.promise;
    },

    }
}])