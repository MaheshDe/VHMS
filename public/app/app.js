﻿'use strict';
var reachApp = angular.module('app', ['ngRoute', 'ngResource', 'ngAnimate', 'ngBackstretch', 'ngHolder', 'datatables', 
                                       'datatables.bootstrap', 'ui.calendar','ui.bootstrap', 'ngSanitize',
                                        'ui.select', 'daterangepicker', 'toggle-switch', 'truncate', 'angular-storage', 
                                        'angularFileUpload', 'ngTinyScrollbar', 
                                        'perfect_scrollbar', 'gridster', 'ngDragDrop', 'angularSpectrumColorpicker', 
                                        'colorpicker.module', 'mwl.calendar', 'naif.base64', 'slideMenu', 
                                        'pb.angular.underscore', 'validation.match'])
.config([
  '$routeProvider', '$httpProvider',  function ($routeProvider, $httpProvider, $rootScope, store) {
    
      var routes, setRoutes;
      //Add the http ajax request interceptors
      $httpProvider.interceptors.push('httpAjaxRequestInterceptor');
      routes = ['login', 'landingpage', 'faq', 'branch', 'role', 'dashboard','speciality', 'user', 'company', 'event', 
                'appointment', 'doctor', 'userschedule','appointmentscheduling','bookanappointment'];

      setRoutes = function (route) {
          var config, url;
          url = '/' + route;
          config = {
              templateUrl: 'app/' + route + '/view/' + route + '.html'
          };
          $routeProvider.when(url, config);
          // use the HTML5 History API

          return $routeProvider;

      };

      routes.forEach(function (route) {

          return setRoutes(route);
      });
      return $routeProvider.when('/', {
          redirectTo: '/landingpage'
      }).when('/register', {
          templateUrl: 'app/patientregistration/view/patientregistration.html'
      }).when('/floor', {
          templateUrl: 'app/floor/view/floor.html'
      }).when('/profile', {
          templateUrl: 'app/patientregistration/view/profile.html'  	
      }).when('/editprofile', {
          templateUrl: 'app/patientregistration/view/updatepatient.html'  	
      }).when('/doctortype', {
          templateUrl: 'app/genericmaster/view/master.html', label: 'Doctor Type'
      }).when('/appointmentstatus', {
          templateUrl: 'app/genericmaster/view/master.html', label: 'Appointment Status'
      }).when('/useridentity', {
          templateUrl: 'app/genericmaster/view/master.html', label: 'User Identity'
      }).when('/appointmentconfirmation', {
          templateUrl: 'app/bookanappointment/view/appointmentSuccess.html'
      }).when('/patient/login', {
          templateUrl: 'app/login/view/patientlogin.html'
      }).when('/appointment/search', {
    	  templateUrl: 'app/searchappointment/view/searchappointment.html'
      }).when('/patient/search', {
    	  templateUrl: 'app/searchpatient/view/searchpatientlayout.html'
      }).when('/usertype', {
    	  templateUrl: 'app/genericmaster/view/master.html', label: 'User Type'	  
      }).when('/documenttype', {
    	  templateUrl: 'app/genericmaster/view/master.html', label: 'Document Type'	  
      }).when('/qualification', {
    	  templateUrl: 'app/genericmaster/view/master.html', label: 'Qualification'	  
      }).when('/maritalstatus', {
    	  templateUrl: 'app/genericmaster/view/master.html', label: 'Marital Status'	  
      }).when('/gender', {
    	  templateUrl: 'app/genericmaster/view/master.html', label: 'Gender'	  
      }).when('/roomtype', {
    	  templateUrl: 'app/genericmaster/view/master.html', label: 'RoomType'	  
      }).when('/building', {
    	  templateUrl: 'app/genericmaster/view/master.html', label: 'Building'	  
      }).when('/404', {
          templateUrl: 'app/common/view/pages/404.html'
      }).otherwise({
          redirectTo: '/login'
      });
      
  }]).run(['$rootScope', '$route', '$location', '$window', 'vmuisettings',
           function ($rootScope, $route, $location, $window, vmuisettings) {
      //Interceptor to handle browser navigation (back & forward) and browser refresh
      $rootScope.vmuisettings = vmuisettings
      //$rootScope.$on('$routeChangeStart', function () {
          //alert('refresh');
          //$route.reload();
      //});
      //First store the actual location in $rootScope.actualLocation, then listen to $locationChangeSuccess and when it happens update actualLocation with the new value.
      $rootScope.$on('$locationChangeSuccess', function () {
          $rootScope.actualLocation = $location.path();
      });
      $rootScope.$watch(function ()
      {
          return $location.path()
      },
          function (newLocation, oldLocation) {
          //true only for onPopState
          if ($rootScope.actualLocation === newLocation) {
          //    var back,
          //        historyState = $window.history.state;
          //    back = !!(historyState && historyState.position <= $rootScope.stackPosition);
          //    if (back) {
          //        //back button
          //        $rootScope.stackPosition--;
          //    } else {
          //        //forward button
          //        $rootScope.stackPosition++;
          //    }

          //} else {
          //    //normal-way change of page (via link click)
          //    if ($route.current) {
          //        $window.history.replaceState({
          //            position: $rootScope.stackPosition
          //        });
          //        $rootScope.stackPosition++;
          //    }
          }
      });
  }])
    //User filterMutiple if you need to filter the result based on multiple conditions inside html page in the ng-repeat function
  //Reference for multiple filter istaken from the below site
  //http://stackoverflow.com/questions/15868248/how-to-filter-multiple-values-or-operation-in-angularjs
  .filter('filterMultiple',['$filter',function ($filter) {
  return function (items, keyObj) {
    var filterObj = {
              data:items,
              filteredData:[],
              applyFilter : function(obj,key){
                var fData = [];
                if(this.filteredData.length == 0)
                  this.filteredData = this.data;
                if(obj){
                  var fObj = {};
                  if(!angular.isArray(obj)){
                    fObj[key] = obj;
                    fData = fData.concat($filter('filter')(this.filteredData,fObj));
                  }else if(angular.isArray(obj)){
                    if(obj.length > 0){ 
                      for(var i=0;i<obj.length;i++){
                        if(angular.isDefined(obj[i])){
                          fObj[key] = obj[i];
                          fData = fData.concat($filter('filter')(this.filteredData,fObj));  
                        }
                      }
                      
                    }                   
                  }                 
                  if(fData.length > 0){
                    this.filteredData = fData;
                  }
                }
              }
        };

    if(keyObj){
      angular.forEach(keyObj,function(obj,key){
        filterObj.applyFilter(obj,key);
      });     
    }
    
    return filterObj.filteredData;
  }
}]);