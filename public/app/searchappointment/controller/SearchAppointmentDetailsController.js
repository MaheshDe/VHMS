/* Description: The searchAppointmentDetailsController deals with Configuring The datatable properties, Cancel Appointment ,formatting date.
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:  
* Created On: 
* Modified For: 
* Modified On:
* Modified By:
* 
* */

'use strict';
reachApp.controller('searchAppointmentDetailsController', ['$scope', '$window', '$rootScope', 'store', '$modal', 'underscore','noty', 'ServerSidePaginationService', 'appointmentService', '$filter', function ($scope, $window, $rootScope, store, $modal, _,noty, ServerSidePaginationService, appointmentService ,$filter) {
    var vm = this;
    $scope.attendanceUrl = '/appointment';
    
    
/**
* Subscribe the pagination result  and binding the value to datatable;   
*/ 
    
    $rootScope.$on('pagination_Result',function(event,args){
    	$scope.searchPatient = true; 
    	$rootScope.tblPatientlist_pagesinfo = args;        
    	$rootScope.tblPatientlist_pages = ServerSidePaginationService.paginationServiceParams().ssp_pages;
    	$rootScope.tblPatientlist_pagesinfo = ServerSidePaginationService.paginationServiceParams().paging.ssp_info;    	
    	 });
  
    
/**
* This function is used to cancel the patient appointments.
* 
* */
    
    $rootScope.CancelAppointment = function ()
    
    {
    	
    	var appointment =$rootScope.cancelAppointmentObj;
    	
    	
    	if(appointment !== null)
    	{
    		var request = {};
    		
    		if(appointment.patientCode !== null && appointment.patientCode !== undefined && appointment.patientCode !== '')
    		{
    			request.patientCode = appointment.patientCode;
    		}
    		if(appointment.code !== null && appointment.code !== undefined && appointment.code !== '')
    		{
    			request.code = appointment.code; 
    		}
    		if(appointment.appointmentStatus !== null && appointment.appointmentStatus !== undefined
    				&& appointment.appointmentStatus !== '')
    		{
    			request.appointmentStatus = $rootScope.vmuisettings.appointmentStatusCancelationCode;
    		}
    		
    		 var userInfo = _.find($scope.allUserInfo, function (a) {
                 return a.code == appointment.userCode;
             });
    		
    		var jsonData=appointment.json;
    		jsonData.appointmentStatusCode = $rootScope.vmuisettings.appointmentStatusCancelationCode;
    		jsonData.appointmentStatusDesc = $rootScope.vmuisettings.appointmentStatusCancelationDesc;
    		
    		request.json = jsonData;
    		//appointment.json=request.json;
    		//appointment.appointmentStatus = $rootScope.vmuisettings.appointmentStatusCancelationCode;
    		if(userInfo !== null && userInfo !== undefined){
    			request.userContactNumber=userInfo.mobile;
    		}
    		request.status = true;
    		var code = request.code;
    		appointmentService.updateAppointment(request,code).then(function(res){
    			
    			if(res.viewModel !== null && res.success)
    			{    				    				
    				$scope.cancel();
    				var lists = $scope.tblPatientlist_pages[$scope.tblPatientlist_pagesinfo.currentPage].result;
    			   	var filteredlists = $filter('filter')(lists, function(item) {
    			      return !(item.code == res.ViewModel.viewModel.code);
    			      });
    			   	filteredlists.push(res.ViewModel.viewModel);
    			   	$scope.tblPatientlist_pages[$scope.tblPatientlist_pagesinfo.currentPage].result=[];
    				$scope.tblPatientlist_pages[$scope.tblPatientlist_pagesinfo.currentPage].result = filteredlists;
    			}
    			
        		
        	});
    		
    	
    	}
    	
    	
    }
    
    
/**
*  Configuring the datatable properties for server side pagination ,loads on controller initiate
*/    
        vm.details = [];

    //pagination starts
    //initializing the table 
    $scope.tblPatientlist_pages = ServerSidePaginationService.paginationServiceParams().ssp_pages;
    ServerSidePaginationService.paginationServiceParams().paging.ssp_info.limit = 10;
    var tableoptions = {};
    tableoptions.scrollx = false;
    tableoptions.TableName = 'tblPatientlist';//expects the table id
    var sortableColumns = [];
    ServerSidePaginationService.initializeTable(tableoptions, sortableColumns);
    vm.dtOptions = ServerSidePaginationService.paginationServiceParams().tableParams.dtOptions;
    vm.dtColumnDefs = ServerSidePaginationService.paginationServiceParams().tableParams.dtColumnDefs;
    //pagination ends
    
    
/**
* This function is used to display conformation box to cancel appointment.
* */
    
    $scope.CancelAppt = function (size,apptmt) {
    	
    	$rootScope.cancelAppointmentObj=apptmt;
    	$rootScope.showtimingmsg="";
    	
        var modalInstance = $modal.open({
            templateUrl: 'cancelAppointment.html',
            controller: 'ModalInstanceCtrl',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });
    }
    
 /*$scope.Cancel = function (appointment) {
        $scope.appointment = appointment;
        var message = confirm("Are you sure, you want to cancel the appointment.");
        if (message == true) {
        	 $scope.CancelAppointment(appointment);
        } else {
            return false;
        }
    }*/

    
/**
* This function is used to get all user's(Doctor's) details.
* */
 
	$scope.getAllUser=function()
	{
		appointmentService.findAllUserDetails().then(function (res) {
			if (res != null && res.ViewModels.length > 0) {
            	
              $scope.allUserInfo = res.ViewModels;			
                
            }
		});
	}

	
/**
* This function is used to get all details from Doctor.json.
* */
		$scope.getAllDoctor = function()
		{
			appointmentService.findAllDoctorDetails().then(function (res) {
			if (res != null && res.ViewModels.length > 0) {
			$scope.allDoctorInfo = res.ViewModels;			
				}
			});
		}
					
/**
* This function is used to get all details from userSchedule.json.
* */				
		/*$scope.getUsersSchedule = function()
		{
			appointmentService.findAllDoctorsSchedule().then(function(res){
			if(res != null && res.ViewModels.length > 0){ 
			$scope.allDoctorsScheduleInfo = res.ViewModels;
				}
			});
		}*/
					
	
/***
* Formatting the Datetime based to IST timeZone.
*/
	$scope.formatDate = function(date){

		var istValue=parseFloat("5.5");       
		var dateObj = moment.utc(date).format('DD-MMM-YYYY HH:mm');
		return dateObj;
	}
	
  
/**
* This function is used to compare appointmentStartDateTime and current date.
* */
  
	$scope.date = new Date();
	$scope.compareDate=function(apptStrtDt)
	{
		return  moment(moment(apptStrtDt).format('DD-MMM-YYYY HH:mm:ss')) .isAfter( moment($scope.date).format('DD-MMM-YYYY HH:mm:ss'));
	}
  


/**
* This function is used for view appointment details.
* */

  		$scope.viewAppointmentDetails = function (appointment,mode) {
  		
			$scope.patientCode = appointment.patientCode;
			
			//calling service to get patient all details using patientCode
			appointmentService.viewPatientDetails($scope.patientCode).then(function(res){
				
				if(res.viewModel !== null && res.success)
				{ 
						
						$scope.userCode = appointment.userCode;
					
						//getting user designation using userCode
						$scope.doctorDesignation = _.findWhere($scope.allDoctorInfo, {userCode:$scope.userCode});
						//getting doctor schedule using userCode
						//$scope.doctorSchedule = _.findWhere($scope.allDoctorsScheduleInfo, {userCode:$scope.userCode});
						
						
						//setting appointment details to rootScope object
						$rootScope.appointmentId = appointment.code;
						$rootScope.AppointSuccessAppointmentDate = moment(appointment.appointmentStartDateTime).format('DD-MMM-YYYY');
						$rootScope.AppointSuccessDocDesignation = $scope.doctorDesignation.designation;
						$rootScope.AppointSuccessDocName = appointment.json.doctorName;
						$rootScope.AppointSuccessPreferredTime = moment(appointment.appointmentStartDateTime).format('HH:mm') + " - " + moment(appointment.appointmentEndDateTime).format('HH:mm'); 
						$rootScope.AppointSuccessDocSpeciality = appointment.json.specialityName;
						
						//setting patient details to rootScope
						$rootScope.PatientFirstName = res.ViewModel.firstName;
						$rootScope.PatientMiddleName = res.ViewModel.middleName;
						$rootScope.PatientLastName =  res.ViewModel.lastName;
						$rootScope.PatientEmail = res.ViewModel.email;
						$rootScope.PatientMobileNo = res.ViewModel.contactNumber;
						$rootScope.uhId = res.ViewModel.uhId;
						
						$window.location.href = '#/appointmentconfirmation';
				}
			});
			
}

				
				
/** Fetch all the users when searchAppointmentController loads
 * 
 */				
$scope.getAllUser();

$scope.getAllDoctor();

//$scope.getUsersSchedule();

    
}]);