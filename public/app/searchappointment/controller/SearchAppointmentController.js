/**
 *  Description: The searchAppointmentController deals with search  patient details..etc for an object.
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:  
* Created On: 
* Modified For: 
* Modified On:
* Modified By:
* 
* */

'use strict';
reachApp
		.controller(
				'searchAppointmentController', ['appointmentService', '$scope', '$window', '$timeout','store',
				        						'$rootScope', '$modal','noty','ServerSidePaginationService' ,'underscore',
				function(appointmentService, $scope, $window, $timeout,store,
						$rootScope, $modal,noty,ServerSidePaginationService,_)
						{ 
							$scope.details={};
							$scope.searchPatient = false; 
							var loggedonuserinfo = store.get('loggedonuser');
							if(loggedonuserinfo !== null)
							{
								$scope.details.Patientcode = loggedonuserinfo.code;
							}
							   
							/**
							 * This function is used to clear date range.
						     * 
						     * */
							 $scope.ClearDateRange=function()    {
								 document.getElementById("txtDate_range").value="";
								 $scope.details.Range=null;
							 }
							 
							 /**
							  * This function is used to clear the all search details.
							     * 
							     * */
							 $scope.clear = function(){
								 if($scope.details!=undefined && $scope.details!=null )
								 {
									 $scope.details.Range="";
									 $scope.details.Specialistname="";
									 //clearing speciality dropdown
									 $rootScope.$broadcast('clear_dropdown', 'Speciality_login');
									 document.getElementById("txtDate_range").value="";
								 }
							 }

							 /**This function is used to construct request and call the submit the request and bind the result using serverSide Pagination Directive.
						     * 
						     * */
							$scope.SearchPatientClicked = function(details) 
							{debugger
								var request = {};
								$scope.request = details;
								request.status=true;
								if(details !== null && details !== undefined)
									{
										if(details.Patientcode !== null && details.Patientcode !== undefined && details.Patientcode !== "")
											{
												request.patientCode = details.Patientcode;
											}
										if(details.Specialistname !== null && details.Specialistname !== undefined && details.Specialistname !== "")
											{
											 var userCodes =[];       
										       var result =  _.each($scope.allUserInfo, function (obj){  
								                	 if(obj.name.toUpperCase().indexOf(details.Specialistname.toUpperCase()) > -1){
								                		 userCodes.push(obj.code);
								                	 }
								                });
											
										      if(userCodes == null || userCodes.length == 0 || typeof userCodes == "undefined"){
										    	  userCodes.push("0000");
										      }
										      request.userCodes=userCodes;
											}
										if(details.Speciality !== null && details.Speciality !== undefined && details.Speciality !== "")
										{
										     request.specialityCode=details.Speciality;
										}										
										if(details.Range !== null && details.Range !== undefined && details.Range !== "")
											{
												var sDate=details.Range.startDate;
												var eDate=details.Range.endDate;
												if(sDate !== null && eDate!=null && sDate !== "" && eDate!== "")
												{
													request.appointmentStartDateTime=moment(sDate.toString()).format("YYYY-MM-DD HH:mm:ss");
													request.appointmentEndDateTime=moment(eDate.toString()).format("YYYY-MM-DD HH:mm:ss");
												}
											}
									}
									var jsonData =  JSON.stringify({'request':request});
									var params = {};
									params.Url = '/appointment';
									params.data=jsonData;
									$rootScope.tblPatientlist_pagesinfo={};
									params.paginationServiceParams=$scope.tblPatientlist_pagesinfo; 
									ServerSidePaginationService.attachServerSidePagination(params);
								}
							
			/**
			 * This function is used to get all user's(Doctor's) details.
			 * */
							$scope.getAllUser=function()
							{
								appointmentService.findAllUserDetails().then(function (res) {
									if (res != null && res.ViewModels.length > 0) {
						              $scope.allUserInfo = res.ViewModels;			
						            }
								});
							}
							
			/** Fetch all the users when searchAppointmentController loads
			 * 
			 */				
			$scope.getAllUser();

						}]);