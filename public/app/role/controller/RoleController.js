/**
* Description: The RoleController deals with view,edit the Roles.
* Created On:  
* Modified For: 
* Modified On:
* Modified By:
*/'use strict';
reachApp.controller('rolecontroller', ['roleservice', '$scope', '$rootScope', '$modal', 'noty', function (roleservice, $scope,  $rootScope, $modal,noty) {
	
    // to view the details onclick of view icon //
    $scope.RoleViewClicked = false;
    $scope.$parent.$on('onDetailsViewClicked', function (event, args) {debugger
        $scope.RoleViewClicked = true;
    	$scope.name = args.name;
    	$scope.code = args.code;
    	var userTypeCode = args.userType;
    	var request = {};
    	
    	roleservice.getUserType(userTypeCode).then(function (res)
    	{
    			if(res.success){
    				$scope.userType = res.ViewModel.name;
    			}
    });
        if (args.status == "1") {
            $scope.status = "Enabled";
        }
        else {
            $scope.status = "Disabled";
        }
        $rootScope.vmuisettings.singlemasterviewclicked = true;
        
    });
 // to view the details onclick of edit icon //
    $scope.$parent.$$listeners.onDetailsEditClicked = [];

    $scope.$parent.$on('onDetailsEditClicked', function (event, args) {
        $scope.openRoleModal('sm', args, args.ActionDone);
    });
    
    // to show the title of the modal as Add Role//
    $scope.openRoleModal = function (obj, args) {debugger
    	$scope.RoleViewClicked = false;
        if (args == null) {
            $rootScope.roleModelTitle = {};
            $rootScope.roleModelTitle = "Add Role";
            $rootScope.RoleDetails = {};
          //  $rootScope.RoleDetails.switchStatus = true;
            $rootScope.RoleDetails.id = "0";
            $rootScope.AddRole = true;
        }// to show the title of the modal as Edit Role//
        else {
            $rootScope.roleModelTitle = args;
            $rootScope.roleModelTitle = "Edit Role";
            args.RoleName = args.name;
            args.UserType = args.userType;
            if (args.status == "1") {
                args.switchStatus = true;
            }
            else {
                args.switchStatus = false;
            }
            $rootScope.RoleDetails = args;
            $rootScope.AddRole = false;
        }// to open the modal //
        var modalInstance = $modal.open({
            templateUrl: 'RoleModalContent.html',
            controller: 'ModalInstanceCtrl',
            backdrop: 'static',
            size: 'md',
            resolve: {
                items: function () {
                    return $rootScope.RoleDetails;
                }
            }
        });
    };

    $scope.SaveRole = function (obj) {debugger
        if ($scope.RoleForm.$valid) {
            var request = {};
            request.name = obj.RoleName == undefined ? "" : obj.RoleName;
            request.status = obj.switchStatus == undefined ? "" : obj.switchStatus;
            request.userType = obj.UserType == undefined ? "" : obj.UserType;
            if (request.status == true) {
                request.status = "1";
            }
            else {
                request.status = "0";
            }

            if (obj.id == "0") {
                var id = 0;
                var MethodType = "POST";
            }
            else {
                var id = obj.code;
                var MethodType = "PUT";
                request.code = obj.code;
            }
            
            // to rebind the table when the edit/create was done. //
            roleservice.SaveRole(request, id, MethodType).then(function (res) {debugger
                if (res.success) {
                    $rootScope.$broadcast('RebindRouteTable_role');
                    $scope.cancel();
                    console.log("role save success"+ res.message);
                    noty.add({ type: 'success', body: res.message });
                }
                // To show an error noty,if there is any error. //
                else {
                    $scope.cancel();
                    console.log("role save error"+ res.message);
                    noty.add({ type: 'danger', body: res.message });
                }
            });
        }
    }
} ]);
