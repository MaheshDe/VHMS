/**
Service for creating a Role master and to edit it.
* Author: Sabariraja M  
* Created On: 
* Modified For: 
* Modified On:
* Modified By:
*/
'use strict'; 
reachApp.factory('roleservice', ['$window', '$http', '$q', function ($window, $http, $q) {
    return {
        SaveRole: function (req, id, MethodType) {debugger 
            var deferred = $q.defer(); //promise
            //create JSON
            var URL = '/role/' + id;

            var jsonData = JSON.stringify({ 'request': req });
            $http({ method: MethodType, url: URL, data: jsonData, headers: { 'contentType': 'application/json' } }).
                success(function (data, status, headers) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers) {
                    deferred.reject(status);
                });
            return deferred.promise;
        },
        getUserType:  function (userTypeCode) {debugger 
        	 var deferred = $q.defer(); //promise
        $http({ method: 'GET', url: '/usertype/'+userTypeCode}).
            success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).
            error(function (data, status, headers, config) {
                deferred.reject(status);
            });
        return deferred.promise;
    }
    }
}])