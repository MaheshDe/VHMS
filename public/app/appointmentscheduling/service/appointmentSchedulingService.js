/**
*  Description: The bookAppointmentService deals with base operation like creating ,Updating and fetching the data related Patient Creation And Booking appointment.
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:  JasmitaB
* Created On: 14/08/2015
* Modified For: 
* Modified On:
* Modified By:
* 
* */


/**
Service for bookAppointment page
getting user info from the json and returns to login controller
*/
'use strict';
reachApp.factory('appointmentschedulingService', ['$http', '$q',function ($http, $q) {
    return {
       
    	
        /**
         * Find all Speciality
         */
        findAllSpeciality: function () { 
            var deferred = $q.defer(); //promise
            $http({ method: 'GET', url: 'data/Speciality.json' }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;

        }
    }
    
}]);