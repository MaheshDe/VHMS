'use strict';
reachApp.controller('appointmentschedulingcontroller', ['$scope', '$compile', 'uiCalendarConfig','$rootScope', '$window', function ($scope, $compile, uiCalendarConfig,$rootScope, $window) {

    $scope.SelectedDateFormatted = moment(new Date()).format("dddd, MMMM Do");
    $scope.SelectedDate = moment(new Date());
    $scope.alertMessage = "";
    $scope.EventName = "";
    $scope.SelectedTime = new Date();

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $scope.appointmentSchedulePage = true;
    $scope.addPatientDetailsPage =false;
    
    $rootScope.$on('bookappointment_doctor_detail', function (event ,args) {
   	 console.log("bookappointment_doctor_detail");
       console.log(event);
       console.log(args);  
       
       var docobj = {}
       
       docobj.designation = args.designation;
       docobj.doctorName = args.name; 
       docobj.description = args.description;
       docobj.endTime = args.endTime;
       docobj.startTime = args.startTime;
       docobj.experience = args.experience;
       docobj.profilePictureUrl = args.profilePictureUrl;
       $rootScope.docObject = docobj;
       console.log($rootScope.docObject);
    });
    
    /* config object */
    $scope.uiConfig = {
        calendar: {
            height: 450,
            //Selectable should be true so that current Selection will be hightlighted
            selectable: true,
            //By Default unselectAuto will be true, by setting it value to false on click of outside the current selection will not be cleared.
            unselectAuto: false,
            editable: true,
            //If weekends is false then calender will hide Saturdays and Sundays
            weekends: true,
            //Available weekMode is 'fixed','liquid','variable'
            weekMode: 'fixed',
            //header: {
            //    left: 'month basicWeek basicDay agendaWeek agendaDay',
            //    center: 'title',
            //    right: 'today prev,next'
            //},
            header: {
                left: 'title',
                center: '',
                right: 'today prev,next'
            },
            // events: $scope.events,
            //On dayclick, bind Clientdetails
            dayClick: function (date, allDay, jsEvent, view) {
                $scope.SelectedDateFormatted = moment(date).format("dddd, MMMM Do");
                $scope.SelectedDate = moment(date);
            },
            //event click
            eventClick: function (date, jsEvent, view) {
                alert(date.title + ' was clicked ');
            },
            eventRender: function (event, element, view) {
                element.attr({
                    'tooltip': event.title,
                    'tooltip-append-to-body': true
                });
                $compile(element)($scope);
            }
        }
    };

    //event source that pulls from google.com
    $scope.eventSource = [];
    //    {
    //    url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
    //    className: 'gcal-event',           // an option!
    //    currentTimezone: 'America/Chicago' // an option!
    //};
    //you can add the events in following ways
    $scope.events = [
      { title: 'All Day Event', start: new Date(y, m, 1) },
      { title: 'Long Event', start: new Date(y, m, d - 5), end: new Date(y, m, d - 2) },
      { id: 999, title: 'Repeating Event', start: new Date(y, m, d - 3, 16, 0), allDay: false },
      { id: 999, title: 'Repeating Event', start: new Date(y, m, d + 4, 16, 0), allDay: false },
      { title: 'Birthday Party', start: new Date(y, m, d + 1, 19, 0), end: new Date(y, m, d + 1, 22, 30), allDay: false },      
      { title: 'Click for Google', start: new Date(y, m, 28), end: new Date(y, m, 29), url: 'http://google.com/' }
    ];
    /* event source that calls a function on every view switch */
    $scope.eventsF = function (start, end, timezone, callback) {
        var s = new Date(start).getTime() / 1000;
        var e = new Date(end).getTime() / 1000;
        var m = new Date(start).getMonth();
        var events = [{ title: 'Feed Me ' + m, start: s + (50000), end: s + (100000), allDay: false, className: ['customFeed'] }];
        callback(events);
    };


    ///* event sources array*/
    $scope.eventSources = [$scope.events, $scope.eventSource, $scope.eventsF];

    $scope.appointmentSchedulingPrevious = function()
    {
    	$window.location.href = '#/bookanappointment';
    }
    
    $scope.previousPage = function(){
    	  $scope.appointmentSchedulePage = true;
    	    $scope.addPatientDetailsPage =false;
    }
    
    $scope.CreateAppointment = function () {
    	
    	$scope.appointmentSchedulePage = false;
    	$scope.addPatientDetailsPage = true;
    	
        /*var date1 = new Date($scope.SelectedDate);
        var d1 = date1.getDate();
        var m1 = date1.getMonth();
        var y1 = date1.getFullYear();
        var hours = new Date($scope.SelectedTime).getHours();
        var minutes = new Date($scope.SelectedTime).getMinutes();
        //$scope.SelectedTime
        //this code will add new event and remove the event present on index
        //you can call it explicitly in any method
        $scope.events.push({
            title: $scope.EventName,
            start: new Date(y1, m1, d1, hours, minutes),
            end: new Date(y1, m1, d1, hours, minutes),
            className: ['newtask']
        });

        $scope.events.splice(index, 1);*/
    };
}]);