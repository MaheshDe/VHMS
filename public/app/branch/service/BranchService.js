/*BranchService  is used for creating a branch master and to edit it.
* Author: Sabariraja M  
* Created On: 03.09.2015
* Modified For: 
* Modified On:
* Modified By:
*/
'use strict'; 
reachApp.factory('branchservice', ['$window', '$http', '$q', function ($window, $http, $q) {
    return {
    	SaveBranch: function (req, id, MethodType) {debugger 
            var deferred = $q.defer(); //promise
            //create JSON
            var URL = '/branch/' + id;

            var jsonData = JSON.stringify({ 'request': req });
            $http({ method: MethodType, url: URL, data: jsonData, headers: { 'contentType': 'application/json' } }).
                success(function (data, status, headers) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers) {
                    deferred.reject(status);
                });
            return deferred.promise;
        }
       
    }
}])