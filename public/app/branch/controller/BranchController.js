/**
* Description: The BranchController deals with view,edit the branch.
* Created On:  03.09.2015
* Created By: Sabariraja M
* Modified For: 
* Modified On:
* Modified By:
*/'use strict';
reachApp.controller('branchcontroller', ['branchservice', '$scope', '$rootScope', '$modal', 'noty', function (branchservice, $scope,  $rootScope, $modal,noty) {
	
    // to view the details onclick of view icon //
    $scope.BranchViewClicked = false;
    $scope.$parent.$on('onDetailsViewClicked', function (event, args) {debugger
        $scope.BranchViewClicked = true;
    	$scope.name = args.name;
    	$scope.code = args.code;
    	
    	var address ={};
    	address = JSON.parse(args.address);
    
    	$scope.addressDetails = address.StreetLine1 + ',' + address.StreetLine2 + ',' + address.City + ',' + address.State + ',' + address.PostalCode + ',' + address.Country;
    	
    	//$scope.address = args.address;
        if (args.status == "1") {
            $scope.status = "Enabled";
        }
        else {
            $scope.status = "Disabled";
        }
        $rootScope.vmuisettings.singlemasterviewclicked = true;
        
    });
 // to view the details onclick of edit icon //
    $scope.$parent.$$listeners.onDetailsEditClicked = [];

    $scope.$parent.$on('onDetailsEditClicked', function (event, args) {
        $scope.openBranchModal('sm', args, args.ActionDone);
    });
    
    // to show the title of the modal as Add Branch//
    $scope.openBranchModal = function (obj, args) {
    	$scope.BranchViewClicked = false;
        if (args == null) {
            $rootScope.branchModelTitle = {};
            $rootScope.branchModelTitle = "Add Branch";
            $rootScope.BranchDetails = {};
          //  $rootScope.BranchDetails.switchStatus = true;
            $rootScope.BranchDetails.id = "0";
            $rootScope.AddBranch = true;
            $scope.$on('addressDirective_Loaded_CommunicationAddress',function(){
            	debugger;
            	$rootScope.$broadcast('ClearAddress_CommunicationAddress', null);
            });
        }// to show the title of the modal as Edit Branch//
        else {debugger
            $rootScope.branchModelTitle = args;
            $rootScope.branchModelTitle = "Edit Branch";
            args.BranchName = args.name;
            
            args.AddressLine1 = JSON.parse(args.address).StreetLine1;
            args.AddressLine2 = JSON.parse(args.address).StreetLine2;
            args.Country = JSON.parse(args.address).Country;
            args.State = JSON.parse(args.address).State;           
            args.City = JSON.parse(args.address).City;
            args.PostalCode = JSON.parse(args.address).PostalCode;
            debugger;
            $scope.$on('addressDirective_Loaded_CommunicationAddress',function(){
            	debugger;
            	$rootScope.$broadcast('GetAddressDetails_CommunicationAddress', args.address);
            });
            //$rootScope.$broadcast('AddressDetails_CommunicationAddress', args);
            
            if (args.status == "1") {
                args.switchStatus = true;
            }
            else {
                args.switchStatus = false;
            }
            //$rootScope.CommunicationAddress.AddressLine1 = JSON.parse(args.address).StreetLine1;
            //angular.element("#txtAddressLine1_CommunicationAddress").val(JSON.parse(args.address).StreetLine1);
            $rootScope.BranchDetails = args;
            $rootScope.AddBranch = false;
        }// to open the modal //
        var modalInstance = $modal.open({
            templateUrl: 'BranchModalContent.html',
            controller: 'ModalInstanceCtrl',
            backdrop: 'static',
            size: 'md',
            resolve: {
                items: function () {
                    return $rootScope.BranchDetails;
                }
            }
        });
    };

    $scope.SaveBranch = function (obj) {debugger
        if ($scope.BranchForm.$valid) {
            var request = {};
            request.name = obj.BranchName == undefined ? "" : obj.BranchName;
            request.status = obj.switchStatus == undefined ? "" : obj.switchStatus;
            
            var address = {};
            //request=RegistrationDetails;
            address.StreetLine1 = $scope.BranchForm.txtAddressLine1_CommunicationAddress.$viewValue;
            address.StreetLine2 = $scope.BranchForm.txtAddressLine2_CommunicationAddress.$viewValue;
            address.Country = $scope.BranchForm.cboCountry_CommunicationAddress.$viewValue.code;
            address.State = $scope.BranchForm.cboState_CommunicationAddress.$viewValue.code;
            address.City = $scope.BranchForm.txtCity_CommunicationAddress.$viewValue;
            address.PostalCode = $scope.BranchForm.txtZip_CommunicationAddress.$viewValue;
            
            request.address = JSON.stringify(address);
            
            if (request.status == true) {
                request.status = "1";
            }
            else {
                request.status = "0";
            }

            if (obj.id == "0") {
                var id = 0;
                var MethodType = "POST";
            }
            else {
                var id = obj.code;
                var MethodType = "PUT";
                request.code = obj.code;
            }
            
            // to rebind the table when the edit/create was done. //
            branchservice.SaveBranch(request, id, MethodType).then(function (res) {debugger
                if (res.success) {
                    $rootScope.$broadcast('RebindRouteTable_branch');
                    $scope.cancel();
                    console.log("branch save success"+ res.message);
                    noty.add({ type: 'success', body: res.message });
                }
                // To show an error noty,if there is any error. //
                else {
                    $scope.cancel();
                    console.log("branch save error"+ res.message);
                    noty.add({ type: 'danger', body: res.message });
                }
            });
        }
    }
} ]);
