﻿/* Description: The appointmentgridcontroller deals with all the basic operation like save,update,search..etc for an object.
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:  JasmitaB
* Created On: 29/07/2015
* Modified For: 
* Modified On:
* Modified By:
* */


'use strict';
reachApp.controller('appointmentgridcontroller', ['$scope', '$rootScope', function ($scope, $rootScope) {
    
    $scope.showAppointmentDetails = function (obj) {
        $rootScope.$broadcast('ShowAppointmentEvent', obj);
    };
    
}]);
