﻿/* Description: The appointmentcontroller deals with all the basic operation like save,update,search..etc for an object.
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:   
* Created On:  
* Modified For: 
* Modified On:
* Modified By:
* */


'use strict';
reachApp.controller('appointmentcontroller', ['$scope', '$rootScope', function ($scope, $rootScope) {

    $scope.DoctorsAvailable = false;
    $scope.DoctorsSchedule = false;
    $scope.AppointmentDetails = {};
    $scope.AppointmentDetails.Speciality = "";
    angular.element("#btnSubmit").removeClass('ng-hide');

    $scope.nextclick = function (obj) {
        if ($scope.AppointmentForm.$valid) {
        $scope.DoctorsAvailable = true;
        angular.element("#btnSubmit").addClass('ng-hide');
        $rootScope.BookAppointmentDetails = obj;
        }
    };

    $rootScope.$on('ShowAppointmentEvent', function (i, obj) {
        $scope.DoctorsSchedule = true;
        $rootScope.DoctorName = obj.name;
        //$rootScope.$broadcast('BindDoctorDetails', obj);
    });

    /*date Picker Starts*/
    $scope.daterange = { startDate: null, endDate: null };
    $scope.openDatepicker = function ($event) {


        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate', 'dd-MMM-yyyy'];
    $scope.format = $scope.formats[4];
    /*date Picker Ends*/

    
}]);
