﻿/* Description: The Master Controller deals with all the basic operations like
 *  create, update, search etc. for a Master. 
* Author: Jasmitha    
* Created On: 10/08/2015  
* Modified For: 
* Modified On:
* Modified By:
* */
'use strict';
reachApp.controller('mastercontroller', ['masterservice', '$scope', '$rootScope', '$modal', '$log', '$window', 'noty', '$filter', function (masterservice, $scope, $rootScope, $modal, $log, $window, noty, $filter) {
    console.log("--> mastercontroller")
    
    $rootScope.noty = noty;
    $scope.ViewClicked = false;
    $scope.$parent.$on('onDetailsViewClicked', function (event, args) {
        $scope.ViewClicked = true;
        $scope.details = args;
        if (args.status == "1") {
            $scope.status = "Enabled";
        }
        else {
            $scope.status = "Disabled";
        }
        $rootScope.vmuisettings.singlemasterviewclicked = true;
    });
    //get the routes for the master from location origin
    var ViewPath = ($window.location.href).split('/');
    //assign the routes to scope variable
    $scope.ViewName = ViewPath[ViewPath.length - 1];
    //for each routes check and assign a corresponding title
    if ($scope.ViewName == $rootScope.vmuisettings.doctortype) {
        $scope.TitleName = $rootScope.vmuisettings.doctortypeLabel;
    }
    else if ($scope.ViewName == $rootScope.vmuisettings.useridentity) {
        $scope.TitleName = $rootScope.vmuisettings.useridentityLabel;
    }
    else if ($scope.ViewName == $rootScope.vmuisettings.usertype) {
        $scope.TitleName = $rootScope.vmuisettings.usertypeLabel;
    }
    else if ($scope.ViewName == $rootScope.vmuisettings.appointmentstatus) {
        $scope.TitleName = $rootScope.vmuisettings.appointmentstatusLabel;
    }
    else if ($scope.ViewName == $rootScope.vmuisettings.documenttype) {
        $scope.TitleName = $rootScope.vmuisettings.documenttypeLabel;
    }
    else if ($scope.ViewName == $rootScope.vmuisettings.maritalstatus) {
        $scope.TitleName = $rootScope.vmuisettings.maritalstatusLabel;
    }
    else {
        $scope.TitleName = ($scope.ViewName).substring(0, 1).toUpperCase() + ($scope.ViewName).substring(1);
    }
    //Get the sourceUrl for the respective Master
    $scope.SourceURL = '/' + ViewPath[ViewPath.length - 1];
  //  $scope.SourceURL = '/data/' + ViewPath[ViewPath.length - 1] + '.json';
       
    $scope.$parent.$$listeners.onDetailsEditClicked = [];

    $scope.$parent.$on('onDetailsEditClicked', function (event, args) {
        $scope.openMasterModal('sm', args, args.ActionDone);
    });
   //To save the Master data
    $scope.SaveData = function (obj) {
    	
    	//Check the form validation
        if ($scope.MasterForm.$valid) {
        	//form request in case of creating a new Master Data
            var request = {};
            request.name = obj.MasterName == undefined ? "" : obj.MasterName;
            request.status = obj.switchStatus == undefined ? false : obj.switchStatus;
            request.fkCompanyCode = $rootScope.vmuisettings.fkCompanyCode;
            // Check the condition for Create or Update
            if (obj.code == null || obj.code == undefined || obj.code == "") {
            	// if its a create operation
                var URL = $scope.SourceURL + '/0';
                var MethodType = "POST";
                request.MethodType = "POST";
            }
            else {
            	// if its a Update operation
                var URL = $scope.SourceURL + '/' + obj.code;
                var MethodType = "PUT";
                //request.Id = obj.Id;
                //request.MethodType = "PUT";
            }
            // Call service to save/update the data to DB
            masterservice.SaveData(request, URL, MethodType).then(function (res) {
            	//if response is success
                if (res.success && res.ViewModel !== null) {
                	//close modal after it saves successfully
                    $scope.cancel();
                    // Show a success noty
                    noty.add({ type: 'success', body: res.message });
                    // after sucess bind the List of master data to table
                    $rootScope.$$listeners['bindingLookupTable_Success_' + $scope.ViewName] = [];
                    $rootScope.$on('bindingLookupTable_Success_' + $scope.ViewName, function (event, args) {
                        if ($rootScope.vmuisettings.singlemasterviewclicked) {
                            var JsonResult = res.ViewModel;
                            var myObject = $filter('filter')(args, { code: JsonResult.code });
                            if (myObject.length > 0) {
                              //  $rootScope.$broadcast('onDetailsViewClicked', myObject[0]);
                            }
                        }
                    });
                    $rootScope.$broadcast('RebindRouteTable_' + $scope.ViewName);
                }else{
                	//if the result fails
                	$scope.cancel();
                	//Show a failure noty message
                	noty.add({ type: 'danger', body: res.message });
                }
            });
        }
    }
   
    // Show popup in case of Create/Edit
    $scope.openMasterModal = function (size, res, action) {
    	$scope.ViewClicked = false;
        //$rootScope.$$listeners.onMasterDetailsEditClicked = [];
    	//Check the action to be performed
        if (action != undefined && action != 'add') {
        	//if its an Edit Event
        	//Bind Modal title
            $rootScope.MasterModelTitle = "Edit" + " " +$scope.TitleName;
            if (res.status == true) {
                res.switchStatus = true;
            }
            else {
                res.switchStatus = false;
            }
            res.MasterName = res.name;
            $rootScope.MasterDetails = res;
        }
        else if (action == 'add') {
        	//if its an Add Event
        	//Bind Modal title
            $rootScope.MasterModelTitle = "Add" + " " + $scope.TitleName;
            $rootScope.MasterDetails = {};
            $rootScope.MasterDetails.Id = "0";
        }
        //Show Modal Popup
        var modalInstance = $modal.open({
            templateUrl: 'myMasterModalContent.html',
            controller: 'ModalInstanceCtrl',
            size: size,
            backdrop: 'static',
            resolve: {
                items: function () {
                    if (action != undefined) {
                        return $rootScope.MasterDetails;
                    }
                }
            }
        });
        modalInstance.result.then(function (res) {
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

} ]);