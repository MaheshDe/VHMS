﻿/**
* Description: The mastercontroller deals with all the basic operation like save,update,search..etc for an object.
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:   
* Created On:  
* Modified For: 
* Modified On:
* Modified By:
* */

'use strict';
reachApp.controller('mastercontroller', ['masterservice', '$scope', '$rootScope', '$modal', '$log', '$window', 'noty', '$filter', function (masterservice, $scope, $rootScope, $modal, $log, $window, noty, $filter) {
    console.log("--> mastercontroller")
    $rootScope.noty = noty;
    $scope.ViewClicked = false;
    $scope.$parent.$on('onDetailsViewClicked', function (event, args) {
        $scope.ViewClicked = true;
        $scope.details = args;
        if (args.Status == "1") {
            $scope.Status = "Enabled";
        }
        else {
            $scope.Status = "Disabled";
        }
        $rootScope.vmuisettings.singlemasterviewclicked = true;
    });
    var ViewPath = ($window.location.href).split('/');
    $scope.ViewName = ViewPath[ViewPath.length - 1];
    if ($scope.ViewName == "doctortype") {
        $scope.TitleName = "Doctor Type"
    }
    else if ($scope.ViewName == "usersidentity") {
        $scope.TitleName = "Users Identity"
    }
    else if ($scope.ViewName == "usertype") {
        $scope.TitleName = "User Type"
    }
    else if ($scope.ViewName == "appointmentstatus") {
        $scope.TitleName = "Appointment Status"
    }
    else {
        $scope.TitleName = ($scope.ViewName).substring(0, 1).toUpperCase() + ($scope.ViewName).substring(1);
    }
    //$scope.SourceURL = '/' + ViewPath[ViewPath.length - 1];
    $scope.SourceURL = '/data/' + ViewPath[ViewPath.length - 1] + '.json';
    
    $scope.$parent.$$listeners.onDetailsEditClicked = [];

    $scope.$parent.$on('onDetailsEditClicked', function (event, args) {
        $scope.openMasterModal('sm', args, args.ActionDone);
    });
    
    
    //This method is used to save the data for masters

    $scope.SaveData = function (obj) {
        if ($scope.MasterForm.$valid) {
            var request = {};
            request.Name = obj.MasterName == undefined ? "" : obj.MasterName;
            request.Status = obj.switchStatus == undefined ? false : obj.switchStatus;
            request.UserID = 1;
            if (obj.Id == "0") {
                var URL = $scope.SourceURL + '/0';
                var MethodType = "POST";
                request.MethodType = "POST";
            }
            else {
                var URL = $scope.SourceURL + '/' + obj.Id;
                var MethodType = "PUT";
                request.Id = obj.Id;
                request.MethodType = "PUT";
            }

            masterservice.SaveData(request, URL, MethodType).then(function (res) {
                if (res.aaData.Success) {
                    $scope.cancel();
                    noty.add({ type: 'success', body: res.aaData.Message });
                    $rootScope.$on('bindingLookupTable_Success_' + $scope.ViewName, function (event, args) {
                        if ($rootScope.vmuisettings.singlemasterviewclicked) {
                            var JsonResult = res.aaData.ViewModel;
                            var myObject = $filter('filter')(args, { Id: JsonResult.Id });
                            if (myObject.length > 0) {
                                $rootScope.$broadcast('onDetailsViewClicked', myObject[0]);
                            }
                        }
                    });
                    $rootScope.$broadcast('RebindRouteTable_' + $scope.ViewName);
                }
            });
        }
    }
/**
 * This methos is used to Open the modal for Masters
 */
    $scope.openMasterModal = function (size, res, action) {
        //$rootScope.$$listeners.onMasterDetailsEditClicked = [];
        if (action != undefined && action != 'add') {
            $rootScope.MasterModelTitle = "Edit";
            if (res.status == true) {
                res.switchStatus = true;
            }
            else {
                res.switchStatus = false;
            }
            res.MasterName = res.name;
            $rootScope.MasterDetails = res;
        }
        else if (action == 'add') {
            $rootScope.MasterModelTitle = "Add";
            $rootScope.MasterDetails = {};
            $rootScope.MasterDetails.Id = "0";
        }
        var modalInstance = $modal.open({
            templateUrl: 'myMasterModalContent.html',
            controller: 'ModalInstanceCtrl',
            size: size,
            backdrop: 'static',
            resolve: {
                items: function () {
                    if (action != undefined) {
                        return $rootScope.MasterDetails;
                    }
                }
            }
        });
        modalInstance.result.then(function (res) {
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

} ]);