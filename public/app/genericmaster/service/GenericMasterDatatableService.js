﻿/**
* Description: The genericmasterdatatableservice deals with all the basic operation like save,update,search..etc for an object.
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:   
* Created On:  
* Modified For: 
* Modified On:
* Modified By:
* */


'use strict';
reachApp.factory('genericmasterdatatableservice', ['$window', '$http', '$q',function ($window, $http, $q) {
    return {
    	
    	/**
    	 * This method is used to fetch list of  the data for dropdown
    	 */
        findAll: function () {
            //'data/' + dataFileName[dataFileName.length-1] + '.js'
            var dataFileName = ($window.location.href).split('/');
            var deferred = $q.defer(); //promise
            $http({ method: 'GET', url: 'data/dropdown-data.json' }).
                success(function (data, status, headers, config) {
                    //console.log(data)
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        }
    }
}])