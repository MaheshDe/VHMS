﻿/**
* Description: The masterservice deals with all the basic operation like save,update,search..etc for an object.
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:   
* Created On:  
* Modified For: 
* Modified On:
* Modified By:
* */
'use strict';
reachApp.factory('masterservice', ['$window', '$http', '$q',function ($window, $http, $q) {
    return {
    	
    	/**
    	 * This is  generic method  used to make http call.  
    	 */
        SaveData: function (req, URL, MethodType) {
            var deferred = $q.defer(); //promise
            var jsonData = JSON.stringify({ 'request': req });
            $http({ method: MethodType, url: URL, data: jsonData, headers: { 'contentType': 'application/json'} }).
                success(function (data, status, headers) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers) {
                    deferred.reject(status);
                });
            return deferred.promise;
        }
    }
}])