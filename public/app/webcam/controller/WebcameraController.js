﻿'use strict';
reachApp.controller('webcameracontroller', ['webcameraservice', '$scope','$rootScope', function (webcameraservice, $scope, $rootScope) {
    //For webcam picture - Starts
    $scope.camera_Live = true;
    $scope.picture = false; // Initial state
    $scope.userImage = "/app/img/user.jpg";
    $scope.$on('snapshotdata', function (e, i) {
        $scope.$apply(function () {
            $scope.picture = true;
            $scope.sourceData = i;
        });
    });

    $scope.$on('Bind_ProfilePicture', function (e, i) {
        $scope.userImage = "/app/img/Logo/logo.png";
    });
    $rootScope.$broadcast('webcameracontroller_loadsuccess', null);

    $scope.getSnapshot = function () {
        $rootScope.$broadcast('getsnapshot', null);
    }

    $scope.$on('bindimagedata', function (e, i) {
        $scope.userImage = i;
        $rootScope.$broadcast('$destroy', null);
    });

    $scope.$on('camera_live', function (e, i) {
        $scope.camera_Live = false;
    });

    $scope.confirmSnapshot = function (data) {
        $scope.cancel();
        $rootScope.$broadcast('bindimagedata', data);
    }

    $scope.onLoad = function (e, reader, file, fileList, fileOjects, fileObj) {
        $scope.userImage = 'data:' + fileObj.filetype + ';base64,' + fileObj.base64;
    };
    //For webcam picture - Ends
} ]);
