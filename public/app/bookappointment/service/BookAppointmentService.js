/**
*  Description: The bookAppointmentService deals with base operation like creating ,Updating and fetching the data related Patient Creation And Booking appointment.
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:  
* Created On: 
* Modified For: 
* Modified On:
* Modified By:
* 
* */


/**
Service for bookAppointment page
getting user info from the json and returns to login controller
*/
'use strict';
reachApp.factory('bookAppointmentService', ['$http', '$q',function ($http, $q) {
    return {
       
    	/**
    	 * Fetching all the user details
    	 */
    	findAllUserDetails: function () {  
            var deferred = $q.defer(); //promise
            $http({ method: 'GET', url: 'data/Users.json' }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        },
        
        /***
         * Fetching all the Doctor details
         */
        findAllDoctorDetails: function () { 
            var deferred = $q.defer(); //promise
            $http({ method: 'GET', url: 'data/Doctors.json' }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        },
        /***
         * 
         * Fetching all the Doctors Schedule details
         */
        findAllUserScheduleDetails: function () { 
            var deferred = $q.defer(); //promise
            $http({ method: 'GET', url: 'data/UsersSchedule.json' }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;

        },
    
        /**
         * Creating New patient
         */
       registerPatient: function (req) { 
            var deferred = $q.defer(); //promise
            //Create JSON 
            var jsonData = JSON.stringify({ 'request': req });
            $http({ method: 'POST', url: '/patient/0', data: jsonData, headers: { 'contentType': 'application/json' } }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        } ,
        /**
         * Creating new appointment
         */
        bookAppointmentForPatient: function (req) { 
            var deferred = $q.defer(); //promise
            //Create JSON 
            var jsonData = JSON.stringify({ 'request': req });
            $http({ method: 'POST', url: '/appointment/0', data: jsonData, headers: { 'contentType': 'application/json' } }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;

        } ,
        
        /**
         * Checking whether appointment is already booked
         */
        checkAppointmentAvailablity: function (req) { 
            var deferred = $q.defer(); //promise
            //Create JSON 
            var jsonData = JSON.stringify({ 'request': req });
            $http({ method: 'POST', url: '/appointment/availablity', data: jsonData, headers: { 'contentType': 'application/json' } }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;

        }
    }
}]);