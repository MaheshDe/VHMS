/**
*  Description: The bookAppointmentController deals with base operation like creating ,Updating and fetching the data.
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:  
* Created On: 
* Modified For: 
* Modified On:
* Modified By:
* 
* */

'use strict';
reachApp.controller('bookAppointmentController',['$scope', '$timeout','$http', '$window', '$rootScope', 'bookAppointmentService', 'store', '$modal', 'underscore','noty', function ($scope, $timeout,$http, $window, $rootScope, bookAppointmentService, store, $modal, _,noty) {
	
	$scope.showavailabledoctordetails = false;
    $scope.patientregistered = false;
    $scope.showage = false;
    $scope.isAdmin = false;
    $scope.noty = noty;
    
    
    $rootScope.Booking=$rootScope.Booking == undefined ? {} : $rootScope.Booking;
    
    
    $scope.init=function()
    {
    	if($rootScope.appointmentId ==undefined ||  $rootScope.appointmentId ==null)
    		{
    			$rootScope.logout();
    		}
    }
    
    
	/**
	 * Open the model For Adding Appointment Information
	 */
	$scope.BookAnAppointment = function (size) {
    	$rootScope.firsttimeshowtimingmsg = false;
    var modalInstance = $modal.open({
        templateUrl: 'app/bookappointment/view/bookappointment.html',
        controller: 'ModalInstanceCtrl',
        backdrop: 'static',
        size: size,
        resolve: {
            items: function () {
                return $scope.items;
            }
        }
      });
    }
	
	/**
	 * Loads the User details and  Doctors Timings  
	 */
	 $rootScope.$on('Speciality_login_ChangeEvent', function (i, obj) {
		 	$rootScope.Booking.selectedSpeciality=obj;
	        $scope.showavailabledoctordetails = true;
			$rootScope.firsttimeshowtimingmsg = false;
			$rootScope.showtimingmsg="";
	     //   $scope.shownoavailabledoctor =false;
	        UserDetails(obj);
	        
	    });
	 
	 
	 /**
		 * Loads the User details and  Doctors Timings  
		 */
	 var UserDetails = function (obj) {
	    	if(obj!=undefined){    	
	    	var spltyCode=obj.code;
	    	$rootScope.specialityDetails = obj;    	
	    	bookAppointmentService.findAllUserDetails().then(function (res) {
	            var UserDetails = res.ViewModels;

	            bookAppointmentService.findAllDoctorDetails().then(function (res) {
	                var DoctorDetails = res.ViewModels;
	                var details = _.map(UserDetails, function (item) {
	                    var data = _.find(DoctorDetails, function (o) {
	                        return o.userCode == item.code ;
	                    });
	                    return _.extend(item, data);
	                });
	                UserDetails = details;
	                bookAppointmentService.findAllUserScheduleDetails().then(function (res) {
	                    var docList = _.map(UserDetails, function (item1) {
	                        var data1 = _.find(res.ViewModels, function (a) {
	                            return a.userCode == item1.userCode;
	                        });
	                        return _.extend(item1, data1);
	                    });
	                    var finalList = _.filter(docList, function (splty) {
	                        return _.contains((splty.specialization).split(','), spltyCode);
	                    });
	                   
	                    $scope.UserDetails = finalList;
	                });

	            });
	        });
	    }
	    	else {
	    		  $scope.UserDetails =null;
	    		  
	    	}
	    }
	 
	 $scope.doctorsschedule = function (data) {
	        $scope.BookAnAppointmentDoctorDetails(data);
	    }
	    
	 /**
	  * Check the Appointment Is already booked ,If not booked Will continue booking.
	  */
	    $scope.checkAppointmentAvailablity=function(data, details)
	    {
	    		var requestAppointment  = {};
				var startTime=data.SelectedPreferredTime.split('-')[0].trim()+":00";
				var endTime=data.SelectedPreferredTime.split('-')[1].trim()+":00";
				requestAppointment.userCode=data.userCode;
				requestAppointment.appointmentStatus=$rootScope.vmuisettings.appointmentStatusConfirmationCode;
				requestAppointment.appointmentStartDateTime=moment(data.AppointmentDate).format('YYYY-MM-DD') + " "+ startTime;
				requestAppointment.appointmentEndDateTime=moment(data.AppointmentDate).format('YYYY-MM-DD') + " "+ endTime;
				requestAppointment.status=true;
				
				bookAppointmentService.checkAppointmentAvailablity(requestAppointment).then(function (res) {
		            if(res.success) {
		            $rootScope.Booking.appointmentDetails=data;	
		            var loggedonuserinfo = store.get('loggedonuser');
		            
		            if(loggedonuserinfo!=undefined && loggedonuserinfo.role==$rootScope.vmuisettings.AdminRoleCode )
		            {
		            	if($rootScope.Booking.patientInfo==undefined)
		            	{
		            		$rootScope.openPatientInfoModal=true;
		            		$window.location.href = '#/patient/search';
		            	}
		            	
		            	else
		            		{
		            		$scope.continueBooking(data, details,$rootScope.Booking.patientInfo);
		            		}
		            }
		            
		            
		            else {
		            		$scope.continueBooking(data, details,loggedonuserinfo);
		            	}
		            
		            }
		            else {
		                console.log('In loginController - Book appointment fails');
		               // noty.add({ type: 'danger', body: 'Slot Already Booked' });
		                alert("Slot already booked ,Please try again by changing preferred time");
		            }
		        }, function (error) {
		            console.log('In loginController -Book appointment Error', error);
		            noty.add({ type: 'danger', body: 'Booking failed,Please try again later.' });
		        });
	    }

	    
	    $scope.BookAppointment_Login = function (data, details) {
	    
	    	$scope.checkAppointmentAvailablity(data, details); 
	    	
	    }
	    
	    
	    $rootScope.$on('patientModal_ContinueBooking', function (e,i) {
	    	
	    	$scope.continueBooking(i.appointmentDetails, i.selectedDoctor,i.patientInfo);
	    });
	      
	    /**
	     * Setting all the information in rootscope Form Object to preload the patient info When user logged in  .  
	     * Also Opens the add patient details modal.
	     */
	  $scope.continueBooking = function (data, details,patientinfo) {
		console.log(data);
		$rootScope.Booking.patientInfo=null;
	    $rootScope.doctordetails = data;
		$rootScope.maxappntdobDate = new Date();
	   //  var loggedonuserinfo = store.get('loggedonuser');
	     //code for book appointment after login
	        if (null != patientinfo) {
	        	$rootScope.AppointmentDetailsPatient={};
	        	var doB=new Date(patientinfo.dob);
	        	doB.setHours(doB.getHours() + 5);
	        	doB.setMinutes(doB.getMinutes() + 30);
	        	//setting logged on user details to rootScope object
	        	$rootScope.AppointmentDetailsPatient.PatientFirstName=patientinfo.firstName;
	        	$rootScope.AppointmentDetailsPatient.PatientMiddleName=patientinfo.middleName;
	        	$rootScope.AppointmentDetailsPatient.PatientLastName=patientinfo.lastName;
	        	$rootScope.AppointmentDetailsPatient.PatientEmail=patientinfo.email;
	        	$rootScope.AppointmentDetailsPatient.PatientMobileNo=patientinfo.contactNumber;
	        	$rootScope.AppointmentDetailsPatient.Gender=patientinfo.gender;
	        	$rootScope.AppointmentDetailsPatient.DateofBirth=moment(doB).format('YYYY-MM-DD');
	        	$rootScope.AppointmentDetailsPatient.PatientUHID=patientinfo.uhId;
	        	//make text fields and drop downs readable.
	        	$rootScope.disableControls = true;
	        	$rootScope.disableControlsDropdown=true;
	        	$rootScope.userlogin=false;
	       }
	        else
	        	{
	        	
	        	$rootScope.AppointmentDetailsPatient={};
	        	$rootScope.disableControls = false;
		    	$rootScope.disableControlsDropdown=false;
		    	$rootScope.userlogin=true;
	        	}
	        
	    if ($rootScope.openPatientInfoModal || $scope.BookAppointmentDtails_Login.$valid ) {
	    	$rootScope.openPatientInfoModal=false;
	        $scope.cancel();
	        var modalInstance = $modal.open({
	            templateUrl: 'BookAnAppointmentPatientDetailsModalContent.html',
	            controller: 'ModalInstanceCtrl',
	            backdrop: 'static',
	            size: 'lg',
	            resolve: {
	                items: function () {
	                    return $scope.items;
	                }
	            }
	        });
	    }
	  }
	  
	  /***
	   * Create request Object for both patient registration and Book appointment
	   */
	    $scope.BookAppointmentPatientDetailsSave_Login = function (data) {
	    	
	        if ($scope.BookAppointmentPatientDetails_Login.$valid) { 
	        	
	            $rootScope.AppointSuccessDocName = $rootScope.doctordetails.name;
	            $rootScope.AppointSuccessAppointmentDate = moment($rootScope.doctordetails.AppointmentDate).format('DD-MMM-YYYY');
	            $rootScope.AppointSuccessDocSpeciality = $rootScope.doctordetails.speciality;
	            $rootScope.AppointSuccessPreferredTime = $rootScope.doctordetails.SelectedPreferredTime;
	            $rootScope.AppointSuccessDocDesignation = $rootScope.doctordetails.designation;
	            $rootScope.PatientFirstName = data.PatientFirstName;
	            $rootScope.PatientMiddleName = data.PatientMiddleName;
	            $rootScope.PatientLastName = data.PatientLastName;
	            $rootScope.PatientEmail = data.PatientEmail;
	            $rootScope.PatientMobileNo = data.PatientMobileNo;
	            $rootScope.PatientUHID=data.PatientUHID;
	            
	            var requestPatient  = {};
	            requestPatient.firstName=data.PatientFirstName;
	            requestPatient.lastName=data.PatientLastName;
	            requestPatient.middleName=data.PatientMiddleName;
	            requestPatient.contactNumber=data.PatientMobileNo;
	            requestPatient.email=data.PatientEmail;
	            requestPatient.gender=data.Gender ;
	            requestPatient.uhId=data.PatientUHID;
	            requestPatient.dob = moment(data.DateofBirth).format('YYYY-MM-DD HH:mm:ss');
	            requestPatient.status=true;
	            //passing patient role code
	            requestPatient.role = $rootScope.vmuisettings.PatientRoleCode;
	            
	            $scope.registerPatient(requestPatient,data);
	        }
	    }
	    
	    /**
	     * Registration a patient after Complete of patient registration .appointment booking will continue
	     */
	 	$scope.registerPatient = function (request,data) {
	 		bookAppointmentService.registerPatient(request).then(function (res) {
	            if (res.success && res.ViewModel!=null ) {
	            	 console.log('In loginController - Patient Registration Success');

	            	 			var requestAppointment  = {};
	            	 			var startTime=$rootScope.doctordetails.SelectedPreferredTime.split('-')[0].trim()+":00";
	            	 			var endTime=$rootScope.doctordetails.SelectedPreferredTime.split('-')[1].trim()+":00";            	 			
	            	 			var jsondata ={};
	            	 			jsondata.doctorName = $rootScope.doctordetails.name;
	            	 			jsondata.doctorCode = $rootScope.doctordetails.userCode;
	            	 			jsondata.specialityCode =$rootScope.specialityDetails.code;
	            	 			jsondata.specialityName =$rootScope.specialityDetails.name;
	            	 			jsondata.appointmentStatusCode =$rootScope.vmuisettings.appointmentStatusConfirmationCode;
	            	 			jsondata.appointmentStatusDesc =$rootScope.vmuisettings.appointmentStatusConfirmationDesc;
	            	 			
	            	 			requestAppointment.patientCode=res.ViewModel.viewModel.code;
	            	 			requestAppointment.userCode=$rootScope.doctordetails.userCode;
	            	 			requestAppointment.patientContactNumber=data.PatientMobileNo;
	            	 			requestAppointment.appointmentStatus=$rootScope.vmuisettings.appointmentStatusConfirmationCode;
	            	 			requestAppointment.patientUhId=res.ViewModel.viewModel.uhId;
	            	 			requestAppointment.userContactNumber=$rootScope.doctordetails.mobile;
	            	 			requestAppointment.appointmentStartDateTime=moment($rootScope.doctordetails.AppointmentDate).format('YYYY-MM-DD') + " "+ startTime;
	            	 			requestAppointment.appointmentEndDateTime=moment($rootScope.doctordetails.AppointmentDate).format('YYYY-MM-DD') + " "+ endTime;
	            	 			requestAppointment.fkLocationCode=$rootScope.doctordetails.fkLocationCode;
	            	 			requestAppointment.fkCompanyCode=$rootScope.doctordetails.fkCompanyCode;
	            	 			requestAppointment.status=true;
	            	 			requestAppointment.json = jsondata;
	            	 			
	            	 			$scope.cancel();
			                	$scope.bookAppointmentForPatient(requestAppointment); 
			                	//noty.add({ type: 'success',  body: res.message });
	            }	            
	            if(res.success==false)
	            	{
	            	if(data.PatientUHID !=null && data.PatientUHID!="" )
	            		{
	            		var r = confirm("Either email Id or UHID is wrong.Can you proceed with new registration?");
	            	   	if (r == true) { 
	            	     	data.switchStatus=false;
	            	     	data.PatientUHID=null;
	            	   	}
	            	   	if(r == false)
	            	   		{
	            	   		$scope.cancel();
	            	   		noty.add({ type: 'danger',  body: 'You cancelled the request.' });
	            	   		}
	            		}
	            	
	            	else {
	                 noty.add({ type: 'danger',  body: res.message });
	             	$scope.cancel();
	             }
	            	}
	 
	        }, function (error) {
	        	noty.add({ type: 'danger',body: 'Patient registration failed'});
	      
	            console.log('In loginController -  Patient Registration Error:', error);
	        });
	 	}
	 	
	 	/**
	 	 * 
	 	 * Create Appointment for Patient 
	 	 */
		$scope.bookAppointmentForPatient = function (request) {
			bookAppointmentService.bookAppointmentForPatient(request).then(function (res) {
	            if (res != null && res.ViewModel!=null ) {
	            	$rootScope.appointmentId=res.ViewModel.viewModel.code;
	            	$rootScope.uhId=request.patientUhId;
	            	$scope.generateReport();
	            	 console.log('In loginController - Book appointment Success');
	            	 noty.add({ type: 'success',  body: res.message });
		            $timeout(function(){$window.location.href = '#/appointmentconfirmation'; },300);
	            }
	            else {
	                console.log('In loginController - Book appointment fails');
	                noty.add({ type: 'danger', body: 'Booking failed,Please try again later.' });
	            }
	        }, function (error) {
	            console.log('In loginController -Book appointment Error', error);
	            noty.add({ type: 'danger', body: 'Booking failed,Please try again later.' });
	        });
	 	}
	 	
	 	
	 	
/**
 * Calculating Age based on selected DOB
 * 
 */
	    $scope.$on('txtDOB_User_appointmentdate_selected', function (e, i) {
	        if (i != undefined) {
	            var ageDifMs = Date.now() - new Date(i);
	            var ageDate = new Date(ageDifMs);
	            $scope.patientage = Math.abs(ageDate.getUTCFullYear() - 1970);
	            $scope.showage = true;
	        }
	    });
/**
 * Opens Modal for Doctors List based on speciality
 */	    
	    $scope.BookAnAppointmentDoctorDetails = function (data) {
	    	$rootScope.Booking.selectedDoctor=data;
	    	$rootScope.showtimingmsg="";
	    	$rootScope.firsttimeshowtimingmsg = false;
	    	data.minappointmentDate = new Date();
	        data.speciality = angular.element('#Speciality_login').find('.ng-binding').html();
	        data.AppointmentDate = "";
	        data.Timings = [];
	        $scope.items = data;
	        $scope.cancel();
	        
	        var modalInstance = $modal.open({
	            templateUrl: 'BookAnAppointmentDoctorModalContent.html',
	            controller: 'ModalInstanceCtrl',
	            backdrop: 'static',
	            size: 'lg',
	            resolve: {
	                items: function () {
	                    return $scope.items;
	                }
	            }
	        });
	/**
	 * 
	 * Calculating the doctor timing based the Current timing.
	 */
	        modalInstance.opened.then(function () {
	            $scope.Timings = [];
	            //subscribes on date selection
	            $rootScope.$on('txtAppointmentDate_doctordate_selected', function (e, i) {
	            	
	            	$rootScope.showtimingmsg="";
	                // if date selected is not equal to undefined them proceed
	                if (i != undefined) {
	                	//$rootScope.$broadcast('clear_dropdown', 'prefferedTime');
	                	$scope.items.SelectedPreferredTime = "";
	                    // get doctor details
	                    var doctordetails = data;
	                    //get start time end time and consultation interval
	                    var startTime = (data.startTime).split(':');
	                    var endTime = (data.endTime).split(':');
	                    var consultationInterval = parseInt(data.consultationInterval);

	                    var startday = moment(i);
	                    var endday = moment(i);
	                    //setting start date time and end date time
	                    startday.set({ hours: startTime[0], minutes: startTime[1] });
	                    endday.set({ hours: endTime[0], minutes: endTime[1] });
	                    // getting the difference between two dates
	                    var diff = moment.duration(endday.diff(startday));
	                    var intervals = [];
	                    //repeat number of time the interval can preceed within two dates
	                    for (var j = 0; j < (diff._milliseconds / (consultationInterval * 60000)); j++) {
	                        if(moment(i).format("DD-MMM-YYYY") == moment().format("DD-MMM-YYYY"))
	                        {
	                            if(startday > moment()._d)
	                            {
	                                intervals.push(moment(startday).format('HH:mm') + " - " + moment(startday).add(consultationInterval * 60, 'seconds').format('HH:mm'));                                
	                            }
	                            startday = moment(startday).add(consultationInterval * 60, 'seconds');
	                        }
	                        else
	                        {
	                            intervals.push(moment(startday).format('HH:mm') + " - " + moment(startday).add(consultationInterval * 60, 'seconds').format('HH:mm'));
	                            startday = moment(startday).add(consultationInterval * 60, 'seconds');
	                        }                        
	                    }
	                    
	                   if(intervals.length<=0 && $rootScope.firsttimeshowtimingmsg )
	                	   {
	                	   		$rootScope.showtimingmsg="No Slots Available";
	                	   		
	                	   }
	                    
	                    $rootScope.firsttimeshowtimingmsg =true;
	                    data.Timings = intervals;
	                    $scope.items = data;
	                }
	                else {
	                    data.AppointmentDate = "";
	                    data.Timings = [];
	                }
	            });
	        });
	    }
	
	    /***
	     * 
	     * Create a request object for generating report.
	     */
	  
		    $scope.generateReport=function()
		    {
		    	var requestReport={};
		    	requestReport.specialistName=$rootScope.AppointSuccessDocName;
		    	requestReport.appointmentDate=$rootScope.AppointSuccessAppointmentDate;
		    	requestReport.specialization=$rootScope.AppointSuccessDocSpeciality;
		    	requestReport.appointmentTime= $rootScope.AppointSuccessPreferredTime;
		    	requestReport.specialistDesignation=$rootScope.AppointSuccessDocDesignation;
		    	requestReport.patientFirstName=$rootScope.PatientFirstName;
		    	requestReport.patientMiddleName=$rootScope.PatientMiddleName;
		    	requestReport.patientLastName=$rootScope.PatientLastName;
		    	requestReport.patientEmail=$rootScope.PatientEmail; 
		    	requestReport.patientMobileNo=$rootScope.PatientMobileNo;
		    	requestReport.patientUHID=$rootScope.uhId;
		    	requestReport.countryName=$rootScope.vmuisettings.countryName;
		    	requestReport.cityName=$rootScope.vmuisettings.cityName;
		    	requestReport.appointmentCode=$rootScope.appointmentId;
		    	requestReport.hospitalName= $rootScope.vmuisettings.hospitalName;
		    	requestReport.specializationName=$rootScope.AppointSuccessDocSpeciality;
		    	$rootScope.requestReport=requestReport;
		     }
		    
/***
 * Clearing UHID on changing Toogle Switch. 
 */
		    $scope.ClearUHID=function()
		    {
		    	if($scope.AppointmentDetailsPatient.PatientUHID!=undefined && $scope.AppointmentDetailsPatient.PatientUHID!=null)
		    	{ 
		    		$scope.AppointmentDetailsPatient.PatientUHID="";
		    	}
		    	return true;
		    }
		    
	    $scope.patientRegistered = function (data) {
	        if (data.switchStatus == true) {
	            $scope.patientregistered = false;
	        }
	        else {
	        	
	            $scope.patientregistered = true;
	        }
	    }
	    
/*	    $scope.isAdmin = function() {debugger;
	    	var loggedonuserinfo = store.get('loggedonuser');
	    	if(loggedonuserinfo.role!=undefined && loggedonuserinfo.role!=null && loggedonuserinfo.role!="")
	    		{
	    		if(loggedonuserinfo.role.indexOf('ADMIN')>0){
	    			return true;
	    		}else
	    			{
	    			return false;
	    			}
	    			
	    		}
	    	else
	    		{
	    		return false; //Just as a test to make sure it works
	    		}
	     
	    }*/
	    
	    var loggedonuserinfo = store.get('loggedonuser');
	    if(loggedonuserinfo !== null && loggedonuserinfo !== undefined ){
	    	if(loggedonuserinfo.role!==undefined ){
	    		{
	    			if(loggedonuserinfo.role!==null && loggedonuserinfo.role!==""&&loggedonuserinfo.role == $rootScope.vmuisettings.AdminRoleCode){
	        			$scope.isAdmin = true;
	        		}else
	        			{
	        			$scope.isAdmin = false;
	        			}
	        			
	        		}
	    	}    		
	    	else
	    		{
	    		$scope.isAdmin = true; //Just as a test to make sure it works
	    		}		   
	    }
    	
    
} ]);