/**
*  Description: The BookAnAppointmentController deals with base operation like creating ,Updating and fetching the data.
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:  JasmitaB
* Created On: 14/08/2015
* Modified For: 
* Modified On:
* Modified By:
* 
* */

'use strict';
reachApp.controller('bookanappointmentcontroller',['$scope','$timeout', '$http', '$window', '$rootScope', 'bookanappointmentService', 'store', '$modal', 'underscore','noty','DTOptionsBuilder', 'DTColumnDefBuilder','$compile', 'uiCalendarConfig', function ($scope,$timeout, $http, $window, $rootScope, bookanappointmentService, store, $modal, _,noty,DTOptionsBuilder, DTColumnDefBuilder,$compile,uiCalendarConfig) {
    //Doctors section will be hidden on page load
    
	
  $scope.init = function()
    {
    	if($rootScope.appointmentId == undefined ||  $rootScope.appointmentId == null)
    	{
    			$rootScope.logout();
    	}
    }
    
    
    $scope.ViewScheduleAppointment = false;
    $scope.ViewUserRegistration = false;
    $scope.ViewSchduledDate = false;
    $scope.ViewNextButton = false;
    $rootScope.maxappntdobDate = new Date();
    
   
    //Controls for enable/disable Gender and Date of birth 
    //in User Details form while booking appointment
    $rootScope.disableControls = false;
	$rootScope.disableControlsDropdown=false;



    $scope.selectSpeciality = true;
    $scope.ViewDoctors = false;
    $scope.appointmentSchedulePage = false;
    $scope.addPatientDetailsPage =false;
   

    	// Book appointment new scheduler html 
    	
    	$scope.SelectedDateFormatted = moment(new Date()).format("dddd, MMMM Do");
        $scope.SelectedDate = moment(new Date());
        $scope.alertMessage = "";
        $scope.EventName = "";
      //  $scope.SelectedTime = new Date();

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();      
        
        
    	/* config object */
        $scope.uiConfig = {
            calendar: {
                height: 450,
                //Selectable should be true so that current Selection will be hightlighted
                selectable: true,
                //By Default unselectAuto will be true, by setting it value to false on click of outside the current selection will not be cleared.
                unselectAuto: false,
                editable: true,
                //If weekends is false then calender will hide Saturdays and Sundays
                weekends: true,
                //Available weekMode is 'fixed','liquid','variable'
                weekMode: 'fixed',
                //header: {
                //    left: 'month basicWeek basicDay agendaWeek agendaDay',
                //    center: 'title',
                //    right: 'today prev,next'
                //},
                header: {
                    left: 'title',
                    center: '',
                    right: 'today prev,next'
                },
                // events: $scope.events,
                //On dayclick, bind Clientdetails

                dayClick: function (date, allDay, jsEvent, view) {
                	
                	//storing selected date
                    $scope.SelectedDateFormatted = moment(date).format("dddd, MMMM Do");
                    $scope.SelectedDate = moment(date);
                    var datetime = moment().set({hours:0,minutes:0,seconds:0});
                    var currentDate = new Date();
                    //formatting currentDate
                    $scope.date = moment(currentDate).format("dddd, MMMM Do");
                    console.log("current date : "+currentDate);
                    
                    if( $scope.SelectedDateFormatted == $scope.date && $rootScope.docObject.startTime <= $scope.currentTime)
                    {
         	    	   $scope.timeAvailibility.SelectedTime = datetime.set({ hours: $scope.currentHours, minutes: $scope.currentMin,seconds:$scope.currentSec });
         	       	}
         	       	else
         	       	{
         	    	   $scope.timeAvailibility.SelectedTime = datetime.set({ hours: $rootScope.docObject.startTime.split(":")[0], minutes: $rootScope.docObject.startTime.split(":")[1],seconds:0 });
         	       	}

                    
                },
                
                //event click
                eventClick: function (date, jsEvent, view) {
                    alert(date.title + ' was clicked ');
                },
                eventRender: function (event, element, view) {
                    element.attr({
                        'tooltip': event.title,
                        'tooltip-append-to-body': true
                    });
                    $compile(element)($scope);
                } 
            }
        };

        //event source that pulls from google.com
        $scope.eventSource = [];
        //    {
        //    url: "http://www.google.com/calendar/feeds/usa__en%40holiday.calendar.google.com/public/basic",
        //    className: 'gcal-event',           // an option!
        //    currentTimezone: 'America/Chicago' // an option!
        //};
        //you can add the events in following ways
        $scope.events = [
          { title: 'Patient 1', start: new Date(y, m, 1) },
          { title: 'Patient 2', start: new Date(y, m, d - 5), end: new Date(y, m, d - 2) },
          { id: 999, title: 'Patient 4', start: new Date(y, m, d - 3, 16, 0), allDay: false },
          { id: 999, title: 'Patient 5 ', start: new Date(y, m, d + 4, 16, 0), allDay: false },
          { title: 'Patient 3', start: new Date(y, m, d + 1, 19, 0), end: new Date(y, m, d + 1, 22, 30), allDay: false },      
          { title: 'Click for Google', start: new Date(y, m, 28), end: new Date(y, m, 29), url: 'http://google.com/' }
        ];
        /* event source that calls a function on every view switch */
        $scope.eventsF = function (start, end, timezone, callback) {
            var s = new Date(start).getTime() / 1000;
            var e = new Date(end).getTime() / 1000;
            var m = new Date(start).getMonth();
            var events = [{ title: 'Feed Me ' + m, start: s + (50000), end: s + (100000), allDay: false, className: ['customFeed'] }];
            callback(events);
        };

                
	       
        ///* event sources array*/
        $scope.eventSources = [$scope.events, $scope.eventSource, $scope.eventsF];
    	

    

    $scope.appointmentSchedulingPrevious = function()
    {
    	
    	$scope.selectSpeciality = true;
        $scope.ViewDoctors = true;
        $scope.appointmentSchedulePage = false;
        
        //for setting back the search data.   
        if($rootScope.details != undefined && $rootScope.details != null && $rootScope.details != "")
     	{
     		$scope.specialityData  = $rootScope.details;
     		$scope.doctorDetails={};
     		$scope.doctorDetails.Speciality = $rootScope.details.Speciality;
     		$scope.doctorDetails.doctorname = $rootScope.details.doctorname;
     		$scope.SearchClicked($scope.specialityData);
     		$rootScope.details = null;
     	}
    	
    }
    
   
    

    $scope.CreateAppointment = function (timeAvailibility) 
    {
    	if(timeAvailibility != undefined && timeAvailibility != null && timeAvailibility != "")
	    	{
	    		var data = $rootScope.details;
	    		
		    		var requestAppointment  = {};
		    		//storing selected date
		    		$scope.AppointmentSelectedDate = moment($scope.SelectedDate).format('YYYY-MM-DD');
		    		$scope.startTime = moment(timeAvailibility.SelectedTime).format('HH:mm:ss');
		    		
		    		// adding 15 minutes to startTime.
		    		Date.prototype.addHours = function(hrs) {
		    		    this.setHours(this.getHours() + hrs);
		    		    return this;
		    		}
		    		Date.prototype.addMinutes = function(mnts) {
		    		    this.setMinutes(this.getMinutes() + mnts);
		    		    return this;
		    		}
		    		
		    	   var dt = new Date(),
		           hoursToAdd = 0,
		           minutesToAdd = 15;

		           // Set current time
		           dt.setHours($scope.startTime.split(":")[0]);
		           dt.setMinutes($scope.startTime.split(":")[1]);
		           //setting selected date and endTime
		           var endTime = (dt.addHours(hoursToAdd).addMinutes(minutesToAdd));
		           
		           $scope.endTime = moment(endTime).format('HH:mm:ss');

		           //if user  is not logged In
		           //constructing the request
		           if(data !== undefined && data !== null && data !== "")
		        	{
		        	   requestAppointment.userCode = data.userCode;
		        	}   
					requestAppointment.appointmentStatus = $rootScope.vmuisettings.appointmentStatusConfirmationCode;
					requestAppointment.appointmentStartDateTime = moment($scope.AppointmentSelectedDate).format('YYYY-MM-DD') + " "+ $scope.startTime;
					requestAppointment.appointmentEndDateTime = moment($scope.AppointmentSelectedDate).format('YYYY-MM-DD') + " "+ $scope.endTime ;
					requestAppointment.status=true;
					
					//checking doctor availability
					if(($scope.startTime < $rootScope.BookAppointmentDetails.startTime && $scope.startTime > $rootScope.BookAppointmentDetails.startTime) 
							|| ($scope.endTime > $rootScope.BookAppointmentDetails.endTime && $scope.endTime < $rootScope.BookAppointmentDetails.endTime))
					{
						alert("doctor not available");
					}
					
					
					bookanappointmentService.checkAppointmentAvailablity(requestAppointment).then(function (res) {
			            if(res.success) 
			            {
			            	
			            	$scope.appointmentSchedulePage = false;
	            			$scope.addPatientDetailsPage = true;
	            			
			            	//$rootScope.Booking.appointmentDetails = data;	
			            	var loggedonuserinfo = store.get('loggedonuser');
			            	$scope.continueBooking(data,loggedonuserinfo);
			            
			            }
			            else 
			            {
			               // noty.add({ type: 'danger', body: 'Slot Already Booked' });
			                alert("Slot already booked ,Please try again by changing preferred time");
			            }
			        }, function (error) {
			            noty.add({ type: 'danger', body: 'Booking failed,Please try again later.' });
			        });
	    	}
	    	else
	    	{
	    		alert("Please select time ");
	    	}
    	
    	
    }
    
    //if patient is logged In then binding the patient Details
    $scope.continueBooking = function (data,patientinfo) {
		console.log(data);
		//$rootScope.Booking.patientInfo=null;
	    $rootScope.doctordetails = data;
		$rootScope.maxappntdobDate = new Date();
	  
	     //code for book appointment after login
	        if (null !== patientinfo && patientinfo !== undefined && loggedonuserinfo.role != $rootScope.vmuisettings.AdminRoleCode) {
	        	$rootScope.AppointmentDetailsPatient={};
	        	var doB=new Date(patientinfo.dob);
	        	doB.setHours(doB.getHours() + 5);
	        	doB.setMinutes(doB.getMinutes() + 30);
	        	//setting logged on user details to rootScope object
	        	$rootScope.AppointmentDetailsPatient.PatientFirstName=patientinfo.firstName;
	        	$rootScope.AppointmentDetailsPatient.PatientMiddleName=patientinfo.middleName;
	        	$rootScope.AppointmentDetailsPatient.PatientLastName=patientinfo.lastName;
	        	$rootScope.AppointmentDetailsPatient.PatientEmail=patientinfo.email;
	        	$rootScope.AppointmentDetailsPatient.PatientMobileNo=patientinfo.contactNumber;
	        	$rootScope.AppointmentDetailsPatient.Gender=patientinfo.gender;
	        	$rootScope.AppointmentDetailsPatient.DateofBirth=moment(doB).format('YYYY-MM-DD');
	        	$rootScope.AppointmentDetailsPatient.PatientUHID=patientinfo.uhId;
	        	//make text fields and drop downs readable.
	        	$rootScope.disableControls = true;
	        	$rootScope.disableControlsDropdown=true;
	        	$rootScope.userlogin=false;       	
    
	       }
	        else
	        {
	        	$rootScope.AppointmentDetailsPatient={};
	        	$rootScope.disableControls = false;
		    	$rootScope.disableControlsDropdown=false;
		    	$rootScope.userlogin=true;   
		    	
	        }
    }
        /*var date1 = new Date($scope.SelectedDate);
        var d1 = date1.getDate();
        var m1 = date1.getMonth();
        var y1 = date1.getFullYear();
        var hours = new Date($scope.SelectedTime).getHours();
        var minutes = new Date($scope.SelectedTime).getMinutes();
        //$scope.SelectedTime
        //this code will add new event and remove the event present on index
        //you can call it explicitly in any method
        $scope.events.push({
            title: $scope.EventName,
            start: new Date(y1, m1, d1, hours, minutes),
            end: new Date(y1, m1, d1, hours, minutes),
            className: ['newtask']
        });

        $scope.events.splice(index, 1);*/
    
    
    $scope.previousPage = function(){
    	
  	  	$scope.appointmentSchedulePage = true;
  	    $scope.addPatientDetailsPage = false;
  }
    
    //Functionality on click of search by selecting speciality - Starts
    $scope.SearchClicked = function (speciality) {
       
    		$rootScope.details = speciality;
    	
    		$scope.selectSpeciality = true;
            $scope.selectedDoctor = '';
            $scope.ViewDoctors = true;
            $scope.ViewScheduleAppointment = false;
            $scope.ViewUserRegistration = false;
            $scope.ViewSchduledDate = false;
            $scope.ViewNextButton = false;
            
            
            
            UserDetails(speciality);
            //Lookup table initiation - Starts
            $scope.lookUpDtOptions = DTOptionsBuilder.newOptions().withDisplayLength(5).withBootstrap().withDOM('<"H"<"leftaligned hide"f>r>t<"F"p>').withBootstrapOptions({
            TableTools: {
                classes: {
                        container: 'btn-group',
                        buttons: {
                        normal: 'btn btn-danger'
                        }
                }
            },
            pagination: {
                classes: {
                        ul: 'pagination pagination-sm'
                }
            }
            });

            $scope.lookUpDtColumnDefs = [
                DTColumnDefBuilder.newColumnDef(0).notSortable(),
                DTColumnDefBuilder.newColumnDef(1).notSortable()
            ];
            //Lookup table initiation - Ends
    }

    

	    //Functinality to get doctors - Starts
	    /**
	    * Loads the User details and  Doctors Timings  
	    */
	    var UserDetails = function (obj) {
	    	
	    	//$rootScope.specialitySearch = obj;
	        if (obj != undefined) {
	            var spltyCode = obj.Speciality;
	            var data=[];
	            
	            //$scope.doctorName = obj.doctorname;
	            
	            //Get the Speciality details based on code
	            bookanappointmentService.findAllSpeciality().then(function (res) {
	            	
	            	if(res.Success && res.ViewModels !== null){
	            		var allSpecialty = res.ViewModels;
			            	angular.forEach(allSpecialty, function (value,key) {
		                		if(value.code == spltyCode){
		                			return data.push(value);
		                		}
		                	});
	            		
	            	}else{
	            		
	            	}
	            	$rootScope.specialityDetails = data[0];
	            });
	            
	            
	            bookanappointmentService.findAllUserDetails().then(function (res) {
	            	 var UserDetails = res.ViewModels;
	            
	            	if(obj.doctorname != undefined && obj.doctorname != null && obj.doctorname != "")
	            	{
	            	
	            		var Users = _.filter(UserDetails, function(object){ 
	            			if(object.name.toLowerCase().indexOf(obj.doctorname.toLowerCase()) > -1)
	            			{return object;} 
	            			});
	            		UserDetails = Users;
	            		$scope.UserDetails = Users;
	            		
	            	}
	            	else
	            	{
	            		$scope.UserDetails = UserDetails;
	            	}
	               
	            	
	            		bookanappointmentService.findAllDoctorDetails().then(function (res) {
	            			var DoctorDetails = res.ViewModels;
	            			var details = _.map(UserDetails, function (item) {
	            				var data = _.find(DoctorDetails, function (o) {
	            					return o.userCode == item.code;
	                        });
	                        return _.extend(item, data);
	                    });
	                    UserDetails = details;
	                    bookanappointmentService.findAllUserScheduleDetails().then(function (res) {
	                        var docList = _.map(UserDetails, function (item1) {
	                            var data1 = _.find(res.ViewModels, function (a) {
	                                return a.userCode == item1.userCode;
	                            });
	                            return _.extend(item1, data1);
	                        });
	                        var finalList = docList;
	                        if(obj.Speciality != undefined && obj.Speciality != null && obj.Speciality != "")
	    	            	{
	                        	finalList   = _.filter(docList, function (splty) {
	                            return _.contains((splty.specialization).split(','), spltyCode);
	                        });
	    	            	}

	                        /*bookanappointmentService.findAllSpeciality().then(function (res) {
	                        	if(res.Success && res.ViewModels !== null){
	        	            		var allSpecialty = res.ViewModels;
	        	            		debugger;
	        	            		finalList = _.map(finalList, function (item) {
	    	            				var data = _.find(allSpecialty, function (o) {
	    	            					return o.code == item.specialization;
	    	                        });
	    	            				item.specializationDetails = data.name;
	    	                        
	    	                    });
	                        	}
	                        });*/
	                        $scope.UserDetails = finalList;
	                      });

	            		});
	            	
	            });
	            
	            
	           /* $scope.doctorName = obj.doctorname;
		    	bookanappointmentService.findAllUserDetails().then(function (res) {debugger
		    		if(res.Success)
		    		{debugger
		    			$scope.allDoctorDetails = res.ViewModels;
		    			//fetching doctor name
		    			$scope.allDoctorName = _.findWhere($scope.allDoctorDetails, {name:$scope.doctorName});
		    			$scope.allDoctors = allDoctorName.name;
		    		}
		    	});*/
		    	
	        }
	        else {
	            $scope.UserDetails = null;

	        }
	    
	   
	    }
	    //Functionality to get doctors - Ends
	 
	    //View the Event scheduler on doctor selected - Starts
	    $scope.doctorsschedule = function (data) {
	    
	    	$scope.selectSpeciality = false;
	    	$scope.appointmentSchedulePage = true;
	    	
	        $scope.ViewDoctors = false;
	        $scope.ViewScheduleAppointment = true;
	        $scope.ViewUserRegistration = false;
	        $scope.ViewSchduledDate = false;
	        $scope.ViewNextButton = false;
	        $rootScope.BookAppointmentDetails = data;
	         
	       var docobj = {}
	       
	       docobj.designation = data.designation;
	       docobj.doctorName = data.name; 
	       docobj.description = data.description;
	       docobj.endTime = data.endTime;
	       docobj.startTime = data.startTime;
	       docobj.experience = data.experience;
	       docobj.profilePictureUrl = data.profilePictureUrl;
	       $rootScope.docObject = docobj;	       
	       
	       
	       var startTime = new Date();
	       startTime.setHours(data.startTime.split(":")[0]);
	       startTime.setMinutes(data.startTime.split(":")[1]);
	       startTime.setSeconds(0);
	       $scope.MinTime = startTime;
	       
	       var datetime = moment().set({hours:0,minutes:0,seconds:0});
	       $scope.timeAvailibility = {};
	       //getting current Date
	       var currDate = new Date();
	       
	       //current time.. Hours,minutes,seconds
	       $scope.currentTime = moment(currDate.getTime()).format('HH:mm:ss');
	       $scope.currentHours = currDate.getHours();
	       $scope.currentMin = currDate.getMinutes();
	       $scope.currentSec = currDate.getSeconds();
	       
	       if(data.startTime <= $scope.currentTime)
	       {
	    	   $scope.timeAvailibility.SelectedTime = datetime.set({ hours: $scope.currentHours, minutes: $scope.currentMin,seconds:$scope.currentSec });
	       }
	       else
	       {
	    	   $scope.timeAvailibility.SelectedTime = datetime.set({ hours: data.startTime.split(":")[0], minutes: data.startTime.split(":")[1],seconds:0 });
	       }

	       
	     //  $scope.timeAvailibility.SelectedTime = datetime.set({ hours: data.startTime.split(":")[0], minutes: data.startTime.split(":")[1],seconds:0 });

	       var endTime = new Date();
	       endTime.setHours(data.endTime.split(":")[0]);
	       endTime.setMinutes(data.endTime.split(":")[1]);
	       endTime.setSeconds(0);
	       $scope.MaxTime = endTime; 
	       
	    }
	    //View the Event scheduler on doctor selected - Ends
	    
	    
	   //Highlighting the look up doctors table - Starts
	    $scope.SetSelectedDoctor = function (code) {
	        $scope.selectedDoctor = code;
	    }
	    //Highlighting the look up doctors table - Ends
	    
	    
	    //Function called on selecting Schedule Date - Starts
	    $scope.$on('DateTime_selected', function (e, i) {
	        $scope.ViewDoctors = true;
	        $scope.ViewScheduleAppointment = true;
	        $scope.ViewUserRegistration = false;
	        $scope.ViewSchduledDate = true;
	        $scope.ViewNextButton = true;
	        $scope.SelectedDateTime = moment(i.startsAt).format("DD-MMM-YYYY") + " " + moment(i.startsAt).format("HH:mm") + " - " + moment(i.endsAt).format("HH:mm");
	    });
	    //Function called on selecting Schedule Date - Starts
	    
	  
	   

	  /***
	   * Create request Object for both patient registration and Book appointment
	   */
	    $scope.BookAppointmentPatientDetailsSave_Login = function (data) {
	    $scope = this;
	      
	        	
	        		$rootScope.AppointSuccessDocName = $rootScope.BookAppointmentDetails.name;
	 	            $rootScope.AppointSuccessAppointmentDate = $scope.AppointmentSelectedDate ;  	/*moment($rootScope.EventDetails.AppointmentDate).format('DD-MMM-YYYY');*/
	 	            $rootScope.AppointSuccessDocSpeciality = $rootScope.BookAppointmentDetails.specialization; 	/*$rootScope.EventDetails.SpecialityName;*/
	 	            $rootScope.AppointSuccessStartTime = moment($scope.AppointmentSelectedDate).format('YYYY-MM-DD') + " "+ $scope.startTime;
	 	            $rootScope.AppointSuccessEndTime = moment($scope.AppointmentSelectedDate).format('YYYY-MM-DD') + " "+ $scope.endTime;
	 	            $rootScope.AppointSuccessDocDesignation = $rootScope.BookAppointmentDetails.designation;   /*$rootScope.EventDetails.Designation;*/
	 	            $rootScope.PatientFirstName = data.PatientFirstName;
	 	            $rootScope.PatientMiddleName = data.PatientMiddleName;
	 	            $rootScope.PatientLastName = data.PatientLastName;
	 	            $rootScope.PatientEmail = data.PatientEmail;
	 	            $rootScope.PatientMobileNo = data.PatientMobileNo;
	 	            $rootScope.PatientUHID=data.PatientUHID;
	 	            $rootScope.AppointSuccessPreferredTime = $rootScope.BookAppointmentDetails.startTime + " - " + $rootScope.BookAppointmentDetails.endTime; 
	 	            
	 	            var requestPatient  = {};
	 	            requestPatient.role = $rootScope.vmuisettings.PatientRoleCode;
	 	            requestPatient.firstName=data.PatientFirstName;
	 	            requestPatient.lastName=data.PatientLastName;
	 	            requestPatient.middleName=data.PatientMiddleName;
	 	            requestPatient.contactNumber=data.PatientMobileNo;
	 	            requestPatient.email=data.PatientEmail;
	 	            requestPatient.gender=data.Gender;
	 	            requestPatient.uhId=data.PatientUHID;
	 	            requestPatient.dob = moment(data.DateofBirth).format('YYYY-MM-DD HH:mm:ss');
	 	            requestPatient.status=true;
	 	            $scope.registerPatient(requestPatient,data);
	       
	        
	    }
	    
	    /**
	     * Registration a patient after Complete of patient registration .appointment booking will continue
	     */
	 	$scope.registerPatient = function (request,data) {
	 		bookanappointmentService.registerPatient(request).then(function (res) {
	            if (res.success && res.ViewModel!=null ) {
	            	
	            	 			var requestAppointment = {};
	            	 			var jsondata = {};
	            	 			
	            	 			jsondata.doctorName = $rootScope.BookAppointmentDetails.name;
	            	 			jsondata.doctorCode = $rootScope.BookAppointmentDetails.userCode;
	            	 		//	jsondata.specialityCode =$rootScope.specialityDetails.code;
	            	 		//	jsondata.specialityName =$rootScope.specialityDetails.name;
	            	 			jsondata.appointmentStatusCode =$rootScope.vmuisettings.appointmentStatusConfirmationCode;
	            	 			jsondata.appointmentStatusDesc =$rootScope.vmuisettings.appointmentStatusConfirmationDesc;
	            	 			
	            	 			requestAppointment.patientCode=res.ViewModel.viewModel.code;
	            	 			requestAppointment.userCode=$rootScope.BookAppointmentDetails.userCode;
	            	 			requestAppointment.patientContactNumber=data.PatientMobileNo;
	            	 			requestAppointment.appointmentStatus=$rootScope.vmuisettings.appointmentStatusConfirmationCode;
	            	 			requestAppointment.patientUhId=res.ViewModel.viewModel.uhId;
	            	 			requestAppointment.userContactNumber=$rootScope.BookAppointmentDetails.mobile;
	            	 			requestAppointment.appointmentStartDateTime = $rootScope.AppointSuccessStartTime;
	            	 			requestAppointment.appointmentEndDateTime = $rootScope.AppointSuccessEndTime;
	            	 			requestAppointment.fkLocationCode=$rootScope.BookAppointmentDetails.fkLocationCode;
	            	 			requestAppointment.fkCompanyCode=$rootScope.BookAppointmentDetails.fkCompanyCode;
	            	 			requestAppointment.status=true;
	            	 			requestAppointment.json = jsondata;
	            	 			//noty.add({ type: 'success',  body: 'Successfully registered patient.' });
	            	 			
	            	 			$scope.bookAppointmentForPatient(requestAppointment); 
			                	
	            }	            
	            if(res.success == false)
	            	{
	            	if(data.PatientUHID !=null && data.PatientUHID!="" )
	            		{
	            		var r = confirm("Either email Id or UHID is wrong.Can you proceed with new registration?");
	            	   	if (r == true) { 
	            	     	data.switchStatus=false;
	            	     	data.PatientUHID=null;
	            	   	}
	            	   	if(r == false)
	            	   		{
	            	   		
	            	   		noty.add({ type: 'danger',  body: 'You cancelled the request.' });
	            	   		}
	            		}
	            	
	            	else {
	                 noty.add({ type: 'danger',  body: res.message });
	             	
	             }
	            	}
	 
	        }, function (error) {
	        	noty.add({ type: 'danger',body: 'Patient registration failed'});
	      
	        });
	 	}
	 	
	 	/**
	 	 * 
	 	 * Create Appointment for Patient 
	 	 */
		$scope.bookAppointmentForPatient = function (request) {
			
			bookanappointmentService.bookAppointmentForPatient(request).then(function (res) {
				
	            if (res != null && res.ViewModel!=null ) {
	            	
	            	$rootScope.appointmentId=res.ViewModel.viewModel.code;
	            	$rootScope.uhId=request.patientUhId;
	            	$scope.generateReport();
	            	
	            	noty.add({ type: 'success',  body: res.message });
	            	$timeout(function(){$window.location.href = '#/appointmentconfirmation'; },300);
	            }
	            else {
	                noty.add({ type: 'danger', body: res.message });
	            }
	        }, function (error) {
	            noty.add({ type: 'danger', body: 'Booking failed,Please try again later.' });
	        });
	 	}
	 	
	 	
	 	
		/**
		 * Calculating Age based on selected DOB
		 * 
		 */
	    $scope.$on('txtDOB_User_appointmentdate_selected', function (e, i) {
	        if (i != undefined) {
	            var ageDifMs = Date.now() - new Date(i);
	            var ageDate = new Date(ageDifMs);
	            $scope.patientage = Math.abs(ageDate.getUTCFullYear() - 1970);
	            $scope.showage = true;
	        }
	    });


	
	    /***
	     * 
	     * Create a request object for generating report.
	     */
	  
		    $scope.generateReport=function()
		    {
		    	var requestReport={};
		    	requestReport.specialistName=$rootScope.AppointSuccessDocName;
		    	requestReport.appointmentDate=$rootScope.AppointSuccessAppointmentDate;
		    	requestReport.specialization=$rootScope.AppointSuccessDocSpeciality;
		    	requestReport.appointmentTime= $rootScope.AppointSuccessPreferredTime;
		    	requestReport.specialistDesignation=$rootScope.AppointSuccessDocDesignation;
		    	requestReport.patientFirstName=$rootScope.PatientFirstName;
		    	requestReport.patientMiddleName=$rootScope.PatientMiddleName;
		    	requestReport.patientLastName=$rootScope.PatientLastName;
		    	requestReport.patientEmail=$rootScope.PatientEmail; 
		    	requestReport.patientMobileNo=$rootScope.PatientMobileNo;
		    	requestReport.patientUHID=$rootScope.uhId;
		    	requestReport.countryName=$rootScope.vmuisettings.countryName;
		    	requestReport.cityName=$rootScope.vmuisettings.cityName;
		    	requestReport.appointmentCode=$rootScope.appointmentId;
		    	requestReport.hospitalName= $rootScope.vmuisettings.hospitalName;
		    	requestReport.specializationName=$rootScope.AppointSuccessDocSpeciality;
		    	$rootScope.requestReport=requestReport;
		     }
		    
/***
 * Clearing UHID on changing Toogle Switch. 
 */
		   $scope.ClearUHID=function()
		    {
		    	if($rootScope.AppointmentDetailsPatient.PatientUHID != undefined && $rootScope.AppointmentDetailsPatient.PatientUHID != null)
		    	{ 
		    		$scope.AppointmentDetailsPatient.PatientUHID="";
		    	}
		    	return true;
		    }
		    
		    
	    $scope.patientRegistered = function (data) {
	        if (data.switchStatus == true) {
	            $scope.patientregistered = false;
	        }
	        else {
	        	
	            $scope.patientregistered = true;
	        }
	    }
	    
	    var loggedonuserinfo = store.get('loggedonuser');
	    if(loggedonuserinfo !== null && loggedonuserinfo !== undefined ){
	    	if(loggedonuserinfo.role!==undefined ){
	    		{
	        		if(loggedonuserinfo.role!==null && loggedonuserinfo.role!=="" && loggedonuserinfo.role == $rootScope.vmuisettings.AdminRoleCode){
	        			$scope.isAdmin = true;
	        		}else
	        			{
	        				$scope.isAdmin = false;
	        			}
	        			
	        		}
	    	}    		
	    	else
	    		{
	    			$scope.isAdmin = true; //Just as a test to make sure it works
	    		}		   
	    };
	    
	    //On clear button click clear data
	    $scope.Clear_Data = function(){
	    	//For clearing dropdown after saving a record - Starts
			$rootScope.$broadcast('clear_dropdown','Speciality');
			$scope.doctorDetails.doctorname = "";
	    }
	    
	    $scope.savePatientDetails = function(patientData,isValid)
	    {
	    	if (isValid) {
	    		$scope.BookAppointmentPatientDetailsSave_Login(patientData);
	    	
	    		
	    	 }
	    }
	    
	    $scope.BookAnAppointment = function () {
	    	$window.location.href = '#/bookanappointment';
	    }
     
} ]);