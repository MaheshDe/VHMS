﻿/**
*  Description: The ScheduleAppointmentController deals with base operation saving the appointment details for a user.
*  
* Author:  JasmitaB
* Created On: 14/08/2015
* Modified For: 
* Modified On:
* Modified By:
* 
* */
'use strict';
reachApp.controller('scheduleappointmentcontroller', ['$scope','scheduleappointmentService','noty', '$rootScope', '$modal', '$log', '$window', '$timeout', function ($scope,scheduleappointmentService,noty, $rootScope, $modal, $log, $window, $timeout) {

    /*To view calendar*/
    $scope.calendarView = 'week';
    $scope.calendarDay = new Date();
    $scope.calendarTitle = moment($scope.calendarDay).format("MMM-YYYY");
    $scope.events = [
        {
            title: 'My event title', // The title of the event
            type: 'Info', // The type of the event (determines its color). Can be important, warning, info, inverse, success or special
            startsAt: new Date(), // A javascript date object for when the event starts
            endsAt: new Date(), // Optional - a javascript date object for when the event ends
            editable: false, // If edit-event-html is set and this field is explicitly set to false then dont make it editable. If set to false will also prevent the event from being dragged and dropped.
            deletable: false, // If delete-event-html is set and this field is explicitly set to false then dont make it deleteable
            incrementsBadgeTotal: true, //If set to false then will not count towards the badge total amount on the month and year view
            recursOn: 'year', // If set the event will recur on the given period. Valid values are year or month
            cssClass: 'a-css-class-name' //A CSS class (or more, just separate with spaces) that will be added to the event when it is displayed on each view. Useful for marking an event as selected / active etc
        },
        {
            title: 'Test Appointment 2', // The title of the event
            type: 'Info', // The type of the event (determines its color). Can be important, warning, info, inverse, success or special
            startsAt: new Date("2015-07-19T12:00:00"), // A javascript date object for when the event starts
            endsAt: new Date("2015-07-19T12:30:00"), // Optional - a javascript date object for when the event ends
            editable: false, // If edit-event-html is set and this field is explicitly set to false then dont make it editable. If set to false will also prevent the event from being dragged and dropped.
            deletable: false, // If delete-event-html is set and this field is explicitly set to false then dont make it deleteable
            incrementsBadgeTotal: true, //If set to false then will not count towards the badge total amount on the month and year view
            recursOn: 'year', // If set the event will recur on the given period. Valid values are year or month
            cssClass: 'a-css-class-name' //A CSS class (or more, just separate with spaces) that will be added to the event when it is displayed on each view. Useful for marking an event as selected / active etc
        }
    ];
   //Set the calender events
    $scope.$on('set_CalendarEvents', function (e, i) {
        $scope.$apply(function () {
            $scope.calendarView = 'week';
            $scope.calendarDay = new Date();
            $scope.calendarTitle = moment($scope.calendarDay).format("MMM-YYYY");
            //$scope.events.push(i);
            $scope.events = [{
                title: 'Testing By Chaitra', // The title of the event
                type: 'Info', // The type of the event (determines its color). Can be important, warning, info, inverse, success or special
                startsAt: new Date(), // A javascript date object for when the event starts
                endsAt: new Date(), // Optional - a javascript date object for when the event ends
                editable: false, // If edit-event-html is set and this field is explicitly set to false then dont make it editable. If set to false will also prevent the event from being dragged and dropped.
                deletable: false, // If delete-event-html is set and this field is explicitly set to false then dont make it deleteable
                incrementsBadgeTotal: true, //If set to false then will not count towards the badge total amount on the month and year view
                recursOn: 'year', // If set the event will recur on the given period. Valid values are year or month
                cssClass: 'a-css-class-name' //A CSS class (or more, just separate with spaces) that will be added to the event when it is displayed on each view. Useful for marking an event as selected / active etc
            }];
        });        
    });
    //Save the appointment details when the Appointment Date time selected for a Doctor
    $scope.SaveEvent = function (o) {
        if ($scope.AddEventForm.$valid) {
           // var events = $scope.events;
            var event = {};
            event.type = "Info";
            event.editable = true;
            event.deletable = true;
            event.title = "My Appointment";
            event.incrementsBadgeTotal = true;
            event.recursOn = 'year';
            event.cssClass = 'a-css-class-name';
            
            var startday = moment(o.AppointmentDate);
            var endday = moment(o.AppointmentDate);
            var preferredTim = o.SelectedPreferredTime.split('-');
            startday.set({ hours: preferredTim[0].split(':')[0].trim(), minutes: preferredTim[0].split(':')[1].trim() });
            endday.set({ hours: preferredTim[1].split(':')[0].trim(), minutes: preferredTim[1].split(':')[1].trim() });
            event.startsAt = new Date(startday);
            event.endsAt = new Date(endday);
            //Check appointment availability for selected data time 
            var requestAppointment  = {};
            var startTime=o.SelectedPreferredTime.split('-')[0].trim()+":00";
			var endTime=o.SelectedPreferredTime.split('-')[1].trim()+":00";
            requestAppointment.userCode=o.SelectedDoctorcode;
			requestAppointment.appointmentStatus=$rootScope.vmuisettings.appointmentStatusConfirmationCode;
			requestAppointment.appointmentStartDateTime=moment(o.AppointmentDate).format('YYYY-MM-DD') + " "+ startTime;
			requestAppointment.appointmentEndDateTime=moment(o.AppointmentDate).format('YYYY-MM-DD') + " "+ endTime;
			requestAppointment.status=true;
            //call the service to check whether the slot is booked
            scheduleappointmentService.checkAppointmentAvailablity_ScheduleAppointment(requestAppointment).then(function (res) {
	            if(res.success) {
	            	 $rootScope.$broadcast('DateTime_selected', event);
	            	 $scope.cancel();
	            	 //To continue booking if the slot is available
	            }
	            else {
	                console.log('In ScheduleAppointmentController - Book appointment fails');
	                //noty.add({ type: 'danger', body: 'Slot is already booked ,Please try again by changing preferred time' });
	                alert("Slot is already booked ,Please try again by changing preferred time");
	            }
	        }, function (error) {
	            console.log('In ScheduleAppointmentController -Book appointment Error', error);
	            noty.add({ type: 'danger', body: 'Booking failed,Please try again later.' });
	        });
           
            $timeout(function () {
            	$scope.$broadcast('set_CalendarEvents', event);
            }, 500);
           
        }
    };

    function showModal(action, event) {
        $modal.open({
            templateUrl: 'deletemodalContent.html',
            controller: 'ModalInstanceCtrl',
            resolve: {
                items: function () {
                }
            }
        });
    }
    $scope.eventClicked = function (event) {
        AddEventModal('Edit', event);
    };

    $scope.eventDropped = function (event) {
        showModal('Dropped', event);
    };
    $scope.eventEdited = function (event) {
        AddEventModal('Edited', event);
    };

    $scope.eventDeleted = function (event) {
        showModal('Deleted', event);
    };

    $scope.toggle = function ($event, field, event) {
        $event.preventDefault();
        $event.stopPropagation();
        event[field] = !event[field];
    };

    $rootScope.$$listeners.Calendar_Clicked = [];
    $rootScope.$on('Calendar_Clicked', function (i, data) {
        AddEventModal('New', data);
    });
   //Show the Add appointment popup and bind the selected speciality and doctor details to it
    function AddEventModal(action, event) {
        if (action == "New") {
            $rootScope.EventDetails = {};
            $rootScope.EventModalTitle = "Add Appointment";
            $rootScope.EventDetails.type = "";
            $rootScope.EventDetails.location = "";
            if ($rootScope.BookAppointmentDetails != undefined) {
                $rootScope.EventDetails.Speciality = $rootScope.BookAppointmentDetails.specialization.split(',')[0];
                $rootScope.EventDetails.MinDate = new Date();
                $rootScope.EventDetails.DoctorName = $rootScope.BookAppointmentDetails.name;
                $rootScope.EventDetails.SelectedDoctorcode = $rootScope.BookAppointmentDetails.userCode;
                $rootScope.EventDetails.Designation = $rootScope.BookAppointmentDetails.designation;
                $rootScope.EventDetails.SpecialityName = $rootScope.specialityDetails.name;
            }
        }
        else {
            $rootScope.EventModalTitle = "Edit Appointment";
            $rootScope.EventDetails = event;
            $rootScope.EventDetails.type = event.type;
        }
        var modalInstance = $modal.open({
            templateUrl: 'NewEventModal.html',
            controller: 'ModalInstanceCtrl',
            backdrop: 'static',
            size: 'md',
            resolve: {
                items: function () {
                    return $rootScope.EventDetails;
                }
            }
        });

     /*** Calculating the doctor timing based the Current timing.*/
        modalInstance.opened.then(function () {
            $scope.EventDetails.Timings = [];
            //subscribes on date selection
            $rootScope.$on('txtAppointmentDate_doctordate_selected', function (e, i) {
                $rootScope.showtimingmsg = "";
                // if date selected is not equal to undefined them proceed
                if (i != undefined) {
                    //$rootScope.$broadcast('clear_dropdown', 'prefferedTime');
                    $scope.EventDetails.SelectedPreferredTime = "";
                    // get doctor details
                    var doctordetails = $rootScope.BookAppointmentDetails;
                    //get start time end time and consultation interval
                    var startTime = (doctordetails.startTime).split(':');
                    var endTime = (doctordetails.endTime).split(':');
                    var consultationInterval = parseInt(doctordetails.consultationInterval);

                    var startday = moment(i);
                    var endday = moment(i);
                    //setting start date time and end date time
                    startday.set({ hours: startTime[0], minutes: startTime[1] });
                    endday.set({ hours: endTime[0], minutes: endTime[1] });
                    // getting the difference between two dates
                    var diff = moment.duration(endday.diff(startday));
                    var intervals = [];
                    //repeat number of time the interval can preceed within two dates
                    for (var j = 0; j < (diff._milliseconds / (consultationInterval * 60000)) ; j++) {
                        if (moment(i).format("DD-MMM-YYYY") == moment().format("DD-MMM-YYYY")) {
                            if (startday > moment()._d) {
                                intervals.push(moment(startday).format('HH:mm') + " - " + moment(startday).add(consultationInterval * 60, 'seconds').format('HH:mm'));
                            }
                            startday = moment(startday).add(consultationInterval * 60, 'seconds');
                        }
                        else {
                            intervals.push(moment(startday).format('HH:mm') + " - " + moment(startday).add(consultationInterval * 60, 'seconds').format('HH:mm'));
                            startday = moment(startday).add(consultationInterval * 60, 'seconds');
                        }
                    }

                    if (intervals.length <= 0 && $rootScope.firsttimeshowtimingmsg) {
                        $rootScope.showtimingmsg = "No Slots Available";

                    }

                    $rootScope.firsttimeshowtimingmsg = true;
                    $scope.EventDetails.Timings = intervals;
                }
                else {
                    $scope.EventDetails.AppointmentDate = "";
                    $scope.EventDetails.Timings = [];
                }
            });
        });
    }

} ]);

