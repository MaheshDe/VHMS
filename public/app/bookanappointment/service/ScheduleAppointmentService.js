/**
*  Description: The ScheduleAppointmentService deals with base operation like checking the availability for the requested slot And Booking appointment.
* Author:  JasmitB
* Created On: 14/08/2015
* Modified For: 
* Modified On:
* Modified By:
* 
* */


/**
Service for bookAppointment page
getting user info from the json and returns to login controller
*/

'use strict';
reachApp.factory('scheduleappointmentService', ['$http', '$q',function ($http, $q) {
    return {
    	/**
         * Checking whether appointment is already booked
         */
        checkAppointmentAvailablity_ScheduleAppointment: function (req) { 
            var deferred = $q.defer(); //promise
            //Create JSON 
            var jsonData = JSON.stringify({ 'request': req });
            $http({ method: 'POST', url: '/appointment/availablity', data: jsonData, headers: { 'contentType': 'application/json' } }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;

        }	
    }
}]);  