﻿/**
Controller Associated to a Navbar
View Changed event tracked to set Logged on user and User roles
*/
'use strict';
reachApp.controller('navBarController', ['$rootScope', '$scope', '$location', '$modal', '$log', '$window', 'store', 'navbarService', '$filter','$timeout', function ($rootScope, $scope, $location, $modal, $log, $window, store, navbarService, $filter,$timeout) {
	
    angular.element("#menu-toggle").click(function (e) {
        e.preventDefault();
        angular.element("#wrapper").toggleClass("toggled");
        $rootScope.$broadcast('menutoggled', angular.element("#wrapper").hasClass('toggled'));
    });

 
    $rootScope.redirectPath=$rootScope.redirectPath !=undefined ? $rootScope.redirectPath :'#/patient/login'; 
    
    $scope.setNavURL = function (url) {
        $scope.SetUrl(url);
    }
    
    $scope.SetUrl = function(url){ 
    	
    	if(url == "#/appointmentstatus" || url == "#/doctortype" || url == "#/documenttype" || url == "#/gender" || url == "#/maritalstatus" || 
    			url == "#/qualification" || url == "#/role" || url == "#/speciality" || url == "#/usertype" || url == "#/useridentity")
    	{
    		$scope.highlightMenu = '/branchmanagement';
    	}
    	else if(url == "#/appointment/search")
    	{
    		$scope.highlightMenu = '/manage';
    	}
    	else if(url == "#/register")
    	{
    		$scope.highlightMenu = '/patientmanagement';
    	}
    	else if(url == "#/user" || url == "#/company" || url == "#/doctortype" || url == "#/speciality" || url == "#/usertype" || url == "#/role" ||
    			url == "#/appointmentstatus" || url == "#/usersidentity")
    	{
    		$scope.highlightMenu = '/admin';
    	}
    	else
    	{
    		$scope.highlightMenu = url;
    	}
    }
    
    $scope.SetUrl($window.location.hash);
    //loading Menu's
    var LoadMenuJson = function(){ 
    	   var loggedonuserinfo = store.get('loggedonuser');
    		if (null != loggedonuserinfo) 
    			{ 
    				$scope.LoggedOnUserRoleCode = loggedonuserinfo.role;
					$scope.MenuJSON = store.get('UserMenu');
					if ($scope.MenuJSON == undefined || $scope.MenuJSON == null) {
					    	navbarService.RendermenufrmJson().then(function(res){ debugger
					    			$scope.MenuJSON = res.ViewModels;
					    		    store.set('UserMenu', $scope.MenuJSON);
					    		});
					    }
					
    			}
    		else
    			{
    				$scope.LoggedOnUserRoleCode ="";
    				$scope.MenuJSON={};
    			}
    }  
   // $scope.LoadMenuJson();
   
    
    $scope.LoggedOnUserName = "";

    $scope.noop = function () {
        console.log("noop")
        angular.noop();
    };

    $scope.isActive = function (menu) {
        return menu == $location.path();
    };
    
    $scope.logout=function()
    {
    	    	 $rootScope.logout();
    }
    
    $rootScope.logout = function () {
        //redirect to patients login if logged in as patient
    	var redirectURL= $rootScope.redirectPath; //'#/login';
        localStorage.clear();
        store.set('loggedonuser', null);
        store.set('authtoken', null);
        $window.location.href = redirectURL;
     
    };


    $rootScope.$on('view-changed', function (event, args) {
    	console.log("inside view-changed");
        $scope.active = "";
        
        var loggedonuserinfo = store.get('loggedonuser');
        if (null != loggedonuserinfo) {
            $scope.LoggedOnUserName = loggedonuserinfo.firstName;
            $scope.LoggedOnUserRole = loggedonuserinfo.role;
            $timeout(function () {  LoadMenuJson(); }, 0);
            //$scope.LoggedOnUserRole =  loggedonuserinfo.firstName;//loggedonuserinfo.Role[0].name;
        }
        else {
            loggedonuserinfo = store.get('oAuth_loggedonuser');
            if (null != loggedonuserinfo) {
                $scope.LoggedOnUserName = loggedonuserinfo.name;
            }
            else {
            	if ($location.path() != '/login' 
					&&  $location.path() !='/patient/login' 
					&& $location.path() !='/appointmentconfirmation'  //if you want to access any page without login it redirects to patient login page(#/patient/login)
					&& $location.path() !='/landingpage'
					&& $location.path() !='/register'					
					&& $location.path() !='/404'
						&& $location.path() !='/bookanappointment'
						&& $location.path() !='/appointmentscheduling')//if you click become a member in landing page it redirects to /404 page..
            	 $scope.logout();
            }
        }
       
        if(loggedonuserinfo !== null && loggedonuserinfo !== undefined ){
        	if(loggedonuserinfo.role!=undefined ){
        		{
            		if(loggedonuserinfo.role!=null && loggedonuserinfo.role!="" && loggedonuserinfo.role.indexOf('ADMIN')> -1){
            			$scope.isAdminMenu = true;
            		}else
            			{
            			$scope.isAdminMenu = false;
            			}
            			
            		}
        	}    		
        	else
        		{
        		$scope.isAdminMenu = false; //Just as a test to make sure it works
        		}		   
        }
    });
    $scope.items = ['item1', 'item2', 'item3'];

    $scope.open = function (size) {

        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            backdrop: 'static',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
    
    
 
    
} ]);
