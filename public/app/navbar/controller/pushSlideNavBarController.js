﻿/**
* Description: Controller Associated to a Sliding Navbar
* Author:   
* Created On:  
* Modified For: 
* Modified On:
* Modified By:
*/
'use strict';
reachApp.controller('pushSlideNavBarController', ['$rootScope', '$scope', '$window', 'store', '$timeout', 'navbarService', function ($rootScope, $scope, $window, store, $timeout, navbarService) {
	// To show the menus in the navbar //
    $scope.showIconMenu = true;
    $scope.$on('menutoggled', function (e, i) {
        $scope.$apply(function () {
            $scope.showIconMenu = i;
        });
    });

    $scope.LoggedOnUserName = "";
    var loggedonuserinfo = store.get('loggedonuser');
    if (null != loggedonuserinfo) {
        $scope.LoggedOnUserName = loggedonuserinfo.FirstName;
    }

    $scope.setNavURL = function (url, level, ele) {
        $scope.SetUL(url, level, ele);
    }
   // To display the URL for Parent/child level//
    $scope.SetUL = function (url, level, ele) {
        var uri = url.replace('#', '');
        if (level == 'parentlevel') {
            $scope.navbarurlfromWindow = uri;
            $scope.navbarsuburlfromWindow = "";
        }
        else if (level == 'childlevel') {
            $scope.navbarsuburlfromWindow = uri;
            var element = ele.currentTarget;
            if (element.parentElement.parentElement.hasAttribute('ng-data-url')) {
                $scope.navbarurlfromWindow = angular.element(element.parentElement.parentElement).attr('ng-data-url');
            }
        }//To set the timeout function //
        else {
            $timeout(function () {
                var navelements = angular.element('.vm-pushslidemenu');
                var href = $window.location.hash.replace('#', '');
                angular.forEach(navelements, function (ele) {
                    if (ele.className.indexOf('vm-parentnav') > -1) {
                        if (angular.element(ele).attr('ng-data-url') == href) {
                            $scope.navbarurlfromWindow = href;
                            $scope.navbarsuburlfromWindow = "";
                            return;
                        }
                    }
                    else {
                        var subnav = angular.element(ele).children('.nav-second-level').children();
                        angular.forEach(subnav, function (nav) {
                            var navurl = angular.element(nav).children('a').attr('href');
                            if (href == navurl.replace('#', '')) {
                                $scope.navbarurlfromWindow = angular.element(ele).attr('ng-data-url');
                                $scope.navbarsuburlfromWindow = href;
                            }
                        });
                    }
                });
            }, 500);
        }
    }
    //to set the anchor part of the current URL//
    $scope.SetUL($window.location.hash);

    var LoadMenuJSON = function () {
        navbarService.RendermenufrmJson().then(function (res) {
            $scope.MenuJSON = res.ViewModels;            
        });
    }

    $timeout(function () { LoadMenuJSON(); }, 0);

    $rootScope.$on('view-changed', function (event, args) {
        $scope.SetUL($window.location.hash);
    });
} ]);
