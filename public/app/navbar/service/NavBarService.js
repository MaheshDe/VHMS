﻿/**
* Description: The navbarService  deals with all the basic operation like save,update,search..etc for an object.
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:   
* Created On:  
* Modified For: 
* Modified On:
* Modified By:
* */


'use strict';
reachApp.factory('navbarService', ['$http', '$q', function ($http, $q) {
    return {
    	
    	/**
    	 * This method is used to fetch the menu data 
    	 */
    	RendermenufrmJson: function () {
            var deferred = $q.defer(); //promise
            $http({ method: 'GET', url: 'data/Menu.json' }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        }
      
        
    }
} ]);