﻿/**
Service for creating a faq, to edit and to delete it.
* Author: Sabariraja M  
* Created On: 2015-08-26
* Modified For: 
* Modified On:
* Modified By:
*/

'use strict';
reachApp.factory('FaqService',  ['$http', '$q',function ($http, $q) {
    return {
        findAll: function (pageNumber) {  
            pageNumber = 1;
            var deferred = $q.defer(); //promise
            $http({ method: 'GET', url: '/faq' }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        },

       /* SaveFaq: function (req, Id, MethodType) {
            var deferred = $q.defer(); //promise
            var URL = '/faq/' + Id;

            var jsonData = JSON.stringify({ 'request': req });
            $http({ method: MethodType, url: URL, data: jsonData, headers: { 'contentType': 'application/json' } }).
                success(function (data, status, headers) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers) {
                    deferred.reject(status);
                });
            return deferred.promise;
        }*/
        
        SaveFaq: function (req) { 
        	  var deferred = $q.defer(); //promise
              $http({ method: 'POST', url: '/faq/0', data: { request: req } }).
                  success(function (data, status, headers, config) {
                      deferred.resolve(data);
                  }).
                  error(function (data, status, headers, config) {
                      deferred.reject(status);
                  });
              return deferred.promise;
        },
        DeleteFaq: function (req,code) { 
      	  var deferred = $q.defer(); //promise
            $http({ method: 'DELETE', url: '/faq/'+code, data: { request: req } }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
      },
        
        UpdateFaq: function (req,code) { 
      	  var deferred = $q.defer(); //promise
            $http({ method: 'PUT', url: '/faq/'+code, data: { request: req } }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
      }
        
    }
}]);