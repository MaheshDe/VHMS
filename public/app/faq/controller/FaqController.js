﻿/**
 * The Faq Controller deals with basic operations like create, update, delete ,search 
 * for a Faq in VHMS.
 * 
 * 
 * @author Sabariraja M
 * @since 2015-08-26
 */

'use strict';
reachApp.controller('faqController',['$scope', 'FaqService', '$rootScope', '$modal', '$window', 'noty','store', function ($scope, FaqService, $rootScope, $modal, $window, noty,store) {
    console.log("--> faqController")
    $scope.oneAtATime = true;
    $scope.status = {
        isFirstOpen: true,
        isFirstDisabled: false
    };
    var ViewPath = ($window.location.href).split('/');
    $scope.ViewName = ViewPath[ViewPath.length - 2];

    var loggedonuserinfo = store.get('loggedonuser');
    if (null != loggedonuserinfo) {
        $scope.LoggedOnUserName = loggedonuserinfo.FirstName;
        $scope.LoggedOnUserRole = loggedonuserinfo.role;
    }
    /*if ($scope.LoggedOnUserRole == $rootScope.vmuisettings.AdminRoleCode) {
        $scope.Admin = true;
    }*/
    else {
        $scope.Admin = false;
    }

    $scope.$parent.$on('onDetailsEditClicked', function (event, args) { 
        $scope.openFaqModel(args);
    });

    $scope.$parent.$on('RebindFaq', function (event, args) {
        findAllFaqs();
    });
    // to open faq model
    $scope.openFaqModel = function (obj, args) {   
        if (obj == null) { 
            $rootScope.FaqDetails = {};
            $rootScope.FaqModelTitle = "Add Faq";
            $rootScope.Id="0";
        }
        else { ;
            //$rootScope.FaqDetails = obj;
            $rootScope.FaqModelTitle = "Edit Faq";
            obj.scopeQuestion = obj.question;
            obj.scopeAnswer = obj.answer;
            $rootScope.FaqDetails = obj;
            $rootScope.FaqDetails.switchStatus = obj.Status;
        }
        var modalInstance = $modal.open({
            templateUrl: 'myFaqModalContent.html',
            controller: 'ModalInstanceCtrl',
            resolve: {
                items: function () {
                    return $rootScope.FaqDetails;
                }
            }
        });
    };

    var findAllFaqs = function () { 
        FaqService.findAll().then(function (res) { 
            $scope.faqs = res.ViewModels;
        });
    }
    findAllFaqs();
    // to save faq
    $scope.SaveFaq = function (obj) {
        if ($scope.Faqform.$valid) { 
        	
        	 var request = {};
        var code={};
        if(obj.code!==null && obj.code!=="")
        	{
        	code=obj.code;
        	}
            var loggedonuserinfo = store.get('loggedonuser');
        	
        	if(loggedonuserinfo.role!==undefined ){
        		request.question = obj.scopeQuestion == undefined ? "" : obj.scopeQuestion;
                request.answer = obj.scopeAnswer == undefined ? "" : obj.scopeAnswer;
        	}
           // to update faq
      if(code!==null && code!==undefined){
    	   FaqService.UpdateFaq(request,code).then(function (res) { 
               if (res.success) {
                   $rootScope.$broadcast('RebindFaq', res);
                   $scope.cancel();
                   noty.add({ type: 'success', body: res.message });
               }
               else {
                   $scope.cancel();
                   noty.add({ type: 'danger', body: res.message });
               }
           });
    	  
      }
      else
    	  {
    	   FaqService.SaveFaq(request).then(function (res) { 
               if (res.success) {
                   $rootScope.$broadcast('RebindFaq', res);
                   $scope.cancel();
                   noty.add({ type: 'success', body: res.message });
               }
               else {
                   $scope.cancel();
                   noty.add({ type: 'danger', body: res.message });
               }
           });
    	  }
     
    		
        }}
    // to open delete model
    $scope.openDeleteModel = function (obj, args) {  
        
        $rootScope.DeleteFaq = obj;
        var modalInstance = $modal.open({
            templateUrl: 'DeleteFaq.html',
            controller: 'ModalInstanceCtrl',
            size:'sm',
            resolve: {
                items: function () {
                    return $rootScope.FaqDetails;
                }
            }
        });
    };
    // to delete a faq
    $scope.DeleteFaq = function (obj, args) {  
        //var ID = $rootScope.DeleteFaq.Id;
        var request = {};
         var code={};
        code = $rootScope.DeleteFaq.code;
        request.Status = "0";
        request.MethodType = "DELETE";
        FaqService.DeleteFaq(request, code).then(function (res) {
            if (res.success) {
                $rootScope.$broadcast('RebindFaq', res);
                $scope.cancel();
                noty.add({ type: 'success', body: res.message });
            }
            else {
                $scope.cancel();
                noty.add({ type: 'danger', body: res.message });
            }
        });
    }


}]);