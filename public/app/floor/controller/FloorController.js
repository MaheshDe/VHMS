/**
 * The Floor Controller deals with basic operations like create, update, delete ,search 
 * for a floor in VHMS.
 * 
 * 
 * @author MaheshDe
 * @since 2015-09-01
 */
'use strict';
reachApp.controller('floorcontroller', ['floorservice', '$scope', '$http', '$window', '$rootScope', '$modal', '$timeout', 'noty', '$filter', function ( floorservice, $scope, $http, $window, $rootScope, $modal, $timeout, noty, $filter) {
    
	
    
    $scope.$parent.$$listeners.onDetailsEditClicked = [];
    $scope.$parent.$on('onDetailsEditClicked', function (event, args) { 
        $scope.openFloorModal('sm', args, args.ActionDone);
    });


    $scope.FloorViewClicked = false;
    $scope.$parent.$on('onDetailsViewClicked', function (event, args) { 
           
    	$scope.FloorViewClicked = true;
    	$scope.name = args.name;
    	$scope.code = args.code;
    	
    	//request.code = args.buildingCode;
    	var buildingCode = args.buildingCode
    	var request = {};
    	 //calling findByCode API of Building to get building name.
        floorservice.getBuldingName(buildingCode).then(function(res){
        	if(res.success)
        	{
        		$rootScope.building = res.ViewModel.name;
        	}
        });
        
    	
    	//$scope.building = args.BuildingCode;
        if (args.status == "1") { 
        	$scope.status = "Enabled";
        }
        else {
        	$scope.status = "Disabled";
        }
        $rootScope.vmuisettings.singlemasterviewclicked = true;
    });

    $scope.openFloorModal = function (size, args, action) {
    	$scope.FloorViewClicked = false;
        $rootScope.disableparentddl = false;
        $rootScope.disabledependentddl = true;
        if (action !== undefined && action == 'add') {
            
            $rootScope.floorModelTitle={};
            $rootScope.floorModelTitle = "Add Floor";
            $rootScope.floorDetails = {};
            $rootScope.floorDetails.Id = "0";
            $rootScope.AddFloor = true;
        }
       
        else { 
        	$rootScope.floorModelTitle={};
            $rootScope.floorModelTitle = "Edit Floor";
            args.FloorName = args.name;
            args.BuildingCode = args.buildingCode;
            if (args.status == "1") {
                args.switchStatus = true;
            }
            else {
                args.switchStatus = false;
            }
            $rootScope.floorDetails = args;
            $rootScope.disableparentddl = true;
            var request = {};
            request.requestCode = args.code;
            request.requestType = 'floor';
         
        }
        var modalInstance = $modal.open({
            templateUrl: 'FloorModalContent.html',
            controller: 'ModalInstanceCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                items: function () {
                    return $rootScope.floorDetails;
                }
            }
        });
    };

    $scope.SaveFloor = function (obj) {  
        if ($scope.FloorForm.$valid) {
            var request = {};
            request.name = obj.FloorName == undefined ? "" : obj.FloorName;
            request.status = obj.switchStatus == undefined ? "" : obj.switchStatus;
            request.buildingCode = obj.BuildingCode == undefined ? "" : obj.BuildingCode;
         
            if (request.status == true) {
                request.status = true;
            }
            else {
                request.status = false;
            }

            if (obj.Id == "0") {
                var Id = "0";
                request.MethodType = "POST";
                request.Id = Id.toString();
            }
            else { 
                var Id = obj.Id;
                request.MethodType = "PUT";
                Id = obj.code;
            }
            
          
            floorservice.SaveFloor(request, Id, request.MethodType).then(function (res) { 
                if (res.success) {        ;           
                    //document upload - starts
                    var request = {};
                    request.requestCode = res.ViewModel.code;
                    obj=res.ViewModel;
                    request.requestType = "floor";
                    //$scope.RequestCode = request;
                   // $rootScope.$emit('SaveDocumentOnSuccess', request);
                    //document upload - ends
                	//close modal after it saves successfully
                    $scope.cancel();
                    
                    
                    $rootScope.$on('bindingLookupTable_Success_floor', function (event, args) {
                    	 
                        if ($rootScope.vmuisettings.singlemasterviewclicked) {
                            var JsonResult = res.ViewModel;
                            var myObject = $filter('filter')(args, { code: JsonResult.code });
                            if (myObject.length > 0) { 
                                //$rootScope.$broadcast('onDetailsViewClicked', myObject[0]);
                            
                            }
                        }
                    });
                    $rootScope.$broadcast('RebindRouteTable_floor');
                     
                    console.log("floor save success"+ res.message);
                   $timeout(function(){ noty.add({ type: 'success', body: res.message })},200);
                   /*$rootScope.$on('AcceptOneDocument', function (event, args) {
                       angular.element('#SpecialityDocument').addClass('ng-hide');
                   });*/

                }
             
                else { 
                	//if the result fails
                    $scope.cancel();
                  //Show a failure noty message
                    console.log("floor save error"+ res.message);
                    noty.add({ type: 'danger', body: res.message });
                }
            
            });
            
            
        }
    }
}]);
