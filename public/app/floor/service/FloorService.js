/**
Service for creating a Floor master and to edit it.
* Author: MaheshDe  
* Created On: 
* Modified For: 
* Modified On:
* Modified By:
*/
'use strict';
reachApp.factory('floorservice', ['$window', '$http', '$q', function ($window, $http, $q) {
    return {
        SaveFloor: function (req, Id, MethodType) { 
            var deferred = $q.defer(); //promise
            //create JSON
            var URL = '/floor/' + Id;

            var jsonData = JSON.stringify({ 'request': req });
            $http({ method: MethodType, url: URL, data: jsonData, headers: { 'contentType': 'application/json' } }).
                success(function (data, status, headers) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers) {
                    deferred.reject(status);
                });
            return deferred.promise;
        },
        
        getBuldingName : function (buildingCode) {
            var deferred = $q.defer(); //promise
        	//var jsonData = JSON.stringify({ 'request': req });
            $http({ method: 'GET', url: '/building/'+buildingCode}).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        }
        
    }
}])