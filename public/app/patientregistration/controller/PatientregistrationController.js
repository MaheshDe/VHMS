﻿/**
Controller for Patient Registration page
Resgister user before booking any appointment in the hospital

* Author:   
* Created On: 
* Modified For: 
* Modified On:
* Modified By:
*/
'use strict';
reachApp.controller('patientregistrationController', ['$scope', '$window', '$rootScope', 'patientregistrationService', 'store', '$modal','noty', function ($scope, $window, $rootScope, patientregistrationService, store, $modal,noty) {
	$scope.noty = noty;
	
	  $rootScope.maxdobDate = new Date();
	$scope.RegistrationDetails={};
    //For validating documents -- Starts
    $scope.$on('documentcontroller_executed', function (e, i) {
        $scope.FormDocumentDetails = {};
        $scope.maxdobDate = new Date();
        $scope.FormDocumentDetails.DocumentRequired = true;
        $scope.FormDocumentDetails.DocumentTypeRequired = $rootScope.vmuisettings.AdminRoleCode;
        $scope.FormDocumentDetails.RequiredDocumentName = "ID Proof";
        $rootScope.$broadcast('applyScopeForValidation', $scope.FormDocumentDetails); 
    });
    $scope.IdProofNo_View = true;
    $scope.$on('UserIdentityType_ChangeEvent', function (e, i) {
        if (null != i) {
            $scope.IdProofNo_View = false;
        }
    });
    //For validating documents -- Ends
    
    //setting logged user information for profile
    var loggedonuserinfo = store.get('loggedonuser');
    $scope.loadProfileInfo=function()
    {
    $scope.LoggedOnUserName = "";
   
    if (null != loggedonuserinfo) {
        var request={};
        var code = loggedonuserinfo.code;
        var data={};
        $scope.RegistrationDetails=loggedonuserinfo;
	    $scope.RegistrationDetails.PatientUHID=loggedonuserinfo.uhId;
	    $scope.RegistrationDetails.dob=moment(loggedonuserinfo.dob).format('DD-MMM-YYYY');
	    patientregistrationService.findByCode(request,code).then(function (res) {
	    	if(res.ViewModel!=null){
            data = res.ViewModel;
	    if($rootScope.mouseclick==undefined || $rootScope.mouseclick=='click'){
	    	 var tempAddrObject ={};
	         var perAddrObject={};
	    tempAddrObject = JSON.parse(data.temporaryAddress);
	    perAddrObject = JSON.parse(data.permenantAddress);
	    $scope.RegistrationDetails.tempAddress = tempAddrObject.StreetLine1 + ',' + tempAddrObject.StreetLine2 + ',' + tempAddrObject.City + ',' + tempAddrObject.State + ',' + tempAddrObject.PostalCode + ',' + tempAddrObject.Country;
	    $scope.RegistrationDetails.perAddress = perAddrObject.StreetLine1 + ',' + perAddrObject.StreetLine2 + ',' + perAddrObject.City + ',' + perAddrObject.State + ',' + perAddrObject.PostalCode + ',' + perAddrObject.Country;
	    if($rootScope.clickId=='editAddress'){
	    $scope.AddressLine1 = tempAddrObject.StreetLine1;
	    $scope.AddressLine2 = tempAddrObject.StreetLine2;
	    $scope.stateModel = tempAddrObject.State;
	    $scope.countryModel = tempAddrObject.Country;
	    $scope.City = tempAddrObject.City;
	    $scope.PostalCode = tempAddrObject.PostalCode;
	    }
	    if($rootScope.clickId=='editPerAddress'){
		    $scope.AddressLine1 = perAddrObject.StreetLine1;
		    $scope.AddressLine2 = perAddrObject.StreetLine2;
		    $scope.stateModel = perAddrObject.State;
		    $scope.countryModel = perAddrObject.Country;
		    $scope.City = perAddrObject.City;
		    $scope.PostalCode = perAddrObject.PostalCode;
		    }
	    }
	  	}
    });
    }
    }    
    /**
     * open pop modal for document upload.
     */
    $rootScope.$$listeners.UserDocument_Clicked = [];
    $rootScope.$on('UserDocument_Clicked', function (event, args) {
        var modalInstance = $modal.open({
            templateUrl: 'app/documentupload/view/documentUploadModal.html',
            controller: 'ModalInstanceCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                items: function () {
                    return $rootScope.Details;
                }
            }
        });
    });

    
    /**
     * This method will  create patient and redirect to the same page again by resetting  form 
     */
    $scope.submitregisterform = function (RegistrationDetails) {
        var $scope = this;
      
        if ($scope.RegisterForm.$valid) {
            var request = {};
            var perAddress = {};
            var tempAddress = {};
            request=RegistrationDetails;
            tempAddress.StreetLine1 = $scope.RegisterForm.txtAddressLine1_CommunicationAddress.$viewValue;
            tempAddress.StreetLine2 = $scope.RegisterForm.txtAddressLine2_CommunicationAddress.$viewValue;
            tempAddress.Country = $scope.RegisterForm.cboCountry_CommunicationAddress.$viewValue.code;
            tempAddress.State = $scope.RegisterForm.cboState_CommunicationAddress.$viewValue.code;
            tempAddress.City = $scope.RegisterForm.txtCity_CommunicationAddress.$viewValue;
            tempAddress.PostalCode = $scope.RegisterForm.txtZip_CommunicationAddress.$viewValue;
            perAddress.StreetLine1 = $scope.RegisterForm.txtAddressLine1_PermenantAddress.$viewValue;
            perAddress.StreetLine2 = $scope.RegisterForm.txtAddressLine2_PermenantAddress.$viewValue;
            perAddress.Country = $scope.RegisterForm.cboCountry_PermenantAddress.$viewValue.code;
            perAddress.State = $scope.RegisterForm.cboState_PermenantAddress.$viewValue.code;
            perAddress.City = $scope.RegisterForm.txtCity_PermenantAddress.$viewValue;
            perAddress.PostalCode = $scope.RegisterForm.txtZip_PermenantAddress.$viewValue;
            request.dob = moment(RegistrationDetails.dob).format('YYYY-MM-DD HH:mm:ss');
            request.permenantAddress = JSON.stringify(perAddress);
            request.temporaryAddress = JSON.stringify(tempAddress);
            //passing patient role code
            request.role = $rootScope.vmuisettings.PatientRoleCode;
            
            patientregistrationService.RegisterUser(request).then(function (res) {
                if (res != null) {
                    if (res.ViewModel != undefined) {
                    $scope.RegistrationDetails={};
                    $scope.RegisterForm.txtAddressLine1_CommunicationAddress.$viewValue = "";
                    angular.element("#txtAddressLine1_CommunicationAddress").val("");
                    angular.element("#txtAddressLine2_CommunicationAddress").val("");
                    angular.element("#txtCity_CommunicationAddress").val("");
                    angular.element("#txtZip_CommunicationAddress").val("");
                    
                    angular.element("#txtAddressLine1_PermenantAddress").val("");
                    angular.element("#txtAddressLine2_PermenantAddress").val("");
                    angular.element("#txtCity_PermenantAddress").val("");
                    angular.element("#txtZip_PermenantAddress").val("");
                    var temporaryAddress = {};
                    $rootScope.$broadcast('SaveDocumentOnSuccess',res.ViewModel.viewModel.code);
                        noty.add({ type: 'success',  body: res.message });
                    	noty.add({ type: 'success', body: 'Successfully registered the patient.' });
                    	$rootScope.$emit('ClearDocumentGrid');
                        //To clear the dropdown
                        $rootScope.$broadcast('clear_dropdown', 'MaritalStatus');
                        $rootScope.$broadcast('clear_dropdown', 'Gender');
                        $rootScope.$broadcast('clear_dropdown', 'UserIdentityType');
                        $rootScope.$broadcast('clear_dropdown', 'cboCountry_CommunicationAddress');
                        $rootScope.$broadcast('clear_dropdown', 'cboState_CommunicationAddress');
                        $rootScope.$broadcast('clear_dropdown', 'cboCountry_PermenantAddress');
                        $rootScope.$broadcast('clear_dropdown', 'cboState_PermenantAddress');
                    }
                    else if(res.success == false){
                    	$scope.RegistrationDetails={};
                    	$scope.CommunicationAddress = {};
                    	$rootScope.$emit('ClearDocumentGrid');
                    	noty.add({ type: 'danger',  body: res.message });
                        //To clear the dropdown
                    	$rootScope.$broadcast('clear_dropdown', 'MaritalStatus');
                        $rootScope.$broadcast('clear_dropdown', 'Gender');
                        $rootScope.$broadcast('clear_dropdown', 'UserIdentityType');
                        $rootScope.$broadcast('clear_dropdown', 'cboCountry_CommunicationAddress');
                        $rootScope.$broadcast('clear_dropdown', 'cboState_CommunicationAddress');
                        $rootScope.$broadcast('clear_dropdown', 'cboCountry_PermenantAddress');
                        $rootScope.$broadcast('clear_dropdown', 'cboState_PermenantAddress');
                    }
                }
                else {
            	    noty.add({ type: 'danger',  body: res.message });
                    console.log('In PatientRegistrationController - PatientRegistrationService data.success is false');
                    $rootScope.$emit('ClearDocumentGrid');
                    //To clear the dropdown
                	//$rootScope.$broadcast('reset_dropdown','MaritalStatus');
                    $rootScope.$broadcast('clear_dropdown', 'MaritalStatus');
                    $rootScope.$broadcast('clear_dropdown', 'Gender');
                    $rootScope.$broadcast('clear_dropdown', 'UserIdentityType');
                    $rootScope.$broadcast('clear_dropdown', 'cboCountry_CommunicationAddress');
                    $rootScope.$broadcast('clear_dropdown', 'cboState_CommunicationAddress');
                    $rootScope.$broadcast('clear_dropdown', 'cboCountry_PermenantAddress');
                    $rootScope.$broadcast('clear_dropdown', 'cboState_PermenantAddress');
                    $scope.RegistrationDetails={};
                    $scope.CommunicationAddress = {};
                    $window.location.href = "/";
                }
            }, function (error) {
                 noty.add({ type: 'danger',  body: res.message });
                // promise rejected, could log the error with: console.log('error', error);
                console.log('In PatientRegistrationController - PatientRegistrationService Error:', error);
                $rootScope.$emit('ClearDocumentGrid');
                //To clear the dropdown
                $rootScope.$broadcast('clear_dropdown', 'MaritalStatus');
                $rootScope.$broadcast('clear_dropdown', 'Gender');
                $rootScope.$broadcast('clear_dropdown', 'UserIdentityType');
                $rootScope.$broadcast('clear_dropdown', 'cboCountry_CommunicationAddress');
                $rootScope.$broadcast('clear_dropdown', 'cboState_CommunicationAddress');
                $rootScope.$broadcast('clear_dropdown', 'cboCountry_PermenantAddress');
                $rootScope.$broadcast('clear_dropdown', 'cboState_PermenantAddress');
                $scope.RegistrationDetails={};
                $window.location.href = "/";
            });
        
        }
    };
    
    /**
	 * Open the model For Editing User Information
	 */
	$scope.editprofile = function (size) {
    	
    var modalInstance = $modal.open({
        templateUrl: 'app/patientregistration/view/updatepatient.html',
        controller: 'ModalInstanceCtrl',
        backdrop: 'static',
        size: size,
        resolve: {
            items: function () {
                return $scope.items;
            }
        }
      });
    }
	
	/**
	 * Open the model For Editing Address Information
	 */
	$scope.EditAddressInfo = function (event,size) {
	//var loggedonuserinfo = store.get('loggedonuser');
	$rootScope.mouseclick = event.type;
	$rootScope.clickId=event.currentTarget.id; 
    var modalInstance = $modal.open({
        templateUrl: 'app/patientregistration/view/address.html',
        controller: 'ModalInstanceCtrl',
        backdrop: 'static',
        size: size,
        resolve: {
            items: function () {
                return $scope.items;
            }
        }
      });
    }
	
	/**
     * This method will  edit patient address information
     */
    $scope.submitAddressform = function (RegistrationDetails) {
        var $scope = this;
        if ($scope.AddressForm.$valid) {
            var request = {};
            var permenantAddress = {};
            var temporaryAddress={};
            var temp = {};
            var perm={};
            request=RegistrationDetails;
            var code = loggedonuserinfo.code;
            if($rootScope.clickId=='editAddress'){
            	
            	   temporaryAddress.StreetLine1 = $scope.AddressForm.txtAddressLine1_.$viewValue;
                   temporaryAddress.StreetLine2 = $scope.AddressForm.txtAddressLine2_.$viewValue;
                   temporaryAddress.Country = $scope.AddressForm.cboCountry_.$viewValue.code;
                   temporaryAddress.State = $scope.AddressForm.cboState_.$viewValue.code;
                   temporaryAddress.City = $scope.AddressForm.txtCity_.$viewValue;
                   temporaryAddress.PostalCode = $scope.AddressForm.txtZip_.$viewValue;
                   request.temporaryAddress = JSON.stringify(temporaryAddress);
            }
            if($rootScope.clickId=='editPerAddress'){
            	permenantAddress.StreetLine1 = $scope.AddressForm.txtAddressLine1_.$viewValue;
            	permenantAddress.StreetLine2 = $scope.AddressForm.txtAddressLine2_.$viewValue;
            	permenantAddress.Country = $scope.AddressForm.cboCountry_.$viewValue.code;
            	permenantAddress.State = $scope.AddressForm.cboState_.$viewValue.code;
            	permenantAddress.City = $scope.AddressForm.txtCity_.$viewValue;
            	permenantAddress.PostalCode = $scope.AddressForm.txtZip_.$viewValue;
                request.permenantAddress = JSON.stringify(permenantAddress);
         }
            request.dob = moment(RegistrationDetails.dob).format('YYYY-MM-DD,HH:mm:ss');
            patientregistrationService.EditUser(request,code).then(function (res) {
                if (res != null && res.success) {
                	$scope.cancel();
                	$rootScope.$broadcast('bindAddress_Display', res.ViewModel);
                	noty.add({ type: 'success',  body: res.message });
                }
                else {
                	$scope.cancel();
            	    noty.add({ type: 'danger',  body: res.message });
                }
            }, function (error) {
            	$scope.cancel();
                 noty.add({ type: 'danger',  body: 'User updation failed' });
               
                // $window.location.href = "#/patient/login";
            });
        }
    };
	
	
	
	   /**
     * This method will  edit patient personal information and submit
     */
    $scope.submiteditform = function (RegistrationDetails) {
        var $scope = this;
        if ($scope.EditForm.$valid) {
            var request = {};
            //var permenantAddress = {};
            var temporaryAddress = {};
            var permenantAddress={};
            request=RegistrationDetails;
            var code = loggedonuserinfo.code;
            request.dob = moment(RegistrationDetails.dob).format('YYYY-MM-DD,HH:mm:ss');
            patientregistrationService.EditUser(request,code).then(function (res) {
                if (res != null && res.success) {
                	$scope.cancel();
                	$scope.RegistrationDetails=res.ViewModel;
                	noty.add({ type: 'success',  body: res.message });
                }
                else {
                	$scope.cancel();
            	    noty.add({ type: 'danger',  body: res.message });
                }
            }, function (error) {
            	$scope.cancel();
                 noty.add({ type: 'danger',  body: 'User updation failed' });
            });
        }
    };
    

    /**
     * Bind the edited address to the profile page.
     */
    $rootScope.$on('bindAddress_Display',function(e,i){
    	 var tempAddrObject ={};
    	var perAddrObject={};
    tempAddrObject = JSON.parse(i.temporaryAddress);
    perAddrObject = JSON.parse(i.permenantAddress);
    i.tempAddress = tempAddrObject.StreetLine1 + ',' + tempAddrObject.StreetLine2 + ',' + tempAddrObject.City + ',' + tempAddrObject.State + ',' + tempAddrObject.PostalCode + ',' + tempAddrObject.Country;
    i.perAddress = perAddrObject.StreetLine1 + ',' + perAddrObject.StreetLine2 + ',' + perAddrObject.City + ',' + perAddrObject.State + ',' + perAddrObject.PostalCode + ',' + perAddrObject.Country;
    $scope.RegistrationDetails=i;
    });

} ]);