﻿/**
Service for Patient Registration page
Resgister user before booking any oppointment in the hospital

* Author:   
* Created On: 
* Modified For: 
* Modified On:
* Modified By:
*/
'use strict';
reachApp.factory('patientregistrationService', ['$http', '$q',function ($http, $q) {
    return {
    	
    	/**
    	 * This method deals with patient registration. 
    	 */
        RegisterUser: function (req) {
            var deferred = $q.defer(); //promise
            $http({ method: 'POST', url: '/patient/0', data: { request: req } }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        },
        
        findByCode: function (req,code) {
            var deferred = $q.defer(); //promise
            $http({ method: 'GET', url: '/patient/'+code}).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        },
        /**
    	 * This method deals with patient registration. 
    	 */
        EditUser: function (req,code) {
            var deferred = $q.defer(); //promise
            var jsonData =  JSON.stringify({'request':req});
            //$http({ method: 'PUT', url: '/patient/' + code , data: { request: req } }).
            $http({ method: 'PUT', url:  '/patient/'+code, data: jsonData , headers: {'contentType': 'application/json'} }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        }
    }
}]);