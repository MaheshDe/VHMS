﻿/**
Controller Associated to a Login Page
Sign In functionality with setting the data in the local storage and Redirecting to dashboard on login is done here
*/
'use strict';
reachApp.controller('LandingpageController', ['$scope', '$http', '$window', '$rootScope', 'store', 
	function ($scope, $http, $window, $rootScope, store) {
	 
	/**
	 * Clearing local storage while loading landing page
	 */
	$scope.init = function () { 
    	  store.set('loggedonuser', null);
          store.set('authtoken', null);
        localStorage.clear();
    };
}]);