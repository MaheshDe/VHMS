/**
 *  Description: The searchPatientController deals with search  patient details..etc for an object.
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:  ShunmugaRajaG
* Created On: 
* Modified For: 
* Modified On:
* Modified By:
* 
* */

'use strict';
reachApp
		.controller(
				'searchPatientController', ['appointmentService', '$scope', '$window', '$timeout','store',
				        						'$rootScope', '$modal','noty','ServerSidePaginationService' ,'underscore',
				function(appointmentService, $scope, $window, $timeout,store,
						$rootScope, $modal,noty,ServerSidePaginationService,_)
						{ 
							
					console.log($rootScope.Booking);
					
					
					 /**This function is used to construct request and call the submit the request and bind the result using serverSide Pagination Directive.
				     * 
				     * */
					$scope.onSearchPatientClicked = function(patientdetails) 
					{		
							var request={};
							//request=patientdetails==undefined ? {} : patientdetails;
							request.status=true;
							
							if(patientdetails!=undefined)
								{
									if(patientdetails.patientName!=undefined && patientdetails.patientName!=null && patientdetails.patientName.trim()!="")
										{
											request.firstName=patientdetails.patientName;
											request.middleName=patientdetails.patientName;
											request.lastName=patientdetails.patientName;
										}
									
									if(patientdetails.contactNumber!=undefined && patientdetails.contactNumber!=null && patientdetails.contactNumber.trim()!="")
									{
										request.contactNumber=patientdetails.contactNumber;
										
									}
									if(patientdetails.uhId!=undefined && patientdetails.uhId!=null && patientdetails.uhId.trim()!="")
									{
										request.uhId=patientdetails.uhId;
									}
									
									if(patientdetails.email!=undefined && patientdetails.email!=null && patientdetails.email.trim()!="")
									{
										request.email=patientdetails.email;
									}
									
								}
							
							var params = {};
							params.Url = '/patient';
							params.data=JSON.stringify({'request':request});
							$scope.tblResultList_pagesinfo={};
							params.paginationServiceParams=$scope.tblResultList_pagesinfo; 
							ServerSidePaginationService.attachServerSidePagination(params);
					}
					
					 /**
					  * This function is used to clear the all search details.
					     * 
					     * */
					 $scope.clearSearch = function(patientdetails){
						 if(patientdetails!=undefined && patientdetails!=null )
						 {
							 patientdetails.patientName="";
							 patientdetails.uhId="";
							 patientdetails.email="";
							 patientdetails.contactNumber="";
						 }
					 }
					 
					 
						}]);