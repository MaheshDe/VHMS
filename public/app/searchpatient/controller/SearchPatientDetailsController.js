/* Description: The searchPatientDetailsController deals with Configuring The datatable properties, searching patient.
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:  ShunmugaRajaG
* Created On: 
* Modified For: 
* Modified On:
* Modified By:
* 
* */

'use strict';
reachApp.controller('searchPatientDetailsController', ['$scope', '$window', '$rootScope', 'store', '$modal', 'underscore','noty', 'ServerSidePaginationService', 'appointmentService', '$filter', function ($scope, $window, $rootScope, store, $modal, _,noty, ServerSidePaginationService, appointmentService ,$filter) {
   
    
    
/**
* Subscribe the pagination result  and binding the value to datatable;   
*/ 
    
    $rootScope.$on('pagination_Result',function(event,args){
    	$scope.showSearchPatient = true; 
    	$scope.tblResultList_pagesinfo = args;        
    	$scope.tblResultList_pages = ServerSidePaginationService.paginationServiceParams().ssp_pages;
    	$scope.tblResultList_pagesinfo = ServerSidePaginationService.paginationServiceParams().paging.ssp_info;    	
    	 });
  
    console.log($rootScope.Booking);
    $scope.BookAppointmentClicked = function(patientInfo) 
	{
    	
    	if($rootScope.Booking!=undefined && $rootScope.Booking.appointmentDetails!=undefined  &&  $rootScope.Booking.selectedDoctor!=undefined)
    	{		
    		$rootScope.Booking.patientInfo=patientInfo;
    		$scope.continueBooking($rootScope.Booking.appointmentDetails,$rootScope.Booking.selectedDoctor,patientInfo);
    	}
    	
    	
    	else
    		{
    			$rootScope.Booking={};
    			$rootScope.Booking.patientInfo=patientInfo;
    			$scope.BookAnAppointment("lg");
    		}
	}
    
    
  
    /**
	 * Open the model For Adding Appointment Information
	 */
	$scope.BookAnAppointment = function (size) {
    	$rootScope.firsttimeshowtimingmsg = false;
    var modalInstance = $modal.open({
        templateUrl: 'app/bookappointment/view/bookappointment.html',
        controller: 'ModalInstanceCtrl',
        backdrop: 'static',
        size: size,
        resolve: {
            items: function () {
                return $scope.items;
            }
        }
      });
    }
	
    
    /**
     * Setting all the information in rootscope Form Object to preload the patient info When user logged in  .  
     * Also Opens the add patient details modal.
     */
  $scope.continueBooking = function (data, details,patientinfo) {
	console.log(data);
	$rootScope.Booking.patientInfo=null;
    $rootScope.doctordetails = data;
	$rootScope.maxappntdobDate = new Date();
   //  var loggedonuserinfo = store.get('loggedonuser');
     //code for book appointment after login
        if (null != patientinfo) {
        	$rootScope.AppointmentDetailsPatient={};
        	var doB=new Date(patientinfo.dob);
        	doB.setHours(doB.getHours() + 5);
        	doB.setMinutes(doB.getMinutes() + 30);
        	//setting logged on user details to rootScope object
        	$rootScope.AppointmentDetailsPatient.PatientFirstName=patientinfo.firstName;
        	$rootScope.AppointmentDetailsPatient.PatientMiddleName=patientinfo.middleName;
        	$rootScope.AppointmentDetailsPatient.PatientLastName=patientinfo.lastName;
        	$rootScope.AppointmentDetailsPatient.PatientEmail=patientinfo.email;
        	$rootScope.AppointmentDetailsPatient.PatientMobileNo=patientinfo.contactNumber;
        	$rootScope.AppointmentDetailsPatient.Gender=patientinfo.gender;
        	$rootScope.AppointmentDetailsPatient.DateofBirth=moment(doB).format('YYYY-MM-DD');
        	$rootScope.AppointmentDetailsPatient.PatientUHID=patientinfo.uhId;
        	//make text fields and drop downs readable.
        	$rootScope.disableControls = true;
        	$rootScope.disableControlsDropdown=true;
        	$rootScope.userlogin=false;
       }
        else
        	{
        	
        	$rootScope.AppointmentDetailsPatient={};
        	$rootScope.disableControls = false;
	    	$rootScope.disableControlsDropdown=false;
	    	$rootScope.userlogin=true;
        	}
        
   // if ($rootScope.openPatientInfoModal || $scope.BookAppointmentDtails_Login.$valid ) {
    	//$rootScope.openPatientInfoModal=false;
       // $scope.cancel();
        var modalInstance = $modal.open({
            templateUrl: 'BookAnAppointmentPatientDetailsModalContent.html',
            controller: 'ModalInstanceCtrl',
            backdrop: 'static',
            size: 'lg',
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });
 //   }
  }
  
  
  
/**
*  Configuring the datatable properties for server side pagination ,loads on controller initiate
*/   //pagination starts
    var vm = this;
	$scope.patientSearchURL = '/patient';
     vm.details = [];
    //initializing the table 
    $scope.tblResultList_pages = ServerSidePaginationService.paginationServiceParams().ssp_pages;
    ServerSidePaginationService.paginationServiceParams().paging.ssp_info.limit = 10;
    var tableoptions = {};
    tableoptions.scrollx = false;
    tableoptions.TableName = 'PatientTable';//expects the table id
    var sortableColumns = [];
    ServerSidePaginationService.initializeTable(tableoptions, sortableColumns);
    vm.dtOptions = ServerSidePaginationService.paginationServiceParams().tableParams.dtOptions;
    vm.dtColumnDefs = ServerSidePaginationService.paginationServiceParams().tableParams.dtColumnDefs;
    //pagination ends
    
    
 		
 
    
}]);