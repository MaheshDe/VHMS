/**
*  Description: The appointmentService deals with base operation like creating ,Updating and fetching the data.
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:  
* Created On: 
* Modified For: 
* Modified On:
* Modified By:
* 
* */
'use strict';
reachApp.factory('appointmentService', ['$http', '$q',function ($http, $q) {
    return {
    	
/**
* Fetch all The user information
*/
    	
    	findAllUserDetails: function () {
            var deferred = $q.defer(); //promise
            $http({ method: 'GET', url: 'data/Users.json' }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        },
        
        
/**
* Fetch All the doctor details
*/
        
        findAllDoctorDetails: function () {
            var deferred = $q.defer(); //promise
            $http({ method: 'GET', url: 'data/Doctors.json' }).
                success(function (data, status, headers, config){
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        },
        
        
/**
* Fetch All the doctor speciality
*/
        
        findAllSpeciality : function() {
            var deferred = $q.defer(); //promise
            $http({ method: 'GET', url: 'data/Speciality.json' }).
                success(function (data, status, headers, config){
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        },
        
        
/**
* Fetch All the doctorSchedule
*/
        
        /*findAllDoctorsSchedule : function(){
        	var deferred = $q.defer(); //promise
            $http({ method: 'GET', url: 'data/UsersSchedule.json' }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        },*/
        
        
/**
* Update the appointment information as requested
* */
        
        updateAppointment :function(req,code){
        	var deferred = $q.defer(); //promise
            $http({ method: 'PUT', url: '/appointment/'+code, data: { request: req } }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        },
     
        
/**
*Fetched the patient all information as requested
*/        
        viewPatientDetails :function(code){
        	var deferred = $q.defer(); //promise
            $http({ method: 'GET', url: '/patient/'+code }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        }
        
        
        
    }
}]);