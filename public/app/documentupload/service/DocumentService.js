﻿/**
* Description: The documentService deals with Document related functionalities -- Adding/Cancelling/Uploading is done here
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:   
* Created On: 
* Modified For: 
* Modified On:
* Modified By:
Document related functionalities -- Adding/Cancelling/Uploading is done here
*/
'use strict';
reachApp.factory('documentService', ['$http', '$q', function ($http, $q) {
    return {
    	/**This method is used to remove the doc - calls the  document delete API
    	 */    	
        DeleteDocument: function (request) {
        	
            var deferred = $q.defer(); //promise
            var URL = '/document/'+request.code+"?requestType="+request.requestType;
            
            $http({ method: 'DELETE', url: URL }).
                success(function (data, status, headers) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers) {
                    deferred.reject(status);
                });
            return deferred.promise;
        },
        
        GetDocument: function (req) {
            var deferred = $q.defer(); //promise
          //create JSON
            var URL = '/document';

            var jsonData = JSON.stringify({ 'request': req });
            $http({ method: 'POST', url: URL, data: jsonData, headers: { 'contentType': 'application/json' } }).
                success(function (data, status, headers) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers) {
                    deferred.reject(status);
                });
            return deferred.promise;
        }
        
    }
}

]);