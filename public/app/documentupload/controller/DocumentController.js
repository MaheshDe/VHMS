﻿/**
* Description: The documentcontroller deals with Document related functionalities -- Adding/Cancelling/Uploading is done here
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:   
* Created On:  
* Modified For: 
* Modified On:
* Modified By:
Document related functionalities -- Adding/Cancelling/Uploading is done here
*/
'use strict';
reachApp.controller('documentcontroller', ['$scope', '$rootScope', '$modal', 'store', '$upload', 'noty', '$timeout', 'documentService','underscore', '$location','masterDataService',
function ($scope, $rootScope, $modal, store, $upload, noty, $timeout, documentService, _,$location,masterDataService) {
    //Initializing for Validations - Starts
    $scope.$on('applyScopeForValidation',function(e,i){ 
        $scope.DocumentRequired = i.DocumentRequired;
        $scope.DocumentTypeRequired = i.DocumentTypeRequired;
        $scope.RequiredDocumentName = i.RequiredDocumentName;
    });
    $rootScope.$broadcast('documentcontroller_executed',null);
    //Initializing for Validations - Ends
    //$scope.uploader = new FileUploader();
    //how to use in html
    //<vm-title title-value="Address" style-value="info alert-info" icon="fa fa-plus" text-value="Add" control-id="UserDocumentAdd"></vm-title>   
    //<section data-ng-include=" 'app/views/directiveTemplates/documentUpload/documentUploadGrid.html' ">
    $rootScope.noty = noty;
    var loggedonuserinfo = store.get('loggedonuser');

	//View controls -- strats
    $scope.ViewDocumentActionButtons = true;

    $rootScope.$on('disbaledocumentactions', function (e, i) { 
        $scope.ViewDocumentActionButtons = i;
    });
	//View controls  - strats

  //Initiating File Accept Type - Starts
    $scope.FileAcceptType = "";
    if ($scope.$parent != null)
    {
        //If FileAcceptType is passed then assign it to Scope variable
        if ($scope.$parent.items != undefined && $scope.$parent.items.FileAcceptType != undefined)
        {
            $scope.FileAcceptType = $scope.$parent.items.FileAcceptType;
        }
    }
    //Initiating File Accept Type - Ends
    
    //Delete the grid on click of delete icon
    $scope.delete = function (obj, file) {  
        obj.target.closest('tr').remove();
        obj.target.closest('tr').setAttribute('class', 'ng-hide');
         
        var request = {};
        request.requestType = file.requestType;
        request.code = file.id;
        documentService.DeleteDocument(request).then(function (res) {
            if (res.success) {
                var filesafterdeleting = _.filter($rootScope.Allfiles, function (item) {
                    return item.id != file.id;
                });
                $rootScope.Allfiles = filesafterdeleting;
                $rootScope.$broadcast('AcceptOneDocument_OnCancel', file);
                 
                console.log("Document Delete save success"+ res.message);
                noty.add({ type: 'success', body: res.message });
                //$rootScope.$emit('BindToDocumentGrid', res.aaData.DocumentDetails);
            }
            else {
            	console.log("Document Delete failed"+ res.message);
            	 noty.add({ type: 'danger', body: res.message });	
            }
        });
    }
    $scope.download = function (obj) {
        
    }
    
    //done to prevent multiple instances of an event from getting created
    $rootScope.$$listeners.SaveDocumentOnSuccess = [];
    //Event called on Click of Save to upload document to DB with RequestCode
    $rootScope.$on('SaveDocumentOnSuccess', function (i, requestdata) { debugger
        if ($rootScope.Allfiles != undefined && $rootScope.Allfiles.length!= 0 ) {
        	 angular.forEach($rootScope.Allfiles, function (file) {
            $scope.upload(file, requestdata);
            
        	 });
        	 
        	 $rootScope.Allfiles=[];
        }
    });

    $scope.filesadded = [];

    $scope.username = "Test User";
    $scope.$watch('files', function () { debugger;
        if ($scope.files != null) { 
            if ($scope.rejFiles.length == 0) {
                if ($scope.files.length > 0) {
                    $scope.files[0].choppedfilename = chopdetails($scope.files[0].name, 0, 20);
                    $scope.files[0].userID = loggedonuserinfo!=null? loggedonuserinfo.Id : 0;
                    //before pushing the file, check whether it exists in global variable
                    var isExists = false;
                    angular.forEach($rootScope.Allfiles, function (item) {
                        if (item.name == $scope.files[0].name) {
                            isExists = true;
                        }
                    });
                    if (!isExists) {
                        //push the file to local array
                        $scope.filesadded.push($scope.files[0]);
                        $scope.currentfileAdded_name = $scope.files[0].name;
                        $scope.currentfileAdded = $scope.files[0];
                        $scope.currentfileAdded.id = 0;
                        $rootScope.$broadcast('ImageAdded',$scope.files);
                       // $scope.upload($scope.files);

                    } 
                   else {
                       alert("File already exists, please upload another file");
                   }
                }
            }
            else {
            	alert("Invalid attachment.\n You can attach jpg/gif/png file");
            
        }
    }
    });
    //Event called to clearDocument
    $rootScope.$on('ClearDocumentGrid', function () {
        $scope.clearDocument();
    });

    $scope.clearDocument = function () {
        $rootScope.file = "";
          $rootScope.Allfiles = [];
        angular.element(document.querySelector('#tbl_document')).addClass('ng-hide');
    };

    //cancel the document
    $scope.cancelDocument = function (event, fi) {  
        //if ($rootScope.file.name == fi.name) {
          //  $rootScope.file = "";
            var filesafterdeleting = _.filter($rootScope.Allfiles, function (item) {
                return (item.name != fi.name);
            });
     
            //var filesaftercancelling = _.filter($scope.files, function (item) {
            //    return (item.name != fi.name);
           // });
           // $scope.files = filesaftercancelling;
            $rootScope.Allfiles = filesafterdeleting;
            event.target.closest('tr').setAttribute('class', 'ng-hide');
            $rootScope.$broadcast('AcceptOneDocument_OnCancel', fi);
      //  }
        //this.file - refers to current row
        //$scope.filesadded.splice($scope.filesadded.indexOf(fi), 1);
    };



    //upload the document
    $scope.upload = function (file, documentcode) { debugger
    //for a new file upload , everytime check for the id 
    	 
    if(file.id == 0){
    	var request = {};
    	request.fileName = file.name;
    	request.requestType = file.RequestType;
    	request.documentTypeCode = file.documenttype;
    	request.fkCompanyCode = 'VM001';
    	request.requestCode = documentcode.requestCode;
    	//request.createdBy = store.get('loggedonuser').id;
    	var jsonData = JSON.stringify(request);
        	 
        	
            //var documentrequest = { 'request': jsonData };
            //console.log($scope.documentUploadFields);
            $upload.upload({
                url  : '/document/upload',
                file : file,
                data : jsonData,
                method  : 'POST',
                headers : { 'Content-Type': 'application/json', 'Accept': 'application/json' }
           /* }).progress(function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);*/
            }).success(function (data, status, headers, config) {

                if (data.success == false) { 
                	console.log("DocumentUpload save error"+ data.message);
                    noty.add({ type: 'danger', body: data.message });

                    $rootScope.$broadcast('DocumentUploadFailed', data);
                    $scope.filesadded = [];
                }
                else {
                    $rootScope.$broadcast('DocumentUploadedSuccessfully', data);
                    //noty.add({ type: 'success', body: data.aaData.Message });
                    $scope.filesadded = [];
                    $scope.clearDocument();
                    console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                    console.log("DocumentUpload save success"+ data.message);
                }
            }).error(function (data, status, headers, config) {
                console.log('file ' + config.file.name + 'upload failed. Response: ' + data);
                $scope.filesadded = [];
            });	
       }	


    };


    //truncates the document
    function chopdetails(str, start, end) {  
        var chopdata = str;

        if (str.length > end) {
            var chopdata = str.substring(start, end);
            chopdata = chopdata + "...";
        }
        return chopdata;
    }
    $rootScope.$on('BindToDocumentGrid', function (i, docfile) {  
        var arr = [];
        for(var i = 0; i < docfile.length; i++){
            var file = {};
            file = docfile[i];
            file.name = docfile[i].fileName;
            file.choppedfilename = docfile[i].fileName;
            file.documenttypename = $scope.getDocumentTypeName(docfile[i].documentTypeCode);
            file.id = docfile[i].code;
            file.RequestType=$location.path().split('/')[1];
            file.RequestCode=docfile[i].requestCode;
            arr.push(file);            
        }
        $rootScope.Allfiles=[];
        $rootScope.Allfiles.push(file);
        $timeout(function () {
            $scope.BinddocumentGrid(arr,docfile[0].RequestCode);
        },500);
        
        //$rootScope.file = docfile;
    });
    //adds the document to the grid
    $scope.BinddocumentGrid = function (file,requestCode) {  
        $("#tbl_document").removeClass('ng-hide');
        $("#tbl_document tbody tr").removeClass('ng-hide');
        if($rootScope.Allfiles.length!=0){
          $rootScope.Allfiles=_.filter($rootScope.Allfiles,function(doc){
          return ((doc.RequestType.toLowerCase()==$location.path().split('/')[1].toLowerCase())&& (doc.RequestCode==requestCode));
        });
         }
         
        $rootScope.Allfiles = file;
       

        //console.log($rootScope.file);
        //console.log($rootScope.file.name);
    }

    //adds the document to the grid
    $scope.addTodocumentGrid = function (file,DocumentTypeObj) { 
    	debugger;
    	if(DocumentTypeObj !== null  && file !== undefined && DocumentTypeObj !== undefined){
    		    file.documenttype = $scope.DocumentType;
    	    	file.documenttypename=$scope.getDocumentTypeName($scope.DocumentType);
    	        $rootScope.file = file;
    	        if ($rootScope.Allfiles == undefined || $rootScope.Allfiles.length == 0) {
    	            $rootScope.Allfiles = [];
    	        }
    	        else{
    	       $rootScope.Allfiles=_.filter($rootScope.Allfiles,function(doc){
    	          return (doc.RequestType.toLowerCase()==$location.path().split('/')[1].toLowerCase());
    	        });
    	        }
    	       file.RequestType=$location.path().split('/')[1];
    	        $("#tbl_document").removeClass('ng-hide');
    	        $("#tbl_document tbody tr").removeClass('ng-hide');
    	        $rootScope.Allfiles.push(file);
    	         
    	        $rootScope.$broadcast('AcceptOneDocument', file);
    	}
       
    }

    //Apply validation for document - Mandatory check -- Starts
    $scope.$on('validateDocuments', function (e, i) {  
        if($scope.DocumentRequired)
        {
            var trElements = angular.element("#tbl_document").children('tbody').children('tr');
            var documentsUploaded={};
            documentsUploaded.Documents = [];
            documentsUploaded.Message = "Atleast one document is required.";
            documentsUploaded.isValid = true;
            var validated=false;
            if(trElements.length > 0)
            {
                angular.forEach(trElements, function (ele) {
                    documentsUploaded.Documents.push(JSON.parse(angular.element(ele).attr('ng-data-value')));
                    if(!validated && $scope.DocumentTypeRequired != undefined)
                    {
                        if($scope.DocumentTypeRequired == JSON.parse(angular.element(ele).attr('ng-data-value')).documenttype)
                        {
                            documentsUploaded.isValid = true;
                            validated=true;                            
                        }
                        else
                        {
                            validated=false;
                            documentsUploaded.isValid = false;
                            documentsUploaded.Message = "Atleast one " + $scope.RequiredDocumentName + " document is required.";
                        }
                    }
                });
            }
            else{
                documentsUploaded.isValid = false;
                if($scope.DocumentTypeRequired != undefined)
                {
                    documentsUploaded.Message = "Atleast one " + $scope.RequiredDocumentName + " document is required.";
                }
            }

            if(!validated)
            {
                documentsUploaded.isValid = false;
            }
            $rootScope.$broadcast('ValidateDocuments_Mandatory',documentsUploaded);
        }
    });
    
    //Apply validation for document - Mandatory check -- Ends
    
    /**
     * Add noty msg after adding the document
     */
	$scope.AddNotyMessage = function(IsValidForm) { 
		if (IsValidForm == true || IsValidForm == undefined) { 
			noty.add({
				type : 'success',
				body : 'Document Added Successfully'
			});
		}
	}
	
	
    /***
     * get the document Type name by passing the doc type code 
     */
    
    $scope.getDocumentTypeName=function(documentTypeCode)
    { 
    		 var data1 = _.find($scope.AllDocumentType, function (a) { 
                 return a.code == documentTypeCode;
             });
    	return data1.name;
    }
    
    
	/**
	 * This function is used to get all  Document Type .
	 * */
					$scope.getAllDocumentType=function()
					{ 
						masterDataService.findAllDataMasterByParam('/documenttype').then(function (res) {
							if (res != null && res.length > 0) {
				              $scope.AllDocumentType = res;			
				            }
						});
					}
					$scope.getAllDocumentType();

} ]);