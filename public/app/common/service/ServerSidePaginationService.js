﻿'use strict';
angular.module('app')
    .factory('ServerSidePaginationService', ['$rootScope', '$http', '$q', function ($rootScope,$http, $q) {
    var paginationServiceParams = {
        ssp_pages: [],
        paging: {
            ssp_info: {
                totalItems: 0,
                totalPages: 1,
                currentPage: 0,
                page: 1,
                limit: 6,
                from: 0,
                where_condition: ""
            }
        },
        tableParams: {
            dtOptions: []
        }
    };


    return {//initializes the table for the first time
        initializeTable: function (dtOptions) {
            paginationServiceParams.tableParams.dtOptions = {
                iDisplayLength: paginationServiceParams.paging.ssp_info.limit,
                aaSorting: dtOptions.Sorting != undefined ? dtOptions.Sorting : [[1, 'asc']],
                bPaginate: false,
                dom:'',
                aoColumnDefs: dtOptions.ColumnDefs != undefined ? dtOptions.ColumnDefs : [{ 'bSortable': false, 'bSearchable': false, 'aTargets': [0] }]
            };
        },
        //attaches  Server Side Pagination to  the table for the first time
        attachServerSidePagination: function (params) {debugger
            this.clear();
            paginationServiceParams.paging.ssp_info.where_condition = params.criteria;
            this.navigate(1, params.Url);
        },
        //onclick of next/previous pages
        navigate: function (pageNumber, serversideUrl) {
            var dfd = $q.defer();
            if (pageNumber > paginationServiceParams.paging.ssp_info.totalPages) {
                return dfd.reject({ error: "page number out of range" });
            }

            if (paginationServiceParams.ssp_pages[pageNumber]) {
                paginationServiceParams.paging.ssp_info.currentPage = pageNumber;
                dfd.resolve();
            } else {
                return this.load(pageNumber, serversideUrl);
            }

            return dfd.promise;
        },
        //http call to load the data
        load: function (pageNumber, serversideUrl) {debugger
            var deferred = $q.defer(); //promise
        	paginationServiceParams.paging.ssp_info.skippedcount=(pageNumber-1)*paginationServiceParams.paging.ssp_info.limit;
        	//serversideUrl=serversideUrl.indexOf('?')>0 ? serversideUrl+'&':serversideUrl+'?';
        	var URL = serversideUrl + '?limit=' + paginationServiceParams.paging.ssp_info.limit + '&skip=' + paginationServiceParams.paging.ssp_info.skippedcount;
            var jsonData = paginationServiceParams.paging.ssp_info.where_condition;
            $http({ method: 'POST', url: URL, data: jsonData, headers: { 'contentType': 'application/json' } }).
            //$http({
            //    url:  serversideUrl + '?Limit=' + paginationServiceParams.paging.ssp_info.limit + '&Page=' + pageNumber
            //    + '&Where=' + paginationServiceParams.paging.ssp_info.where_condition
            //}).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });

            return deferred.promise.then(
                        function (result) {debugger;
                            var newPage = {
                                number: paginationServiceParams.paging.ssp_info.limit,
                                result: []
                            };
                            result.ViewModels.forEach(function (data) {
                                newPage.result.push(data);
                            });

                            paginationServiceParams.ssp_pages[pageNumber] = newPage;
                            paginationServiceParams.paging.ssp_info.currentPage = pageNumber;
                            paginationServiceParams.paging.ssp_info.totalPages = result.TotalPages;
                            paginationServiceParams.paging.ssp_info.totalItems = result.ViewModel;
                            //broadcast to indicate that data load completed
                            $rootScope.$broadcast('DataLoad_done', true);
                            return result.$promise;
                        }, function (result) {
                            return $q.reject(result);
                        });
        },
        //clears the server side pagination
        clear: function () {
            paginationServiceParams.ssp_pages.length = 0;
            paginationServiceParams.paging.ssp_info.totalItems = 0;
            paginationServiceParams.paging.ssp_info.currentPage = 0;
            paginationServiceParams.paging.ssp_info.totalPages = 1;
        },
        //loads the initial params 
        paginationServiceParams: function () { return paginationServiceParams; },
        initialize: function () {
            paginationServiceParams.paging.ssp_info.currentPage = 1;
        }
    }
}]);


