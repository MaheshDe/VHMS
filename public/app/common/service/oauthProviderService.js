﻿

/* Description: The oauthProviderService deals with external or third party authentication.
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:   
* Created On:   
* Modified For: 
* Modified On:
* Modified By:
* */


'use strict';
reachApp.factory('oauthProviderService', ['$q', '$http', 'store', function ($q, $http, store) {

    var authorizationResult = false;

    function SetLocalStorage(providerName, data) {
        //remove data from the local storage and then save data in the local storage
        var localStorageKey = "oAuthResult_" + providerName;
        store.remove(localStorageKey);
        store.set(localStorageKey, data);
    }

    return {
        //checks whether the provider is initialized or not
        isReady: function (providerName) {
            var authorizationResult = store.get("oAuthResult_" + providerName);
            return ((authorizationResult != "") ? authorizationResult : false);
        },
        //Connects to the provider and allows the user to login
        connectProvider: function (providerName, params) {
            var deferred = $q.defer();

            OAuth.popup(providerName, params, function (error, result) {
                // cache means to execute the callback if the tokens are already present
                if (!error) {
                    authorizationResult = result;
                    //set local storage data
                    SetLocalStorage(providerName, authorizationResult)
                    deferred.resolve(result);
                } else {
                    //do something if there's an error
                    alert(error);
                }
            });

            return deferred.promise;
        },
        //clear cache for the provider
        clearCache: function (providerName) {
            //clears the cache for oauth provider
            OAuth.clearCache(providerName);
            //set local storage data
            SetLocalStorage(providerName, false)
            authorizationResult = false;
        },
        //get the logged on user info for provider
        getLoggedonUserInfo: function () {
            var deferred = $q.defer();
            authorizationResult.me().done(function (data) {
                deferred.resolve(data);
            }).fail(function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        },
        //do Signout of User
        signOut: function (providerName, access_token) {
            var urlbyProvider;
            if (providerName == "facebook") {
                urlbyProvider = 'https://www.facebook.com/logout.php?access_token=' + access_token+'&next=https://oauth.io/auth';
            }
            var deferred = $q.defer(); //promise
           
            $http({
                method: 'GET', url: urlbyProvider,
                async: false,
                contentType: "application/json",
                dataType: 'jsonp'
            }).
            success(function (data, status, headers, config) {                
                deferred.resolve(data);
            }).
            error(function (data, status, headers, config) {
                deferred.reject(status);
            });
            return deferred.promise;
        }
    }
}]);