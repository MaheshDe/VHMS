﻿/* Description: The NotyServive deals with all the basic operation like push and pop the Message.
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:   
* Created On:   
* Modified For: 
* Modified On:
* Modified By:
* */

'use strict';
reachApp.factory('noty', ['$rootScope', function( $rootScope ) {
 var queue = [];

 //this method is used to add the meesage in queue.
     return {
         queue: queue,
         add: function( item ) {
             queue.push(item);
             setTimeout(function () {
                $('#notyMessage').toggleClass('in');
            }, 10);
            setTimeout(function () {
                $('#notyMessage').toggleClass('in');
                $('.alerts .alert').eq(0).remove();
                queue.shift();
            }, 3000);
             /*setTimeout(function(){
                 // remove the alert after 2000 ms
                 $('.alerts .alert').eq(0).remove();
                 queue.shift();
             },2000);*/
         },
         
         //this method is used to retrive the meesage from queue.
         pop: function(){
             return this.queue.pop();
         }
     };
     }])