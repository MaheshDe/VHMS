/* Description: The vmuisettings deals with Constants and Configurable  Parameter in client side 
* Author:   
* Created On:   
* Modified For: 
* Modified On:
* Modified By:
* */



'use strict';
reachApp.constant('vmuisettings',{
      singlemasterviewclicked:false,
      appointmentStatusConfirmationCode: 'CNFRD',
      
      appointmentStatusCancelationCode: 'CNCLD',
      appointmentStatusConfirmationDesc:'Approved',
      appointmentStatusCancelationDesc: 'Cancelled',
  	  countryName:'India',
  	  cityName:'Bangalore',
  	  hospitalName:'Apollo Hospital',
  	  fkCompanyCode:'vHMS',
  	  doctortype:'doctortype',
  	  doctortypeLabel:'Doctor Type',
  	  useridentity:'useridentity',
  	  useridentityLabel:'User Identity',
  	  usertype:'usertype',
  	  usertypeLabel:'User Type',
  	  appointmentstatus:'appointmentstatus',
  	  appointmentstatusLabel:'Appointment Status',
  	  documenttype:'documenttype',
  	  documenttypeLabel:'Document Type',
  	  maritalstatus:'maritalstatus',
  	  maritalstatusLabel:'Marital Status',
  	  
  	  AdminRoleCode: 'ADMN',
  	  PatientRoleCode: 'PTNT',
  	//BranchAdminRoleCode: 'HMS32',
  	//UserRoleCode: 'HMS33'

  	//  maritalstatusLabel:'Marital Status',
  	//  AdminRoleCode:'ADMIN'

     
});