﻿/**
Controller Associated to a Index page
View Changed event and Layout related functionalities are done here
*/
'use strict';
reachApp.controller('mainController', ['$rootScope', '$scope', '$location', 'noty','store', function ($rootScope, $scope, $location, noty,store) {
    $scope.navVisible = true;
    $scope.pushslidenavVisible = true;
    $scope.noty = noty;
    $scope.ParseJSON = function (obj) {
        var result = JSON.parse(obj);
        return result;
    }

    //http://stackoverflow.com/questions/21715256/angularjs-event-to-call-after-content-is-loaded
    $scope.$on('$viewContentLoaded', function (args, item) {
        console.log("Loaded View = [ " + $location.path() + "]");
        $rootScope.$broadcast('view-changed', { "view": $location.path() });
        angular.element("#div_maincontainer").addClass('container');
        angular.element("#app").css('padding-top', '70px');
        $scope.navVisible = true;
        $scope.loggedonuserVisible = false;
        var loggedOnUser = store.get('loggedonuser');
        if(loggedOnUser !== null && loggedOnUser !== undefined){
        	$scope.loggedonuserVisible = true;   
        }
        else
    	{
        	$scope.loggedonuserVisible = false;
    	}
        $scope.pushslidenavVisible = false;
        $(".background-image-overlay").hide();
        $scope.showFooter=false;
        if ($location.path() == '/login' || $location.path() == '/patient/login' ) {
             $scope.showFooter=false;
            $scope.navVisible = false;
            $.backstretch(["app/img/Background/All_background.png"], { fade: 1000, duration: 6000 });
        }
        
        else if ($location.path() == '/landingpage') {
        	$scope.loggedonuserVisible = false;
            angular.element("#div_maincontainer").removeClass('container');
            angular.element("#app").css('padding-top', '50px');
            $.backstretch("app/img/white.png", { fade: 1000 });
             $scope.showFooter=true;
        }
        
        else if ($location.path() == '/dashboard') {
           // $scope.loggedonuserVisible = true;
            $.backstretch("app/img/Background/Background.png", { fade: 1000 });
        }
        else if ($location.path() == '/appointmentconfirmation') {
            $("#footer").hide();
            angular.element("#div_maincontainer").removeClass('container');
            angular.element("#app").css('padding-top', '50px');
            $.backstretch("app/img/white.png", { fade: 1000 });
            $scope.showFooter=true;
        }
        else if ($location.path() == '/404' ) {  
        	 
             angular.element("#div_maincontainer").removeClass('container');
            angular.element("#app").css('padding-top', '50px');
            $.backstretch("app/img/white.png", { fade: 1000 });
        }
        else {
           // $scope.loggedonuserVisible = true;
            $.backstretch("app/img/white.png", { fade: 1000 });
        }
    });
} ])