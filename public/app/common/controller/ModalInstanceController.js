/**
Controller Associated to a modal to close the model on 'x' click
Getting all campaigns and binding in this controller
*/
reachApp.controller('ModalInstanceCtrl', ['$scope', '$modalInstance', 'items', '$rootScope',function ($scope, $modalInstance, items, $rootScope) {
    $scope.items = items;
    $scope.cancel = function (IsValidForm) {
        if (IsValidForm == true || IsValidForm == undefined) {
            $modalInstance.dismiss('cancel');
        }

    };
    $scope.$on('$routeChangeStart', function () {
        $modalInstance.close();
    });
}]);
