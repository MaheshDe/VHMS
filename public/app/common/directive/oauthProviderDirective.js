﻿//This directive initialize the Oauth Provider
'use strict';
//http://plnkr.co/edit/octwC4BCuXLmRhHgLD8T?p=preview
reachApp.directive('vmOauthProvider', ['$timeout', '$rootScope', 'store', function ($timeout, $rootScope, store) {
    return {
        restict: 'E',
        scope: {
            providerName: '@',
            appKey: '@'
        },
        controller: ['$scope', '$http', function ($scope, $http) {
            //initialize OAuth.io with public key of the application
            OAuth.initialize($scope.appKey);
            //try to create an authorization result when the page loads,
            // this means a returning user won't have to click the twitter button again
            var authorizationResult = OAuth.create($scope.providerName);
            //remove data from the local storage and then save data in the local storage
            var localStorageKey = "oAuthResult_" + $scope.providerName;
            store.remove(localStorageKey);
            store.set(localStorageKey, authorizationResult);
        }],
        link: function ($scope, element, attrs, controller) {

        }
    };

}]);