﻿/**
Directive used for Resizing any div
Just have to use 'resizable' attribute in the div which we want to resize
If we want to do some functionality on resize we have to call some function  with 'on-resize' event
*/
'use strict';
//http://plnkr.co/edit/octwC4BCuXLmRhHgLD8T?p=preview
reachApp.directive('resizable', function ()
{
    return {
        restrict: 'A',
        scope: {
            callback: '&onResize'
        },
        link: function postLink(scope, elem, attrs) {
            var resizeOpts = {
                handles: "ne, se, sw, nw", autoHide: false, containment: elem.attr('containment')
            };
            elem.resizable(resizeOpts);
            elem.on('resize', function (evt, ui) {
                scope.$apply(function () {
                    if (scope.callback) {
                        scope.callback({ $evt: evt, $ui: ui });
                    }
                })
            });
        }
    };
} );