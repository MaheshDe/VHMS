'use strict';
//https://gist.github.com/jakemmarsh/5867727
//Move the control the back (browser back), See a sample in 404 page
reachApp.directive('backButton', ['$window', function($window) {
        return {
            restrict: 'A',
            link: function (scope, elem, attrs) {
                elem.bind('click', function () {
                    $window.history.back();
                });
            }
        };
    }]);