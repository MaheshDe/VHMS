﻿'use strict';
//http://plnkr.co/edit/octwC4BCuXLmRhHgLD8T?p=preview
reachApp.directive('vmUiSelect2',[ '$timeout', '$rootScope', 'store' , 'underscore',function ($timeout, $rootScope, store,_) {
    return {
        restict: 'E',
        //replace : true, //-> this will not work in this case.
        scope: {
            ngSelectModel: '=', //isolated scope property used
            controlId: '@',
            requiredField: '@',
            placeHolder: '@',
            isMultiple: '@',
            masterName: '@',
            isDisabled: '@',
            isDependent: '@',
            dependentColumn: '@',
            dependentDropdownId: '@',
            sourceUrl: '@', // source url
            sourceObj: '@', // object from the restult set
            sourceNameCol: '@', //source name column
            sourceCodeCol: '@', //source value column
            disableChoice:'@'
        },
        controller: function ($scope, $http) {
            $scope.items = [];
            if ($scope.sourceUrl != undefined) {
                return $http({ method: 'GET', url: $scope.sourceUrl }).
                    success(function (data, status, headers, config) {debugger
                    	
                        if ($scope.isMultiple == undefined) {
                            //loop through the data object
                            angular.forEach(data[$scope.sourceObj], function (item) {
                                //check whether sourceNameCol is not undefined and property exists
                                if ($scope.sourceNameCol != undefined && item.hasOwnProperty($scope.sourceNameCol)) {
                                    item.name = item[$scope.sourceNameCol];
                                }
                                //check whether sourceCodeCol is not undefined and property exists
                                if ($scope.sourceCodeCol != undefined && item.hasOwnProperty($scope.sourceCodeCol)) {
                                    item.code = item[$scope.sourceCodeCol];
                                }
                            });
                        }
                        else {
                            var multiItems = [];
                            //loop through the data object
                            angular.forEach(data[$scope.sourceObj], function (item) {
                                var multiitem = {};
                                //check whether sourceNameCol is not undefined and property exists
                                if ($scope.sourceNameCol != undefined && item.hasOwnProperty($scope.sourceNameCol)) {
                                    multiitem.name = item[$scope.sourceNameCol];
                                }
                                //check whether sourceCodeCol is not undefined and property exists
                                if ($scope.sourceCodeCol != undefined && item.hasOwnProperty($scope.sourceCodeCol)) {
                                    multiitem.code = item[$scope.sourceCodeCol];
                                }

                                if (item.status == true) {
                                    if ($scope.sourceNameCol == undefined && $scope.sourceCodeCol == undefined) {
                                        multiitem.name = item.name;
                                        multiitem.code = item.code;
                                    }
                                    multiItems.push(multiitem);
                                }
                            });
                            data[$scope.sourceObj] = multiItems;
                        }

                        //remove data from the local storage and then save data in the local storage
                        store.remove($scope.masterName);
                        var dataValue= _.filter(data[$scope.sourceObj], function (objt){ return objt.status });
                        	
                      
                        store.set($scope.masterName, dataValue);
                         //enable/disable ddl on page load - Starts
                        if (($scope.isDisabled == undefined || $scope.isDisabled == "false")) {
                            $scope.isDisabled = false;
                        }
                        else {
                            $scope.isDisabled = true;
                        }
                        //enable/disable ddl on page load - ends
                        $scope.items = dataValue;
                        $rootScope.$broadcast($scope.masterName + '_Executed', null);
                        return true;
                    }).
                    error(function (data, status, headers, config) {
                        console.log("error retrieving " + $scope.masterName + " details");
                    });
            }
        },
        templateUrl: 'app/common/view/directiveTemplates/uiselect2/uiselect2.html',
        link: function ($scope, element, attrs, controller) {
            $scope.items = [];
            if ($scope.isDependent) {
                $scope.$on($scope.dependentDropdownId + '_ChangeEvent', function (event, args) {
                    var localstoragedata = store.get($scope.masterName);
                    if (localstoragedata != null) {
                        var data = [];
                        if (args != undefined) {
                            angular.forEach(localstoragedata, function (value, key) {
                                if (value[$scope.dependentColumn] == args.code) {
                                    this.push(value);
                                }
                            }, data);
                        }
                        else if (args == undefined && ($scope.isDisabled == undefined || $scope.isDisabled == false)) {
                            data = localstoragedata;
                        }
                    }
                    $scope.$parent.$applyAsync(function () {
                        //Swathi(5-May-15): Remove ng-hide class and show placeholder as select for default select-chosen element
                        var defaultSelected = angular.element("#" + $scope.controlId + " a span.select2-chosen.ng-binding");
                        defaultSelected.removeClass("ng-hide");
                        defaultSelected[0].innerHTML = $scope.placeHolder;
                        //For clearing dropdown  - Starts -- Chaitra
                        angular.element("#" + $scope.controlId).scope().$select.selected = undefined;
                        $timeout(function () {
                            angular.element("#" + $scope.controlId).removeClass('ng-invalid');
                        }, 100);
                        //For clearing dropdown  - Ends -- Chaitra
                        //end
                        if (($scope.isDisabled == undefined || $scope.isDisabled == false)) {
                            $scope.isDisabled = false;
                        }
                        else {
                            $scope.isDisabled = data.length == 0 ? true : false;
                        }
                        $scope.items = data;
                        //$scope.$parent[$scope.masterName] = data;
                    });
                });
            }
            $scope.onChangeEvent = function (obj) {
                if ($scope.isMultiple == undefined) {
                    //Swathi(5-May-15): when item is selected in dropdown, set the below select2-chosen element
                    var defaultSelected = angular.element("#" + $scope.controlId + " a span.select2-chosen.ng-binding");
                    //ends
                    if (obj != undefined) {
                        //resetting the ng-model value on change event
                        $scope.ngSelectModel = obj.code;
                        //Swathi(5-May-15): set the innerhtml for select2-chosen element
                        defaultSelected[0].innerHTML = obj.name;
                    } else { 
                    	$scope.ngSelectModel = "";
                        defaultSelected.removeClass("ng-hide");
                        defaultSelected[0].innerHTML = $scope.placeHolder;
                        //For clearing dropdown  - Starts
                        angular.element("#" + $scope.controlId).scope().$select.selected = undefined;
                        $timeout(function () {
                            angular.element("#" + $scope.controlId).removeClass('ng-invalid');
                        }, 100);
                        //For clearing dropdown  - Ends
                    }
                    //ends
                }
                else {
                    console.log(this.ngModel.$$rawModelValue);
                    $scope.ngSelectModel = this.ngModel.$$rawModelValue; // this.$select.selected;
                }
                $rootScope.$broadcast(attrs.controlId + '_ChangeEvent', obj);                
            }

            //To forcefully set the dropdown list with Inactive object (i.e obj with Status = 0)
            $rootScope.$on('setValue_dropdown', function (e, i) {
                if (i.ddlName == $scope.controlId)
                {
                    var itemExists = false;
                    angular.forEach($scope.items, function (data) {
                        if (data.Code == i.ddlObj.Code)
                        {
                            itemExists = true;
                            return;
                        }
                    });
                    if (!itemExists)
                    {
                        $scope.items.push(i.ddlObj);
                    }
                }                
            });

            //To reset the dropdown value to only Active obj(i.e obj with Status = 1)
            $rootScope.$on('reset_dropdown', function (e, i) {
                if (i == $scope.controlId)
                {
                    //filter JSONObj to get data with Status = 1
                    var ActiveObj = _.filter($scope.items, function (item) {
                        return item.Status == true;
                    });
                    $scope.items = ActiveObj;
                }                
            });

            //To clear the dropdown value
            $rootScope.$on('clear_dropdown', function (e, i) {
                if (i == $scope.controlId) {
                    $timeout(function () {
                        //For clearing dropdown  - Starts -- Chaitra
                        var defaultSelected = angular.element("#" + $scope.controlId + " a span.select2-chosen.ng-binding");
                        defaultSelected.removeClass("ng-hide");
                        defaultSelected[0].innerHTML = $scope.placeHolder;
                        angular.element("#" + $scope.controlId).scope().$select.selected = undefined;
                        $timeout(function() {
                            angular.element("#" + $scope.controlId).removeClass('ng-invalid');
                        }, 100);
                        //For clearing dropdown  - Ends  -- Chaitra
                    }, 0);
                }
            });
        }
    };

}]);