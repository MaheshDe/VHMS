﻿/**
Controller Associated to a Login Page
Sign In functionality with setting the data in the local storage and Redirecting to dashboard on login is done here
*/
'use strict';
reachApp.controller('loginController', ['$scope', '$window', '$rootScope', 'loginService', 'store', '$modal', 'underscore','noty', function ($scope, $window, $rootScope, loginService, store, $modal, _,noty) {
	$scope.showavailabledoctordetails = false;
    $scope.patientregistered = false;
    $scope.showage = false;
    $scope.noty = noty;
    //set to hold the logged in url - Start
    $rootScope.redirectPath = $window.location.hash;
    //set to hold the logged in url - End
    
    
    /**This method will be called by ngInit in login html
     * 
     */
    $scope.init = function () { 
    	  store.set('loggedonuser', null);
          store.set('authtoken', null);
        localStorage.clear();
    };
    
    /**
     *This method will be called submitting Username and Password by User , Authentication is done will redirect to dashboard. 
     */
    $scope.signIn = function () {
        var $scope = this;
        if ($scope.LoginForm.$valid) {
            var request = {};
            request.email = $scope.username;
            request.password = $scope.password;
            loginService.accountSignIn(request).then(function (res) {
            	
            	if(res != null && res.success)
            		{
            			var user = res.ViewModel;
            			//set localstorage of loggedOnUser
            			store.set("loggedonuser",user);
            			//set localstorage of Auth token
            			var authTokenFromLocalStorage = store.get('authtoken');
            			if(authTokenFromLocalStorage != null)
            			{
            				store.remove('authtoken');
            			}
            			if(user != undefined && user != null)
            			{
            				if(user.authToken !== undefined && user.authToken !== null)
            					{
            						store.set('authtoken',user.authToken);
            						store.set('patientLogin',false);
            						//Redirect to dashboard Page
                                    $window.location.href = '#/dashboard';
            					}
            			}
            		}
            		else
            		{ 
            			console.log('In LoginController - LoginService data.success is false ie: Not a valid credentials');
            			 noty.add({ type: 'danger', body: 'Please enter valid username and password.' });
            			 $window.href = "/";
            		}
            	}, function (error) {
            		
            		// promise rejected, could log the error with: console.log('error', error);
            		console.log('In LoginController - LoginService Error:', error);
            		 noty.add({ type: 'danger', body: 'Please enter valid username and password.' });
            		 $window.href = "/";
            	});
        }
    };

    
    /**
     *This method will be called submitting Username and Password by User 
     */ 
    $scope.patientSignIn = function () {
        var $scope = this;
        if ($scope.LoginForm.$valid) {
            var request = {};
            request.email = $scope.username;
            request.password = $scope.password;
            loginService.patientSignIn(request).then(function (res) {
            	if(res != null && res.success)
            		{
            			var user = res.ViewModel;
            			//set localstorage of loggedOnUser
            			store.set("loggedonuser",user);
            			//set localstorage of Auth token
            			var authTokenFromLocalStorage = store.get('authtoken');
            			if(authTokenFromLocalStorage != null)
            			{
            				store.remove('authtoken');
            			}
            			if(user != undefined && user != null)
            			{
            				if(user.authToken !== undefined && user.authToken !== null)
            					{
            						store.set('authtoken',user.authToken);
            						store.set('patientLogin',true);
            						 //Redirect to dashboard Page
                                    $window.location.href = '#/dashboard';
            					}
            			}
            		}
            		else
            		{ 
            			console.log('In LoginController - LoginService data.success is false ie: Not a valid credentials');
            			 noty.add({ type: 'danger', body: 'Please enter valid username and password.' });
            			 $window.location.href = "#/patient/login";
            		}
            	}, function (error) {
            		
            		// promise rejected, could log the error with: console.log('error', error);
            		console.log('In LoginController - LoginService Error:', error);
            		 noty.add({ type: 'danger', body: 'Please enter valid username and password.' });
            		 $window.location.href = "#/patient/login";
            	});
        }
    };
    
    /**
     * Opens the forgot password Modal 
     */
    $scope.open = function (size) {
    	$rootScope.showtimingmsg="";
        var modalInstance = $modal.open({
            templateUrl: 'forgotPasswrdModalContent.html',
            controller: 'ModalInstanceCtrl',
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });
    }
    
    
    /**
     * This method is called on Submitting Forgot Password page 
     * Forgot password Api will be called based on the login page URL (both user And patient).
     * 
     */
    $scope.forgotPassword = function () {
    var $scope = this;
    if ($scope.ForgotPasswordForm.$valid) {
        var request = {};
       var url = '/user/forgotPassword';
        request.email = $scope.fgEmail;
        
        if($rootScope.redirectPath == '#/patient/login')
        {
        	 url = '/patient/forgotPassword';
        }
        loginService.forgotPassword(request,url).then(function (res) {
            if (res != null && res.success) {
            	 $scope.noty.add({ type: 'success', body: 'Please check your email to proceed further.' });
            	  console.log('In LoginController - LoginService data.success is true ie: forgotPassword Success');
            }
            else {
            	 $scope.noty.add({ type: 'danger', body: 'Invalid email or email not found.' });
                console.log('In LoginController - LoginService data.success is false ie: forgotPassword Failed');
            }
        }, function (error) {
        	 $scope.noty.add({ type: 'danger', body: 'Failed,Please try again later.' });
             console.log('In LoginController - forgotPassword Failed Error:', error);
             $window.href = "/";
        });
        $scope.cancel();
    }
};
    
} ]);