﻿/**
Service for login page
getting user info from the json and returns to login controller
*/
'use strict';
reachApp.factory('loginService', ['$http', '$q',function ($http, $q) {
    return {
    	/**
    	 * Authenticate the credentials for User 
    	 */
        accountSignIn: function (req) {
            var deferred = $q.defer(); //promise
            $http({ method: 'POST', url: 'auth/login', data: { request: req } }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        },
        /**
    	 * Authenticate the credentials for patient 
    	 */
        patientSignIn: function (req) {
            var deferred = $q.defer(); //promise
            $http({ method: 'POST', url: 'patient/auth/login', data: { request: req } }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        },
        
        /**
    	 * Fetch all the USER Information
    	 */
        findAllUserDetails: function () {
            var deferred = $q.defer(); //promise
            $http({ method: 'GET', url: 'data/Users.json' }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        },
        
        /**
         * Fetch all the Doctor Details 
         */
        findAllDoctorDetails: function () {
            var deferred = $q.defer(); //promise
            $http({ method: 'GET', url: 'data/Doctors.json' }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        },
        
        /**
         * Fetch all the User Schedule Details
         */
        
        findAllUserScheduleDetails: function () {
            var deferred = $q.defer(); //promise
            $http({ method: 'GET', url: 'data/UsersSchedule.json' }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;

        },
        
        /**
         * Call the forgot password API  based URL passed for both user and patient 
         */
        forgotPassword: function (req,url) { 
            var deferred = $q.defer(); //promise
            //Create JSON 
      	   	var jsonData =  JSON.stringify({'request':req});
      	   	
            $http({ method: 'POST', url:url,data: jsonData, headers: {'contentType': 'application/json'} }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        },
      
    }
}]);