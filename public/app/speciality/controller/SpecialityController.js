/**
 * The Speciality Controller deals with basic operations like create, update, delete ,search 
 * for a speciality in VHMS.
 * 
 * 
 * @author JasmitaB
 * @since 2015-08-10
 */
'use strict';
reachApp.controller('specialitycontroller', ['specialityservice','documentService', '$scope', '$http', '$window', '$rootScope', '$modal', '$timeout', 'noty', '$filter', function (specialityservice, documentService, $scope, $http, $window, $rootScope, $modal, $timeout, noty, $filter) {
    
	//For validating documents -- Starts
   /* $scope.$on('documentcontroller_executed', function (e, i) {
        $scope.FormDocumentDetails = {};
        $scope.FormDocumentDetails.DocumentRequired = true;
        $scope.FormDocumentDetails.DocumentTypeRequired = $rootScope.vmuisettings.AdminRoleCode;
        $scope.FormDocumentDetails.RequiredDocumentName = "ID Proof";
        $rootScope.$broadcast('applyScopeForValidation', $scope.FormDocumentDetails);
    });*/
	angular.element('#SpecialityDocument').removeClass('ng-hide');
    
    $scope.$parent.$$listeners.onDetailsEditClicked = [];
    $scope.$parent.$on('onDetailsEditClicked', function (event, args) { 
        $scope.openSpecialityModal('md', args, args.ActionDone);
    });

    $scope.Details = {};
    $scope.Details.FileAcceptType = "image/*";
    
    $rootScope.$$listeners.SpecialityDocument_Clicked = [];
    $rootScope.$on('SpecialityDocument_Clicked', function (event, args) {
        var modalInstance = $modal.open({
            templateUrl: 'app/documentupload/view/documentUploadModal.html',
            controller: 'ModalInstanceCtrl',
            backdrop: 'static',
            size: 'sm',
            resolve: {
                items: function () {
                    return $rootScope.Details;
                }
            }
        });
    });

    //Added to show/hide the ADD button for documents add
    $rootScope.$on('AcceptOneDocument', function (event, args) {
        angular.element('#SpecialityDocument').addClass('ng-hide');
    });

    $rootScope.$on('AcceptOneDocument_OnCancel', function (event, args) { 
        $rootScope.file = [];
     /*   $rootScope.Allfiles = [];*/
        angular.element('#SpecialityDocument').removeClass('ng-hide');
    });

    $scope.SpecialityViewClicked = false;
    $scope.$parent.$on('onDetailsViewClicked', function (event, args) { 
        //$scope.profilepicpresent = false;
        //var request = {};
        //request.RequestCode = args.Code;
        //request.RequestType = "Speciality";
        //Specialityservice.GetDocument(request).then(function (res) {
        //    if (res.aaData.Success && res.aaData.DocumentDetails.length > 0) {
        //        $timeout(function () {
        //            $scope.profilepicpresent = true;
        //            $scope.userProfileImage = 'data:' + res.aaData.DocumentDetails[0].ContentType + ';base64,' + res.aaData.DocumentDetails[0].File;
        //        }, 10);                
        //    }
        //});      
    	$scope.SpecialityViewClicked = true;
    	$scope.name = args.name;
    	$scope.code = args.code;
        if (args.status == "1") { 
        	$scope.status = "Enabled";
        }
        else {
        	$scope.status = "Disabled";
        }
        $rootScope.vmuisettings.singlemasterviewclicked = true;
    });

    //$scope.$on('ImageAdded', function (e, i) {
    //    var oFReader = new FileReader();
    //    oFReader.readAsDataURL(i[0]);

    //    oFReader.onload = function (oFREvent) {
    //        $scope.$apply(function () {
    //            $scope.userImage = oFREvent.target.result;
    //        });            
    //    };
    //});

    $scope.openSpecialityModal = function (size, args, action) {
    	$scope.SpecialityViewClicked = false;
        $rootScope.disableparentddl = false;
        $rootScope.disabledependentddl = true;
        if (action !== undefined && action == 'add') {
            
            $rootScope.Allfiles = [];
            $rootScope.specialityModelTitle={};
            $rootScope.specialityModelTitle = "Add Speciality";
            $rootScope.SpecialityDetails = {};
            $rootScope.SpecialityDetails.Id = "0";
            $rootScope.AddSpeciality = true;
        }
       
        else { 
        	$rootScope.specialityModelTitle={};
            $rootScope.specialityModelTitle = "Edit Speciality";
            args.SpecialityName = args.name;
            if (args.status == "1") {
                args.switchStatus = true;
            }
            else {
                args.switchStatus = false;
            }
            $rootScope.SpecialityDetails = args;
            $rootScope.disableparentddl = true;
            var request = {};
            request.requestCode = args.code;
            request.requestType = 'speciality';
            documentService.GetDocument(request).then(function (res) { ;
                if (res.success) { 
                	
                    $rootScope.$emit('BindToDocumentGrid', res.ViewModels);
                	$rootScope.$broadcast('AcceptOneDocument', res.ViewModels);
                    /*angular.element('#SpecialityDocument').removeClass('ng-hide');*/
                   
                }
                
                else {
                    $rootScope.$emit('ClearDocumentGrid');
                }
            });
        }
        var modalInstance = $modal.open({
            templateUrl: 'SpecialityModalContent.html',
            controller: 'ModalInstanceCtrl',
            backdrop: 'static',
            size: 'md',
            resolve: {
                items: function () {
                    return $rootScope.SpecialityDetails;
                }
            }
        });
    };

    $scope.SaveSpeciality = function (obj) {  
        if ($scope.SpecialityForm.$valid) {
            var request = {};
            request.name = obj.SpecialityName == undefined ? "" : obj.SpecialityName;
            request.status = obj.switchStatus == undefined ? "" : obj.switchStatus;
         
            if (request.status == true) {
                request.status = true;
            }
            else {
                request.status = false;
            }

            if (obj.Id == "0") {
                var Id = "0";
                request.MethodType = "POST";
                request.Id = Id.toString();
            }
            else { 
                var Id = obj.Id;
                request.MethodType = "PUT";
                Id = obj.code;
            }
            specialityservice.SaveSpeciality(request, Id, request.MethodType).then(function (res) { 
                if (res.success) {        ;           
                    //document upload - starts
                    var request = {};
                    request.requestCode = res.ViewModel.code;
                    obj=res.ViewModel;
                    request.requestType = "speciality";
                    //$scope.RequestCode = request;
                    $rootScope.$emit('SaveDocumentOnSuccess', request);
                    //document upload - ends
                	//close modal after it saves successfully
                    $scope.cancel();
                    
                    
                    $rootScope.$$listeners.DocumentUploadedSuccessfully = [];
                    $rootScope.$on('DocumentUploadedSuccessfully', function () { 
                        $rootScope.$on('bindingLookupTable_Success_Speciality', function (event, args) {
                            if ($rootScope.vmuisettings.singlemasterviewclicked) { ;
                                var JsonResult = res.ViewModel;
                                var myObject = $filter('filter')(args, { Code: JsonResult.Code });
                                if (myObject.length > 0) {  
                                        $rootScope.$broadcast('onDetailsViewClicked', myObject[0]);
                                        
                                }
                                
                            }
                        });
                        $rootScope.$broadcast('RebindRouteTable_Speciality');
                        
                    });
                    	
                    
                    $rootScope.$on('bindingLookupTable_Success_speciality', function (event, args) {
                    	 
                        if ($rootScope.vmuisettings.singlemasterviewclicked) {
                            var JsonResult = res.ViewModel;
                            var myObject = $filter('filter')(args, { code: JsonResult.code });
                            if (myObject.length > 0) { 
                              //  $rootScope.$broadcast('onDetailsViewClicked', myObject[0]);
                            
                            }
                        }
                    });
                    $rootScope.$broadcast('RebindRouteTable_speciality');
                     
                    console.log("speciality save success"+ res.message);
                   $timeout(function(){ noty.add({ type: 'success', body: res.message })},200);
                   $rootScope.$on('AcceptOneDocument', function (event, args) {
                       angular.element('#SpecialityDocument').addClass('ng-hide');
                   });

                }
             
                else { 
                	//if the result fails
                    $scope.cancel();
                  //Show a failure noty message
                    console.log("speciality save error"+ res.message);
                    noty.add({ type: 'danger', body: res.message });
                }
            
            });
        }
    }
}]);
