/**
Service for creating a Speciality master and to edit it.
* Author: Sabariraja M  
* Created On: 
* Modified For: 
* Modified On:
* Modified By:
*/
'use strict';
reachApp.factory('specialityservice', ['$window', '$http', '$q', function ($window, $http, $q) {
    return {
        SaveSpeciality: function (req, Id, MethodType) { 
            var deferred = $q.defer(); //promise
            //create JSON
            var URL = '/speciality/' + Id;

            var jsonData = JSON.stringify({ 'request': req });
            $http({ method: MethodType, url: URL, data: jsonData, headers: { 'contentType': 'application/json' } }).
                success(function (data, status, headers) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers) {
                    deferred.reject(status);
                });
            return deferred.promise;
        }/*,
        GetDocument: function (req) {
            var deferred = $q.defer(); //promise
          //create JSON
            var URL = '/document';

            var jsonData = JSON.stringify({ 'request': req });
            $http({ method: 'POST', url: URL, data: jsonData, headers: { 'contentType': 'application/json' } }).
                success(function (data, status, headers) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers) {
                    deferred.reject(status);
                });
            return deferred.promise;
        }*/
    }
}])