﻿/**
Controller Associated to a creator page
Getting all campaigns and binding in this controller
*/
'use strict';
reachApp.controller('admincontroller', ['$scope', '$rootScope', '$modal', '$log', '$window', function ($scope, $rootScope, $modal, $log, $window) {
    $scope.UploadFiles = function () {
        $rootScope.$emit('UploadFiles');
    }
    $rootScope.$on('AdminDocument_Clicked', function (event, args) {
        var modalInstance = $modal.open({
            templateUrl: 'app/documentupload/view/documentUploadModal.html',
            controller: 'ModalInstanceCtrl',
            size: 'sm',
            resolve: {
                items: function () {
                    return ""
                }
            }
        });
    });

} ]);

