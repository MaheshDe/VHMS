/**
 *  Description: The changePasswordService deals with Change password for both  User And Patient..etc for an object.
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:  
* Created On: 
* Modified For: 
* Modified On:
* Modified By:
* 
* */

'use strict';
reachApp.factory('changePasswordService', function ($http, $q) {
    return {
    	
    	/**
    	 * ChangePassword  method will be called based on the URL (for both patient and user ) 
    	 */
        ChangePassword: function (req,url) {
            var deferred = $q.defer(); //promise
            //Create JSON 
      	   	var jsonData =  JSON.stringify({'request':req});
      	   	
            $http({ method: 'POST', url: url,data: jsonData, headers: {'contentType': 'application/json'} }).
                success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(status);
                });
            return deferred.promise;
        }
    }
});

