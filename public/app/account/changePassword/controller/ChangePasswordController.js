/**
 *  Description: The changePasswordController deals with Change password for both  User And Patient..etc for an object.
* Note: All the method or object Declaration should be followed by the module name. e.g - {actionName}_Object.
* Author:  
* Created On: 
* Modified For: 
* Modified On:
* Modified By:
* 
* */


'use strict';
reachApp.controller('changePasswordController', function ($rootScope, $scope, $location, $modal, $log, $window, store, changePasswordService, noty) {

	/**
	 * On Submitting ChangePassword Model this method will be called.
	 * Change password will happen based on the "patientLogin" flag from local storage.
	 */
	$scope.ChangePassword = function (User) { 
        if ($scope.changePasswordForm.$valid) {
            var request = User;
            var  url='/user/changePassword';
            var patientLogin = store.get('patientLogin');
        	if(patientLogin){
	        	request={};
	            request.password=User.currentPassword;
	            request.newPassword=User.newPassword;
	            //request.confirmPassword=User.confirmPassword;
	            url='/patient/changePassword';
        	}
            request.email = store.get("loggedonuser").email;
            $scope.noty = noty;
            changePasswordService.ChangePassword(request,url).then(function (res) {
                if (res != null && res.success) {

                    // $window.location.href = '/';
                	$rootScope.logout();
                    noty.add({ type: 'success', body: 'Successfully changed the password.Please login with new password.' });
                    console.log('In ChangePasswordController - ChangePasswordService data.success  Password Changed sucessfully.');
                }
                else {
                    //  $window.location.href = '#/dashboard';
                    console.log('In ChangePasswordController - ChangePasswordService data.success is false ie: Change  Password false');
                    noty.add({ type: 'danger', body: 'Old password is incorrect,Please try again.' });
                }
            }, function (error) {
                console.log('In ChangePasswordController - ChangePasswordService Error:', error);
                noty.add({ type: 'danger', body: 'Could not change the password,Try again later.' });
            });
            $scope.cancel();
        }
    }
});
