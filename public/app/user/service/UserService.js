﻿/**
*  Description: The UserService  is the service for userController.
* Author:  
* Created On: 
* Modified For: 
* Modified On:
* Modified By:
* 
* */'use strict';
reachApp.factory('userservice', ['$window', '$http', '$q', function ($window, $http, $q) {
    return {
        SaveUser: function (req, Id, MethodType) {
            var deferred = $q.defer(); //promise
            // create JSON
            var URL = '/User/' + Id;

            var jsonData = JSON.stringify({ 'request': req });
            $http({ method: MethodType, url: URL, data: jsonData, headers: { 'contentType': 'application/json'} }).
                success(function (data, status, headers) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers) {
                    deferred.reject(status);
                });
            return deferred.promise;
        },
        Roles: function () {
            var deferred = $q.defer(); //promise
            // create JSON
            var URL = '/data/Role.json';
            $http({ method: 'GET', url: URL}).
                success(function (data, status, headers) {
                    deferred.resolve(data);
                }).
                error(function (data, status, headers) {
                    deferred.reject(status);
                });
            return deferred.promise;
        }
    }
} ])