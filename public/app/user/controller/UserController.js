﻿/**
* Description: The UserController deals with view,edit the users.
* Created On:  
* Modified For: 
* Modified On:
* Modified By:
*/'use strict';
reachApp.controller('usercontroller', ['userservice', '$scope', '$rootScope', '$modal',  function (userservice, $scope,  $rootScope, $modal) {
    $scope.Roles = [];
    userservice.Roles().then(function (res) {
        if (res.Success) {
            $scope.Roles = res.ViewModels;
        }
    });
    // to view the details onclick of view icon //
    $scope.UserViewClicked = false;
    $scope.$parent.$on('onDetailsViewClicked', function (event, args) {
        $scope.UserViewClicked = true;
        $scope.details = args;
        if (args.Status == "1") {
            $scope.Status = "Enabled";
        }
        else {
            $scope.Status = "Disabled";
        }
        $rootScope.vmuisettings.singlemasterviewclicked = true;
        $scope.$on('webcameracontroller_loadsuccess', function (e, i) {
            //$rootScope.$broadcast('Bind_ProfilePicture', null);
        });
    });
    // to view the details onclick of view icon //
    $scope.$parent.$$listeners.onDetailsEditClicked = [];

    $scope.$parent.$on('onDetailsEditClicked', function (event, args) {
        $scope.openUserModal('sm', args, args.ActionDone);
    });
    // to show the title of the modal as Add User//
    $scope.openUserModal = function (obj, args) {
        if (obj == null) {
            $rootScope.userModelTitle = {};
            $rootScope.userModelTitle = "Add User";
            $rootScope.UserDetails = {};
            $rootScope.UserDetails.switchStatus = true;
            $rootScope.UserDetails.Id = "0";
            $rootScope.AddUser = true;
        }// to show the title of the modal as Add User//
        else {
            $rootScope.userModelTitle = obj;
            $rootScope.userModelTitle = "Edit User";
            args.UserFirstName = args.FirstName;
            args.UserMiddleName = args.MiddleName;
            args.UserLastName = args.LastName;
            args.UserEmail = args.Email;
            if (args.Status == "1") {
                args.switchStatus = true;
            }
            else {
                args.switchStatus = false;
            }
            $rootScope.UserDetails = args;
            $rootScope.AddUser = false;
        }// to open the modal //
        var modalInstance = $modal.open({
            templateUrl: 'userModalContent.html',
            controller: 'ModalInstanceCtrl',
            backdrop: 'static',
            size: 'lg',
            resolve: {
                items: function () {
                    return $rootScope.UserDetails;
                }
            }
        });
    };

    $scope.SaveUser = function (obj) {
        if ($scope.UserForm.$valid) {
            var request = {};
            request.FirstName = obj.UserFirstName == undefined ? "" : obj.UserFirstName;
            request.MiddleName = obj.UserMiddleName == undefined ? "" : obj.UserMiddleName;
            request.LastName = obj.UserLastName == undefined ? "" : obj.UserLastName;
            request.Email = obj.UserEmail == undefined ? "" : obj.UserEmail;
            request.Password = obj.Password == undefined ? "" : obj.Password;
            request.OldPassword = obj.Password == undefined ? "" : obj.Password;
            request.Status = obj.switchStatus == undefined ? "" : obj.switchStatus;
            if (request.Status == true) {
                request.Status = "1";
            }
            else {
                request.Status = "0";
            }

            if (obj.Id == "0") {
                var Id = 0;
                var MethodType = "POST";
            }
            else {
                var Id = obj.Id;
                var MethodType = "PUT";
                request.Id = obj.Id;
            }
            // to rebind the table when the edit/create was done. //
            userservice.SaveUser(request, Id, MethodType).then(function (res) {
                if (res.aaData.Success) {
                    $rootScope.$broadcast('RebindRouteTable_user');
                    $scope.cancel();
                    noty.add({ type: 'success', body: res.aaData.Message });
                }
                // To show an error noty,if there is any error. //
                else {
                    $scope.cancel();
                    noty.add({ type: 'danger', body: res.aaData.Message });
                }
            });
        }
    }
} ]);
