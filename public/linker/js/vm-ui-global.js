﻿var vm_ui_globalObj = {
    //Global variable for Order
    orderData: {},
    g_MasterSeedInfo: "",
    ProjectData: {},
    CommentsPVActionSettions: {},
    oMonthlydatatable: [],
    oQuarterlydatatable: [],
    oAnnuallydatatable: [],
    oUserdatatable: [],
    Arr_NewAddedDocs: [],
    Arr_NewlyUploadeddocs: [],
    No_NewlyUploadedDocs: 0,
    bindeddocument: false,
    IsNewRequest: true,
    validatorObj: {},
    Suppliers: "",
    //Swathi(8-july-14):Redmine #10692 - Added below variable to avoid two progress bar issue in draft mode save of request
    IsRequestFromDraftNSubmitClick : false
}